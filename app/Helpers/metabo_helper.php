<?php

//namespace App\Libraries;

use App\Models\FileClass;
use App\Models\Filetype;
use App\Models\Mimetype;
use Ppci\Libraries\PpciException;

function projectVerify(int $project_id = 0): bool
{
    $ok = false;
    if ($project_id == 0 && !empty($_SESSION["current_project"])) {
        $project_id = $_SESSION["current_project"];
    }
    if ($_SESSION["droits"]["project"] == 1) {
        $ok = true;
    } else {
        if (!empty($_SESSION["projects"][$project_id])) {
            $ok = true;
        }
    }
    if (!$ok) {
        $message = service("MessagePpci");
        $message->set(_("Vous ne disposez pas des droits suffisants pour le projet considéré"), true);
    }
    return $ok;
}
function fileInitUpload(string $modulename, int $parent_id, $numOccur = 0)
{
    if ($numOccur > 0) {
        $ext = $numOccur;
    } else {
        $ext = "";
    }
    $vue = service("Smarty");
    /**
     * Set the parameters to upload files
     */
    $mimetype = new Mimetype();
    $vue->set($mimetype->getListExtensions(false), "extensions");
    $vue->set($modulename, "modulename" . $ext);
    $vue->set($parent_id, "parent_id" . $ext);
    $vue->set(date($_SESSION["MASKDATELONG"]), "defaultDateTime");
    $filetype = new Filetype();
    $vue->set($filetype->getListFromType($modulename), "filetypes" . $ext);
    /**
     * Get the list of files attached to the object
     */
    $file = new FileClass();
    $vue->set($file->getListFromObject($modulename, $parent_id), "files" . $ext);
    $vue->set($_SESSION["dbparams"]["external_path_default"], "external_path_default");
}
/**
 * Return to the default page if the asked project is not authorized
 *
 * @return void
 */
function projectRefused()
{
    $message = service("MessagePpci");
    $message->set(_("Vous ne disposez pas des droits suffisants pour le projet considéré"), true);
    return defaultPage();
}
/**
 * Set defaults parameters for maps to vue
 *
 * @param Vue $vue
 * @param boolean $isChange
 * @return void
 */
function setParamMap($vue, $isChange = false)
{
    if (isset($vue)) {
        foreach (
            array(
                "mapDefaultZoom",
                "mapDefaultLong",
                "mapDefaultLat",
                "mapCacheMaxAge",
                /*"mapSeedMinZoom",
      "mapSeedMaxZoom",
      "mapSeedMaxAge",*/
                "mapMinZoom",
                "mapMaxZoom"
            ) as $mapParam
        ) {
            if (isset($_SESSION["dbparams"][$mapParam])) {
                $vue->set($_SESSION["dbparams"][$mapParam], $mapParam);
            }
        }
        if ($isChange) {
            $vue->set("edit", "mapMode");
        }
    }
}
/**
 * call a api with curl
 * code from
 * @param string $method
 * @param string $url
 * @param array $data
 * @return string
 */
function apiCall($method, $url, $certificate_path = "", $data = array(), $modeDebug = false)
{
    $curl = curl_init();
    if (!$curl) {
        throw new PpciException(_("Impossible d'initialiser le composant curl"));
    }
    switch ($method) {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, true);
            if (!empty($data)) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            if (!empty($data)) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
            break;
        default:
            if (!empty($data)) {
                $url = sprintf("%s?%s", $url, http_build_query($data));
            }
    }
    /**
     * Set options
     */
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    if (!empty($certificate_path)) {
        curl_setopt($curl, CURLOPT_SSLCERT, $certificate_path);
    }
    if ($modeDebug) {
        curl_setopt($curl, CURLOPT_SSL_VERIFYSTATUS, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_VERBOSE, true);
    }
    /**
     * Execute request
     */
    $res = curl_exec($curl);
    if (!$res) {
        throw new PpciException(
            sprintf(
                _("Une erreur est survenue lors de l'exécution de la requête vers le serveur distant. Code d'erreur CURL : %s"),
                curl_error($curl)
            )
        );
    }
    curl_close($curl);
    return $res;
}
