<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

class AcquisitionSample extends PpciModel
{
    private $sql = "select acquisition_sample_id, acquisition_id, sample_id, is_calibration
                  , sample_name, project_id
                  from acquisition_sample
                  join sample using (sample_id)";

    public function __construct()
    {
        $this->table = "acquisition_sample";
        $this->fields = array(
            "acquisition_sample_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "acquisition_id" => array("type" => 1, "requis" => 1),
            "sample_id" => array("type" => 1, "requis" => 1),
            "is_calibration" => array("type" => 1)
        );
        parent::__construct();
    }

    function write($data): int
    {
        if ($data["acquisition_sample_id"] == 0) {
            /**
             * Verify if the couple sample/acquisition not exists
             */
            $sql = "select acquisition_sample_id from acquisition_sample where sample_id = :sample_id: and acquisition_id = :acquisition_id:";
            $param = array("sample_id" => $data["sample_id"], "acquisition_id" => $data["acquisition_id"]);
            $dbefore = $this->lireParamAsPrepared($sql, $param);
            if ($dbefore["acquisition_sample_id"] > 0) {
                $data["acquisition_sample_id"] = $dbefore["acquisition_sample_id"];
            }
            return parent::write($data);
        }
    }
    /**
     * Get the detail of a record
     *
     * @param integer $id
     * @return array|null
     */
    function getDetail(int $id): ?array
    {
        $where = " where acquisition_sample_id = :id:";
        return $this->lireParamAsPrepared($this->sql . $where, array("id" => $id));
    }
    /**
     * Get acquisition_sample_id from a sample and an acquisition
     *
     * @param integer $sample_id
     * @param integer $acquisition_id
     * @return integer|null
     */
    function getIdFromSampleAcquisition(int $sample_id, int $acquisition_id): ?int
    {
        $sql = "select acquisition_sample_id from acquisition_sample where sample_id = :sample_id: and acquisition_id = :acquisition_id:";
        $res = $this->lireParamAsPrepared($sql, array("sample_id" => $sample_id, "acquisition_id" => $acquisition_id));
        return $res["acquisition_sample_id"];
    }
    /**
     * Get the number of files attached to an item
     *
     * @param integer $id
     * @return integer
     */
    function getNbFiles(int $sample_id, int $acquisition_id): int
    {
        $sql = "select count(*) as nb from acquisition_sample_file
    join acquisition_sample using (acquisition_sample_id)
    where acquisition_sample_id = acquisition_sample_id
    and sample_id = :sample_id:
    and acquisition_id = :acquisition_id:";
        $data = $this->lireParamAsPrepared($sql, array("sample_id" => $sample_id, "acquisition_id" => $acquisition_id));
        return $data["nb"];
    }

    /**
     * Surround of supprimer, to test the project_id from sample
     *
     * @param [type] $id
     * @return void
     */
    function supprimerFromSampleId(int $sample_id, int $acquisition_id)
    {
        /**
         * Verify the project
         */
        $sql = "select acquisition_sample_id, project_id, sample_name from acquisition_sample
            join sample using (sample_id)
            where sample_id = :sample_id:
            and acquisition_id = :acquisition_id:";
        $data = $this->lireParamAsPrepared($sql, array("sample_id" => $sample_id, "acquisition_id" => $acquisition_id));
        if (!projectVerify($data["project_id"])) {
            throw new PpciException(sprintf(_("Droits insuffisants pour supprimer une acquisition attachée à l'aliquot %s"), $data["sample_name"]));
        }
        if (!$this->isDeletable($sample_id, $acquisition_id)) {
            throw new PpciException(sprintf(_("Des fichiers ou des traitements sont rattachés à l'aliquot %s, il ne peut être supprimé"), $data["sample_name"]));
        }

        return parent::supprimer($data["acquisition_sample_id"]);
    }

    /**
     * Purge a sample from acquisition_sample
     *
     * @param integer $sample_id
     * @return void
     */
    function deleteAllFromSample(int $sample_id)
    {
        $sql = "select acquisition_sample_id from acquisition_sample where sample_id = :sample_id:";
        $list = $this->getListeParamAsPrepared($sql, array("sample_id" => $sample_id));
        foreach ($list as $item) {
            $this->supprimerFromSampleId($sample_id, $item["acquisition_sample_id"]);
        }
    }

    /**
     * Surround of supprimer to verify if the item is deletable
     *
     * @param [type] $acquisition_sample_id
     * @return void
     */
    function supprimer($acquisition_sample_id)
    {
        $data = $this->lire($acquisition_sample_id);
        $this->supprimerFromSampleId($data["sample_id"], $data["acquisition_id"]);
    }

    /**
     * Search if a sample is deletable
     *
     * @param integer $sample_id
     * @return boolean
     */
    function isDeletable(int $sample_id, int $acquisition_id): bool
    {
        if ($this->getNbFiles($sample_id, $acquisition_id) > 0) {
            return false;
        } else {
            $sql = "select count(*) as nb from treatment_as
              join acquisition_sample using (acquisition_sample_id)
      where sample_id = :sample_id: and acquisition_id = :acquisition_id:";
            $result = $this->lireParamAsPrepared($sql, array("sample_id" => $sample_id, "acquisition_id" => $acquisition_id));
            if ($result["nb"] > 0) {
                return false;
            } else {
                return true;
            }
        }
    }
}
