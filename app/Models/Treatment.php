<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

class Treatment extends PpciModel
{
    private $sql = "select treatment_id, treatment_date
                  , project_id, project_name
                  ,software_id, software_name
                  , (select count(*) from treatment_as tas where tas.treatment_id = t. treatment_id) as acquisition_nb
                  from treatment t
                  join project using (project_id)
                  left outer join software using (software_id)
                  ";
    public $sample,  $acquisitionSample, $treatmentAs;
    /**
     *
     * @param PDO $bdd
     * @param array $param
     */
    public function __construct()
    {
        $this->table = "treatment";
        $this->fields = array(
            "treatment_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "treatment_date" => array(
                "type" => 2,
                "requis" => 1,
                "defaultValue" => "getDateJour"
            ),
            "project_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "software_id" => array(
                "type" => 1
            )
        );
        parent::__construct();
    }
    /**
     * Get the list of treatments attached to a project
     *
     * @param integer $project_id
     * @return array|null
     */
    function getListFromProject(int $project_id, $order = ""): ?array
    {
        $where = " where project_id = :project_id: ";
        return $this->getListeParamAsPrepared($this->sql . $where . $order, array("project_id" => $project_id));
    }
    /**
     * Get the detail of a treatment
     *
     * @param int $id
     * @return array
     */
    function getDetail($id)
    {
        return ($this->lireParamAsPrepared(
            $this->sql . " where treatment_id = :id:",
            array("id" => $id)
        )
        );
    }
    /**
     * Create (or update) a treatment, and attach the acquisition_samples provided in array $samples
     *
     * @param [type] $data
     * @return integer|null
     */
    function createWithSamples($data): ?int
    {
        if (!$this->acquisitionSample) {
            $this->acquisitionSample = new AcquisitionSample;
        }
        if (!$this->treatmentAs) {
            $this->treatmentAs = new TreatmentAs;
        }
        $treatment_id = $this->ecrire($data);
        if (!$treatment_id > 0) {
            throw new PpciException(_("Une erreur est survenue lors de la création du traitement"));
        }
        foreach ($data["samples"] as $sample_id) {
            test($this->acquisitionSample->getIdFromSampleAcquisition($sample_id, $data["acquisition_id"]));
            $tas = array(
                "treatment_id" => $treatment_id,
                "acquisition_sample_id" => $this->acquisitionSample->getIdFromSampleAcquisition($sample_id, $data["acquisition_id"]),
                "treatment_as_id" => 0
            );
            if (is_null($tas["acquisition_sample_id"])) {
                throw new PpciException(sprintf(_("impossible de trouver l'acquisition pour l'aliquot n° %s"), $sample_id));
            }
            $treatmentAsId = $this->treatmentAs->ecrire($tas);
            if (!$treatmentAsId > 0) {
                throw new PpciException(_("Une erreur est survenue lors de l'attachement des aliquots (acquisition) au traitement"));
            }
        }
        return $treatment_id;
    }
}
