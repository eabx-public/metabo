<?php

namespace App\Models;

use Ppci\Models\PpciModel;

/**
 * ORM of the table mime_type
 */
class Mimetype extends PpciModel
{


    public function __construct()
    {
        $this->table = "mimetype";
        $this->fields = array(
            "mimetype_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "extension" => array(
                "type" => 0,
                "requis" => 1
            ),
            "content_type" => array(
                "type" => 0,
                "requis" => 1
            )
        );
        parent::__construct();
    }

    /**
     * Retourne le numero de type mime correspondant a l'extension
     *
     * @param string $extension
     * @return int
     */
    function getTypeMime($extension): ?int
    {
        if (strlen($extension) > 0) {
            $extension = strtolower($extension);
            $sql = "select mimetype_id from mimetype where extension = :extension:";
            $res = $this->lireParamAsPrepared($sql, array("extension" => $extension));
            if ($res["mimetype_id"] > 0) {
                return $res["mimetype_id"];
            } else {
                return null;
            }
        }
    }
    /**
     * Get the list of extensions, in array form or in string form with commas
     *
     * @param boolean $isArray
     * @return void
     */
    function getListExtensions($isArray = false)
    {
        $sql = "select extension from mimetype order by extension";
        $data = $this->getListeParam($sql);
        if (!$isArray) {
            $result = "";
            $comma = "";
            foreach ($data as $value) {
                $result .= $comma . $value["extension"];
                $comma = _(", ");
            }
            return $result;
        } else {
            return $data;
        }
    }
}
