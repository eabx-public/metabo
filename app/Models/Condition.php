<?php

namespace App\Models;

use Ppci\Models\PpciModel;

/**
 * ORM of the table condition
 */
class Condition extends PpciModel
{
    private $sql = "select condition_id, experimentation_id, station_id
                  ,matrice_type_id
                  ,condition_description, condition_code
                  ,matrice_type_name, station_name
                  ,experimentation_name
                  ,e.project_id, project_name
                  from condition c
                  join experimentation e using (experimentation_id)
                  join project using (project_id)
                  left outer join matrice_type using (matrice_type_id)
                  left outer join station using (station_id)
                  ";
    /**
     * Constructor
     *
     * @param pdo $bdd
     * @param string $tablename
     */
    public function __construct()
    {
        $this->table = "condition";
        $this->fields = array(
            "condition_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "station_id" => array("type" => 1),
            "experimentation_id" => array("type" => 1, "requis" => 1, "parentAttrib" => 1),
            "matrice_type_id" => array("type" => 1),
            "condition_description" => array("type" => 0),
            "condition_code" => array("type" => 0, "requis" => 1),
        );
        parent::__construct();
    }

    /**
     * Get the list of conditions attached to an experimentation
     *
     * @param integer $exp_id
     * @return array|null
     */
    function getListFromExperimentation(int $exp_id): ?array
    {
        $data = array();
        $where = " where experimentation_id = :exp_id:";
        $data = $this->getListeParamAsPrepared($this->sql . $where, array("exp_id" => $exp_id));
        return $data;
    }

    /**
     * Get the detail of a condition
     *
     * @param integer $id
     * @return array|null
     */
    function getDetail(int $id): ?array
    {
        $data = array();
        $where = " where condition_id = :id:";
        $data = $this->lireParamAsPrepared($this->sql . $where, array("id" => $id));
        return $data;
    }
    /**
     * Get the number of files attached to an item
     *
     * @param integer $id
     * @return integer
     */
    function getNbFiles(int $id): int
    {
        $sql = "select count(*) as nb from condition_file where condition_id = :id:";
        $data = $this->lireParamAsPrepared($sql, array("id" => $id));
        return $data["nb"];
    }
    /**
     * Get the list of conditions from a project
     *
     * @param integer $project_id
     * @return array|null
     */
    function getListFromProject(int $project_id): ?array
    {
        $where = " where e.project_id = :project_id:";
        $order = " order by experimentation_name, condition_code";
        return $this->getListeParamAsPrepared($this->sql . $where . $order, array("project_id" => $project_id));
    }

    /**
     * get the id of a condition from its code
     *
     * @param string $codeName
     * @param integer $exp_id
     * @return integer
     */
    function getIdFromCode(string $codeName, int $exp_id): int
    {
        $condition_id = 0;
        $sql = "select condition_id from condition 
            where condition_code = :code:
            and experimentation_id = :exp_id:";
        $res = $this->lireParamAsPrepared($sql, array("code" => $codeName, "exp_id" => $exp_id));
        if ($res["condition_id"] > 0) {
            $condition_id = $res["condition_id"];
        }
        return $condition_id;
    }
}
