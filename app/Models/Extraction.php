<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

class Extraction extends PpciModel
{

    private $sql = "select extraction_id, extraction_method_id, extraction_method_name, extraction_date
                  ,sampling_id, sampling_code, sampling_date, sampling_method_name
                  ,condition_id, condition_code
                  ,experimentation_id, experimentation_name
                  ,e.project_id, project_name
                  from extraction e
                  join project p on (p.project_id = e.project_id)
                  left outer join sampling using (sampling_id)
                  left outer join condition using (condition_id)
                  left outer join experimentation exp using (experimentation_id)
                  left outer join extraction_method using (extraction_method_id)
                  left outer join sampling_method using (sampling_method_id)
                ";
    public $classPath = "modules/classes";
    public $sample, $condition, $sampling;

    public function __construct()
    {
        $this->table = "extraction";
        $this->fields = array(
            "extraction_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "extraction_method_id" => array("type" => 1),
            "sampling_id" => array("type" => 1),
            "project_id" => array("type" => 1, "requis" => 1, "parentAttrib" => 1),
            "extraction_date" => array("type" => 2, "defaultValue" => "getDateJour")
        );
        parent::__construct();
    }

    /**
     * Get the detail of an extraction
     *
     * @param integer $id
     * @return array|null
     */
    function getDetail(int $id): ?array
    {
        $where = " where extraction_id = :extraction_id:";
        return $this->lireParamAsPrepared($this->sql . $where, array("extraction_id" => $id));
    }

    /**
     * Get the list of extractions attached to a condition
     *
     * @param integer $condition_id
     * @return array|null
     */
    function getListFromsampling(int $sampling_id): ?array
    {
        $where = " where sampling_id = :sampling_id:";
        return $this->getListeParamAsPrepared($this->sql . $where, array("sampling_id" => $sampling_id));
    }

    /**
     * Create (or no) an extraction, samples of type extract, and attach them to the extraction
     *
     * @param integer $project_id
     * @param integer $extraction_id
     * @param array $samples
     * @param string $extraction_date
     * @param float $volume
     * @param float $weight
     * @param string $unit
     * @param integer $extraction_method_id
     * @param integer $sampling_id
     * @return integer
     */
    function createWithSamples(int $project_id, int $extraction_id, array $samples, string $extraction_date, $volume = null, $weight = null, $unit_volume = null, $unit_weight = 'mg', $nbextrait = 1, $extraction_method_id = null, int $sampling_id = null, $letters = "A,B,C,D,E"): int
    {
        if (empty($samples)) {
            throw new PpciException(_("Aucun échantillon n'a été sélectionné"));
        }
        if (!projectVerify($project_id)) {
            throw new PpciException(_("Les droits sont insuffisants pour le projet considéré"));
        }
        if (!isset($this->sample)) {
            $this->sample = new Sample;
        }
        if ($extraction_id == 0) {
            $data = array("project_id" => $project_id, "extraction_id" => 0, "extraction_date" => $extraction_date, "extraction_method_id" => $extraction_method_id);
            if ($sampling_id > 0) {
                $data["sampling_id"] = $sampling_id;
            }
            $extraction_id = $this->ecrire($data);
        }
        $aletters = explode(",", $letters);
        $currentLetter = 0;
        /**
         * Treatment of samples
         */
        $data = array(
            "extraction_id" => $extraction_id,
            "sample_id" => 0,
            "sample_type_id" => 2,
            "volume" => $volume,
            "weight" => $weight,
            "unit_volume" => $unit_volume,
            "unit_weight" => $unit_weight
        );
        if ($nbextrait > 1) {
            $withletter = true;
            if (count($aletters) < $nbextrait) {
                throw new PpciException(_("Le nombre de lettres fournies est insuffisant pour numéroter tous les extraits à générer"));
            }
        } else {
            $withletter = false;
        }

        foreach ($samples as $sample_id) {
            $dataSample = $this->sample->lire($sample_id);
            if (!projectVerify($dataSample["project_id"])) {
                throw new PpciException(sprintf(_("Les droits sont insuffisants pour l'échantillon %s"), $sample_id));
            }
            for ($i = 1; $i <= $nbextrait; $i++) {
                $data["project_id"] = $dataSample["project_id"];
                $data["sample_name"] = $dataSample["sample_name"];
                if ($withletter) {
                    $data["sample_name"] .= "-" . $aletters[$currentLetter];
                    $currentLetter++;
                    //$data["sample_name"] .= chr(64 + $i);
                }
                $data["sample_parent_id"] = $sample_id;
                $this->sample->ecrire($data);
            }
            $currentLetter = 0;
        }
        return $extraction_id;
    }

    /**
     * Get the list of extractions attached to a project
     *
     * @param integer $project_id
     * @return array|null
     */
    function getListFromProject(int $project_id): ?array
    {
        $this->fields["sampling_date"] = array("type" => 2);
        $where = " where e.project_id = :project_id:";
        $order = " order by experimentation_name, sampling_date, extraction_date";
        return $this->getListeParamAsPrepared($this->sql . $where . $order, array("project_id" => $project_id));
    }
}
