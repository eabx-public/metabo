<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

class Filelink extends PpciModel
{
    public $linkDuration = 7;
    public FileClass $fileObject;

    public function __construct()
    {
        $this->table = "filelink";
        $this->fields = array(
            "filelink_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "file_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "linkname" => array(
                "type" => 0,
                "requis" => 1
            ),
            "expiration_date" => array(
                "type" => 3,
                "requis" => 1
            ),
            "last_use" => ["type" => 3]
        );
        parent::__construct();
    }

    /**
     * Generate the default duration of a link
     *
     * @return string
     */
    function generateExpirationDate(int $linkDuration = 7): string
    {
        $date = new \DateTime();
        date_add($date, new \DateInterval("P" . $linkDuration . "D"));
        return date_format($date, $_SESSION["date"]["maskdatelong"]);
    }

    /**
     * Generate a link for a file, and save it
     *
     * @param integer $file_id
     * @param integer $linkDuration
     * @return string
     */
    function generateLink(int $file_id, int $linkDuration = 7): string
    {
        if (!isset($this->fileObject)) {
            $this->fileObject = new FileClass;
        }
        $dfile = $this->fileObject->lire($file_id);
        /**
         * Verify the project
         */
        if (!projectVerify($dfile["project_id"])) {
            throw new PpciException(_("Le lien ne peut pas être généré pour le fichier, le projet auquel il est rattaché ne vous est pas accessible"));
        }
        $data = array(
            "filelink_id" => 0,
            "file_id" => $file_id,
            "expiration_date" => $this->generateExpirationDate($linkDuration),
            "linkname" => bin2hex(random_bytes(32))
        );
        $filelink_id = $this->ecrire($data);
        if ($filelink_id > 0) {
            return $data["linkname"];
        } else {
            throw new PpciException(_("La génération du lien a échoué lors de l'écriture dans la base de données"));
        }
    }

    /**
     * Get the data from the link
     *
     * @param string $link
     * @return array
     */
    function getFromLink(string $link): array
    {
        if (!isset($this->fileObject)) {
            $this->fileObject = new FileClass;
        }
        $sql = "select file_id, expiration_date, filelink_id
            from filelink
            where linkname = :link:
            and expiration_date > :expiration_date:";
        $data = $this->lireParamAsPrepared($sql, array("link" => $link, "expiration_date" => date('Y-m-d H:i:s')));
        if (empty($data["file_id"])) {
            throw new PpciException(_("Le lien fourni n'existe pas ou est expiré"));
        }
        return $data;
    }

    function updateLastUse(int $id) {
        $sql = "update filelink set last_use = :lastuse: where filelink_id = :id:";
        $this->executeQuery($sql, ["lastuse"=>date('Y-m-d H:i:s'),"id"=>$id], true);
    }
}
