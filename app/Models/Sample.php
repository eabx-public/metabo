<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

class Sample extends PpciModel
{
    private $sql = "select s.sample_id
                ,samp.sampling_id, samp.sampling_code
                ,cond.condition_id, cond.condition_code
                ,s.sample_type_id, s.sample_parent_id,s.uuid, s.sample_name
                ,s.volume, s.weight, s.unit_volume, s.unit_weight, s.sampled_volume
                ,st.sample_type_name, st.collec_type_name
                ,samp.sampling_date
                ,tm.transport_method_name
                ,e.experimentation_id, e.experimentation_name
                ,p.project_id, p.project_name
                ,sp.sample_name as sample_parent_name, sp.uuid as parent_uuid, stp.sample_type_name as parent_sample_type_name
                ,s.extraction_id, ext.extraction_date
                ,sta.station_name
                ,s.sample_nature_id, sample_nature_name
                ,s.storage_location_id, storage_location_name
                ,acquisition_date, acquisition_id
                ,treatment_date, treatment_id
                ,s.storage_type_id, storage_type_name
                ,s.remarks
                from sample s
                join sample_type st on (s.sample_type_id = st.sample_type_id)
                join project p on (s.project_id = p.project_id)
                join sample_nature sn on(sn.sample_nature_id = s.sample_nature_id)
                left outer join condition cond on (get_condition_id(s.sample_id) = cond.condition_id)
                left outer join sampling samp on (get_sampling_id(s.sample_id) = samp.sampling_id)
                left outer join extraction ext on (s.extraction_id = ext.extraction_id)
                left outer join experimentation e on (cond.experimentation_id = e.experimentation_id)
                left outer join sample sp on (s.sample_parent_id = sp.sample_id)
                left outer join sample_type stp on (sp.sample_type_id = stp.sample_type_id)
                left outer join transport_method tm on (s.transport_method_id = tm.transport_method_id)
                left outer join station sta on (cond.station_id = sta.station_id)
                left outer join storage_location sl on (sl.storage_location_id = s.storage_location_id)
                left outer join acquisition_sample aqs on (aqs.sample_id = s.sample_id)
                left outer join acquisition using (acquisition_id)
                left outer join treatment_as using (acquisition_sample_id)
                left outer join treatment using (treatment_id)
                left outer join storage_type stb on (s.storage_type_id = stb.storage_type_id)
  ";

    public AcquisitionSample $acquisitionSample;

    /**
     * Constructor
     *
     * @param pdo $bdd
     * @param string $tablename
     */
    public function __construct()
    {
        $this->table = "sample";
        $this->fields = array(
            "sample_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "sample_type_id" => array("type" => 1, "requis" => 1),
            "project_id" => array("type" => 1, "requis" => 1),
            "sampling_id" => array("type" => 1),
            "extraction_id" => array("type" => 1),
            "sample_name" => array("requis" => 1),
            "sample_parent_id" => array("type" => 1),
            "uuid" => array("type" => 0, "defaultValue" => "getUUID"),
            "volume" => array("type" => 1),
            "transport_method_id" => array("type" => 1),
            "storage_location_id" => array("type" => 1),
            "sample_nature_id" => array("type" => 1, "defaultValue" => 1),
            "weight" => array("type" => 1),
            "unit_volume" => array("type" => 0),
            "unit_weight" => array("type" => 0, "defaultValue" => "mg"),
            "storage_type_id" => array("type" => 1),
            "remarks" => array("type" => 0),
            "sampled_volume"=>["type"=>1]
        );
        parent::__construct();
    }
    /**
     * Calculate the unit of the quantity of sample
     *
     * @param array $data
     * @return int
     */
    function write($data): int
    {
        if ($data["sample_type_id"] <= 2) {
            $data["unit_volume"] = "ml";
        } else {
            $data["unit_volume"] = "µl";
        }
        return parent::write($data);
    }
    /**
     * Delete a sample with acquisition_sample
     *
     * @param int $id
     * @return void
     */
    function supprimer($id)
    {
        if ($this->isDeletable($id)) {
            if (empty($this->acquisitionSample)) {
                $this->acquisitionSample = new AcquisitionSample;
            }
            $this->acquisitionSample->deleteAllFromSample($id);
            parent::supprimer($id);
        } else {
            $data = $this->lire($id);
            throw new PpciException(sprintf(_("L'échantillon %s est trop avancé dans le traitement pour être supprimé"), $data["sample_name"]));
        }
    }

    /**
     * Get the list from a sampling
     *
     * @param integer $sampling_id
     * @return array|null
     */
    function getListFromSampling(int $sampling_id): ?array
    {
        $where = " where s.sampling_id = :sampling_id:";
        $this->addDateFields();
        $data = $this->getListeParamAsPrepared($this->sql . $where, array("sampling_id" => $sampling_id));
        return $this->addParents($data);
    }

    /**
     * get the list of samples from an extraction
     *
     * @param integer $extraction_id
     * @return array|null
     */
    function getListFromExtraction(int $extraction_id): ?array
    {
        $where = " where s.extraction_id = :extraction_id:";
        $this->addDateFields();
        $data = $this->getListeParamAsPrepared($this->sql . $where, array("extraction_id" => $extraction_id));
        return $this->addParents($data);
    }

    /**
     * Get the list of samples from acquisition
     *
     * @param integer $acquisition_id
     * @return array|null
     */
    function getListFromAcquisition(int $acquisition_id): ?array
    {
        $sql = "select s.sample_id, s.sample_name, s.uuid, s.volume, s.unit_volume, s.unit_weight, s.sampled_volume
            , s.project_id
            ,s.sample_parent_id ,sp.sample_name as sample_parent_name
            ,st.sample_type_name
            ,ans.acquisition_sample_id
            ,ans.is_calibration
            from sample s
            join acquisition_sample ans on (ans.sample_id = s.sample_id)
            join sample_type st on (s.sample_type_id = st.sample_type_id)
            left outer join sample sp on (s.sample_parent_id = sp.sample_id)
            ";
        $where = " where ans.acquisition_id = :acquisition_id:";
        $this->addDateFields();
        return $this->addParents($this->getListeParamAsPrepared($sql . $where, array("acquisition_id" => $acquisition_id)));
    }

    /**
     * Get the list of samples attached to a treatment
     *
     * @param integer $treatment_id
     * @return array|null
     */
    function getListFromTreatment(int $treatment_id): ?array
    {
        $sql = "select s.sample_id, s.sample_name, s.uuid, s.volume, s.weight, s.unit_volume, s.unit_weight, s.sampled_volume
            , s.project_id
            ,s.sample_parent_id ,sp.sample_name as sample_parent_name
            ,st.sample_type_name
            ,treatment_id, treatment_as_id
            ,acquisition_id, acquisition_type_id, acquisition_method_id
            ,acquisition_date,acquisition_type_name, acquisition_method_name
            from treatment_as
            join acquisition_sample using (treatment_id)
            join sample_id using (sample_id)
            join acquisition using (acquisition_id)
            join acquisition_type using (acquisition_type_id)
            left outer join acquisition_method using (acquisition_method_id)
            where treatment_id = :treatment_id:
            ";
        $this->addDateFields();
        return $this->addParents($this->getListeParamAsPrepared($sql, array("treatment_id" => $treatment_id)));
    }

    /**
     * Search all parents of a sample
     *
     * @param array $list
     * @return array
     */
    function addParents(array $list, $isList = true): array
    {
        $sql = "with recursive sample_parents as (
      select sample_id, sample_name, sample_parent_id
      from metabo.sample
      where sample_id = :sample_id:
      union
      select s1.sample_id, s1.sample_name
      ,s1.sample_parent_id
      from metabo.sample s1
      join sample_parents p on (p.sample_parent_id = s1.sample_id)
      )
      select sp.sample_id, sp.sample_name
      from sample_parents sp
      where sample_id <> :sample_id:
      order by sample_parent_id";
        if ($isList) {
            foreach ($list as $key => $item) {
                $list[$key]["parents"] = $this->getListeParamAsPrepared($sql, array("sample_id" => $item["sample_id"]));
            }
        } else {
            $list["parents"] = $this->getListeParamAsPrepared($sql, array("sample_id" => $list["sample_id"]));
        }
        return $list;
    }
    /**
     * List of acquisition proceeded on the sample
     *
     * @param integer $sample_id
     * @return array|null
     */
    function getListAcquisition(int $sample_id): ?array
    {
        $sql = "select sample_id, acquisition_id, acquisition_date, machine_id, machine_name
            ,acquisition_sample_id,is_calibration
            from sample
            join acquisition_sample using (sample_id)
            join acquisition using (acquisition_id)
            left outer join machine using (machine_id)
    ";
        $this->addDateFields();
        $where = " where sample_id = :sample_id:";
        return $this->getListeParamAsPrepared($sql . $where, array("sample_id" => $sample_id));
    }

    function getListTreatment(int $sample_id): ?array
    {
        $sql = "select sample_id, acquisition_id, acquisition_date,software_name
          ,acquisition_sample_id,is_calibration
          ,treatment_id, treatment_date
          from sample
          join acquisition_sample using (sample_id)
          join treatment_as using (acquisition_sample_id)
          join acquisition using (acquisition_id)
          join treatment using (treatment_id)
          left outer join software using (software_id)
          where sample_id =:sample_id:";
        $this->addDateFields();
        return $this->getListeParamAsPrepared($sql, array("sample_id" => $sample_id));
    }

    /**
     * Get the detail of a sample
     *
     * @param integer $sample_id
     * @return array|null
     */
    function getDetail(int $sample_id): ?array
    {
        $data = array();
        $where = " where s.sample_id = :sample_id:";
        $this->addDateFields();
        $data = $this->lireParamAsPrepared($this->sql . $where, array("sample_id" => $sample_id));
        return $this->addParents($data, false);
    }
    /**
     * Get the number of files attached to an item
     *
     * @param integer $id
     * @return integer
     */
    function getNbFiles(int $id): int
    {
        $sql = "select count(*) as nb from sample_file where sample_id = :id:";
        $data = $this->lireParamAsPrepared($sql, array("id" => $id));
        return $data["nb"];
    }
    /**
     * Verify if a sample can be deleted
     *
     * @param integer $id
     * @return boolean
     */
    function isDeletable(int $id): bool
    {
        $rep = true;
        if ($this->getNbFiles($id) > 0) {
            $rep = false;
        } else {
            $children = $this->getChildren($id);
            if (count($children) > 0) {
                $rep = false;
            } else {
                $acquisitions = $this->getListAcquisition($id);
                if (count($acquisitions) > 0) {
                    $rep = false;
                } else {
                    $treatments = $this->getListTreatment($id);
                    if (count($treatments) > 0) {
                        $rep = false;
                    }
                }
            }
        }
        return $rep;
    }
    /**
     * Return the list of samples attached to an other
     *
     * @param integer $id
     * @return array|null
     */
    function getChildren(int $id): ?array
    {
        $where = " where s.sample_parent_id = :id:";
        return $this->addParents($this->getListeParamAsPrepared($this->sql . $where, array("id" => $id)));
    }
    /**
     * Surround of lire to get detail of a sample
     *
     * @param integer $id
     * @param boolean $getDefault
     * @param integer $parentValue
     * @return array
     */
    function read($id, $getDefault = true, $parentValue = 0): array
    {
        $data = array();
        if ($id == 0) {
            $data = parent::getDefaultValues($parentValue);
        } else {
            $data = $this->getDetail($id);
        }
        return $data;
    }
    /**
     * Generate a name of a sample, based on the last sample name known, with -1, -2, etc.
     *
     * @param string $sampleName
     * @param integer $project_id
     * @return string
     */
    function generateSampleName(string $sampleName, int $project_id): string
    {
        $sql = "select sample_id from sample where sample_name = :name: and project_id =:project_id:";
        $param = array("project_id" => $project_id);
        for ($i = 1; $i < 100; $i++) {
            $param["name"] = $sampleName . "-" . $i;
            $data = $this->lireParamAsPrepared($sql, $param);
            if (!$data["sample_id"] > 0) {
                break;
            }
        }
        return $param["name"];
    }

    /**
     * Get the list of samples attached to a project,
     * with or without the sample_type
     *
     * @param integer $project_id
     * @param integer $sample_type_id
     * @return array|null
     */
    function getListFromProject(int $project_id, int $sample_type_id = 0): ?array
    {
        $where = " where s.project_id = :project_id:";
        $param = array("project_id" => $project_id);
        if ($sample_type_id > 0) {
            $where .= " and s.sample_type_id = :sample_type_id:";
            $param["sample_type_id"] = $sample_type_id;
        }
        $this->fields["acquisition_date"] = array("type" => 2);
        $this->fields["treatment_date"] = array("type" => 2);
        $this->addDateFields();
        return $this->addParents($this->getListeParamAsPrepared($this->sql . $where, $param));
    }

    /**
     * Get the sample_if from the name, for a project
     *
     * @param string $sample_name
     * @param integer $project_id
     * @return integer
     */
    function getIdFromName(string $sample_name, int $project_id): int
    {
        $sql = "select sample_id from sample where project_id = :project_id: and upper (sample_name) = upper(:sample_name:)";
        $data = $this->lireParamAsPrepared($sql, array("project_id" => $project_id, "sample_name" => $sample_name));
        if (!empty($data["sample_id"])) {
            return $data["sample_id"];
        } else {
            return 0;
        }
    }
    /**
     * Get the sample_id from uuid
     *
     * @param string $uuid
     * @return integer
     */
    function getIdFromUUID(string $uuid): int
    {
        $sql = "select sample_id from sample where uuid = :uuid:";
        $data = $this->lireParamAsPrepared($sql, array("uuid" => $uuid));
        if (!empty($data["sample_id"])) {
            return $data["sample_id"];
        } else {
            return 0;
        }
    }
    /**
     * Add the complementary date fields to display it correctly
     *
     * @return void
     */
    function addDateFields()
    {
        $dates = ["sampling_date", "extraction_date", "acquisition_date", "treatment_date"];
        foreach ($dates as $f) {
            $this->dateFields[] = $f;
        }
    }
}
