<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

/**
 * ORM of the table condition
 */
class Sampling extends PpciModel
{
    private $sql = "select  sampling_id, condition_id, experimentation_id, sampling_method_id
                  ,sampling_code, sampling_description, sampling_date
                  ,default_weight, default_volume, default_unit_volume, default_unit_weight
                  ,separator, level1_code, level1_number
                  ,level2_code, level2_number
                  ,sampling_method_name
                  ,condition_code, experimentation_name
                  ,project_id, project_name
                  ,sample_number
                  ,default_storage_type, storage_type_name
                  ,default_storage_location, storage_location_name
                  from sampling
                  join condition using (condition_id)
                  join experimentation using (experimentation_id)
                  join project using (project_id)
                  left outer join sampling_method using (sampling_method_id)
                  left outer join v_sampling_sample using (sampling_id)
                  left outer join storage_type on (storage_type_id = default_storage_type)
                  left outer join storage_location on (storage_location_id = default_storage_location)
                  ";
    public Sample $sample;
    public FileClass $fileClass;
    /**
     * Constructor
     *
     * @param pdo $bdd
     * @param string $tablename
     */
    public function __construct()
    {
        $this->table = "sampling";
        $this->fields = array(
            "sampling_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "condition_id" => array("type" => 1, "requis" => 1, "parentAttrib" => 1),
            "sampling_method_id" => array("type" => 1),
            "sampling_date" => array("type" => 2, "defaultValue" => "getDateJour"),
            "sampling_description" => array("type" => 0),
            "sampling_code" => array("type" => 0, "requis" => 1),
            "default_weight" => array("type" => 1),
            "default_volume" => array("type" => 1),
            "default_unit_volume" => array("type" => 0, "defaultValue" => "µl"),
            "default_unit_weight" => array("type" => 0, "defaultValue" => "mg"),
            "separator" => array("type" => 0, "defaultValue" => "-"),
            "level1_code" => array("type" => 0),
            "level1_number" => array("type" => 1, "defaultValue" => "1", "requis" => 1),
            "level2_code" => array("type" => 0),
            "level2_number" => array("type" => 1),
            "default_storage_type" => array("type" => 1),
            "default_storage_location" => array("type" => 1)
        );
        parent::__construct();
    }
    /**
     * Get the list of sampling attached to a condition
     *
     * @param integer $condition_id
     * @return array|null
     */
    function getListFromCondition(int $condition_id): ?array
    {
        $data = array();
        $where = " where condition_id = :condition_id:";
        $data = $this->getListeParamAsPrepared($this->sql . $where, array("condition_id" => $condition_id));
        return $data;
    }
    /**
     * Get the list of sampling from a project
     *
     * @param integer $project_id
     * @return array|null
     */
    function getListFromProject(int $project_id): ?array
    {
        $data = array();
        $where = " where project_id = :project_id:";
        $data = $this->getListeParamAsPrepared($this->sql . $where, array("project_id" => $project_id));
        return $data;
    }
    /**
     * surround of lire to add others informations
     *
     * @param [type] $id
     * @param boolean $getDefault
     * @param integer $parentId
     * @return array
     */
    function read($id, $getDefault = true, $parentId = 0): array
    {
        if ($id == 0) {
            $data = $this->getDefaultValues($parentId);
            $data["sample_number"] = 0;
        } else {
            $where = " where sampling_id = :id:";
            $data = $this->lireParamAsPrepared($this->sql . $where, array("id" => $id));
        }
        return $data;
    }

    /**
     * Get the detail of a sampling
     *
     * @param integer $id
     * @return array|null
     */
    function getDetail(int $id): ?array
    {
        $data = array();
        $where = " where sampling_id = :id:";
        $data = $this->lireParamAsPrepared($this->sql . $where, array("id" => $id));
        return $data;
    }
    /**
     * Get the number of files attached to an item
     *
     * @param integer $id
     * @return integer
     */
    function getNbFiles(int $id): int
    {
        $sql = "select count(*) as nb from sampling_file where sampling_id = :id:";
        $data = $this->lireParamAsPrepared($sql, array("id" => $id));
        return $data["nb"];
    }
    /**
     * Delete sampling with attached samples
     *
     * @param int $id
     */
    function supprimer($id)
    {
        $extract = $this->readParam(
            "select count(*) as extraction_number from extraction where sampling_id = :id:",
            array("id" => $id)
        );
        if ($extract["extraction_number"] > 0) {
            throw new PpciException(sprintf(
                _("%s extractions sont rattachées à l'échantillonnage, la suppression n'est pas possible"),
                $extract["extraction_number"]
            ));
        }
        if (!isset($this->sample)) {
            $this->sample = new Sample;
        }
        if (!isset($this->fileClass)) {
            $this->fileClass = new FileClass;
        }

        $samples = $this->sample->getListFromSampling($id);
        foreach ($samples as $sample) {
            $this->sample->supprimer($sample["sample_id"]);
        }
        $files = $this->fileClass->getListFromObject("sampling", $id);
        foreach ($files as $file) {
            $this->fileClass->supprimer($file["file_id"]);
        }
        return parent::supprimer($id);
    }

    function createWithSamples(array $data): int
    {
        $samplenumber = 0;
        try {
            /**
             * Generate the sampling
             */
            $sampling_id = $this->ecrire($data);
            if (!$sampling_id > 0) {
                throw new PpciException(_("La création de l'échantillonnage a échoué pour une raison inconnue"));
            }
            /**
             * Treatment of samples
             */
            if (!isset($this->sample)) {
                $this->sample = new Sample;
            }
            $dsample = array(
                "sampling_id" => $sampling_id,
                "sample_id" => 0,
                "sample_type_id" => 1,
                "volume" => $data["volume"],
                "weight" => $data["weight"],
                "unit_volume" => $data["unit_volume"],
                "unit_weight" => $data["unit_weight"],
                "project_id" => $data["project_id"],
                "storage_type_id" => $data["storage_type_id"],
                "storage_location_id" => $data["storage_location_id"],
                "transport_method_id" => $data["transport_method_id"]
            );
            $radical = $data["sampling_code"];
            for ($i = 0; $i < $data["level1_number"]; $i++) {
                $name = $radical . $data["separator"] . $data["level1_code"] . ($i + 1);
                if ($data["level2_number"] > 0) {
                    for ($j = 1; $j <= $data["level2_number"]; $j++) {
                        $dsample["sample_name"] = $name . $data["separator"] . $data["level2_code"] . $j;
                        $this->sample->ecrire($dsample);
                        $samplenumber++;
                    }
                } else {
                    $dsample["sample_name"] = $name;
                    $this->sample->ecrire($dsample);
                    $samplenumber++;
                }
            }
        } catch (\Exception $e) {
            throw new PpciException($e->getMessage());
        }
        return $samplenumber;
    }
}
