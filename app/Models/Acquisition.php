<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

class Acquisition extends PpciModel
{
    private $sql = "select
                  acquisition_id, acquisition_date, project_id, machine_id
                  ,project_name, machine_name
                  ,acquisition_type_id, acquisition_type_name
                  ,chromatography_method_id, chromatography_method_name
                  ,acquisition_method_id, acquisition_method_name
                  from acquisition
                  join project using (project_id)
                  join acquisition_type using (acquisition_type_id)
                  left outer join machine using (machine_id)
                  left outer join chromatography_method using (chromatography_method_id)
                  left outer join acquisition_method using (acquisition_method_id)
                  ";
    public $sample, $acquisition, $acquisitionSample;
    /**
     *
     * @param PDO $bdd
     * @param array $param
     */
    public function __construct()
    {
        $this->table = "acquisition";
        $this->fields = array(
            "acquisition_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "acquisition_date" => array(
                "type" => 2,
                "requis" => 1,
                "defaultValue" => "getDateJour"
            ),
            "project_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "machine_id" => array(
                "type" => 1
            ),
            "acquisition_type_id" => array(
                "type" => 1,
                "requis" => 1
            ),
            "chromatography_method_id" => array("type" => 1),
            "acquisition_method_id" => array("type" => 1)
        );
        parent::__construct();
    }
    /**
     * Get the number of files attached to an item
     *
     * @param integer $id
     * @return integer
     */
    function getNbFiles(int $id): int
    {
        $sql = "select count(*) as nb from acquisition_file where acquisition_id = :id:";
        $data = $this->lireParamAsPrepared($sql, array("id" => $id));
        return $data["nb"];
    }
    /**
     * Get the detail of an acquisition
     *
     * @param integer $id
     * @return array|null
     */
    function getDetail(int $id): ?array
    {
        $where = " where acquisition_id = :id:";
        return $this->lireParamAsPrepared($this->sql . $where, array("id" => $id));
    }

    /**
     * Get the list of acquisition attached to a project
     *
     * @param integer $project_id
     * @param string $order
     * @return array|null
     */
    function getListFromProject(int $project_id, string $order = ""): ?array
    {
        $where = " where project_id = :project_id:";
        if (!empty($order)) {
            $order = " order by $order";
        }
        return $this->getListeParamAsPrepared($this->sql . $where . $order, array("project_id" => $project_id));
    }

    /**
     * Create an acquisition and aliquotes if necessary
     *
     * @param array $data: content of $_POST (form)
     * @return integer
     */
    function createWithSamples(array $data): int
    {
        if (empty($data["samples"])) {
            throw new PpciException(_("Aucun échantillon n'a été sélectionné"));
        }
        if (!isset($this->sample)) {
            $this->sample = new Sample;
        }
        if (!isset($this->acquisition)) {
            $this->acquisition = new Acquisition;
        }
        if (!isset($this->acquisitionSample)) {
            $this->acquisitionSample = new AcquisitionSample;
        }
        /**
         * Verify project
         */
        if (!projectVerify($data["project_id"])) {
            throw new PpciException(_("Les droits sont insuffisants pour réaliser cette opération"));
        }
        if ($data["acquisition_id"] == 0) {
            $acquisition_id = $this->acquisition->ecrire($data);
        } else {
            $dacquisition = $this->acquisition->getDetail($data["acquisition_id"]);
            if (!projectVerify($dacquisition["project_id"])) {
                throw new PpciException(_("Les droits sont insuffisants pour réaliser cette opération"));
            }
            $acquisition_id = $data["acquisition_id"];
        }
        $dAcquisitionSample = array(
            "acquisition_sample_id" => 0,
            "acquisition_id" => $acquisition_id,
            "is_calibration" => $data["is_calibration"]
        );
        $dSample = array(
            "sample_type_id" => 3,
            "volume" => $data["volume"],
            "sampled_volume"=>$data["sampled_volume"],
            "unit" => "µl"
        );
        foreach ($data["samples"] as $sample_id) {
            $dataSample = $this->sample->lire($sample_id);
            if (!projectVerify($dataSample["project_id"])) {
                throw new PpciException(sprintf(_("Les droits sont insuffisants pour l'échantillon %s"), $sample_id));
            }
            if ($data["sample_type_id"] == 2) {
                /**
                 * Creation of aliquote
                 */
                $dSample["project_id"] = $dataSample["project_id"];
                $dSample["sample_name"] = $this->sample->generateSampleName($dataSample["sample_name"], $data["project_id"]);
                $dSample["sample_parent_id"] = $sample_id;
                $sampleId = $this->sample->ecrire($dSample);
            } else {
                $sampleId = $sample_id;
            }
            /**
             * Attachment to the acquisition
             */
            $dAcquisitionSample["sample_id"] = $sampleId;
            $this->acquisitionSample->ecrire($dAcquisitionSample);
        }
        return $acquisition_id;
    }
}
