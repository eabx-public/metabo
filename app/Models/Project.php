<?php

namespace App\Models;

use Ppci\Models\Aclgroup;
use Ppci\Models\PpciModel;

class Project extends PpciModel
{

    /**
     *
     * @param PDO $bdd
     * @param array $param
     */
    public function __construct()
    {
        $this->table = "project";
        $this->fields = array(
            "project_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "project_name" => array(
                "type" => 0,
                "requis" => 1
            ),
            "project_description" => array(
                "type" => 0
            ),
            "project_year" => array("type" => 1, "defaultValue" => "getYear"),
            "project_code" => array("type" => 0),
            "external_path" => array("type" => 0, "defaultValue" => $_SESSION["external_path_default"]),
            "mama_code" => array("type" => 0)
        );
        parent::__construct();
    }

    /**
     * Ajoute la liste des groupes a la liste des projects
     *
     * {@inheritdoc}
     *
     * @see ObjetBDD::getListe()
     */
    function getListe($order = "project_year desc, project_name"): array
    {
        $sql = "select project_id, project_name, project_description,
            array_to_string(array_agg(groupe),', ') as groupe
            ,project_year, project_code, external_path, mama_code
				  from project
            left outer join project_group using (project_id)
				    left outer join aclgroup using (aclgroup_id)
				  group by project_id, project_name, project_description,project_year
				  order by $order";
        return $this->getListeParam($sql);
    }

    /**
     * Retourne la liste des projects autorises pour un login
     *
     * @return array
     */
    function getprojectsFromLogin()
    {
        return $this->getprojectsFromGroups($_SESSION["groupes"]);
    }

    /**
     * Get the list of projects corresponding to the specified groups
     * Each item of the list is identified by the project_id
     *
     * @param array $groups
     *
     * @return array
     */
    function getprojectsFromGroups(array $groups)
    {
        $projects = array();
        if (count($groups) > 0) {
            /*
             * Preparation de la clause in
             */
            $comma = false;
            $in = "(";
            foreach ($groups as $value) {
                if (!empty($value["groupe"])) {
                    $comma == true ? $in .= ", " : $comma = true;
                    $in .= "'" . $value["groupe"] . "'";
                }
            }
            $in .= ")";
            $sql = "select distinct project_id, project_name, project_year, project_code, external_path, mama_code
					from project
					join project_group using (project_id)
					join aclgroup using (aclgroup_id)
					where groupe in $in";
            $order = " order by project_year desc, project_name";
            $data = $this->getListeParam($sql . $order);

            foreach ($data as $project) {
                $projects[$project["project_id"]] = $project;
            }
        }
        return $projects;
    }

    /**
     * Surcharge de la fonction ecrire, pour enregistrer les groupes autorises
     *
     * {@inheritdoc}
     *
     * @see ObjetBDD::ecrire()
     */
    function write($data): int
    {
        $id = parent::write($data);
        if ($id > 0) {
            /*
             * Ecriture des groupes
             */
            $this->ecrireTableNN("project_group", "project_id", "aclgroup_id", $id, $data["groupes"]);
        }
        return $id;
    }

    /**
     * Delete a project
     *
     * {@inheritdoc}
     *
     * @see ObjetBDD::supprimer()
     */
    function supprimer($id)
    {
        if ($id > 0 && is_numeric($id)) {
            $sql = "delete from project_group where project_id = :project_id:";
            $this->executeSQL($sql, array("project_id" => $id),true);
            return parent::supprimer($id);
        }
    }
    /**
     * Get the number of files attached to a project
     *
     * @param integer $id
     * @return integer
     */
    function getNbFiles(int $id): int
    {
        $sql = "select count(*) as nb from file where project_id = :id:";
        $data = $this->lireParamAsPrepared($sql, array("id" => $id));
        return $data["nb"];
    }

    /**
     * Retourne la liste de tous les groupes, en indiquant s'ils sont ou non presents
     * dans le projet (attribut checked a 1)
     *
     * @param int $project_id
     * @return array
     */
    function getAllGroupsFromproject($project_id)
    {
        if ($project_id > 0 && is_numeric($project_id)) {
            $data = $this->getGroupsFromproject($project_id);
            $dataGroup = array();
            foreach ($data as $value) {
                $dataGroup[$value["aclgroup_id"]] = 1;
            }
        }
        $aclgroup = new Aclgroup;
        $groupes = $aclgroup->getListe(2);
        foreach ($groupes as $key => $value) {
            $groupes[$key]["checked"] = $dataGroup[$value["aclgroup_id"]];
        }
        return $groupes;
    }

    /**
     * Retourne la liste des groupes attaches a un projet
     *
     * @param int $project_id
     * @return array
     */
    function getGroupsFromproject($project_id)
    {
        $data = array();
        if ($project_id > 0 && is_numeric($project_id)) {
            $sql = "select aclgroup_id, groupe from project_group
					join aclgroup using (aclgroup_id)
					where project_id = :project_id:";
            $var["project_id"] = $project_id;
            $data = $this->getListeParamAsPrepared($sql, $var);
        }
        return $data;
    }

    /**
     * Initialise la liste des connexions rattachees au login
     */
    function initprojects()
    {
        $_SESSION["projects"] = $this->getprojectsFromLogin();
        /*
         * Attribution des droits de gestion si attache a un projet
         */
        if (count($_SESSION["projects"]) > 0) {
            $_SESSION["droits"]["gestion"] = 1;
        }
    }
    /**
     * Delete the aclgroup from projects
     *
     * @param int $group_id
     * @return void
     */
    function deleteGroup(int $group_id)
    {
        $sql = "delete from project_group where aclgroup_id = :group_id:";
        $this->executeAsPrepared($sql, array("group_id" => $group_id), true);
    }
}
