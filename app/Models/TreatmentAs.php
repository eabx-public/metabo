<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\PpciModel;

class TreatmentAs extends PpciModel
{
    private $sql = "select treatment_as_id, treatment_id, acquisition_sample_id
                  ,software_id, software_name
                  ,s.project_id,project_name
                  ,sample_id, sample_name, is_calibration, uuid, volume, unit_volume, weight, unit_weight
                  ,sample_type_name
                  ,acquisition_date, acquisition_type_name
                  from treatment_as tas
                  join treatment t using (treatment_id)
                  join acquisition_sample using (acquisition_sample_id)
                  join acquisition using (acquisition_id)
                  join acquisition_type using (acquisition_type_id)
                  join sample s using (sample_id)
                  join sample_type using (sample_type_id)
                  join project p on (p.project_id = t.project_id)
                  left outer join software using (software_id)
  ";
    /**
     *
     * @param PDO $bdd
     * @param array $param
     */
    public function __construct()
    {
        $this->table = "treatment_as";
        $this->fields = array(
            "treatment_as_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "treatment_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "acquisition_sample_id" => array(
                "type" => 1,
                "requis" => 1
            )
        );
        parent::__construct();
    }

    /**
     * Get the detail of a treatment_acquisition_sample
     *
     * @param integer $id
     * @return array|null
     */
    function getDetail(int $id): ?array
    {
        $where = " where treatment_as_id = :id:";
        return $this->lireParamAsPrepared($this->sql . $where, array("id" => $id));
    }
    /**
     * surround of supprimer, to test the presence of files
     *
     * @param int $id
     * @return ?
     */
    function supprimer($id)
    {
        if ($this->isDeletable($id)) {
            return parent::supprimer($id);
        } else {
            throw new PpciException(_("Des fichiers sont rattachés, la suppression n'est pas possible"));
        }
    }

    /**
     * Get the list of treatement_as from the treatment
     *
     * @param int $treatment_id
     * @return array|null
     */
    function getListFromTreatment(int $treatment_id): ?array
    {
        $this->fields["acquisition_date"] = array("type" => 2);
        return ($this->getListeParamAsPrepared(
            $this->sql . " where treatment_id =:treatment_id:",
            array("treatment_id" => $treatment_id)
        )
        );
    }
    /**
     * Get the number of files attached to an item
     *
     * @param integer $id
     * @return integer
     */
    function getNbFiles(int $treatment_as_id): int
    {
        $sql = "select count(*) as nb from treatment_as_file
    join treatment_as using (treatment_as_id)
    where treatment_as_id = :id:";
        $data = $this->lireParamAsPrepared($sql, array("id" => $treatment_as_id));
        return $data["nb"];
    }
    /**
     * Search if is deletable
     *
     * @param integer $acquisition_sample_id
     * @param integer $treatment_id
     * @return boolean
     */
    function isDeletable(int $treatment_as_id): bool
    {
        if ($this->getNbFiles($treatment_as_id) > 0) {
            return false;
        } else {
            return true;
        }
    }
}
