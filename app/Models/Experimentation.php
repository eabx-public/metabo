<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Experimentation extends PpciModel
{
    private $sql = "select experimentation_id, project_id, experimentation_name
                  ,experimentation_from, experimentation_to, experimentation_comment
                  ,project_name
                  ,experimentation_type_id, experimentation_type_name
                  from experimentation
                  join project using (project_id)
                  join experimentation_type using (experimentation_type_id)";
    public function __construct()
    {
        $this->table = "experimentation";
        $this->fields = array(
            "experimentation_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "project_id" => array(
                "type" => 1,
                "requis" => 1,
                "parentAttrib" => 1
            ),
            "experimentation_name" => array(
                "type" => 0,
                "requis" => 1
            ),
            "experimentation_from" => array(
                "type" => 2
            ),
            "experimentation_to" => array(
                "type" => 2
            ),
            "experimentation_type_id" => array(
                "type" => 1,
                "requis" => 1
            ),
            "experimentation_comment" => array("type" => 0)
        );
        parent::__construct();
    }

    /**
     * Get the detail of an experimentation
     *
     * @param integer $id
     * @return array
     */
    function getDetail(int $id): array
    {
        $data = array();
        $where = " where experimentation_id = :id:";
        $data = $this->lireParamAsPrepared($this->sql . $where, array("id" => $id));
        return $data;
    }
    /**
     * Get the number of files attached to an item
     *
     * @param integer $id
     * @return integer
     */
    function getNbFiles(int $id): int
    {
        $sql = "select count(*) as nb from experimentation_file where experimentation_id = :id:";
        $data = $this->lireParamAsPrepared($sql, array("id" => $id));
        return $data["nb"];
    }
    /**
     * Get the list of experimentations attached to a project
     *
     * @param integer $project_id
     * @return array|null
     */
    function getListFromProject(int $project_id): ?array
    {
        $where = " where project_id = :project_id:";
        return $this->getListeParamAsPrepared($this->sql . $where, array("project_id" => $project_id));
    }
}
