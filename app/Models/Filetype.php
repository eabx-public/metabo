<?php

namespace App\Models;

use Ppci\Models\PpciModel;

/**
 * ORM of the table filetype
 */
class Filetype extends PpciModel
{
    public function __construct()
    {
        $this->table = "filetype";
        $this->fields = array(
            "filetype_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "filetype_name" => array(
                "type" => 0,
                "requis" => 1
            ),
            "is_condition" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "is_sampling" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "is_experimentation" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "is_sample" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "is_acquisition_sample" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "is_acquisition" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "is_extraction" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "is_treatment" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "is_treatment_as" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "is_project" => array(
                "type" => 1,
                "defaultValue" => 0
            ),
            "sortorder" => array(
                "type" => 1,
                "defaultValue" => 1
            )
        );
        parent::__construct();
    }
    function getListFromType(string $type): array
    {
        $types = array("condition", "experimentation", "sampling", "sample", "acquisition_sample", "acquisition", "extraction", "treatment", "treatment_as", "project");
        if (!in_array($type, $types)) {
            return array();
        } else {
            $sql = "select * from filetype where is_" . $type . " = true order by sortorder, filetype_name";
            return $this->getListeParam($sql);
        }
    }
}
