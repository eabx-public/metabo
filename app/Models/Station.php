<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class Station extends PpciModel
{
    private $sql = "select station_id, station_name, wgs84_x, wgs84_y
            ,project_id, project_name
            from station
            left outer join project using (project_id)
            ";

    public function __construct()
    {
        $this->table = "station";
        $this->fields = array(
            "station_id" => array(
                "type" => 1,
                "key" => 1,
                "requis" => 1,
                "defaultValue" => 0
            ),
            "station_name" => array(
                "type" => 0,
                "requis" => 1
            ),
            "project_id" => array(
                "type" => 1
            ),
            "wgs84_x" => array(
                "type" => 1
            ),
            "wgs84_y" => array(
                "type" => 1
            ),
            "geom_point" => array("type" => 4)
        );
        $this->srid = 4326;
        parent::__construct();
    }

    /**
     * Add the generation of the geom point to the station
     *
     * @param array $data
     * @return int
     */
    function write($data): int
    {
        if (!empty($data["wgs84_x"]) && !empty($data["wgs84_y"])) {
            $data["geom_point"] = "POINT(" . $data["wgs84_x"] . " " . $data["wgs84_y"] . ")";
        } else {
            $data["geom_point"] = "";
        }
        return parent::write($data);
    }

    /**
     * Get station_id from name
     *
     * @param string $name
     * @return int
     */
    function getIdFromName($name)
    {
        $id = 0;
        if (strlen($name) > 0) {
            $sql = "select station_id from station where station_name = :name:";
            $data = $this->lireParamAsPrepared($sql, array(
                "name" => $name
            ));
            if ($data["station_id"] > 0) {
                $id = $data["station_id"];
            }
        }
        return $id;
    }

    /**
     * Get the list of stations attached at a project
     *
     * @param int $project_id
     * @param boolean $with_noaffected
     *            : if true, all non-affected places are associated
     *            at the project places
     * @return array
     */
    function getListFromProject($project_id = 0, $with_noaffected = true)
    {

        $where = "";
        $order = " order by station_name";
        if ($with_noaffected) {
            $where = " where project_id is null";
        }
        if ($with_noaffected && $project_id > 0) {
            $where .= " or ";
        }
        if ($project_id > 0) {
            if ($where == "") {
                $where = " where ";
            }
            $where .= " project_id = :project_id:";
            return $this->getListeParamAsPrepared($this->sql . $where . $order, array(
                "project_id" => $project_id
            ));
        } else {
            return $this->getListeParam($this->sql . $where . $order);
        }
    }

    /**
     * Get geographic coords from station
     * @param int $station_id
     * @return array
     */
    function getCoordinates($station_id)
    {
        if ($station_id > 0) {
            $sql = "select wgs84_x, wgs84_y from station
                    where station_id = :station_id:";
            return $this->lireParamAsPrepared($sql, array("station_id" => $station_id));
        }
    }
    /**
     * Get the detail of a station
     *
     * @param integer $station_id
     * @return array
     */
    function getDetail(int $station_id): array
    {
        $where = " where station_id = :id:";
        return $this->lireParamAsPrepared($this->sql . $where, array("id" => $station_id));
    }
}
