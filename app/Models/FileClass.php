<?php

namespace App\Models;

use Ppci\Libraries\PpciException;
use Ppci\Models\Dbparam;
use Ppci\Models\PpciModel;

/**
 * ORM of the table File
 */
class FileClass extends PpciModel
{
    private $sql = "select file_id, f.project_id, project_name, mimetype_id, extension, content_type
                  , filetype_id, filetype_name
                  , filename, storage_path, comment, filedate, filedateimport
                  , filesize, trunc(filesize::numeric / (1024*1024), 3) as filesize_mb
                  from file f
                  join project p on (f.project_id = p.project_id)
                  left outer join mimetype using (mimetype_id)
                  left outer join filetype using (filetype_id)
                  ";
    /**
     * Root dir of the storage. In it, news folders will be create
     *
     * @var string
     */
    public $rootdir = "metabo";
    private Dbparam $dbparam;
    private MimeType $mimetype;
    public $maxFilesByFolder = 1000;
    public $currentContent;
    private $currentId = 0;
    /**
     * List of objects linkable to a file
     *
     * @var array
     */
    private array $objects = array("acquisition_sample", "acquisition", "experimentation", "sample", "condition", "extraction", "sampling", "project");

    public function __construct()
    {
        $this->table = "file";
        $this->fields = array(
            "file_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "project_id" => array("type" => 1, "requis" => 1),
            "mimetype_id" => array("type" => 1),
            "filetype_id" => array("type" => 1),
            "filename" => array("requis" => 1),
            "filedateimport" => array("defaultValue" => "getDateHeure", "type" => 3),
            "storage_path" => array("type" => 0),
            "comment" => array("type" => 0),
            "filedate" => array("type" => 3, "defaultValue" => "getDateTime"),
            "filesize" => array("type" => 1),
            "storage_type" => array("type" => 0, "defaultValue" => "I")
        );
        parent::__construct();
        $this->rootdir = $_SESSION["dbparams"]["real_storage_path"];
    }
    /**
     * surround of lire to caching the data
     *
     * @param int|array $id
     * @param boolean $getDefault
     * @param integer $parentValue
     * @return array
     */
    function read($id, $getDefault = true, $parentValue = 0): array
    {
        if ($id != $this->currentId || !isset($this->currentContent)) {
            $this->currentContent = parent::read($id, $getDefault, $parentValue);
            if (empty($this->currentContent["external_path"])) {
                $this->currentContent["external_path"] = $_SESSION["dbparams"]["external_path_default"];
            }
            $this->currentId = $id;
        }
        return $this->currentContent;
    }

    /**
     * Get the list of files attached to a project
     *
     * @param integer $id
     * @param integer $filetype_id
     * @return array|null
     */
    function getListFromProject(int $id, int $filetype_id = 0): ?array
    {
        $where = " where p.project_id = :id:";
        $data = array("id" => $id);
        if ($filetype_id > 0) {
            $where .= " and filetype_id = :filetype_id:";
            $data["filetype_id"] = $filetype_id;
        }
        return $this->getListeParamAsPrepared($this->sql . $where, $data);
    }

    /**
     * Get the list of files attached to an object
     *
     * @param string $objectName: name of the parent table
     * @param integer $objectId: id of the parent table
     * @param integer $filetype_id: type of the file to retrieve
     * @return array|null
     */
    function getListFromObject(string $objectName, int $objectId, int $filetype_id = 0): ?array
    {
        $sql = $this->sql . " join " . $objectName . "_file obj using (file_id)";
        if ($objectName == "project") {
            $where = " where obj.project_id = :id:";
        } else {
            $where = " where " . $objectName . "_id = :id:";
        }

        $data = array("id" => $objectId);
        if ($filetype_id > 0) {
            $where .= " and filetype_id = :filetype_id:";
            $data["filetype_id"] = $filetype_id;
        }
        $files =  $this->getListeParamAsPrepared($sql . $where, $data);
        if ($objectName == "sample") {
            $sql = $this->sql . "
      join acquisition_sample_file using (file_id)
      join acquisition_sample using (acquisition_sample_id)
      ";

            $files2 = $this->getListeParamAsPrepared($sql . $where, $data);
            $files = array_merge($files, $files2);
            /**
             * Get the list of files attached to a treatment
             */
            $sql = $this->sql . "
            join treatment_as_file using (file_id)
            join treatment_as using (treatment_as_id)
            join acquisition_sample using (acquisition_sample_id)
      ";
            $files2 = $this->getListeParamAsPrepared($sql . $where, $data);
            $files = array_merge($files, $files2);
        }
        return $files;
    }
    /**
     * Generate a name not used into a folder, to create a file or a folder
     *
     * @param string $folder
     * @return string
     */
    function generateName(string $folder = ""): string
    {
        if (empty($folder)) {
            $folder = $this->rootdir;
        }
        $name = tempnam($folder, "");
        unlink($name);
        /**
         * Extract the rootdir from the name
         */
        $name = substr($name, strlen($folder) + 1);
        return $name;
    }
    /**
     * Create a directory into $rootdir
     *
     * @return string
     */
    function createDir($root): string
    {
        $dirname = $this->generateName($root);
        if (!mkdir($root . "/" . $dirname, 0750)) {
            throw new PpciException(_("Impossible de créer un dossier dans l'arborescence de stockage"));
        }
        /**
         * Update the current used folder
         */
        if (!isset($this->dbparam)) {
            $this->dbparam = service("Dbparam");
        }
        $this->dbparam->setParameter("last_folder_used", $dirname);
        return $dirname;
    }
    /**
     * Get the current usable folder.
     * If the number of files > $thisMaxFilesByFolder, create a new folder
     *
     * @return string
     */
    function getCurrentFolder(): string
    {
        if (!isset($this->dbparam)) {
            $this->dbparam = service("Dbparam");
        }
        $currentFolder = $this->dbparam->getParam("last_folder_used");
        /**
         * Calculate the number of files into the currentFolder
         */
        if (is_dir($this->rootdir . "/" . $currentFolder)) {
            $nbFiles = count(glob($this->rootdir . "/" . $currentFolder . "/" . "*"));
            if ($nbFiles > $this->maxFilesByFolder) {
                $currentFolder = $this->createDir($this->rootdir);
            }
        } else {
            $currentFolder = $this->createDir($this->rootdir);
        }
        return $currentFolder;
    }

    /**
     * Add a file into the storage and update the database
     *
     * @param string $tmpname
     * @param string $filename
     * @param integer $project_id
     * @param integer $filetype_id
     * @param string $objectName
     * @param integer $objectId
     * @param string $comment
     * @return integer
     */
    function addFile(string $tmpname, string $filename, int $project_id, int $filetype_id, string $objectName, int $objectId, string $filedate = "", string $comment = ""): int
    {
        if (!is_writable($this->rootdir)) {
            throw new PpciException(_("Le dossier de stockage des fichiers n'est pas accessible ou ne permet pas l'ajout de fichiers"));
        }
        $newid = 0;
        if (!in_array($objectName, $this->objects)) {
            throw new PpciException(sprintf(_("L'objet %s est inconnu, le fichier ne peut lui être rattaché"), $objectName));
        }
        if (empty($tmpname) || empty($filename) || !$project_id > 0 || !$filetype_id > 0 || !$objectId > 0) {
            throw new PpciException(_("Des informations sont manquantes pour traiter le fichier"));
        }
        $currentFolder = $this->getCurrentFolder();
        $newName = $this->generateName($this->rootdir . "/" . $currentFolder);
        $data = $this->getDefaultValues();
        $data["project_id"] = $project_id;
        $data["filename"] = $filename;
        $data["mimetype_id"] = $this->getmimetype($filename);
        $data["filetype_id"] = $filetype_id;
        $data["filedate"] = $filedate;
        $data["comment"] = $comment;
        $data["storage_path"] = $currentFolder . "/" . $newName;
        $data["filedateimport"] = $this->getDateHeure();
        /**
         * Copy the file to the new place
         */
        if (rename($tmpname, $this->rootdir . "/" . $currentFolder . "/" . $newName)) {
            /**
             * Get the size of the file
             */
            $data["filesize"] = filesize($this->rootdir . "/" . $currentFolder . "/" . $newName);
            $newid = $this->ecrire($data);
            if ($newid > 0) {
                $sql = "insert into " . $objectName . "_file (" . $objectName . "_id, file_id) values (:object_id:, :file_id:)";
                $this->executeSQL($sql, array("object_id" => $objectId, "file_id" => $newid), true);
                return $newid;
            } else {
                /**
                 * Suppression du déplacement
                 */
                rename($this->rootdir . "/" . $newName, $tmpname);
                throw new PpciException(_("Une erreur est survenue lors de l'enregistrement dans la table file"));
            }
        } else {
            throw new PpciException(sprintf(_("La recopie du fichier dans %s a échoué"), $this->rootdir . "/" . $newName));
        }
    }
    /**
     * Get the list of files presents into the download folder
     *
     * @return array
     */
    function getListDownloadedFiles(): array
    {
        $files = array();
        if (!isset($this->dbparam)) {
            $this->dbparam = service("Dbparam");
        }
        $dir = $this->dbparam->getParam("download_path");
        if ($handle = opendir($dir)) {
            $i = 1;
            while (false !== ($entry = readdir($handle))) {
                if (is_file($dir . "/" . $entry)) {
                    $files[] = array(
                        "id" => $i,
                        "filename" => $entry,
                        "radical" => substr($entry, 0, strrpos($entry, ".")),
                        "filesize" => round(filesize($dir . "/" . $entry) / (1024 * 1024), 3)
                    );
                    $i++;
                }
            }
            closedir($handle);
        }
        return $files;
    }
    /**
     * Move a file from the download folder to the storage
     * and update the database
     *
     * @param string $filename
     * @param string $filehash
     * @param integer $project_id
     * @param integer $filetype_id
     * @param string $objectName
     * @param integer $objectId
     * @param string $comment
     * @return integer
     */
    function moveDownloadedFile(string $filename, int $project_id, int $filetype_id, string $objectName, int $objectId, string $filedate = "", string $comment = ""): int
    {
        if (!isset($this->dbparam)) {
            $this->dbparam = service("Dbparam");
        }
        $dir = $this->dbparam->getParam("download_path");
        if (empty($filename) || !$project_id > 0 || !$filetype_id > 0 || !$objectId > 0) {
            throw new PpciException(_("Des informations sont manquantes pour traiter le fichier"));
        }
        $newid = $this->addFile($dir . "/" . $filename, $filename, $project_id, $filetype_id, $objectName, $objectId, $filedate, $comment);
        if ($newid > 0) {
            unlink($dir . "/" . $filename);
        }
        return $newid;
    }

    /**
     * Delete a downloaded file
     *
     * @param string $filename
     * @return boolean
     */
    function deleteDownloadedFile(string $filename): bool
    {
        if (!isset($this->dbparam)) {
            $this->dbparam = service("Dbparam");
        }
        $dir = $this->dbparam->getParam("download_path");
        $filename = filter_var($filename, FILTER_SANITIZE_STRING);
        if (is_file($dir . "/" . $filename)) {
            return unlink($dir . "/" . $filename);
        } else {
            return false;
        }
    }
    /**
     * Store a uploaded file into the database and the tree
     *
     * @param array $data
     * @param array $file
     * @param integer $project_id
     * @return int|null
     */
    function writeUpload(array $data, array $file)
    {
        return $this->addFile($file["tmp_name"], $file["name"], $data["project_id"], $data["filetype_id"], $data["modulename"], $data["id"], $data["filedate"], $data["comment"]);
    }

    /**
     * Get the typemime from the filename
     *
     * @param string $filename
     * @return integer|null
     */
    function getMimetype(string $filename): ?int
    {
        $mimetype_id = 0;
        $extension = substr($filename, strrpos($filename, ".") + 1);
        if (!isset($this->mimetype)) {
            $this->mimetype = new Mimetype;
        }
        $mimetype_id = $this->mimetype->getTypeMime($extension);
        return $mimetype_id;
    }

    /**
     * Delete a file on disk and into the database
     *
     * @param int $id
     * @return void
     */
    function supprimer($id)
    {
        /**
         * Verify the project
         */
        $data = $this->read($id);
        if (projectVerify($data["project_id"])) {
            /**
             * Delete the storage
             */
            if ($data["storage_type"] == "I") {
                if (!unlink($this->rootdir . "/" . $data["storage_path"])) {
                    throw new PpciException(_("La suppression du fichier dans l'arborescence a échoué"));
                }
            }
            /**
             * Delete all references
             */
            $idArray = array("id" => $id);
            foreach ($this->objects as $object) {
                $sql = "delete from " . $object . "_file where file_id = :id:";
                $this->executeSQL($sql, $idArray, true);
            }
            /**
             * delete links
             */
            $sql = "delete from filelink where file_id = :id:";
            $this->executeSQL($sql, $idArray, true);
            parent::supprimer($id);
        }
    }

    /**
     * Calculate the md5 hash
     *
     * @param integer $id
     * @return string
     */
    function getmd5(int $id): string
    {
        $data = $this->lire($id);
        if ($data["storage_type"] == "I") {
            $path = $this->rootdir . "/" . $data["storage_path"];
        } else {
            $path = $_SESSION["projects"][$data["project_id"]]["external_path"] . "/" . $data["storage_path"];
        }
        if (file_exists($path)) {
            return md5_file($path);
        } else {
            throw new PpciException(sprintf(_("Le fichier %s n'a pas été trouvé dans l'arborescence"), $id));
        }
    }
}
