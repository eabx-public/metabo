<?php

namespace App\Models;

use Ppci\Libraries\PpciException;

/**
 * Read a csv file
 */
class CSVread
{
    /**
     * Read a csv file and returns it into an array
     *
     * @param string $filename
     * @param string $separator
     * @return array
     */
    function  csvAsArray(string $filename, string $separator): array
    {
        if ($separator == "tab" || $separator == "t") {
            $separator = "\t";
        }
        $handle = fopen($filename, 'r');
        if (!$handle) {
            throw new PpciException(_("Ouverture du fichier impossible"));
        }
        $header = fgetcsv($handle, 0, $separator);
        if (empty($header)) {
            throw new PpciException(_("Le fichier est vide"));
        }
        $data = array();
        while (($line = fgetcsv($handle, 0, $separator)) !== false) {
            $data[] = array_combine($header, $line);
        }
        fclose($handle);
        return $data;
    }
}
