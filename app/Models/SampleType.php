<?php

namespace App\Models;

use Ppci\Models\PpciModel;

class SampleType extends PpciModel
{
    /**
     * Constructor
     *
     * @param pdo $bdd
     * @param string $tablename
     */
    public function __construct()
    {
        $this->table = "sample_type";
        $this->fields = array(
            "sample_type_id" => array("type" => 1, "requis" => 1, "key" => 1, "defaultValue" => 0),
            "sample_type_name" => array("type" => 0, "requis" => 1),
            "sortorder" => array("type" => 1),
            "collec_type_name" => array("type" => 0)
        );
        parent::__construct();
    }
    /**
     * Get the id of a record from the name
     *
     * @param string $name
     * @param boolean $withCreate: if true and the record not exists, the parameter is created
     * @return int
     */
    function getIdFromName($name, $withCreate = false)
    {
        if (strlen($name) > 0) {
            $sql = "select sample_type_id  as id
                from sample_type
                where sample_type_name = :name:";
            $data = $this->lireParamAsPrepared($sql, array("name" => $name));
            if ($withCreate && !$data["id"]) {
                $data["id"] = $this->ecrire(array("sample_type_name" => $name));
            }
            return $data["id"];
        }
    }
}
