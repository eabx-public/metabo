<?php

namespace App\Libraries;

use App\Models\Acquisition;
use App\Models\Condition;
use App\Models\Experimentation;
use App\Models\Extraction;
use App\Models\FileClass;
use App\Models\Filetype;
use App\Models\Param;
use App\Models\Project as ModelsProject;
use App\Models\Sample;
use App\Models\Sampling;
use App\Models\Treatment;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;

class Project extends PpciLibrary
{

    public $keyName;
    public int $id = 0;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsProject();
        $this->keyName = "project_id";
        if (!empty($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
    }

    function list()
    {
        $this->vue = service('Smarty');
        /***
         * Display the list of all records of the table
         */
        try {
            $this->vue->set($this->dataclass->getListe(), "data");
            $this->vue->set("project/projectList.tpl", "corps");
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
        }
        return $this->vue->send();
    }
    function display()
    {
        $this->vue = service('Smarty');
        if (empty($this->id) && !empty($_COOKIE["project_id"])) {
            $this->id = $_COOKIE["project_id"];
        }
        if ($this->id > 0) {
            if (! projectVerify($this->id)) {
                return projectRefused();
            } else {
                $_SESSION["current_project"] = $this->id;
                !isset($_REQUEST["sample_type_id"]) ? $sample_type_id = $_COOKIE["sample_type_id"] : $sample_type_id = $_REQUEST["sample_type_id"];
                if (is_null($sample_type_id)) {
                    $sample_type_id = 0;
                }
                !isset($_REQUEST["filetype_id"]) ? $filetype_id = $_COOKIE["filetype_id"] : $filetype_id = $_REQUEST["filetype_id"];
                if (is_null($filetype_id)) {
                    $filetype_id = 0;
                }
                $this->dataRead($this->id, "project/projectDisplay.tpl");
                $experimentation = new Experimentation();
                $this->vue->set($experimentation->getListFromProject($this->id), "experimentations");
                $file = new FileClass;
                $this->vue->set($file->getListFromProject($this->id, $filetype_id), "files");
                $filetype = new Filetype();
                $this->vue->set($filetype->getListe("sortorder, filetype_name"), "fileTypes");
                $this->vue->set($filetype_id, "filetype_id");
                $this->vue->set(1, "fileListIsForm");
                $acquisition = new Acquisition();
                $this->vue->set($acquisition->getListFromProject($this->id), "acquisitions");
                /**
                 * Samples
                 */
                $param = new Param("sample_type");
                $this->vue->set($param->getListe("3,2"), "sampleTypes");
                $this->vue->set($sample_type_id, "sample_type_id");
                if ($sample_type_id >= 0) {
                    $sample = new Sample();
                    $this->vue->set($sample->getListFromProject($this->id, $sample_type_id), "samples");
                }
                $condition = new Condition();
                $sampling = new Sampling();
                $this->vue->set($sampling->getListFromProject($this->id), "samplings");
                $this->vue->set($condition->getListFromProject($this->id), "conditions");
                $extraction = new Extraction();
                $this->vue->set($extraction->getListFromProject($this->id), "extractions");
                /**
                 * Creation of acquisition
                 */
                $this->vue->set(1, "sampleListIsForm");
                $param = new Param("machine");
                $this->vue->set($param->getListe("3,2"), "machines");
                $param = new Param("software");
                $this->vue->set($param->getListe("3,2"), "softwares");
                $this->vue->set(date($_SESSION["MASKDATE"]), "acquisition_date");
                $param = new Param("acquisition_method");
                $this->vue->set($param->getListe("3,2"), "acquisition_methods");
                $param = new Param("acquisition_type");
                $this->vue->set($param->getListe("3,2"), "acquisitionTypes");
                $param = new Param("chromatography_method");
                $this->vue->set($param->getListe("3,2"), "chromatographys");
                /**
                 * Creation of extracts from aliquots
                 */
                $this->vue->set(date($_SESSION["MASKDATE"]), "extraction_date");
                $param = new Param("extraction_method");
                $this->vue->set($param->getListe("3,2"), "extraction_methods");
                $param = new Param("sample_nature");
                $this->vue->set($param->getListe("3,2"), "sample_natures");
                $param = new Param("transport_method");
                $this->vue->set($param->getListe("3,2"), "transport_methods");
                /**
                 * Creation of samplings from conditions
                 */
                $param = new Param("sampling_method");
                $this->vue->set($param->getListe("3,2"), "sampling_methods");
                $this->vue->set($sampling->getDefaultValues(0), "sampling");
                $storageLocation = new Param("storage_location");
                $this->vue->set($storageLocation->getListe("3,2"), "storage_locations");
                $storageType = new Param("storage_type");
                $this->vue->set($storageType->getListe("3,2"), "storage_types");
                /**
                 * Treatments
                 */
                $treatment = new Treatment();
                $this->vue->set($treatment->getListFromProject($this->id), "treatments");
                /**
                 * Specific files of project
                 */
                fileInitUpload("project", $this->id, 2);
            }
        } else {
            $this->vue->set("project/projectDisplay.tpl", "corps");
        }
        $this->vue->set($this->id, "project_id");
        $this->vue->set($_SESSION["projects"], "projects");
        return $this->vue->send();
    }
    function change()
    {
        $this->vue = service('Smarty');
        /***
         * open the form to modify the record
         * If is a new record, generate a new record with default value :
         * $_REQUEST["idParent"] contains the identifiant of the parent record
         */
        $this->dataRead($this->id, "project/projectChange.tpl");
        /**
         * Recuperation des groupes
         */
        $this->vue->set($this->dataclass->getAllGroupsFromproject($this->id), "groupes");
        return $this->vue->send();
    }
    function write()
    {
        try {
            $this->id = $this->dataWrite($_REQUEST);
            $_REQUEST[$this->keyName] = $this->id;
            /**
             * Rechargement eventuel des projects autorises pour l'utilisateur courant
             */
            $this->dataclass->initprojects();
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
        }
    }
    function delete()
    {
        /**
         * delete record
         */
        if ($nb = $this->dataclass->getNbFiles($this->id) == 0) {
            try {
                $this->dataDelete($this->id);
                return true;
            } catch (PpciException $e) {
                return false;
            }
        } else {
            $this->message->set(sprintf(_("La suppression est impossible, %s fichiers sont rattachés"), $nb), true);
            return false;
        }
    }
}
