<?php

namespace App\Libraries;

use App\Models\Condition;
use App\Models\CSVread;
use App\Models\Experimentation;
use App\Models\Param;
use App\Models\Station;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;

class ConditionImport extends PpciLibrary
{

    public $keyName;
    public Experimentation $experimentation;
    public $projectAllowed = false;
    private array $dataExp = [];
    private int $project_id = 0;
    private array $messages;
    public int $conditionsWrote = 0;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new Condition;
        $this->experimentation = new Experimentation;
        $this->keyName = "experimentation_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
            if ($this->id > 0) {
                $this->dataExp = $this->experimentation->getDetail($this->id);
                $this->project_id = $this->dataExp["project_id"];
            } elseif (!empty($_SESSION["current_project"])) {
             $this->project_id = $_SESSION["current_project"];
            }
        }
        if (projectVerify($this->project_id)) {
            $_SESSION["current_project"] = $this->project_id;
            $this->projectAllowed = true;
        }
    }

    function import()
    {
        if ($this->projectAllowed) {
            $this->vue = service("Smarty");
            $this->vue->set($this->id, "experimentation_id");
            $this->vue->set($this->dataExp, "experimentation");
            $this->vue->set("sample/conditionImport.tpl", "corps");
            if (isset($this->messages)) {
                /**
                 * Result of the import
                 */
                $this->vue->set($this->messages, "messages");
            }
            return $this->vue->send();
        } else {
            defaultPage();
        }
    }
    function importExec()
    {
        if ($this->projectAllowed) {
            /**
             * Import conditions from a CSV file
             */
            $matrice = new Param("matrice_type");
            $station = new Station;
            $csv = new CSVread;
            try {
                $db = $this->dataclass->db;
                $db->transBegin();
                if ($_FILES["filename"]["error"] != UPLOAD_ERR_OK) {
                    throw new PpciException(_("Un problème est survenu pendant le téléchargement du fichier"));
                }
                $model = array(
                    "condition_id" => 0,
                    "experimentation_id" => $_POST["experimentation_id"],
                );
                $lines = $csv->csvAsArray($_FILES["filename"]["tmp_name"], $_POST["separator"]);
                $lineNumber = 0;
                $this->messages = [];
                $error = false;
                foreach ($lines as $line) {
                    $lineNumber++;
                    if (empty($line)) {
                        $this->messages[] = array(
                            "line" => $lineNumber,
                            "message" => _("Ligne vide")
                        );
                        continue;
                    }
                    $data = $model;
                    if (empty($line["condition_code"])) {
                        $error = true;
                        $this->messages[] = array(
                            "line" => $lineNumber,
                            "message" => _("ERREUR : le code de la condition n'a pas été renseigné")
                        );
                        continue;
                    } else {
                        /**
                         * Search for pre-existent condition
                         */
                        $condition_id = $this->dataclass->getIdFromCode(trim($line["condition_code"]), $this->id);
                        if ($condition_id > 0) {
                            $this->messages[] = array(
                                "line" => $lineNumber,
                                "message" => sprintf(_("La condition %s existe, elle est mise à jour"), $line["condition_code"])
                            );
                            $data["condition_id"] = $condition_id;
                        }
                    }
                    /**
                     * Recherche de la matrice
                     */
                    if (!empty($line["matrice_type_id"])) {
                        $dmatrice = $matrice->lire($line["matrice_type_id"]);
                        if (!$dmatrice["matrice_type_id"] > 0) {
                            $error = true;
                            $this->messages[] = array(
                                "line" => $lineNumber,
                                "message" => sprintf(_("La matrice n° %s n'existe pas"), $line["matrice_type_id"])
                            );
                        } else {
                            $data["matrice_type_id"] = $dmatrice["matrice_type_id"];
                        }
                    }
                    if (!empty($line["matrice_type_name"])) {
                        $matrice_type_id = $matrice->getIdFromName($line["matrice_type_name"]);
                        if (empty($matrice_type_id)) {
                            $error = true;
                            $this->messages[] = array(
                                "line" => $lineNumber,
                                "message" => sprintf(_("La matrice %s n'existe pas"), $line["matrice_type_name"])
                            );
                        } else {
                            $data["matrice_type_id"] = $matrice_type_id;
                        }
                    }
                    /**
                     * Recherche de la station
                     */
                    if (!empty($line["station_id"])) {
                        $dstation = $station->lire($line["station_id"]);
                        if (!$dstation["station_id"] > 0) {
                            $error = true;
                            $this->messages[] = array(
                                "line" => $lineNumber,
                                "message" => sprintf(_("La station n° %s n'existe pas"), $line["station_id"])
                            );
                        } else {
                            $data["station_id"] = $dstation["station_id"];
                        }
                    }

                    if (!empty($line["station_name"])) {
                        $station_id = $station->getIdFromName($line["station_name"]);
                        if (empty($station_id)) {
                            $error = true;
                            $this->messages[] = array(
                                "line" => $lineNumber,
                                "message" => sprintf(_("La station %s n'existe pas"), $line["station_name"])
                            );
                        } else {
                            $data["station_id"] = $station_id;
                        }
                    }
                    if (!$error) {
                        /**
                         * Write condition
                         */
                        $data["condition_code"] = trim($line["condition_code"]);
                        if (!empty($line["condition_description"])) {
                            $data["condition_description"] = $line["condition_description"];
                        }
                        $this->dataclass->ecrire($data);
                        $this->conditionsWrote++;
                    }
                }
                if (!$error) {
                    $this->messages[] = array(
                        "message" => sprintf(_("%s conditions importées ou mises à jour"), $this->conditionsWrote)
                    );
                } else {
                    $this->messages[] = array(
                        "message" => _("L'importation a échoué, aucun échantillon n'a été créé")
                    );
                    throw new PpciException(_("Echec de l'importation"));
                }
                $db->transCommit();
                return true;
            } catch (PpciException $e) {
                $this->message->set($e->getMessage(), true);
                if ($db->transEnabled) {
                    $db->transRollback();
                }
                return false;
            }
        } else {
            return false;
        }
    }
}
