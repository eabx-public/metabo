<?php

namespace App\Libraries;

use App\Models\Condition;
use App\Models\Experimentation as ModelsExperimentation;
use App\Models\Param;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;

class Experimentation extends PpciLibrary
{

    public $keyName;
    private int $project_id = 0;
    private array $data = [];
    public $projectAllowed = false;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsExperimentation();
        $this->keyName = "experimentation_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
            if ($this->id > 0) {
                $this->data = $this->dataclass->getDetail($this->id);
                $this->project_id = $this->data["project_id"];
            } elseif ($_REQUEST["project_id"] > 0) {
                $this->project_id = $_REQUEST["project_id"];
            }
        }
        if (projectVerify($this->project_id)) {
            $_SESSION["current_project"] = $this->project_id;
            $this->projectAllowed = true;
        }
    }
    function display()
    {
        if ($this->id > 0) {
            if (!$this->projectAllowed) {
                return projectRefused();
            }
            $this->vue = service('Smarty');
            $this->vue->set($this->data, "experimentation");
            $this->vue->set("project/experimentationDisplay.tpl", "corps");
            fileInitUpload("experimentation", $this->id);
            $condition = new Condition();
            $this->vue->set($condition->getListFromExperimentation($this->id), "conditions");
            $this->vue->set($this->project_id, "project_id");
            return $this->vue->send();
        } else {
            return defaultPage();
        }
    }
    function change()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service('Smarty');
        $this->dataRead($this->id, "project/experimentationChange.tpl", $_REQUEST["project_id"]);
        $param = new Param("experimentation_type");
        $this->vue->set($param->getListe(2), "types");
        return $this->vue->send();
    }
    function write()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->id = $this->dataWrite($_REQUEST);

            $_REQUEST[$this->keyName] = $this->id;
            return true;
        } catch (PpciException) {
            return false;
        }
    }
    function delete()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        /**
         * delete record
         */
        if ($nb = $this->dataclass->getNbFiles($this->id) == 0) {
            try {
                $this->dataDelete($this->id);
                return true;
            } catch (PpciException) {
                return false;
            }
        } else {
            $this->message->set(sprintf(_("La suppression est impossible, %s fichiers sont rattachés"), $nb), true);
            return false;
        }
    }
}
