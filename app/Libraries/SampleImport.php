<?php

namespace App\Libraries;

use App\Models\Condition;
use App\Models\CSVread;
use App\Models\Extraction;
use App\Models\Param;
use App\Models\Sample;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;

class SampleImport extends PpciLibrary
{

    public $keyName;
    public $projectAllowed = false;
    private int $project_id = 0;
    private array $messages;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new Sample;
        $this->keyName = "sample_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
        if ($_REQUEST["project_id"] > 0) {
            $this->project_id = $_REQUEST["project_id"];
        } elseif (!empty($_COOKIE["project_id"])) {
            $this->project_id = $_COOKIE["project_id"];
        } elseif (!empty($_SESSION["projects"][0]["project_id"])) {
            $this->project_id = $_SESSION["projects"][0]["project_id"];
        }
        if (projectVerify($this->project_id)) {
            $_SESSION["current_project"] = $this->project_id;
            $this->projectAllowed = true;
        }
    }



    function import()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service("Smarty");
        $this->vue->set($_SESSION["projects"], "projects");
        isset($_REQUEST["sample_type_id"]) ? $sample_type_id = $_REQUEST["sample_type_id"] : $sample_type_id = 0;
        isset($_REQUEST["extraction_id"]) ? $extraction_id = $_REQUEST["extraction_id"] : $extraction_id = 0;
        isset($_REQUEST["condition_id"]) ? $condition_id = $_REQUEST["condition_id"] : $condition_id = 0;
        $this->vue->set($this->project_id, "project_id");
        $this->vue->set($sample_type_id, "sample_type_id");
        $this->vue->set($extraction_id, "extraction_id");
        $this->vue->set($condition_id, "condition_id");

        $param = new Param("sample_type");
        $this->vue->set($param->getListe("3,2"), "sampleTypes");
        $transport = new Param("transport_method");
        $this->vue->set($transport->getListe("3,2"), "transport_methods");
        $condition = new Condition;
        $this->vue->set($condition->getListFromProject($this->project_id), "conditions");
        $extraction = new Extraction;
        $this->vue->set($extraction->getListFromProject($this->project_id), "extractions");
        $this->vue->set("sample/sampleImport.tpl", "corps");
        if (isset($this->messages)) {
            /**
             * Result of the import
             */
            $this->vue->set($this->messages, "messages");
        }
        return $this->vue->send();
    }
    function importExec()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        /**
         * Import samples from a CSV file
         */
        $sample = new Sample;
        $csv = new CSVread;
        try {
            $db = $this->dataclass->db;
            $db->transBegin();
            if (!$_POST["sample_type_id"] > 0 || ($_POST["extraction_id"] > 0 && $_POST["condition_id"] > 0)) {
                throw new PpciException(_("Les paramètres fournis semblent incohérents"));
            }
            if ($_FILES["filename"]["error"] != UPLOAD_ERR_OK) {
                throw new PpciException(_("Un problème est survenu pendant le téléchargement du fichier"));
            }
            $model = array(
                "sample_id" => 0,
                "sample_type_id" => $_POST["sample_type_id"],
                "transport_method_id" => $_POST["transport_method_id"],
                "project_id" => $this->project_id
            );
            if ($_POST["extraction_id"] > 0) {
                $model["extraction_id"] = $_POST["extraction_id"];
            }
            if ($_POST["condition_id"] > 0) {
                $model["condition_id"] = $_POST["condition_id"];
            }
            $lines = $csv->csvAsArray($_FILES["filename"]["tmp_name"], $_POST["separator"]);
            $lineNumber = 0;
            $samplesWrote = 0;
            $error = false;
            foreach ($lines as $line) {
                $lineNumber++;
                if (empty($line)) {
                    $this->messages[] = array(
                        "line" => $lineNumber,
                        "message" => _("Ligne vide")
                    );
                    continue;
                }
                $data = $model;
                if (strlen($line["uuid"]) == 36) {
                    /**
                     * Verify if the uuid exists
                     */
                    $sample_id = $sample->getIdFromUUID($line["uuid"]);
                    if ($sample_id > 0) {
                        $this->messages[] = array(
                            "line" => $lineNumber,
                            "message" => sprintf(_("Un échantillon existe pour l'UUID %s"), $line["uuid"])
                        );
                    } else {
                        $data["uuid"] = $line["uuid"];
                    }
                }
                if (empty($line["sample_name"])) {
                    $error = true;
                    $this->messages[] = array(
                        "line" => $lineNumber,
                        "message" => _("ERREUR : le nom de l'échantillon n'a pas été renseigné")
                    );
                    continue;
                } else {
                    /**
                     * Search for pre-existent sample
                     */
                    $sample_id = $sample->getIdFromName(trim($line["sample_name"]), $this->project_id);
                    if ($sample_id > 0) {
                        $this->messages[] = array(
                            "line" => $lineNumber,
                            "message" => sprintf(_("L'échantillon %s existe"), $line["sample_name"])
                        );
                        continue;
                    }
                }
                /**
                 * Search for the parent
                 */
                if (!empty($line["sample_parent"])) {
                    $parent_id = $sample->getIdFromName(trim($line["sample_parent"]), $this->project_id);
                    if (!$parent_id > 0) {
                        $this->messages[] = array(
                            "line" => $lineNumber,
                            "message" => sprintf(_("ERREUR : l'échantillon parent %s n'existe pas"), $line["sample_parent"])
                        );
                        $error = true;
                        continue;
                    } else {
                        $data["sample_parent_id"] = $parent_id;
                    }
                }
                if (!$error) {
                    /**
                     * Write sample
                     */
                    $data["sample_name"] = trim($line["sample_name"]);
                    if (!empty($line["volume"])) {
                        $data["volume"] = $line["volume"];
                    }
                    if (!empty($line["weight"])) {
                        $data["weight"] = $line["weight"];
                    }
                    if (!empty($line["unit_volume"])) {
                        $data["unit_volume"] = $line["unit_volume"];
                    }
                    if (!empty($line["unit_weight"])) {
                        $data["unit_weight"] = $line["unit_weight"];
                    }
                    $sample->ecrire($data);
                    $samplesWrote++;
                }
            }
            if (!$error) {
                $this->message->set(sprintf(_("%s lignes parcourues dans le fichier"), $lineNumber));
                $this->message->set(sprintf(_("%s nouveaux échantillons importés"), $samplesWrote));
            } else {
                $this->message->set(_("L'importation a échoué, aucun échantillon n'a été créé"));
                throw new PpciException(_("Echec de l'importation"));
            }
            $db->transCommit();
            return true;
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
            if ($db->transEnabled) {
                $db->transRollback();
            }
            return false;
        }
    }
}
