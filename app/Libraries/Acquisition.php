<?php

namespace App\Libraries;

use App\Models\Acquisition as ModelsAcquisition;
use App\Models\AcquisitionSample;
use App\Models\FileClass;
use App\Models\Filetype;
use App\Models\Param;
use App\Models\Sample;
use App\Models\Treatment;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;

class Acquisition extends PpciLibrary
{

    public $keyName;
    private $data;
    private $project_id = 0;
    public $projectAllowed = false;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsAcquisition();
        $this->keyName = "acquisition_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
        if ($this->id > 0) {
            $this->data = $this->dataclass->getDetail($this->id);
            $this->project_id = $this->data["project_id"];
        } elseif (!empty($_SESSION["current_project"])) {
            $this->project_id = $_SESSION["current_project"];
        }
        if (projectVerify($this->project_id) == 1) {
            $_SESSION["current_project"] = $this->project_id;
            $this->projectAllowed = true;
        }
    }

    function display()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        if ($this->id > 0) {
            $this->vue = service('Smarty');
            $this->vue->set($this->data, "data");
            $this->vue->set("acquisition/acquisitionDisplay.tpl", "corps");
            fileInitUpload("acquisition", $this->id);
            $sample = new Sample();
            $this->vue->set($sample->getListFromAcquisition($this->id), "samples");
            /**
             * Data for treatments
             */
            $treatment = new Treatment();
            $this->vue->set($treatment->getListFromProject($this->project_id, " order by treatment_date desc"), "treatments");
            $software = new Param("software");
            $this->vue->set($software->getListe("3,2"), "softwares");
            $this->vue->set(date($_SESSION["date"]["maskdate"]), "dateDefault");
            /**
             * Add the files from acquisition_sample_id
             */
            if ($_REQUEST["acquisition_sample_id"] > 0) {
                $acquisitionSample = new AcquisitionSample();
                fileInitUpload("acquisition_sample", $_REQUEST["acquisition_sample_id"], 2);
                $this->vue->set($_REQUEST["acquisition_sample_id"], "acquisition_sample_id");
                $this->vue->set($acquisitionSample->getDetail($_REQUEST["acquisition_sample_id"]), "acquisitionSample");
            }
            $this->vue->set($this->project_id, "project_id");
            return $this->vue->send();
        } else {
            defaultPage();
        }
    }
    function change()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service('Smarty');
        $this->dataRead($this->id, "acquisition/acquisitionChange.tpl", $_REQUEST["project_id"]);
        $param = new Param("machine");
        $this->vue->set($param->getListe("3,2"), "machines");
        $this->vue->set($this->project_id, "project_id");
        $param = new Param("acquisition_type");
        $this->vue->set($param->getListe("3,2"), "acquisitionTypes");
        $param = new Param("chromatography_method");
        $this->vue->set($param->getListe("3,2"), "chromatographys");
        $param = new Param("acquisition_method");
        $this->vue->set($param->getListe("3,2"), "acquisition_methods");
        return $this->vue->send();
    }
    function write()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->id = $this->dataWrite($_REQUEST);
            $_REQUEST[$this->keyName] = $this->id;
            return true;
        } catch (PpciException) {
            return false;
        }
    }
    function delete()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->dataDelete($this->id);
            return true;
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
            return false;
        }
    }
    function createWithSamples()
    {
        return $this->attachSamples();
    }
    function attachSamples()
    {
        /**
         * Create or no an extraction, create samples of type "extract" from samples,
         * and attach them to the extraction
         */
        if (!$this->projectAllowed) {
            $this->message->set(_("Vous ne disposez pas des droits suffisants pour réaliser cette opération"), true);
            return false;
        }
        try {
            $db = $this->dataclass->db;
            $db->transBegin();
            $_REQUEST["acquisition_id"] = $this->dataclass->createWithSamples($_POST);
            $db->transCommit();
            return true;
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
            if ($db->transEnabled) {
                $db->transRollback();
                return false;
            }
        }
    }
    function addDownloadedFile()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service("Smarty");
        $this->vue->set($this->dataclass->getDetail($this->id), "acquisition");
        $file = new FileClass();
        $files = $file->getListDownloadedFiles();
        $this->vue->set($files, "files");
        $this->vue->set("acquisition/acquisitionSampleAddFiles.tpl", "corps");
        $filetype = new Filetype();
        $this->vue->set($filetype->getListFromType("acquisition_sample"), "filetypes");
        $sample = new Sample();
        $samples = $sample->getListFromAcquisition($this->id);
        /**
         * Pre-positioning of files
         */
        foreach ($samples as $ks => $s) {
            foreach ($files as $f) {
                if ($s["sample_name"] == $f["radical"]) {
                    $samples[$ks]["filename"] = $f["filename"];
                }
            }
        }
        $this->vue->set($samples, "samples");
        /**
         * Get default datetime
         */
        $this->vue->set(date($_SESSION["date"]["maskdatelong"]), "defaultDateTime");
        return $this->vue->send();
    }
    function addDownloadedFileExec()
    {
        $file = new FileClass();
        $asample = new AcquisitionSample();
        try {
            $db = $this->dataclass->db;
            $db->transBegin();
            /**
             * Search for files to associate
             */
            foreach ($_POST as $varname => $content) {
                $radical = substr($varname, 0, 6);
                if ($radical == "sample" && !empty($content)) {
                    $asample_id = substr($varname, 6);
                    $dsample = $asample->getDetail($asample_id);
                    if (projectVerify($dsample["project_id"]) != 1) {
                        throw new PpciException(sprintf(_("Vous ne disposez pas des droits suffisants pour l'échantillon %s"), $dsample["sample_name"]));
                    }
                    /**
                     * Move the file to it new place
                     */
                    $sample_type_id = $_POST["type" . $asample_id];
                    $sample_date = $_POST["date" . $asample_id];
                    $file->moveDownloadedFile($content, $dsample["project_id"], $sample_type_id, "acquisition_sample", $asample_id, $sample_date);
                }
            }
            $db->transCommit();
            $this->message->set(_("Appariement et déplacement effectués"));
            return true;
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
            if ($db->transEnabled) {
                $db->transRollback();
                return false;
            }
        }
    }
    function sampleDelete()
    {
        /**
         * Delete the acquisition_sample_id attached to the acquisition
         */
        if (!$this->projectAllowed) {
            return false;
        }
        $acquisitionSample = new AcquisitionSample();
        try {
            $acquisitionSample->supprimer($_REQUEST["acquisition_sample_id"]);
            $this->message->set(_("Suppression effectuée"));
            return true;
        } catch (PpciException $e) {
            $this->message->set(_("Une erreur est survenue pendant la suppression"), true);
            $this->message->set($e->getMessage());
            return false;
        }
    }
}
