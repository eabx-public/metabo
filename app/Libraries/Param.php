<?php

namespace App\Libraries;

use App\Models\Param as ModelsParam;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class Param extends PpciLibrary
{

	public $keyName;
	public $tablename;

	function __construct(string $tablename)
	{
		parent::__construct();
		$this->tablename = $tablename;
		$this->dataclass = new ModelsParam($tablename);
		$this->keyName = $tablename . "_id";
		if (isset($_REQUEST[$this->keyName])) {
			$this->id = $_REQUEST[$this->keyName];
		}
	}
	function generateSet()
	{
		$this->vue->set($this->tablename . "_id", "fieldid");
		$this->vue->set($this->tablename . "_name", "fieldname");
		$this->vue->set($this->tablename . "_code", "fieldcode");
		$this->vue->set($this->dataclass->getDescription(), "tabledescription");
		$this->vue->set($this->tablename, "tablename");
	}

	function list()
	{
		$this->vue = service('Smarty');
		$this->vue->set($this->dataclass->getListe(1), "data");
		$this->vue->set("param/paramList.tpl", "corps");
		$this->generateSet();
		return $this->vue->send();
	}
	function change()
	{
		$this->vue = service('Smarty');
		/*
		 * open the form to modify the record
		 * If is a new record, generate a new record with default value :
		 * $_REQUEST["idParent"] contains the identifiant of the parent record
		 */
		$this->dataRead($this->id, "param/paramChange.tpl");
		$this->generateSet();
		return $this->vue->send();
	}
	function write()
	{
		/*
		 * write record in database
		 */
		try {
			$this->id = $this->dataWrite($_REQUEST);
			$_REQUEST[$this->keyName] = $this->id;
			return true;
		} catch (PpciException) {
			return false;
		}
	}
	function delete()
	{
		/*
		 * delete record
		 */
		try {
			$this->dataDelete($this->id);
			return true;
		} catch (PpciException) {
			return false;
		}
	}
}
