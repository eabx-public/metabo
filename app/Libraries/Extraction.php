<?php

namespace App\Libraries;

use App\Models\Acquisition;
use App\Models\Extraction as ExtractionModels;
use App\Models\Param;
use App\Models\Sample;
use App\Models\Sampling;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;

class Extraction extends PpciLibrary
{

    public $keyName;
    public int $id = 0;
    private array $data = [];
    public $projectAllowed = false;
    private int $project_id = 0;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ExtractionModels();
        $this->keyName = "extraction_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
            if ($this->id > 0) {
                $this->data = $this->dataclass->getDetail($this->id);
                $this->project_id = $this->data["project_id"];
            } elseif (!empty($_SESSION["current_project"])) {
                $this->project_id = $_SESSION["current_project"];
            }
        }
        if (projectVerify($this->project_id)) {
            $_SESSION["current_project"] = $this->project_id;
            $this->projectAllowed = true;
        }
    }

    function display()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        if ($this->id > 0) {
            $this->vue = service('Smarty');
            $this->vue->set($this->data, "data");
            $this->vue->set("sample/extractionDisplay.tpl", "corps");
            fileInitUpload("extraction", $this->id);
            if ($this->data["sampling_id"] > 0) {
                $sampling = new Sampling();
                $this->vue->set($sampling->getDetail($this->data["sampling_id"]), "sampling");
            }
            $sample = new Sample();
            $this->vue->set($sample->getListFromExtraction($this->id), "samples");
            $this->vue->set($this->project_id, "project_id");
            if ($_SESSION["userRights"]["manage"] == 1) {
                $this->vue->set(1, "sampleListIsForm");
                $acquisition = new Acquisition;
                $this->vue->set($acquisition->getListFromProject($this->project_id), "acquisitions");
                $param = new Param("machine");
                $this->vue->set($param->getListe("3,2"), "machines");
                $this->vue->set($this->project_id, "project_id");
                $param = new Param("acquisition_type");
                $this->vue->set($param->getListe("3,2"), "acquisitionTypes");
                $param = new Param("chromatography_method");
                $this->vue->set($param->getListe("3,2"), "chromatographys");
                $param = new Param("acquisition_method");
                $this->vue->set($param->getListe("3,2"), "acquisition_methods");
            }
            return $this->vue->send();
        } else {
            return defaultPage();
        }
    }
    function change()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service('Smarty');
        $data = $this->dataRead($this->id, "sample/extractionChange.tpl", $_REQUEST["project_id"]);
        $param = new Param("extraction_method");
        $this->vue->set($param->getListe("3,2"), "methods");
        $this->vue->set($this->project_id, "project_id");
        if ($data["sampling_id"] > 0) {
            $sampling = new Sampling();
            $this->vue->set($sampling->getDetail($data["sampling_id"]), "sampling");
        }
        return $this->vue->send();
    }
    function write()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->id = $this->dataWrite($_REQUEST);

            $_REQUEST[$this->keyName] = $this->id;
            return true;
        } catch (PpciException) {
            return false;
        }
    }
    function delete()
    {
        /**
         * delete record
         */
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->dataDelete($this->id);
            return true;
        } catch (PpciException) {
            return false;
        }
    }
    function createWithSamples()
    {
        /**
         * Create or no an extraction, create samples of type "extract" from samples,
         * and attach them to the extraction
         */
        if (!$this->projectAllowed) {
            return false;
        }
        if (!empty($_POST["samples"])) {
            try {
                ($_POST["volumeExtract"] > 0) ? $volume = $_POST["volumeExtract"] : $volume = $_POST["volume"];
                $db = $this->dataclass->db;
                $db->transBegin();
                $unit_volume = "µl";
                $unit_weight = "mg";
                $_REQUEST["extraction_id"] = $this->dataclass->createWithSamples(
                    $this->project_id,
                    $this->id,
                    $_POST["samples"],
                    $_POST["extraction_date"],
                    $volume,
                    $_POST["weight"],
                    $unit_volume,
                    $unit_weight,
                    $_POST["nbextrait"],
                    $_POST["extraction_method_id"],
                    $_POST["sampling_id"],
                    $_POST["extractLetter"]
                );
                $db->transCommit();
                $this->message->set(_("Création de l'extraction et des extraits effectuée"));
                return true;
            } catch (PpciException $e) {
                $this->message->set($e->getMessage(), true);
                if ($db->transEnabled) {
                    $db->transRollback();
                }
                return false;
            }
        } else {
            $this->message->set(_("Aucun réplicat n'a été sélectionné pour créer les extraits"), true);
            return false;
        }
    }
    function createFromSamplings()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        if (empty($_POST["samplings"])) {
            $this->message->set(_("Aucun échantillonnage n'a été sélectionné"), true);
            return false;
        } else {
            try {
                $db = $this->dataclass->db;
                $db->transBegin();
                $data = array(
                    "extraction_id" => 0,
                    "project_id" => $this->project_id,
                    "extraction_date" => $_POST["extraction_date"],
                    "extraction_method_id" => $_POST["extraction_method_id"]
                );
                $nb = 0;
                foreach ($_POST["samplings"] as $sampling) {
                    $data["sampling_id"] = $sampling;
                    $this->dataclass->ecrire($data);
                    $nb++;
                }
                $db->transCommit();
                $this->message->set(sprintf(_("%s extractions créées"), $nb));
                return true;
            } catch (PpciException $e) {
                $this->message->set($e->getMessage(), true);
                if ($db->transEnabled) {
                    $db->transRollback();
                }
                return false;
            }
        }
    }
}
