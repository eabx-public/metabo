<?php

namespace App\Libraries;

use App\Models\Condition as ModelsCondition;
use App\Models\Experimentation;
use App\Models\Param;
use App\Models\Sampling;
use App\Models\Station;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;

class Condition extends PpciLibrary
{

    public $keyName;
    private array $data = [];
    public $project_id = 0;
    public $projectAllowed = false;
    public int $id = 0;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsCondition();
        $this->keyName = "condition_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
        if ($this->id > 0) {
            $this->data = $this->dataclass->getDetail($this->id);
            $this->project_id = $this->data["project_id"];
        } elseif (!empty($_SESSION["current_project"])) {
            $this->project_id = $_SESSION["current_project"];
        }
        if (projectVerify($this->project_id) == 1) {
            $_SESSION["current_project"] = $this->project_id;
            $this->projectAllowed = true;
        }
    }

    function display()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        if ($this->id > 0) {
            if (!$this->projectAllowed) {
                return projectRefused();
            }
            $this->vue = service('Smarty');
            $this->vue->set($this->data, "data");
            $this->vue->set("sample/conditionDisplay.tpl", "corps");
            fileInitUpload("condition", $this->id);
            $experimentation = new Experimentation;
            $this->vue->set($experimentation->getDetail($this->data["experimentation_id"]), "experimentation");
            $sampling = new Sampling;
            $this->vue->set($sampling->getListFromCondition($this->id), "samplings");
            $this->vue->set($this->project_id, "project_id");
            return $this->vue->send();
        } else {
            defaultPage();
        }
    }

    function change()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service('Smarty');
        $data = $this->dataRead($this->id, "sample/conditionChange.tpl", $_REQUEST["experimentation_id"]);
        $param = new Param("matrice_type");
        $this->vue->set($param->getListe("3,2"), "types");
        $this->vue->set($this->project_id, "project_id");
        $experimentation = new Experimentation();
        $this->vue->set($experimentation->getDetail($data["experimentation_id"]), "experimentation");
        $station = new Station;
        $this->vue->set($station->getListFromProject($this->project_id), "stations");
        return $this->vue->send();
    }
    function write()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->id = $this->dataWrite($_REQUEST);
            $_REQUEST[$this->keyName] = $this->id;
            return true;
        } catch (PpciException) {
            return false;
        }
    }
    function delete()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->dataDelete($this->id);
            return true;
        } catch (PpciException) {
            return false;
        }
    }
}
