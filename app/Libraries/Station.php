<?php

namespace App\Libraries;

use App\Models\Station as ModelsStation;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\Import;
use Ppci\Models\PpciModel;

class Station extends PpciLibrary
{

    public $keyName;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsStation;
        $this->keyName = "station_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
    }
    function list()
    {
        $this->vue = service('Smarty');
        /*
         * Display the list of all records of the table
         */
        $this->vue->set($this->dataclass->getListFromproject(0, false), "data");
        $this->vue->set("sample/stationList.tpl", "corps");
        $this->vue->set($_SESSION["projects"], "projects");
        return $this->vue->send();
    }
    function change()
    {
        $this->vue = service('Smarty');
        $this->dataRead($this->id, "sample/stationChange.tpl");
        setParamMap($this->vue);
        $this->vue->set($_SESSION["projects"], "projects");
        return $this->vue->send();
    }
    function write()
    {
        /*
         * write record in database
         */
        try {
            $this->id = $this->dataWrite($_REQUEST);
            $_REQUEST[$this->keyName] = $this->id;
            return true;
        } catch (PpciException) {
            return false;
        }
    }
    function delete()
    {
        /*
         * delete record
         */
        try {
            $this->dataDelete($this->id);
            return true;
        } catch (PpciException $e) {
            return false;
        }
    }
    function import()
    {
        if (file_exists($_FILES['upfile']['tmp_name'])) {
            $i = 0;
            try {
                $db = $this->dataclass->db;
                $db->transBegin();
                $import = new Import($_FILES['upfile']['tmp_name'], $_POST["separator"], false, array(
                    "name",
                    "wgs84_x",
                    "wgs84_y"
                ));
                $rows = $import->getContentAsArray();
                foreach ($rows as $row) {
                    if (!empty($row["name"])) {
                        /*
                         * Ecriture en base, en mode ajout ou modification
                         */
                        $data = array(
                            "station_name" => $row["name"],
                            "project_id" => $_POST["project_id"],
                            "wgs84_x" => $row["wgs84_x"],
                            "wgs84_y" => $row["wgs84_y"],

                        );
                        $station_id = $this->dataclass->ecrire($data);
                        $i++;
                    }
                }
                $db->transCommit();
                $this->message->set(sprintf(_("%d stations(s) importée(s)"), $i));
                return true;
            } catch (PpciException $e) {
                if ($db->transEnabled) {
                    $db->transRollback();
                }
                $this->message->set(_("Impossible d'importer les stations"));
                $this->message->set($e->getMessage());
                return false;
            }
        } else {
            $this->message->set(_("Impossible de charger le fichier à importer"));
            return false;
        }
    }
    function getFromProject()
    {
        $this->vue->set($this->dataclass->getListFromproject($_REQUEST["project_id"]));
    }
    function getCoordinate()
    {
        $this->vue->set($this->dataclass->getCoordinates($_REQUEST["station_id"]));
    }
}
