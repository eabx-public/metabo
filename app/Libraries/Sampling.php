<?php

namespace App\Libraries;

use App\Models\Condition;
use App\Models\Extraction;
use App\Models\Param;
use App\Models\Sample;
use App\Models\Sampling as ModelsSampling;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class Sampling extends PpciLibrary
{

    public $keyName;
    public int $id = 0;
    private array $data = [];
    public $projectAllowed = false;
    private int $project_id = 0;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsSampling;
        $this->keyName = "sampling_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
            if ($this->id > 0) {
                $this->data = $this->dataclass->getDetail($this->id);
                $this->project_id = $this->data["project_id"];
            } elseif (!empty($_SESSION["current_project"])) {
             $this->project_id = $_SESSION["current_project"];
            }
        }
        if (projectVerify($this->project_id)) {
            $_SESSION["current_project"] = $this->project_id;
            $this->projectAllowed = true;
        }
    }

    function display()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        if ($this->id > 0) {
            $this->vue = service('Smarty');
            $this->vue->set($this->data, "data");
            $this->vue->set("sample/samplingDisplay.tpl", "corps");
            fileInitUpload("sampling", $this->id);
            $condition = new Condition;
            $this->vue->set($condition->getDetail($this->data["condition_id"]), "condition");
            $sample = new Sample();
            $this->vue->set($sample->getListFromsampling($this->id), "replicates");
            /**
             * Data for create extractions
             */
            $this->vue->set(1, "sampleListIsForm");
            $extraction = new Extraction();
            $this->vue->set($extraction->getListFromsampling($this->id), "extractions");
            $this->vue->set(date($_SESSION["date"]["maskdate"]), "extraction_date");
            $param = new Param("extraction_method");
            $this->vue->set($param->getListe("3,2"), "methods");
            $param = new Param("sample_nature");
            $this->vue->set($param->getListe("3,2"), "sample_natures");
            $this->vue->set($this->project_id, "project_id");
            return $this->vue->send();
        } else {
            return defaultPage();
        }
    }
    function change()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service('Smarty');
        $data = $this->dataRead($this->id, "sample/samplingChange.tpl", $_REQUEST["condition_id"]);
        $param = new Param("sampling_method");
        $this->vue->set($param->getListe("3,2"), "methods");
        $this->vue->set($this->project_id, "project_id");
        $condition = new Condition();
        $this->vue->set($condition->getDetail($data["condition_id"]), "condition");
        $storageLocation = new Param("storage_location");
        $this->vue->set($storageLocation->getListe("3,2"), "storage_locations");
        $storageType = new Param("storage_type");
        $this->vue->set($storageType->getListe("3,2"), "storage_types");
        return $this->vue->send();
    }
    function write()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->id = $this->dataWrite($_REQUEST);
            $_REQUEST[$this->keyName] = $this->id;
            return true;
        } catch (PpciException) {
            return false;
        }
    }
    function delete()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        /**
         * delete record
         */
        try {
            $db = $this->dataclass->db;
            $db->transBegin();
            $this->dataDelete($this->id, true);
            $this->message->set(_("Suppression effectuée !"));
            $db->transCommit();
            return true;
        } catch (PpciException) {
            if ($db->transEnabled) {
                $db->transRollback();
                $this->message->set(_("La suppression a échoué"), true);
                return false;
            }
        }
    }
    function generateReplicate()
    {
        if (!$this->projectAllowed) {
            $this->message->set(_("Vous ne disposez pas des droits suffisants pour le projet considéré"),true);
            return false;
        }
        $this->data["project_id"] = $this->project_id;
        $this->data["sample_type_id"] = 1;
        $this->data["sample_nature_id"] = 1;
        try {
            $db = $this->dataclass->db;
            $db->transBegin();
            $nbgenerate = $this->dataclass->createWithSamples($this->data);
            $db->transCommit();
            $this->message->set(sprintf(_("%s réplicats d'échantillons générés"), $nbgenerate));
            return true;
        } catch (PpciException $e) {
            if ($db->transEnabled) {
                $db->transRollback();
            }
            $this->message->set($e->getMessage(), true);
            $this->message->setSyslog($e->getMessage(),true);
            return false;
        }
    }
    function createSamplingsFromConditions()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $db = $this->dataclass->db;
            $db->transBegin();
            $nbsamplings = 0;
            $nbreplicates = 0;
            $data = array(
                "sampling_id" => 0,
                "sampling_description" => $_POST["sampling_description"],
                "sampling_date" => $_POST["sampling_date"],
                "default_volume" => $_POST["default_volume"],
                "default_weight" => $_POST["default_weight"],
                "default_unit_volume" => $_POST["default_unit_volume"],
                "separator" => $_POST["separator"],
                "level1_code" => $_POST["level1_code"],
                "level1_number" => $_POST["level1_number"],
                "level2_code"  => $_POST["level2_code"],
                "level2_number"  => $_POST["level2_number"],
                "default_storage_type"  => $_POST["default_storage_type"],
                "default_storage_location"  => $_POST["default_storage_location"],
                "default_unit_weight" => $_POST["default_unit_weight"]
            );
            if ($_POST["default_storage_type"] > 0) {
                $data["storage_type_id"] = $_POST["default_storage_type"];
            }
            if ($_POST["default_storage_location"] > 0) {
                $data["storage_location_id"] = $_POST["default_storage_location"];
            }
            if ($_POST["default_transport_method"] > 0) {
                $data["transport_method_id"] = $_POST["default_transport_method"];
            }
            $condition = new Condition();
            foreach ($_POST["conditions"] as $condition_id) {
                $dcondition = $condition->getDetail($condition_id);
                if (!projectVerify($dcondition["project_id"])) {
                    throw new PpciException(
                        sprintf(
                            _("Vous ne disposez pas des droits suffisants sur la condition %s pour réaliser l'opération"),
                            $dcondition["condition_code"]
                        )
                    );
                }
                $data["condition_id"] = $condition_id;
                $data["sampling_code"] = $dcondition["condition_code"] . $_POST["sampling_suffix"];
                $data["project_id"] = $dcondition["project_id"];
                $nbreplicates += $this->dataclass->createWithSamples($data);
                $nbsamplings++;
            }
            $db->transCommit();
            $this->message->set(sprintf(_("%s échantillonnages générés"), $nbsamplings));
            $this->message->set(sprintf(_("%s réplicats générés"), $nbreplicates));
            return true;
        } catch (PpciException $se) {
            if ($db->transEnabled) {
                $db->transRollback();
            }
            $this->message->set(_("L'opération a échoué"), true);
            $this->message->set($se->getMessage());
            $this->message->setSyslog($se->getMessage());
            return false;
        }
    }
}
