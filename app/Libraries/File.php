<?php

namespace App\Libraries;

use App\Models\FileClass;
use App\Models\Filelink;
use App\Models\Project;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;

class File extends PpciLibrary
{

    public $keyName;
    public int $project_id = 0;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new FileClass;
        $this->keyName = "file_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
        if (!empty($_REQUEST["project_id"])) {
            $this->project_id = $_REQUEST["project_id"];
        } else if (!empty($_SESSION["current_project"])) {
            $this->project_id = $_SESSION["current_project"];
        }
        if (isset($_POST["filetype_id2"])) {
            $_POST["filetype_id"] = $_POST["filetype_id2"];
        }
    }
    function write()
    {
        /*
		 * write record in database
		 */
        /*
			 * Preparation de files
			 */
        $files = array();
        $fdata = $_FILES['filename'];
        if (is_array($fdata['name'])) {
            for ($i = 0; $i < count($fdata['name']); ++$i) {
                $files[] = array(
                    'name' => $fdata['name'][$i],
                    'type' => $fdata['type'][$i],
                    'tmp_name' => $fdata['tmp_name'][$i],
                    'error' => $fdata['error'][$i],
                    'size' => $fdata['size'][$i]
                );
            }
        } else {
            $files[] = $fdata;
        }
        try {
            foreach ($files as $file) {
                if ($file["error"] == 0 && $file["size"] > 0) {
                    $this->id = $this->dataclass->writeUpload($_POST, $file);
                }
                $this->message->set(_("Import effectué"));
                return true;
            }
        } catch (PpciException $fe) {
            $this->message->set($fe->getMessage(), true);
            return false;
        }
    }
    function supprimer()
    {
        /*
		 * delete record
		 */
        try {
            $this->dataclass->supprimer($this->id);
            return true;
        } catch (PpciException) {
            return false;
        }
    }
    function getExternal()
    {
        $filelink = new Filelink;
        $filelink->fileObject = $this->dataclass;
        try {
            if (empty($_REQUEST["uid"])) {
                throw new PpciException(_("Pas de lien fourni pour télécharger le fichier"));
            }
            $dfilelink = $filelink->getFromLink($_REQUEST["uid"]);
            $this->id = $dfilelink["file_id"];
            $data = $this->dataclass->lire($this->id);
            if ($data["storage_type"] == "I") {
                $path = $_SESSION["dbparams"]["real_storage_path"] . "/" . $data["storage_path"];
            } else {
                $project = new Project;
                $dproject = $project->lire($data["project_id"]);
                $path = $dproject["external_path"] . "/" . $data["storage_path"];
            }
            if (file_exists($path)) {
                $filelink->updateLastUse($dfilelink["filelink_id"]);
                $this->vue = service("BinaryView");
                $this->vue->setParam(
                    array(
                        "filename" => $data["filename"],
                        "tmp_name" => $path,
                        "content_type" => $data["content_type"]
                    )
                );
                $this->vue->send();
            } else {
                throw new PpciException(_("File not found to download: $path"));
            }
        } catch (PpciException $e) {
            $this->message->setSyslog($e->getMessage(),true);
            return false;
        }
    }
    function get()
    {
        /**
         * send the file to the browser
         */
        $data = $this->dataclass->lire($this->id);
        if (projectVerify($data["project_id"])) {
            if ($data["storage_type"] == "I") {
                $path = $_SESSION["dbparams"]["real_storage_path"] . "/" . $data["storage_path"];
            } else {
                $path = $_SESSION["projects"][$data["project_id"]]["external_path"] . "/" . $data["storage_path"];
            }
            if (file_exists($path)) {
                $this->vue = service("BinaryView");
                $this->vue->setParam(
                    array(
                        "filename" => $data["filename"],
                        "tmp_name" => $path,
                        "content_type" => $data["content_type"]
                    )
                );
                $this->vue->send();
            }
        }
    }
    function downloadedFileDelete()
    {
        if (!empty($_REQUEST["filename"])) {
            if ($this->dataclass->deleteDownloadedFile($_REQUEST["filename"])) {
                $this->message->set(_("Suppression effectuée"));
                return true;
            } else {
                $this->message->set(_("Une erreur est survenue lors de la suppression du fichier"), true);
                return false;
            }
        } else {
            return false;
        }
    }
    function generateLink()
    {
        if (empty($_POST["files"])) {
            $this->message->set(_("Aucun fichier sélectionné"), true);
            return false;
        } else {
            $filelink = new Filelink;
            $filelink->fileObject = $this->dataclass;
            $content = array();
            $radical = $this->appConfig->baseURL . "/fileGetExternal";
            $duration = $_SESSION["dbparams"]["fileLinkDuration"];
            try {
                foreach ($_POST["files"] as $file_id) {
                    $item = array("link" => $radical . "?uid=" . $filelink->generateLink($file_id, $duration));
                    $data = $this->dataclass->lire($file_id);
                    if (projectVerify($data["project_id"])) {
                        $item["filename"] = $data["filename"];
                        $item["md5sum"] = $this->dataclass->getmd5($file_id);
                        $content[] = $item;
                    }
                }
                $this->vue = service("CsvView");
                $this->vue->setFilename("fileList.txt");
                $this->vue->set($content);
                $this->vue->send();
            } catch (PpciException $e) {
                $this->message->set($e->getMessage(), true);
                $this->message->setSyslog($e->getMessage(),true);
                return false;
            }
        }
    }
    function generateSimpleLink()
    {
        if (empty($_POST["files"])) {
            $this->message->set(_("Aucun fichier sélectionné"), true);
            return false;
        } else {
            $filelink = new Filelink;
            $filelink->fileObject = $this->dataclass;
            $content = array();
            $radical = $this->appConfig->baseURL . "/fileGetExternal";
            $duration = $_SESSION["dbparams"]["fileLinkDuration"];
            try {
                foreach ($_POST["files"] as $file_id) {
                    $item = array("link" => $radical . "?uid=" . $filelink->generateLink($file_id, $duration));
                    $data = $this->dataclass->lire($file_id);
                    if (projectVerify($data["project_id"])) {
                        $content[] = $item;
                    }
                }
                $this->vue = service("CsvView");
                $this->vue->setFilename("fileList.txt");
                $this->vue->set($content);
                $this->vue->send();
            } catch (PpciException $e) {
                $this->message->set($e->getMessage(), true);
                $this->message->setSyslog($e->getMessage(),true);
                return false;
            }
        }
    }
    /**
     * browse the tree structure for select external file
     */
    function getListExternal()
    {
        $this->vue = service("AjaxView");
        $listFiles = array();
        $path = $_REQUEST["path"];
        $project_path = $_SESSION["projects"][$_SESSION["current_project"]]["external_path"];
        if (empty($project_path)) {
            $project_path = $_SESSION["dbparams"]["external_path_default"];
        }
        $dir = $project_path . str_replace(["..", $project_path], ["", ""], $path);
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file != ".." && $file != ".") {
                        $f = array("label" => $file, "isDrive" => false);
                        $f["path"] = $dir . "/" . $file;
                        if (filetype($dir . "/" . $file) == "dir") {
                            $f["isFolder"] = true;
                            $f["hasSubfolder"] = true;
                        } else {
                            $f["ext"] =  pathinfo($dir . "/" . $file, PATHINFO_EXTENSION);
                            $f["subitems"][] = date($_SESSION["date"]["maskdate"], filemtime($dir . "/" . $file));
                            $f["subitems"][] = number_format((filesize($dir . "/" . $file) / (1024 * 1024)), 3) . " Mb";
                        }
                        $listFiles[] = $f;
                    }
                }
            }
        }

        $this->vue->set($listFiles);
        $this->vue->send();
    }
    function assignExternal()
    {
        $project_path = $_SESSION["projects"][$_SESSION["current_project"]]["external_path"];
        if (empty($project_path)) {
            $project_path = $_SESSION["dbparams"]["external_path_default"];
        }
        $filepath = $project_path . str_replace(array("..", $project_path, "//"), array("", "", "/"), $_REQUEST["path"]);

        if (file_exists($filepath) && projectVerify($_REQUEST["project_id"]) && $_COOKIE["filetype_id" . $_REQUEST["fileLoadNumber"]] > 0) {
            $db = $this->dataclass->db;
            $db->transBegin();
            $data = $this->dataclass->getDefaultValues();
            $f = pathinfo($filepath);
            $data["project_id"] = $_REQUEST["project_id"];
            $data["filename"] = $f["basename"];
            $data["mimetype_id"] = $this->dataclass->getmimetype($filepath);
            $data["filesize"] = filesize($filepath);
            $data["filetype_id"] = $_COOKIE["filetype_id" . $_REQUEST["fileLoadNumber"]];
            $data["filedate"] = date($_SESSION["date"]["maskdatelong"], filemtime($filepath));
            $data["storage_path"] = str_replace($project_path . "/", "", $filepath);
            $data["filedateimport"] = $this->dataclass->getDateHeure();
            $data["storage_type"] = "E";
            try {
                $newid = $this->dataclass->ecrire($data);
                if ($newid > 0) {
                    $sql = "insert into " . $_REQUEST["modulename"] . "_file (" . $_REQUEST["modulename"] . "_id, file_id) values (:object_id:, :file_id:)";
                    $this->dataclass->executeSQL($sql, array("object_id" => $_REQUEST[$_REQUEST["modulename"] . "_id"], "file_id" => $newid), true);
                } else {
                    throw new PpciException(_("Une erreur est survenue pendant la création dans la table file"));
                }
                $db->transCommit();
                $t_module["retourok"] = $_REQUEST["modulename"] . "Display";
                $this->message->set(_("Appariement effectué"));
                return true;
            } catch (PpciException $e) {
                if ($db->transEnabled) {
                    $db->transRollback();
                }
                $this->message->set(_("L'enregistrement des informations sur le fichier a échoué"), true);
                $this->message->set($e->getMessage());
                return false;
            }
        } else {
            $this->message->set(_("Des informations sont manquantes ou erronées, ne permettant pas de réaliser l'opération"), true);
            return false;
        }
    }
}
