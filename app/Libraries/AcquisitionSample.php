<?php

namespace App\Libraries;

use App\Models\AcquisitionSample as ModelsAcquisitionSample;
use App\Models\Sample;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class AcquisitionSample extends PpciLibrary
{

    public $keyName;
    private $sample;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsAcquisitionSample();
        $this->keyName = "acquisition_sample_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
        }
        $this->sample = new Sample();
    }


    function deleteMulti()
    {
        try {
            $db = $this->dataclass->db;
            $db->transBegin();
            $nbDelete = 0;
            foreach ($_POST["samples"] as $sample_id) {
                if ($this->dataclass->isDeletable($sample_id, $_POST["acquisition_id"])) {
                    $this->dataclass->supprimerFromSampleId($sample_id, $_POST["acquisition_id"]);
                    $nbDelete++;
                } else {
                    $data = $this->sample->lire($sample_id);
                    $this->message->set(sprintf(_("L'aliquot %s a des fichiers rattachés, il ne peut pas être détaché de l'acquisition"), $data["sample_name"]));
                }
            }
            $this->message->set(sprintf(_("%s aliquots détachés de l'acquisition"), $nbDelete));
            $db->transCommit();
            return true;
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
            if ($db->transEnabled) {
                $db->transRollback();
                return false;
            }
        }
    }
    function delete()
    {
        try {
            $data = $this->dataclass->lire($this->id);
            $this->dataclass->supprimerFromSampleId($data["sample_id"], $data["acquisition_id"]);
            return true;
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
            return false;
        }
    }
    function addSampleToAcquisition()
    {
        try {
            if ($_POST["acquisition_id"] > 0 && $_POST["sample_id"] > 0) {
                $dataSample = $this->sample->lire($_POST["sample_id"]);
                if (!projectVerify($dataSample["project_id"])) {
                    throw new PpciException(_("Les droits sont insuffisants pour l'échantillon"));
                }
                $res = $this->dataclass->ecrire(
                    array(
                        "sample_id" => $_POST["sample_id"],
                        "acquisition_id" => $_POST["acquisition_id"],
                        "is_calibration" => $_POST["is_calibration"]
                    )
                );
                if (!$res > 0) {
                    throw new PpciException(_("Un problème est survenu pendant l'enregistrement en base de données"));
                }
                return true;
            } else {
                throw new PpciException(_("L'association de l'acquisition à l'échantillon est impossible, des informations sont manquantes"), true);
            }
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
            return false;
        }
    }
}
