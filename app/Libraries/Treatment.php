<?php

namespace App\Libraries;

use App\Models\FileClass;
use App\Models\Filetype;
use App\Models\Param;
use App\Models\Sample;
use App\Models\Treatment as ModelsTreatment;
use App\Models\TreatmentAs;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class Treatment extends PpciLibrary
{

    public $keyName;
    public int $id = 0;
    private array $data = [];
    public $projectAllowed = false;
    private int $project_id = 0;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsTreatment;
        $this->keyName = "treatment_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
            if ($this->id > 0) {
                $this->data = $this->dataclass->getDetail($this->id);
                $this->project_id = $this->data["project_id"];
            }elseif (!empty($_SESSION["current_project"])) {
             $this->project_id = $_SESSION["current_project"];
            }
        }
        if (projectVerify($this->project_id)) {
            $_SESSION["current_project"] = $this->project_id;
            $this->projectAllowed = true;
        }
    }

    function display()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        if ($this->id > 0) {
            $this->vue = service('Smarty');
            $this->vue->set($this->data, "data");
            $this->vue->set("acquisition/treatmentDisplay.tpl", "corps");
            include_once "modules/file/file.functions.php";
            fileInitUpload("treatment", $this->id);
            include_once "modules/classes/acquisition/treatment_as.class.php";
            $treatmentAs = new TreatmentAs();
            $this->vue->set($treatmentAs->getListFromTreatment($this->id), "treatmentsAs");
            /**
             * Add the files from treatment_as_id
             */
            if ($_REQUEST["treatment_as_id"] > 0) {
                fileInitUpload("treatment_as", $_REQUEST["treatment_as_id"], 2);
                $this->vue->set($_REQUEST["treatment_as_id"], "treatment_as_id");
                $this->vue->set($treatmentAs->getDetail($_REQUEST["treatment_as_id"]), "treatmentAs");
            }
            $this->vue->set($this->project_id, "project_id");
            return $this->vue->send();
        } else {
            return defaultPage();
        }
    }
    function change()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service('Smarty');
        $this->dataRead($this->id, "acquisition/treatmentChange.tpl", $_REQUEST["project_id"]);
        $param = new Param("software");
        $this->vue->set($param->getListe("3,2"), "softwares");
        $this->vue->set($this->project_id, "project_id");
        return $this->vue->send();
    }
    function write()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->id = $this->dataWrite($_REQUEST);

            $_REQUEST[$this->keyName] = $this->id;
            return true;
        } catch (PpciException) {
            return false;
        }
    }
    function delete()
    {
        /**
         * delete record
         */
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->dataDelete($this->id);
            return true;
        } catch (PpciException $e) {
            return false;
        }
    }
    function createWithSamples()
    {
        return $this->attachSamples();
    }
    function attachSamples()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        /**
         * Create or no an extraction, create samples of type "extract" from samples,
         * and attach them to the extraction
         */
        if (count($_POST["samples"]) > 0) {
            try {
                $db = $this->dataclass->db;
                $db->transBegin();
                $_REQUEST["treatment_id"] = $this->dataclass->createWithSamples($_POST);
                $db->transCommit();
                return true;
            } catch (PpciException $e) {
                $this->message->set($e->getMessage(), true);
                if ($db->transEnabled) {
                    $db->transRollback();
                }
                return false;
            }
        } else {
            $this->message->set(_("Aucun aliquot n'a été sélectionné"), true);
            return false;
        }
    }
    function addDownloadedFile()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service("Smarty");
        $this->vue->set($this->dataclass->getDetail($this->id), "treatment");
        $file = new FileClass;
        $files = $file->getListDownloadedFiles();
        $this->vue->set($files, "files");
        $this->vue->set("treatment/treatmentSampleAddFiles.tpl", "corps");
        $filetype = new Filetype;
        $this->vue->set($filetype->getListFromType("treatment_sample"), "filetypes");
        $sample = new Sample;
        $samples = $sample->getListFromTreatment($this->id);
        /**
         * Pre-positioning of files
         */
        foreach ($samples as $ks => $s) {
            foreach ($files as $f) {
                if ($s["sample_name"] == $f["radical"]) {
                    $samples[$ks]["filename"] = $f["filename"];
                }
            }
        }
        $this->vue->set($samples, "samples");
        /**
         * Get default datetime
         */
        $this->vue->set(date($_SESSION["date"]["maskdatelong"]), "defaultDateTime");
        return $this->vue->send();
    }


    function addDownloadedFileExec()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        $file = new FileClass();
        $asample = new TreatmentAs();
        try {
            $db = $this->dataclass->db;
            $db->transBegin();
            /**
             * Search for files to associate
             */
            foreach ($_POST as $varname => $content) {
                $radical = substr($varname, 0, 6);
                if ($radical == "sample" && !empty($content)) {
                    $asample_id = substr($varname, 6);
                    $dsample = $asample->getDetail($asample_id);
                    if (projectVerify($dsample["project_id"]) != 1) {
                        throw new PpciException(sprintf(_("Vous ne disposez pas des droits suffisants pour l'échantillon %s"), $dsample["sample_name"]));
                    }
                    /**
                     * Move the file to it new place
                     */
                    $sample_type_id = $_POST["type" . $asample_id];
                    $sample_date = $_POST["date" . $asample_id];
                    $file->moveDownloadedFile($content, $dsample["project_id"], $sample_type_id, "treatment_sample", $asample_id, $sample_date);
                }
            }
            $db->transCommit();
            $this->message->set(_("Appariement et déplacement effectués"));
            return true;
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
            if ($db->transEnabled) {
                $db->transRollback();
            }
            return false;
        }
    }
    function asDelete()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        /**
         * Delete the acquisition_sample_id attached to the treatment
         */
        $treatmentAs = new TreatmentAs();
        try {
            $treatmentAs->supprimer($_REQUEST["treatment_as_id"]);
            $this->message->set(_("Suppression effectuée"));
            return true;
        } catch (PpciException $e) {
            $this->message->set(_("Une erreur est survenue pendant la suppression"), true);
            $this->message->set($e->getMessage());
            return false;
        }
    }
}
