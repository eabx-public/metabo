<?php

namespace App\Libraries;

use App\Models\Acquisition;
use App\Models\AcquisitionSample;
use App\Models\Condition;
use App\Models\Experimentation;
use App\Models\Extraction;
use App\Models\Param;
use App\Models\Sample as ModelsSample;
use App\Models\Sampling;
use Ppci\Libraries\PpciException;
use Ppci\Libraries\PpciLibrary;
use Ppci\Models\PpciModel;

class Sample extends PpciLibrary
{

    public $keyName;
    public int $id = 0;
    private array $data = [];
    public $projectAllowed = false;
    private int $project_id = 0;

    function __construct()
    {
        parent::__construct();
        $this->dataclass = new ModelsSample();
        $this->keyName = "sample_id";
        if (isset($_REQUEST[$this->keyName])) {
            $this->id = $_REQUEST[$this->keyName];
            if ($this->id > 0) {
                $this->data = $this->dataclass->getDetail($this->id);
                $this->project_id = $this->data["project_id"];
            } elseif (!empty($_SESSION["current_project"])) {
                $this->project_id = $_SESSION["current_project"];
            }
        }
        if (projectVerify($this->project_id)) {
            $_SESSION["current_project"] = $this->project_id;
            $this->projectAllowed = true;
        }
    }

    function display()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service('Smarty');
        $this->vue->set($this->data, "data");
        $this->vue->set("sample/sampleDisplay.tpl", "corps");
        include_once "modules/file/file.functions.php";
        fileInitUpload("sample", $this->id);
        $data = $this->data;
        if ($data["experimentation_id"] > 0) {
            $experimentation = new Experimentation;
            $this->vue->set($experimentation->getDetail($data["experimentation_id"]), "experimentation");
        }
        if ($data["condition_id"] > 0) {
            $condition = new Condition;
            $this->vue->set($condition->getDetail($data["condition_id"]), "condition");
        }
        if ($data["extraction_id"] > 0) {
            $extraction = new Extraction;
            $this->vue->set($extraction->getDetail($data["extraction_id"]), "extraction");
        }
        /**
         * Children
         */
        $this->vue->set($this->dataclass->getChildren($this->id), "samples");
        if ($data["sample_type_id"] == 3) {
            /**
             * Acquisition
             */
            $this->dataclass->colonnes["acquisition_date"] = array("type" => 2);
            $this->vue->set($this->dataclass->getListAcquisition($this->id), "acquisition");
            /**
             * Get the list of acquisition of the project, to associate with sample
             */
            $acquisition = new Acquisition;
            $this->vue->set($acquisition->getListFromProject($data["project_id"], "acquisition_date desc"), "acquisitions");
            /**
             * Treatments
             */
            $this->dataclass->colonnes["treatment_date"] = array("type" => 2);
            $this->vue->set($this->dataclass->getListTreatment($this->id), "treatments");
        }
        return $this->vue->send();
    }
    function change()
    {
        if (!$this->projectAllowed) {
            return projectRefused();
        }
        $this->vue = service('Smarty');
        $data = $this->dataRead($this->id, "sample/sampleChange.tpl", $_REQUEST["sample_parent_id"]);
        if ($this->id == 0) {
            /**
             * Add data from condition
             */
            if ($_REQUEST["sampling_id"] > 0) {
                $sampling = new Sampling;
                $dsampling = $sampling->getDetail($_REQUEST["sampling_id"]);
                $data["project_id"] = $dsampling["project_id"];
                $data["experimentation_name"] = $dsampling["experimentation_name"];
                $data["project_name"] = $dsampling["project_name"];
                $data["experimentation_id"] = $dsampling["experimentation_id"];
                $data["sampling_id"] = $_REQUEST["sampling_id"];
            }
            /**
             * Add data from parent
             */
            if ($_REQUEST["sample_parent_id"] > 0) {
                $data["sample_parent_id"] = $_REQUEST["sample_parent_id"];
                $dparent = $this->dataclass->getDetail($_REQUEST["sample_parent_id"]);
                $data["project_id"] = $dparent["project_id"];
                $data["experimentation_name"] = $dparent["experimentation_name"];
                $data["project_name"] = $dparent["project_name"];
                $data["sample_name"] = $dparent["sample_name"] . "-";
                if ($dparent["sample_type_id"] == 1) {
                    $data["sample_type_id"] = 2;
                } elseif ($dparent["sample_type_id"] == 2) {
                    $data["sample_type_id"] = 3;
                }
                $this->vue->set($dparent, "parent");
            }
            $this->vue->set($data, "data");
        }
        $param = new Param("sample_type");
        $this->vue->set($param->getListe("3,2"), "types");
        $transport = new Param("transport_method");
        $this->vue->set($transport->getListe("3,2"), "transport_methods");
        $sampleNature = new Param("sample_nature");
        $this->vue->set($sampleNature->getListe("3"), "sample_natures");
        $storageLocation = new Param("storage_location");
        $this->vue->set($storageLocation->getListe("3,2"), "storage_locations");
        $storageType = new Param("storage_type");
        $this->vue->set($storageType->getListe("3,2"), "storage_types");
        $this->vue->set($this->project_id, "project_id");
        include_once "modules/classes/sample/condition.class.php";
        $condition = new Condition();
        if (!empty($data["condition_id"])) {
            $this->vue->set($condition->getDetail($data["condition_id"]), "condition");
        }
        return $this->vue->send();
    }
    function write()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        try {
            $this->id = $this->dataWrite($_REQUEST);

            $_REQUEST[$this->keyName] = $this->id;
            return true;
        } catch (PpciException) {
            return false;
        }
    }
    function delete()
    {
        if (!$this->projectAllowed) {
            return false;
        }
        /**
         * delete record
         */
        if ($nb = $this->dataclass->getNbFiles($this->id) == 0) {
            try {
                $this->dataDelete($this->id);
                return true;
            } catch (PpciException) {
                return false;
            }
        } else {
            $this->message->set(sprintf(_("La suppression est impossible, %s fichiers sont rattachés"), $nb), true);
            return false;
        }
    }
    function deleteMulti()
    {
        if (count($_POST["samples"]) > 0) {
            try {
                $db = $this->dataclass->db;
                $db->transBegin();
                $nbDeleted = 0;
                foreach ($_POST["samples"] as $sample_id) {
                    if ($this->dataclass->isDeletable($sample_id)) {
                        $this->dataclass->supprimer($sample_id);
                        $nbDeleted++;
                    } else {
                        $data = $this->dataclass->lire($sample_id);
                        $this->message->set(sprintf(_("L'échantillon %s n'est pas supprimable (fichiers ou échantillons enfants rattachés, échantillon associé à une acquisition)"), $data["sample_name"]));
                    }
                }
                $db->transCommit();
                $this->message->set(sprintf(_("%s échantillons supprimés"), $nbDeleted));
                return true;
            } catch (PpciException $e) {
                $this->message->set(_("Une erreur est survenue pendant l'opération"), true);
                $this->message->set($e->getMessage());
                $this->message->setSyslog($e->getMessage(), true);
                if ($db->transEnabled) {
                    $db->transRollback();
                }
                return false;
            }
        } else {
            $this->message->set(_("Aucun échantillon n'a été sélectionné"), true);
            return false;
        }
    }
    function sendToCollec()
    {
        $uidMin = 99999999;
        $uidMax = 0;
        try {
            if (empty($_POST["samples"])) {
                throw new PpciException(_("Aucun échantillon n'a été sélectionné"));
            }
            /**
             * Verify the parameters for Collec-science
             */
            if (empty($_SESSION["collec_token"]) || empty($_SESSION["collec_sample_address"] || empty($_SESSION["collec_login"]))) {
                throw new PpciException(_("Les paramètres pour Collec-Science n'ont pas été renseignés"));
            }
            foreach ($_POST["samples"] as $sample_id) {
                $this->dataclass->autoFormatDate = false;
                $dataSample = $this->dataclass->getDetail($sample_id);
                if (empty($dataSample["uuid"])) {
                    throw new PpciException(sprintf(_("Impossible de lire les informations de l'échantillon %s"), $sample_id));
                }
                if (!projectVerify($dataSample["project_id"])) {
                    throw new PpciException(sprintf(_("Vous ne disposez pas des droits nécessaires sur l'échantillon %s"), $sample_id));
                }
                $data = array();
                $data["sample_type_name"] = $dataSample["collec_type_name"];
                $data["sampling_date"] = $dataSample["sampling_date"];
                $data["identifier"] = $dataSample["sample_name"];
                $data["uuid"] = $dataSample["uuid"];
                $data["station_name"] = $dataSample["station_name"];
                $data["wgs84_x"] = $dataSample["wgs84_x"];
                $data["wgs84_y"] = $dataSample["wgs84_y"];
                $data["token"] = $_SESSION["collec_token"];
                $data["login"] = $_SESSION["collec_login"];
                $result_json = apiCall("POST", $_SESSION["dbparams"]["collec_sample_address"], "", $data, $this->appConfig->apiDebug);
                $result = json_decode($result_json, true);
                if ($result["error_code"] != 200) {
                    throw new PpciException(sprintf(_("L'erreur %1\$s a été générée lors du traitement de l'échantillon %3\$s : %2\$s"), $result["error_code"], $result["error_message"] . " " . $result["error_detail"], $sample_id));
                }
                $uid = $result['uid'];
                if ($uid > 0) {
                    if ($uid < $uidMin) {
                        $uidMin = $uid;
                    }
                    if ($uid > $uidMax) {
                        $uidMax = $uid;
                    }
                }
            }
            if ($uidMax > 0) {
                $this->message->set(sprintf(_("UID min traité : %s"), $uidMin));
                $this->message->set(sprintf(_("UID max traité : %s"), $uidMax));
            }
            return true;
        } catch (PpciException $e) {
            $this->message->set($e->getMessage(), true);
            return false;
        }
    }

    function createAliquot()
    {
        $error = false;
        $total = 0;
        try {
            $db = $this->dataclass->db;
            $db->transBegin();
            if (empty($_POST["samples"]) || count($_POST["samples"]) == 0) {
                $this->message->set(_("Aucun extrait n'a été sélectionné"),true);
                throw new PpciException();
            }
            $error = true;
            $data = [];
            $acquisition = new Acquisition;
            $acquisitionSample = new AcquisitionSample;
            $acquisition_id = $_POST["acquisition_id"];
            if ($acquisition_id == 0) {
                /**
                 * create a new acquisition
                 */
                $dacquisition = $_POST;
                $acquisition_id = $acquisition->ecrire($dacquisition);
            } else if ($_POST["acquisition_id"] > 0) {
                $dacquisition = $acquisition->getDetail($_POST["acquisition_id"]);
                if (!projectVerify($dacquisition["project_id"])) {
                    throw new PpciException(_("Les droits sont insuffisants pour réaliser cette opération"));
                }
            }
            $dSample = array(
                "sample_type_id" => 3,
                "volume" => $_POST["volume"],
                "sampled_volume" => $_POST["sampled_volume"],
                "unit" => "µl"
            );
            $dAcquisitionSample = [
                "is_calibration" => $_POST["is_calibration"],
                "acquisition_id" => $acquisition_id
            ];
            foreach ($_POST["samples"] as $sample_id) {
                $dataSample = $this->dataclass->lire($sample_id);
                if (!projectVerify($dataSample["project_id"])) {
                    throw new PpciException(sprintf(_("Les droits sont insuffisants pour l'échantillon %s"), $sample_id));
                }
                for ($i = 1; $i <= $_POST["number_aliquots"]; $i++) {
                    /**
                     * Creation of aliquote
                     */
                    $dSample["project_id"] = $dataSample["project_id"];
                    $dSample["sample_name"] = $this->dataclass->generateSampleName($dataSample["sample_name"], $_POST["project_id"]);
                    $dSample["sample_parent_id"] = $sample_id;
                    $dSample["sample_type_id"] = 3;
                    $sampleId = $this->dataclass->ecrire($dSample);
                    /**
                     * Attachment to the acquisition
                     */
                    if ($acquisition_id > 0) {
                        $dAcquisitionSample["sample_id"] = $sampleId;
                        $acquisitionSample->ecrire($dAcquisitionSample);
                    }
                    $total++;
                }
            }
            $this->message->set(sprintf(_("%s aliquots créés"),$total));
            $db->transCommit();
        } catch (PpciException $e) {
            $db->transRollback();
            if ($error) {
                $this->message->setSyslog($e->getMessage(), true);
            }
            $this->message->set($e->getMessage());
        }
    }
}
