<?php

namespace App\Controllers;

use App\Libraries\Acquisition;
use App\Libraries\Project;
use \Ppci\Controllers\PpciController;
use App\Libraries\Treatment as Lib;

class Treatment extends PpciController
{

    function display()
    {
        if (strlen($_REQUEST["treatment_id"]) == 0) {
            if ($_COOKIE["treatment_id"] > 0) {
                $_REQUEST["treatment_id"] = $_COOKIE["treatment_id"];
            } else {
                return defaultPage();
            }
        }
        $lib = new Lib;
        return $lib->display();
    }
    function asDelete()
    {
        $lib = new Lib;
        $lib->asDelete();
        return $lib->display();
    }
    function change()
    {
        $lib = new Lib;
        return $lib->change();
    }
    function write()
    {
        $lib = new Lib;
        if ($lib->write()) {
            return $this->display();
        } else {
            return $this->change();
        }
    }
    function delete()
    {
        $lib = new Lib;
        if ($lib->delete()) {
            $project = new Project;
            return $project->display();
        } else {
            return $this->change();
        }
    }
    function createWithSamples()
    {
        $lib = new Lib;
        if ($lib->createWithSamples()) {
            return $lib->display();
        } else {
            $acquisition = new Acquisition;
            return $acquisition->display();
        }
    }
}
