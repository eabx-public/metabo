<?php

namespace App\Controllers;

use App\Libraries\Condition;
use \Ppci\Controllers\PpciController;
use App\Libraries\Extraction as Lib;
use App\Libraries\Project;
use App\Libraries\Sampling;

class Extraction extends PpciController
{

    function display()
    {
        if (strlen($_REQUEST["extraction_id"]) == 0) {
            if (isset($_COOKIE["extraction_id"])) {
                $_REQUEST["extraction_id"] = $_COOKIE["extraction_id"];
            } else {
                return defaultPage();
            }
        }
        $lib = new Lib;
        return $lib->display();
    }
    function change()
    {
        $lib = new Lib;
        return $lib->change();
    }
    function write()
    {
        $lib = new Lib;
        if ($lib->write()) {
            return $this->display();
        } else {
            return $this->change();
        }
    }
    function delete()
    {
        $lib = new Lib;
        if ($lib->delete()) {
            $condition = new Condition;
            return $condition->display();
        } else {
            return $this->change();
        }
    }
    function createWithSamples()
    {
        $lib = new Lib;
        if ($lib->createWithSamples()) {
            return $lib->display();
        } else {
            $sampling = new Sampling;
            return $sampling->display();
        }
    }
    function createFromSamplings()
    {
        $lib = new Lib;
        $lib->createFromSamplings();
        $project = new Project;
        return $project->display();
    }
}
