<?php
namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Home as LibrariesHome;

class Home extends PpciController {
protected $lib;
function __construct() {
$this->lib = new LibrariesHome();
}
function index() {
return $this->lib->index();
}
}
