<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\SampleImport as LibrariesSampleImport;

class SampleImport extends PpciController
{
    protected $lib;
    function __construct()
    {
        $this->lib = new LibrariesSampleImport();
    }
    function import()
    {
        return $this->lib->import();
    }
    function importExec()
    {
        $this->lib->importExec();
        return $this->lib->import();
    }
}
