<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Param as Lib;

class Param extends PpciController
{
    protected $lib;
    function __construct() {}
    function list(string $tablename)
    {
        $lib = new Lib($tablename);
        return $lib->list();
    }
    function change($tablename)
    {
        $lib = new Lib($tablename);
        return $lib->change();
    }
    function write($tablename)
    {
        $lib = new Lib($tablename);
        if ($lib->write()) {
            return $lib->list();
        } else {
            return $lib->change();
        }
    }
    function delete($tablename)
    {
        $lib = new Lib($tablename);
        if ($lib->delete()) {
            return $lib->list();
        } else {
            return $lib->change();
        }
    }
}
