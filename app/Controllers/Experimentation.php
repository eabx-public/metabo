<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Experimentation as Lib;
use App\Libraries\Project;

class Experimentation extends PpciController
{
    function display()
    {
        if (strlen($_REQUEST["experimentation_id"]) == 0) {
            if (isset($_COOKIE["experimentation_id"])) {
                $_REQUEST["experimentation_id"] = $_COOKIE["experimentation_id"];
            } else {
                return defaultPage();
            } 
        }
        $lib = new Lib;
        return $lib->display();
    }
    function change()
    {
        $lib = new Lib;
        return $lib->change();
    }
    function write()
    {
        $lib = new Lib;
        if ($lib->write()) {
            return $this->display();
        } else {
            return $this->change();
        }
    }
    function delete()
    {
        $lib = new Lib;
        if ($lib->delete()) {
            $project = new Project;
            return $project->display();
        } else {
            return $this->change();
        }
    }
}
