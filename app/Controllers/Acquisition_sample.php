<?php
namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Acquisition_sample as LibrariesAcquisition_sample;

class Acquisition_sample extends PpciController {
protected $lib;
function __construct() {
$this->lib = new LibrariesAcquisition_sample();
}
function addSampleToAcquisition() {
return $this->lib->addSampleToAcquisition();
}
function delete() {
if ($this->lib->delete()) {
return $this->list();
} else {
return $this->change();
}
}
function deleteMulti() {
return $this->lib->deleteMulti();
}
}
