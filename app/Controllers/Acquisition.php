<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Acquisition as Lib;
use App\Libraries\Experimentation;

class Acquisition extends PpciController
{
    function display()
    {
        if (strlen($_REQUEST["acquisition_id"]) == 0) {
            if ($_COOKIE["acquisition_id"] > 0) {
                $_REQUEST["acquisition_id"] = $_COOKIE["acquisition_id"];
            } else {
                return defaultPage();
            }
        }
        $lib = new Lib;
        return $lib->display();
    }
    function change()
    {
        $lib = new Lib;
        return $lib->change();
    }
    function write()
    {
        $lib = new Lib;
        if ($lib->write()) {
            return $this->display();
        } else {
            return $this->change();
        }
    }
    function delete()
    {
        $lib = new Lib;
        if ($lib->delete()) {
            $exp = new Experimentation;
            return $exp->display();
        } else {
            return $this->change();
        }
    }
    function sampleDelete()
    {
        $lib = new Lib;
        $lib->sampleDelete();
        return $this->display();
    }
    function addDownloadedFile()
    {
        $lib = new Lib;
        return $lib->addDownloadedFile();
    }
    function addDownloadedFileExec()
    {
        $lib = new Lib;
        return $lib->addDownloadedFileExec();
    }
    function createWithSamples()
    {
        $lib = new Lib;
        return $lib->createWithSamples();
    }
    function attachSamples()
    {
        $lib = new Lib;
        $lib->attachSamples();
        $project = new Project;
        return $project->display();
    }
}
