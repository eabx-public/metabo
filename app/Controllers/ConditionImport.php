<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\ConditionImport as LibrariesConditionImport;

class ConditionImport extends PpciController
{
    protected $lib;
    function __construct()
    {
        $this->lib = new LibrariesConditionImport();
    }
    function import()
    {
        return $this->lib->import();
    }
    function importExec()
    {
        $this->lib->importExec();
        return $this->lib->import();
    }
}
