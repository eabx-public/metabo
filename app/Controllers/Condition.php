<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Condition as Lib;
use App\Models\Experimentation;

class Condition extends PpciController
{

    function display()
    {
        if (strlen($_REQUEST["condition_id"]) == 0) {
            if ($_COOKIE["condition_id"] > 0) {
                $_REQUEST["condition_id"] = $_COOKIE["condition_id"];
            } else {
                return defaultPage();
            }
        }
        $lib = new Lib;
        return $lib->display();
    }
    function change()
    {
        $lib = new Lib;
        return $lib->change();
    }
    function write()
    {
        $lib = new Lib;
        if ($lib->write()) {
            return $this->display();
        } else {
            return $this->change();
        }
    }
    function delete()
    {
        $lib = new Lib;
        if ($lib->delete()) {
            $exp = new Experimentation;
            return $exp->display();
        } else {
            return $this->change();
        }
    }
}
