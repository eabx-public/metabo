<?php
namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\Markdown as LibrariesMarkdown;

class Markdown extends PpciController {
protected $lib;
function __construct() {
$this->lib = new LibrariesMarkdown();
}
function documentation/sshkeys_use/utiliser_ssh_keys_fr.md() {
return $this->lib->documentation/sshkeys_use/utiliser_ssh_keys_fr.md();
}
function documentation/parameters/parametres_fr.md() {
return $this->lib->documentation/parameters/parametres_fr.md();
}
function documentation/sftp/sftp_server_fr.md() {
return $this->lib->documentation/sftp/sftp_server_fr.md();
}
function documentation/miscellaneous/uuid_fr.md() {
return $this->lib->documentation/miscellaneous/uuid_fr.md();
}
}
