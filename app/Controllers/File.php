<?php

namespace App\Controllers;

use \Ppci\Controllers\PpciController;
use App\Libraries\File as LibrariesFile;
use App\Libraries\Acquisition;
use App\Libraries\AcquisitionSample;
use App\Libraries\Condition;
use App\Libraries\Experimentation;
use App\Libraries\Extraction;
use App\Libraries\Project;
use App\Libraries\Sampling;
use App\Libraries\Treatment;

class File extends PpciController
{
    protected $lib;
    protected $origin;

    function __construct()
    {
        $this->lib = new LibrariesFile();
    }
    function write($origin)
    {
        $this->initOrigin($origin);
        $this->lib->write();
        return $this->origin->display();
    }
    function delete($origin)
    {
        $this->initOrigin($origin);
        $this->lib->supprimer();
        return $this->origin->display();
    }
    function getListExternal()
    {
        return $this->lib->getListExternal();
    }
    function assignExternal()
    {
        $this->lib->assignExternal();
        $module = ucfirst($_REQUEST["modulename"]);
        if (!empty($module)) {
            $this->initOrigin($module);
            return $this->origin->display();
        } else {
            defaultPage();
        }
        
    }
    function get()
    {
        return $this->lib->get();
    }
    function getExternal()
    {
        if (! $this->lib->getExternal()) {
            foreach ($this->message->get() as $mess) {
                echo $mess;
            }
            
        }
    }
    function generateLink()
    {
        if (! $this->lib->generateLink()) {
            $project = new Project;
            return $project->display();
        }
    }
    function generateSimpleLink()
    {
        if (! $this->lib->generateSimpleLink()) {
            $project = new Project;
            return $project->display();
        }
    }

    function downloadedFileDelete()
    {
        return $this->lib->downloadedFileDelete();
    }
    function initOrigin($origin)
    {
        switch ($origin) {
            case "Acquisition":
                $this->origin = new Acquisition;
                break;
            case "AcquisitionSample":
                $this->origin = new AcquisitionSample;
                break;
            case "Condition":
                $this->origin = new Condition;
                break;
            case "Experimentation":
                $this->origin = new Experimentation;
                break;
            case "Extraction":
                $this->origin = new Extraction;
                break;
            case "Project":
                $this->origin = new Project;
                break;
            case "Sampling":
                $this->origin = new Sampling;
                break;
            case "Treatment":
                $this->origin = new Treatment;
                break;
        }
    }
}
