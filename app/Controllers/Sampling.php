<?php

namespace App\Controllers;

use App\Libraries\Condition;
use App\Libraries\Project;
use \Ppci\Controllers\PpciController;
use App\Libraries\Sampling as Lib;

class Sampling extends PpciController
{

    function display()
    {
        if (strlen($_REQUEST["sampling_id"]) == 0) {
            if (isset($_COOKIE["sampling_id"])) {
                $_REQUEST["sampling_id"] = $_COOKIE["sampling_id"];
            } else {
                return defaultPage();
            }
        }
        $lib = new Lib;
        return $lib->display();
    }
    function change()
    {
        $lib = new Lib;
        return $lib->change();
    }
    function write()
    {
        $lib = new Lib;
        if ($lib->write()) {
            return $this->display();
        } else {
            return $this->change();
        }
    }
    function delete()
    {
        $lib = new Lib;
        if ($lib->delete()) {
            $condition = new Condition;
            return $condition->display();
        } else {
            return $this->change();
        }
    }
    function generateReplicate()
    {
        $lib = new Lib;
        $lib->generateReplicate();
        return $lib->display();
    }
    function createSamplingsFromConditions()
    {
        $lib = new Lib;
        $lib->createSamplingsFromConditions();
        $project = new Project;
        return $project->display();
    }
}
