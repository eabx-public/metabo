<?php

namespace App\Controllers;

use App\Libraries\Condition;
use App\Libraries\Extraction;
use App\Libraries\Project;
use \Ppci\Controllers\PpciController;
use App\Libraries\Sample as Lib;

class Sample extends PpciController
{

    function display()
    {
        if (strlen($_REQUEST["sample_id"]) == 0) {
            if (isset($_COOKIE["sample_id"])) {
                $_REQUEST["sample_id"] = $_COOKIE["sample_id"];
            } else {
                return defaultPage();
            }
        }
        $lib = new Lib;
        return $lib->display();
    }
    function change()
    {
        $lib = new Lib;
        return $lib->change();
    }
    function write()
    {
        $lib = new Lib;
        if ($lib->write()) {
            return $this->display();
        } else {
            return $this->change();
        }
    }
    function delete()
    {
        $lib = new Lib;
        if ($lib->delete()) {
            $condition = new Condition;
            return $condition->display();
        } else {
            return $this->change();
        }
    }
    function deleteMulti()
    {
        $lib = new Lib;
        $lib->deleteMulti();
        $project = new Project;
        return $project->display();
    }
    function sendToCollec()
    {
        $lib = new Lib;
        $lib->sendToCollec();
        $project = new Project;
        return $project->display();
    }
    function createAliquot() {
        $lib = new Lib;
        $lib->createAliquot();
        $extraction = new Extraction;
        return $extraction->display();
    }
}
