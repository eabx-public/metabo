<h2>{t}Types de fichiers{/t}</h2>
<div class="row">
  <div class="col-md-6 bg-info">
    {t}Les types de fichiers permettent de caractériser les fichiers, en fonction de leur contenu ou de leur usage, par exemple : acquisition physico-chimique, résultat d'acquisition métabolomique, etc.{/t}
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    {if $rights.param == 1}
    <a href="filetypeChange?filetype_id=0">
      {t}Nouveau...{/t}
    </a>
    {/if}
    <table id="filetypeList" class="table table-bordered table-hover datatable display">
      <thead>
        <tr>
          <th></th>
          <th colspan="10" class="center">{t}Utilisable pour :{/t}</th>
          <th></th>
        </tr>
        <tr>
          <th>{t}Nom{/t}</th>
          <th>{t}Projet{/t}</th>
          <th>{t}Expérimentation{/t}</th>
          <th>{t}Condition{/t}</th>
          <th>{t}Échantillonnage{/t}</th>
          <th>{t}Extraction{/t}</th>
          <th>{t}Échantillon{/t}</th>
          <th>{t}Opération d'acquisition{/t}</th>
          <th>{t}Résultat par aliquot (après acquisition){/t}</th>
          <th>{t}Retraitement des résultats (opération globale){/t}</th>
          <th>{t}Retraitement des résultats (fichiers par aliquot){/t}</th>
          <th>{t}Ordre d'affichage{/t}</th>
        </tr>
      </thead>
      <tbody>
        {foreach $data as $row}
          <tr>
            <td nowrap>
              {if $rights.param == 1}
                <a href="filetypeChange?filetype_id={$row.filetype_id}">{$row.filetype_name}</a>
              {else}
                {$data.filetype_name}
              {/if}
            </td>
            <td class="center">{if $row.is_project == 't'}{t}oui{/t}{/if}</td>
            <td class="center">{if $row.is_experimentation == 't'}{t}oui{/t}{/if}</td>
            <td class="center">{if $row.is_condition == 't'}{t}oui{/t}{/if}</td>
            <td class="center">{if $row.is_sampling == 't'}{t}oui{/t}{/if}</td>
            <td class="center">{if $row.is_extraction == 't'}{t}oui{/t}{/if}</td>
            <td class="center">{if $row.is_sample == 't'}{t}oui{/t}{/if}</td>
            <td class="center">{if $row.is_acquisition == 't'}{t}oui{/t}{/if}</td>
            <td class="center">{if $row.is_acquisition_sample == 't'}{t}oui{/t}{/if}</td>
            <td class="center">{if $row.is_treatment == 't'}{t}oui{/t}{/if}</td>
            <td class="center">{if $row.is_treatment_as == 't'}{t}oui{/t}{/if}</td>
            <td class="center">{$row.sortorder}</td>
          </tr>
        {/foreach}
      </tbody>
    </table>
  </div>
</div>
