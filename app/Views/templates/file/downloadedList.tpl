<table class="table table-bordered table-hover datatable-nopaging display">
  <thead>
    <tr>
      <th>{t}Nom du fichier{/t}</th>
      <th>{t}Taille (Mb){/t}</th>
      <th class="center"><img src="display/images/corbeille.png" height="25"></th>
    </tr>
  </thead>
  <tbody>
    {foreach $files as $file}
      <tr>
        <td class="source">{$file.filename}</td>
        <td>{$file.filesize}</td>
        <td class="center">
          <a href="downloadedFileDelete?filename={$file.filename}&acquisition_id={$acquisition.acquisition_id}" onclick="return confirm('{t}Confirmez-vous la suppression ?{/t}');">
            <img src="display/images/corbeille.png" height="25" title="{t}Supprimer le fichier{/t}">
          </a>
        </td>
      </tr>
    {/foreach}
  </tbody>
</table>
