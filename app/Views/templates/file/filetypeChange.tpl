<h2>{t}Création - Modification d'un type de fichiers{/t}</h2>
<div class="row">
    <div class="col-md-6">
        <a href="filetypeList">{t}Retour à la liste{/t}</a>

        <form class="form-horizontal protoform" id="filetypeForm" method="post" action="filetypeWrite">
            <input type="hidden" name="moduleBase" value="filetype">
            <input type="hidden" name="filetype_id" value="{$data.filetype_id}">
            <div class="form-group">
                <label for="filetype_name" class="control-label col-md-4">
                    <span class="red">*</span> {t}Nom :{/t}
                </label>
                <div class="col-md-8">
                    <input id="filetype_name" type="text" class="form-control" name="filetype_name"
                        value="{$data.filetype_name}" autofocus required>
                </div>
            </div>
            <fieldset>
                <legend>{t}Utilisable pour :{/t}</legend>

                <div class="form-group">
                    <label for="is_project" class="control-label col-md-4">
                        {t}le projet{/t}
                    </label>
                    <div class="col-md-8" id="is_experimentation">
                        <label class="radio-inline">
                            <input type="radio" name="is_project" id="is_project1" value="1" {if
                                $data.is_project == 't'}checked{/if}>
                            {t}oui{/t}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_project" id="is_project0" value="0" {if
                                $data.is_project!= 't'}checked{/if}>
                            {t}non{/t}
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_experimentation" class="control-label col-md-4">
                        {t}l'expérimentation{/t}
                    </label>
                    <div class="col-md-8" id="is_experimentation">
                        <label class="radio-inline">
                            <input type="radio" name="is_experimentation" id="is_experimentation1" value="1" {if
                                $data.is_experimentation== 't'}checked{/if}>{t}oui{/t}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_experimentation" id="is_experimentation0" value="0" {if
                                $data.is_experimentation!= 't'}checked{/if}>{t}non{/t}
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_condition" class="control-label col-md-4">
                        {t}la condition{/t}
                    </label>
                    <div class="col-md-8" id="is_condition">
                        <label class="radio-inline">
                            <input type="radio" name="is_condition" id="is_condition1" value="1" {if
                                $data.is_condition== 't'}checked{/if}>
                            {t}oui{/t}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_condition" id="is_condition0" value="0" {if
                                $data.is_condition!= 't'}checked{/if}>
                            {t}non{/t}
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_sampling" class="control-label col-md-4">
                        {t}l'échantillonnage{/t}
                    </label>
                    <div class="col-md-8" id="is_sampling">
                        <label class="radio-inline">
                            <input type="radio" name="is_sampling" id="is_sampling1" value="1" {if
                                $data.is_sampling== 't'}checked{/if}>
                            {t}oui{/t}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_sampling" id="is_sampling0" value="0" {if
                                $data.is_sampling!= 't'}checked{/if}>
                            {t}non{/t}
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_extraction" class="control-label col-md-4">
                        {t}l'extraction{/t}
                    </label>
                    <div class="col-md-8" id="is_extraction">
                        <label class="radio-inline">
                            <input type="radio" name="is_extraction" id="is_extraction1" value="1" {if
                                $data.is_extraction== 't'}checked{/if}>
                            {t}oui{/t}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_extraction" id="is_extraction0" value="0" {if
                                $data.is_extraction!= 't'}checked{/if}>
                            {t}non{/t}
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_sample" class="control-label col-md-4">
                        {t}l'échantillon lui-même (tout type){/t}
                    </label>
                    <div class="col-md-8" id="is_sample">
                        <label class="radio-inline">
                            <input type="radio" name="is_sample" id="is_sample1" value="1" {if
                                $data.is_sample== 't'}checked{/if}>
                            {t}oui{/t}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_sample" id="is_sample0" value="0" {if
                                $data.is_sample!= 't'}checked{/if}>
                            {t}non{/t}
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_acquisition" class="control-label col-md-4">
                        {t}l'opération d'acquisition{/t}
                    </label>
                    <div class="col-md-8" id="is_acquisition">
                        <label class="radio-inline">
                            <input type="radio" name="is_acquisition" id="is_acquisition1" value="1" {if
                                $data.is_acquisition== 't'}checked{/if}>
                            {t}oui{/t}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_acquisition" id="is_acquisition0" value="0" {if
                                $data.is_acquisition!= 't'}checked{/if}>
                            {t}non{/t}
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_acquisition_sample" class="control-label col-md-4">
                        {t}la phase d'acquisition de l'aliquote{/t}
                    </label>
                    <div class="col-md-8" id="is_acquisition_sample">
                        <label class="radio-inline">
                            <input type="radio" name="is_acquisition_sample" id="is_acquisition_sample1" value="1" {if
                                $data.is_acquisition_sample== 't'}checked{/if}>{t}oui{/t}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_acquisition_sample" id="is_acquisition_sample0" value="0" {if
                                $data.is_acquisition_sample!= 't'}checked{/if}>{t}non{/t}
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="is_treatment" class="control-label col-md-4">
                        {t}le retraitement des données{/t}
                    </label>
                    <div class="col-md-8" id="is_treatment">
                        <label class="radio-inline">
                            <input type="radio" name="is_treatment" id="is_treatment1" value="1" {if
                                $data.is_treatment== 't'}checked{/if}>
                            {t}oui{/t}
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="is_treatment" id="is_treatment0" value="0" {if
                                $data.is_treatment!= 't'}checked{/if}>
                            {t}non{/t}
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="is_treatment_as" class="control-label col-md-4">
                            {t}le retraitement des données (résultats individuels par aliquot){/t}
                        </label>
                        <div class="col-md-8" id="is_treatment_as">
                            <label class="radio-inline">
                                <input type="radio" name="is_treatment_as" id="is_treatment_as1" value="1" {if
                                    $data.is_treatment_as== 't'}checked{/if}>
                                {t}oui{/t}
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="is_treatment_as" id="is_treatment_as0" value="0" {if
                                    $data.is_treatment_as!= 't'}checked{/if}>
                                {t}non{/t}
                            </label>
                        </div>
                    </div>
            </fieldset>
            <div class="form-group">
                <label for="sortOrder" class="control-label col-md-4">
                    <span class="red">*</span> {t}Ordre de tri :{/t}
                </label>
                <div class="col-md-8">
                    <input id="sortOrder" type="number" class="form-control" name="sortorder" value="{$data.sortorder}">
                </div>
            </div>

            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
                {if $data.filetype_id > 0 }
                <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
                {/if}
            </div>
            {$csrf}
        </form>
    </div>
</div>