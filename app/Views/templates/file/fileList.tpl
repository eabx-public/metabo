<script>
    $(document).ready(function () {
        $("#checkFile").change(function () {
            $(".checkFile").prop("checked", this.checked);
        });
    });
</script>
{if $fileListIsForm == 1}
<table id="fileList" class="table table-bordered table-hover datatable-searching display"
    data-order='[[4,"desc"],[1,"asc"]]'>
{else}
<table id="fileList" class="table table-bordered table-hover datatable-searching display"
    data-order='[[3,"desc"],[0,"asc"]]'>
{/if}
    <thead>
        <tr>
            {if $fileListIsForm == 1}
            <td class="center">
                <input type="checkbox" id="checkFile" class="checkFile">
            </td>
            {/if}
            <th>{t}Nom du fichier{/t}</th>
            <th>{t}Type{/t}</th>
            <th>{t}Taille (Mb){/t}</th>
            <th>{t}Date de création{/t}</th>
            <th>{t}Description{/t}</th>
            <th>{t}Supprimer{/t}</th>
        </tr>
    </thead>
    <tbody>
        {foreach $files as $file}
        <tr>
            {if $fileListIsForm == 1}
            <td class="center">
                <input type="checkbox" class="checkFile" name="files[]" value="{$file.file_id}">
            </td>
            {/if}
            <td title="{t}Télécharger le fichier{/t}">
                <a href="fileGet?file_id={$file.file_id}">{$file.filename}</a>
            </td>
            <td>{$file.filetype_name}</td>
            <td class="right">{$file.filesize_mb}</td>
            <td>{$file.filedate}</td>
            <td class="tableTextarea">{$file.comment}</td>
            <td class="center">
                <a href="file{$modulename}Delete?file_id={$file.file_id}&project_id={$project_id}&{$modulename}_id={$parent_id}"
                    onclick="return confirm('{t}Confirmez-vous la suppression ?{/t}');">
                    <img src="display/images/corbeille.png" height="25">
                </a>
            </td>
        </tr>
        {/foreach}
    </tbody>
</table>