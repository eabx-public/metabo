<script>
  $(document).ready(function() {
    $("#checkFile2").change(function() {
      $(".checkFile2").prop("checked", this.checked);
    });
  });
</script>
{if $fileListIsForm2 == 1}
<table id="fileList2" class="table table-bordered table-hover datatable-searching display" data-order='[[4,"desc"],[1,"asc"]]'>
{else}
<table id="fileList2" class="table table-bordered table-hover datatable-searching display" data-order='[[3,"desc"],[0,"asc"]]'>
{/if}
  <thead>
    <tr>
      {if $fileListIsForm2 == 1}
        <td class="center">
          <input type="checkbox" id="checkFile2" class="checkFile2" >
        </td>
      {/if}
      <th>{t}Nom du fichier{/t}</th>
      <th>{t}Type{/t}</th>
      <th>{t}Taille (Mb){/t}</th>
      <th>{t}Date de création{/t}</th>
      <th>{t}Description{/t}</th>
      <th>{t}Supprimer{/t}</th>
    </tr>
  </thead>
  <tbody>
    {foreach $files2 as $file}
      <tr>
      {if $fileListIsForm2 == 1}
          <td class="center">
            <input type="checkbox" class="checkFile" name="files[]" value="{$file.file_id}" >
          </td>
        {/if}
        <td title="{t}Télécharger le fichier{/t}">
          <a href="fileGet?file_id={$file.file_id}">{$file.filename}</a>
        </td>
        <td>{$file.filetype_name}</td>
        <td class="right">{$file.filesize_mb}</td>
        <td>{$file.filedate}</td>
        <td class="tableTextarea">{$file.comment}</td>
        <td class="center">
          <a href="file{$modulename2}Delete?file_id={$file.file_id}&project_id={$data.project_id}&{$modulename2}_id={$parent_id2}" onclick="return confirm('{t}Confirmez-vous la suppression ?{/t}');">
            <img src="display/images/corbeille.png" height="25">
          </a>
        </td>
      </tr>
      {/foreach}
  </tbody>
</table>
