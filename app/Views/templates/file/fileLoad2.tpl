<link rel="stylesheet" href="display/javascript/windows-file-explorer-tree/jQueryFileExplorer.css">
<script src="display/javascript/windows-file-explorer-tree/jQueryFileExplorer.js"></script>
<script>
	$(document).ready(function() {
		try {
			$("#filetype_id_external2").val(Cookies.get("filetype_id2"));
		} catch (Exception) {}
		$("#filetype_id_external2").change(function () {
			Cookies.set("filetype_id2", $(this).val(), { secure: true});
		});
		$("#fileForm2").submit(function(event) {
			$("#fileSpinner2").show();
		});
		$("#fileloadlink2").on("click", function () {
			$("#fileLoad2").show();
			$("#externalTree2").jQueryFileExplorer({
				root: "/",
				rootLabel: "{t}Fichiers archivés{/t}",
				script: "fileGetListExternal",
				fileScript: "fileAssignExternal?{$modulename2}_id={$parent_id2}&modulename={$modulename2}&project_id={$project_id}&fileLoadNumber=2"
			});
		});
	});
</script>
{if !empty($filetypes2)}
	<a href="#" id="fileloadlink2"><img src="display/images/download.png" height="25">
		{t}Importer un ou plusieurs fichiers{/t}
	</a>
	<div class="row" id="fileLoad2" hidden>
		<fieldset class="col-lg-6">
		<legend >{t}Importer un ou plusieurs fichiers du même type{/t}</legend>
		<form id="fileForm2" class="form-horizontal protoform" method="post"
			action="file{$modulename2}Load" enctype="multipart/form-data" >
			<input type="hidden" name="file_id" value="0">
			<input type="hidden" name="id" value="{$parent_id2}">
			<input type="hidden" name="{$modulename2}_id" value="{$parent_id2}">
			<input type="hidden" name="modulename" value="{$modulename2}">
			<input type="hidden" name="project_id" value="{$data.project_id}">
			<div class="form-group">
				<label for="filename2" class="control-label col-md-4">
					{t}Fichier(s) à importer :{/t} <br>({$extensions})
				</label>
				<div class="col-md-7">
					<input id="filename2" type="file" class="form-control"
						name="filename[]" multiple required>
				</div>
			</div>
			<div class="form-group">
				<label for="filetype_id2" class="control-label col-md-4">
					{t}Type de fichier :{/t}
				</label>
				<div class="col-md-8">
					<select id="filetype_id2" name="filetype_id2" class="form-control">
						{foreach $filetypes2 as $filetype}
							<option value="{$filetype.filetype_id}">{$filetype.filetype_name}</option>
						{/foreach}
					</select>
			</div>
			<div class="form-group">
				<label for="comment" class="control-label col-md-4">
					{t}Description :{/t} </label>
				<div class="col-md-7">
					<textarea id="comment" name="comment" class="form-control"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="filedate" class="control-label col-md-4">
					{t}Date de création du fichier :{/t} </label>
				<div class="col-md-7">
					<input id="filedate" name="filedate"
						class="form-control datetimepicker" value="{$defaultDateTime}">
				</div>
			</div>
			<div class="form-group center">
				<button type="submit" class="btn btn-primary">{t}Envoyer le fichier{/t}</button>
				<img id="fileSpinner2" src="display/images/spinner.gif" height="25" hidden >
			</div>
		{$csrf}</form>
		</fieldset>
		<fieldset class="col-lg-6">
		<legend>{t}Associer un fichier depuis l'arborescence externe{/t}</legend>
		<div class="form-horizontal">
			<div class="row">
				<div class="form-group">
					<label for="filetype_id_external2" class="control-label col-md-4">
						<span class="red">*</span>{t}Type de fichier :{/t}
					</label>
					<div class="col-md-7">
						<select id="filetype_id_external2" name="filetype_id" class="form-control">
							{foreach $filetypes2 as $filetype}
								<option value="{$filetype.filetype_id}">{$filetype.filetype_name}</option>
							{/foreach}
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div id="externalTree2"></div>
		</div>
		</fieldset>
	</div>
{else}
	<div class="messageError">{t}Aucun type de fichiers n'est associé, il n'est pas possible d'en importer.{/t}</div>
	<div><a href="filetypeList">{t}Définissez un type de fichier...{/t}</a></div>
{/if}
