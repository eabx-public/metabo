<script>
$(document).ready( function () {
  var sample_id = "{$data.sample_id}";
  try {
    if (parseInt(sample_id) > 0) {
      Cookies.set('sample_id', sample_id, { expires: 35, secure: true });
    }
  } catch (e) { }
  /**
   * Management of tabs
   */
  var moduleName = "sampleDisplay";
  var localStorage = window.localStorage;
  try {
    activeTab = localStorage.getItem(moduleName + "Tab");
  } catch (Exception) {
    activeTab = "";
  }
  try {
    if (activeTab.length > 0) {
      $("#"+activeTab).tab('show');
    }
  } catch (Exception) { }
  $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
			localStorage.setItem(moduleName + "Tab", $(this).attr("id"));
		});
});
</script>
<h2>{if $data.sample_type_id == 1}{t}Réplicat d'échantillon{/t}{elseif $data.sample_type_id == 2}{t}Extrait{/t}{elseif $data.sample_type_id == 3}{t}Aliquot{/t}{/if} {$data.sample_name}</h2>
<div class="row">
  <div class="col-md-12">
    <a href="projectDisplay?project_id={$data.project_id}"><img src="display/images/project.png" height="25">{t}Retour au projet{/t}&nbsp;{$data.project_name}</a>
    {if $data.experimentation_id > 0}
    &nbsp;
      <a href="experimentationDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}"><img src="display/images/experiment.png" height="25">
      {t}Retour à l'expérimentation{/t}&nbsp;{$data.experimentation_name}
      </a>
    {/if}
    {if $data["condition_id"] > 0}
      &nbsp;
      <a href="conditionDisplay?project_id={$data.project_id}&condition_id={$data.condition_id}">
        <img src="display/images/condition.png" height="25">{t}Retour à la condition{/t} ({$data.condition_code})
      </a>
    {/if}
    {if $data["sampling_id"] > 0}
      &nbsp;
      <a href="samplingDisplay?project_id={$sampling.project_id}&sampling_id={$data.sampling_id}">
        <img src="display/images/sampling.png" height="25">{t}Retour à l'échantillonnage{/t} {$data.sampling_code}
      </a>
    {/if}
    {if $data.extraction_id > 0}
      &nbsp;
      <a href="extractionDisplay?extraction_id={$data.extraction_id}">
        <img src="display/images/extract.png" height="25">{t}Retour à l'extraction du {$data.extraction_date}{/t}
      </a>
    {/if}
    {if $data.sample_parent_id > 0}
      &nbsp;
      <a href="sampleDisplay?project_id={$data.project_id}&sample_id={$data.sample_parent_id}">
        <img src="display/images/sample.png" height="25">{t}Accès {/t}&nbsp;{if $data.sample_type_id == 2}{t}au réplicat parent{/t}{elseif $data.sample_type_id == 3}{t}à l'extrait parent{/t}{/if}&nbsp;{$data.sample_parent_name}
      </a>
    {/if}
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <ul class="nav nav-tabs" id="myTab" role="tablist" >
          <li class="nav-item active">
              <a class="nav-link" id="tab-detail" data-toggle="tab"  role="tab" aria-controls="nav-detail" aria-selected="true" href="#nav-detail">
          <img src="display/images/zoom.png" height="25">
          {t}Détails{/t}
        </a>
      </li>
      {if $data.sample_type_id < 3}
      <li class="nav-item">
        <a class="nav-link" id="tab-sample" href="#nav-sample"  data-toggle="tab" role="tab" aria-controls="nav-sample" aria-selected="false">
          <img src="display/images/sample.png" height="25">
          {if $data.sample_type_id == 1}
          {t}Extraits rattachés{/t}
          {else}
          {t}Aliquots rattachés{/t}
          {/if}
        </a>
      </li>
      {/if}
      <li class="nav-item">
        <a class="nav-link" id="tab-file" href="#nav-file"  data-toggle="tab" role="tab" aria-controls="nav-file" aria-selected="false">
          <img src="display/images/files.png" height="25">
          {t}Fichiers{/t}
        </a>
      </li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane active in" id="nav-detail" role="tabpanel" aria-labelledby="tab-detail">
        <div class="row">
          <a href="sampleChange?sample_id={$data.sample_id}&project_id={$data.project_id}&condition_id={$data.condition_id}">
          <img src="display/images/edit.gif" height="25">&nbsp;{t}Modifier{/t}
        </a>
        </div>
        <div class="row">
          <div class="col-md-6 form-display">
            <dl class="dl-horizontal">
              <dt>{t}Code :{/t}</dt>
              <dd>{$data.sample_name}</dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>{t}Type :{/t}</dt>
              <dd>{$data.sample_type_name}</dd>
            </dl>
            {if !empty($data.parents)}
              <dl class="dl-horizontal">
                <dt>{t}Parents :{/t}</dt>
                <dd>
                  {$br = ""}
                  {foreach $data.parents as $parent}
                    <a href="sampleDisplay?project_id={$data.project_id}&sample_id={$parent.sample_id}">
                      <span class="nowrap">{$br}{$parent.sample_name}</span>
                      {$br="<br>"}
                    </a>
                  {/foreach}
                </dd>
              </dl>
            {/if}
            <dl class="dl-horizontal">
              <dt>{t}Mode de transport du réplicat d'échantillon :{/t}</dt>
              <dd>{$data.transport_method_name}</dd>
            </dl>
            {if $data.volume > 0}
            <dl class="dl-horizontal">
              <dt>{t}Volume réel :{/t}</dt>
              <dd>{$data.volume} {$data.unit_volume}</dd>
            </dl>
            {/if}
            {if $data.sampled_volume > 0}
            <dl class="dl-horizontal">
              <dt>{t}Volume initial (avant dilution ou concentration) :{/t}</dt>
              <dd>{$data.sampled_volume} {$data.unit_volume}</dd>
            </dl>
            {/if}
            {if $data.weight > 0}
            <dl class="dl-horizontal">
              <dt>{t}Masse :{/t}</dt>
              <dd>{$data.weight} {$data.unit_weight}</dd>
            </dl>
            {/if}
            <dl class="dl-horizontal">
              <dt>{t}Nature :{/t}</dt>
              <dd>{$data.sample_nature_name}</dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>{t}Type de stockage :{/t}</dt>
              <dd>{$data.storage_type_name}</dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>{t}Lieu de stockage :{/t}</dt>
              <dd>{$data.storage_location_name}</dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>{t}Commentaires :{/t}</dt>
              <dd><span class="textareaDisplay">{$data.remarks}</span></dd>
            </dl>
            <dl class="dl-horizontal">
              <dt>{t}UUID :{/t}</dt>
              <dd><span class="nowrap">{$data.uuid}</span></dd>
            </dl>
          </div>
        </div>
        {if $data["sample_type_id"] == 3 }
          <div class="row">
            <fieldset class="col-md-6">
              <legend>{t}Liste des acquisitions réalisées{/t}</legend>
                {include file="sample/acquisitionList.tpl"}
            </fieldset>
            <fieldset class="col-md-6">
              <legend>{t}Liste des traitements réalisés{/t}</legend>
                {include file="sample/treatmentList.tpl"}
            </fieldset>
          </div>
          <div class="row">
            <fieldset class="col-md-6">
              <legend>{t}Associer l'aliquot à une acquisition{/t}</legend>
              <form id="sampleSelect" method="post" action="sampleAddToAcquisition" class="form-horizontal">
                <input type="hidden" name="sample_id" value="{$data.sample_id}">
                <div class="form-group">
                  <label for="acquisition_id" class="control-label col-md-4">{t}Acquisition{/t}</label>
                  <div class="col-md-8">
                    <select id="acquisition_id" name="acquisition_id" class="form-control">
                      {foreach $acquisitions as $ana}
                        <option value="{$ana.acquisition_id}">{$ana.acquisition_date} ({$ana.machine_name})</option>
                      {/foreach}
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="is_calibration" class="control-label col-md-4">{t}Utilisé pour la calibration ?{/t}</label>
                  <div class="col-md-8" id="is_calibration">
                    <label class="radio-inline">
                      <input type="radio" name="is_calibration" id="is_calibration1" value="1">{t}oui{/t}
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="is_calibration" id="is_calibration0" value="0" checked>{t}non{/t}
                    </label>
                  </div>
                </div>
                <div class="form-group center">
                  <button id="sampleButton" type="submit" class="btn btn-primary button-valid">{t}Associer à l'acquisition{/t}</button>
                </div>
              {$csrf}</form>
            </fieldset>
          </div>
        {/if}
      </div>
      <div class="tab-pane fade" id="nav-sample" role="tabpanel" aria-labelledby="tab-sample">
        <div class="row">
          <div class="col-md-8">
            {if $rights.manage == 1}
              <a href="sampleChange?project_id={$data.project_id}&sample_id=0&sampling_id={$data.sampling_id}&sample_parent_id={$data.sample_id}">
                <img src="display/images/new.png" height="25">
                {if $data.sample_type_id == 1}
                {t}Nouvel extrait{/t}
                {elseif $data.sample_type_id == 2}
                {t}Nouvel aliquot{/t}
                {/if}
              </a>
            {/if}
            {include file="sample/sampleList.tpl"}
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-file" role="tabpanel" aria-labelledby="tab-file">
        <div class="row">
          <div class="col-md-12">
            {include file="file/fileLoad.tpl"}
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            {include file="file/fileList.tpl"}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
