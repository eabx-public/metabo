<div id="map" class="map"></div>
<div id="radar">
<img src="display/images/gps.png" height="25" title="{t}Repérez votre position !{/t}">
</div>
{include file="mapDefault.tpl"}
<script>
  var map = setMap("map");

  var long = "{$data.wgs84_x}";
  var lat = "{$data.wgs84_y}";
  var point;

  function setPosition(lat, long) {
    if (point == undefined) {
      point = L.marker([lat, long]);
      point.addTo(map).bindPopup("1");
    } else {
      point.setLatLng([lat, long]);
    }
    map.setView([lat, long]);
  }
  $(".position").change(function () {
    var lon = $("#wgs84_x").val();
    var lat = $("#wgs84_y").val();
    if (lon.length > 0 && lat.length > 0) {
      setPosition(lat, lon);
    }
  });

  if (long.length > 0 && lat.length > 0) {
    mapData.mapDefaultLong = long;
    mapData.mapDefaultLat = lat;
    point = L.marker([lat, long]);
  }
  mapDisplay(map);
  if (point != undefined) {
    point.addTo(map).bindPopup("1");
  }

  if (mapIsChange == true) {
    map.on('click', function (e) {
      setPosition(e.latlng.lat, e.latlng.lng);
      $("#wgs84_x").val(e.latlng.lng);
      $("#wgs84_y").val(e.latlng.lat);
    });
    $("#radar").click(function () {
      if (navigator && navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
          var lon = position.coords.longitude;
          var lat = position.coords.latitude;
          setPosition(lat, lon);
          $("#wgs84_x").val(lon);
          $("#wgs84_y").val(lat);
        });
      }
    });
  }


</script>
