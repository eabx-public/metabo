<script>
    $(document).ready(function () {
        var sampling_id = "{$data.sampling_id}";
        try {
            if (parseInt(sampling_id) > 0) {
                Cookies.set('sampling_id', sampling_id, { expires: 35, secure: true });
            }
        } catch (e) { }
        /**
         * Management of tabs
         */
        var moduleName = "samplingDisplay";
        var localStorage = window.localStorage;
        try {
            activeTab = localStorage.getItem(moduleName + "Tab");
        } catch (Exception) {
            activeTab = "";
        }
        try {
            if (activeTab.length > 0) {
                $("#" + activeTab).tab('show');
            }
        } catch (Exception) { }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
            localStorage.setItem(moduleName + "Tab", $(this).attr("id"));
        });
        $("#extraction_id").change(function () {
            var extractionId = $(this).val();
            if (extractionId == 0) {
                $(".newExtraction").show();
            } else {
                $(".newExtraction").hide();
            }
        });
        $("#replicateSelect").submit(function (event) {
            if (!operationConfirm()) {
                event.preventDefault();
            }
        });
        $("#form-create-replicate").submit(function (event) {
            if (!operationConfirm()) {
                event.preventDefault();
            }
        });
    });
</script>
<div class="row">
    <h2>{t 1=$data.samping_code}Échantillonnage %1 réalisé le {/t}{$data.sampling_date} ({$data.sampling_method_name})
    </h2>
    <div class="col-md-12">
        <a href="projectDisplay?project_id={$data.project_id}"><img src="display/images/project.png"
                height="25">
                {t}Retour au projet{/t}&nbsp;{$data.project_name}
            </a>
        &nbsp;
        <a href="experimentationDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}">
            <img src="display/images/experiment.png" height="25">
                {t}Retour à l'expérimentation{/t}&nbsp;{$condition.experimentation_name}
            </a>
        <a
            href="conditionDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}&condition_id={$data.condition_id}">
            <img src="display/images/condition.png" height="25">
            {t}Retour à la condition{/t} {$condition.condition_code}
        </a>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item active">
                <a class="nav-link" id="tab-detail" data-toggle="tab" role="tab" aria-controls="nav-detail"
                    aria-selected="true" href="#nav-detail">
                    <img src="display/images/zoom.png" height="25">
                    {t}Détails{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-replicate" href="#nav-replicate" data-toggle="tab" role="tab"
                    aria-controls="nav-replicate" aria-selected="false">
                    <img src="display/images/sample.png" height="25">
                    {t}Réplicats d'échantillons{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-extraction" href="#nav-extraction" data-toggle="tab" role="tab"
                    aria-controls="nav-extraction" aria-selected="false">
                    <img src="display/images/extract.png" height="25">
                    {t}Extractions{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-file" href="#nav-file" data-toggle="tab" role="tab" aria-controls="nav-file"
                    aria-selected="false">
                    <img src="display/images/files.png" height="25">
                    {t}Fichiers{/t}
                </a>
            </li>
        </ul>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane active in" id="nav-detail" role="tabpanel" aria-labelledby="tab-detail">
                <div class="row">
                    <a
                        href="samplingChange?experimentation_id={$data.experimentation_id}&project_id={$data.project_id}&condition_id={$data.condition_id}&sampling_id={$data.sampling_id}">
                        <img src="display/images/edit.gif" height="25">&nbsp;
                        {t}Modifier{/t}
                    </a>
                </div>
                <div class="col-md-6 form-display">
                    <dl class="dl-horizontal">
                        <dt>{t}Code :{/t}</dt>
                        <dd>{$data.sampling_code}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>{t}Description :{/t}</dt>
                        <dd class="textareaDisplay">{$data.sampling_description}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>{t}Méthode utilisée :{/t}</dt>
                        <dd>{$data.sampling_method_name}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>{t}date d'échantillonnage :{/t}</dt>
                        <dd>{$data.sampling_date}</dd>
                    </dl>
                    <fieldset>
                        <legend>{t}Paramètres de création des réplicats d'échantillons{/t}</legend>
                        <dl class="dl-horizontal">
                            <dt>{t}Volume par défaut :{/t}</dt>
                            <dd>{$data.default_volume}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>{t}Poids par défaut :{/t}</dt>
                            <dd>{$data.default_weight}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>{t}Unité de mesure par défaut (volume) :{/t}</dt>
                            <dd>{$data.default_unit_volume}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>{t}Unité de mesure par défaut (poids) :{/t}</dt>
                            <dd>{$data.default_unit_weight}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>{t}Code de niveau 1 :{/t}</dt>
                            <dd>{$data.level1_code}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>{t}Nombre d'occurrences à créer :{/t}</dt>
                            <dd>{$data.level1_number}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>{t}Code de niveau 2 :{/t}</dt>
                            <dd>{$data.level2_code}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>{t}Nombre d'occurrences à créer :{/t}</dt>
                            <dd>{$data.level2_number}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>{t}Séparateur :{/t}</dt>
                            <dd>{$data.separator}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>{t}Type de stockage par défaut :{/t}</dt>
                            <dd>{$data.storage_type_name}</dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>{t}Lieu de stockage par défaut :{/t}</dt>
                            <dd>{$data.storage_location_name}</dd>
                        </dl>
                    </fieldset>
                    <div class="message">
                        {t}Les réplicats d'échantillons seront nommés ainsi lors de la génération automatique :{/t}<br>
                        {t}code de la condition + séparateur + code de l'échantillonnage + séparateur + code du niveau 1 + numéro de 1 au nombre d'occurrences du niveau 1 + code du niveau 2 + séparateur + numéro de 1 au nombre d'occurrences du niveau 2{/t}
                        <br>
                        {t}Exemple : COND1-S1-A1-B1, COND1-S1-A1-B2, COND1-S1-A2-B1, COND1-S1-A2-B2{/t}
                        <br>
                        {t}Si le nombre d'occurrences de niveau 2 est inférieur à 1, le niveau 2 ne sera pas utilisé.{/t}
                        <br>
                        {t}Si le nombre d'occurrences de niveau 1 est inférieur à 1, la génération automatique n'aboutira pas{/t}
                    </div>
                    {if $data.level1_number > 0}
                    <div class="center">
                        <form id="form-create-replicate" method="post" action="samplingGenerateReplicate">
                            <input type="hidden" name="sampling_id" value="{$data.sampling_id}">
                            <button id="replicateButton" type="submit" class="btn btn-primary button-valid">
                                {t}Créer les réplicats d'échantillons{/t}
                            </button>
                            {$csrf}
                        </form>
                    </div>
                    {/if}
                </div>
            </div>
            <div class="tab-pane fade" id="nav-replicate" role="tabpanel" aria-labelledby="tab-replicate">
                <div class="row">
                    <div class="col-md-8">
                        {if $rights.manage == 1}
                        <a
                            href="sampleChange?project_id={$data.project_id}&sample_id=0&sampling_id={$data.sampling_id}">
                            <img src="display/images/new.png" height="25">
                            {t}Nouveau réplicat{/t}
                        </a>
                        {/if}
                        <form id="replicateSelect" method="post" action="extractionCreateWithSamples">
                            <input type="hidden" name="condition_id" value="{$data.condition_id}">
                            <input type="hidden" name="project_id" value="{$data.project_id}">
                            <input type="hidden" name="sampling_id" value="{$data.sampling_id}">
                            {$samples = $replicates}
                            <div class="row">
                                <div class="col-md-12">
                                   {include file="sample/sampleList.tpl"} 
                                </div>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend>
                                        {t}Pour les réplicats sélectionnés, créez les extraits et associez-les à une extraction{/t}
                                    </legend>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label for="extraction_id"
                                                class="control-label col-md-4">
                                                {t}Extraction{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <select id="extraction_id" name="extraction_id" class="form-control">
                                                    <option value="0" selected>{t}Nouvelle extraction{/t}</option>
                                                    {foreach $extractions as $extraction}
                                                    <option value="{$extraction.extraction_id}">
                                                        {$extraction.extraction_date} - {$extraction.extraction_method_name}
                                                    </option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group newExtraction">
                                            <label for="extraction_date" class="control-label col-md-4">
                                                {t}Date d'extraction :{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <input id="extraction_date" name="extraction_date"
                                                    value="{$extraction_date}" class="datepicker form-control">
                                            </div>
                                        </div>
                                        <div class="form-group newExtraction">
                                            <label for="extraction_method_id" class="control-label col-md-4">
                                                {t}Méthode d'extraction :{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="extraction_method_id"
                                                    name="extraction_method_id">
                                                    <option value="" {if $data.extraction_method_id=="" }selected{/if}>
                                                        {t}Choisissez{/t}</option>
                                                    {foreach $methods as $method}
                                                    <option value="{$method.extraction_method_id}" {if
                                                        $method.extraction_method_id==$data.extraction_method_id}selected{/if}>
                                                        {$method.extraction_method_name}
                                                    </option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="sample_nature_id" class="control-label col-md-4">
                                                {t}Nature des extraits :{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <select class="form-control" id="sample_nature_id" name="sample_nature_id">
                                                    {foreach $sample_natures as $sample_nature}
                                                    <option value="{$sample_nature.sample_nature_id}" {if
                                                        $sample_nature.sample_nature_id==$data.sample_nature_id}selected{/if}>
                                                        {$sample_nature.sample_nature_name}
                                                    </option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nbextrait" class="control-label col-md-4"> 
                                                {t}Nombre d'extraits à créer (numérotés à partir de A,B si > 1) :{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <input type="number" id="nbextrait" class="form-control nombre"
                                                    name="nbextrait" value="1">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="extractLetter" class="control-label col-md-4"> 
                                                {t}Lettres à utiliser pour numéroter les extraits, séparées par une virgule :{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <input type="text" id="nbextrait" class="form-control" name="extractLetter"
                                                    value="A,B,C,D,E">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="volume" class="control-label col-md-4"> 
                                                {t}Volume extrait (µl) :{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <input id="volume" class="form-control taux" name="volume" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="weight" class="control-label col-md-4"> 
                                                {t}ou masse extraite (mg) :{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <input id="weight" class="form-control taux" name="weight" value="">
                                            </div>
                                        </div>
                                        {if $data.level1_number > 0}
                                        <div class="form-group center">
                                            <button id="sampleButton" type="submit"
                                                class="btn btn-primary button-valid">
                                                {t}Créer les extraits{/t}
                                            </button>
                                        </div>
                                        {/if}
                                    </div>
                                </fieldset>  
                            </div>
                            {$csrf}
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade in" id="nav-extraction" role="tabpanel" aria-labelledby="tab-extraction">
                <div class="row">
                    <div class="col-md-8">
                        {if $rights.manage == 1}
                        <a
                            href="extractionChange?project_id={$data.project_id}&extraction_id=0&sampling_id={$data.sampling_id}">
                            <img src="display/images/new.png" height="25">
                            {t}Nouvelle extraction{/t}
                        </a>
                        {/if}
                        <div class="row">
                            <div class="col-md-12">
                                {include file="sample/extractionList.tpl"}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-file" role="tabpanel" aria-labelledby="tab-file">
                <div class="row">
                    <div class="col-md-12">
                        {include file="file/fileLoad.tpl"}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {include file="file/fileList.tpl"}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>