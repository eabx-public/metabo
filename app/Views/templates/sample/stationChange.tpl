<script>
    var mapIsChange = true;
</script>
<h2>{t}Création - Modification d'une station{/t}</h2>
<a href="stationList">
    <img src="display/images/list.png" height="25">
    {t}Retour à la liste{/t}
</a>
<div class="row">
    <div class="col-md-6">


        <form class="form-horizontal protoform" id="stationForm" method="post" action="stationWrite">
            <input type="hidden" name="moduleBase" value="station">
            <input type="hidden" name="station_id" value="{$data.station_id}">
            <div class="form-group">
                <label for="stationName" class="control-label col-md-4">
                    <span class="red">*</span> {t}Nom :{/t}
                </label>
                <div class="col-md-8">
                    <input id="stationName" type="text" class="form-control" name="station_name"
                        value="{$data.station_name}" autofocus required>
                </div>
            </div>
            <div class="form-group">
                <label for="project_id" class="control-label col-md-4">
                    {t}Projet de rattachement :{/t}
                </label>
                <div class="col-md-8">
                    <select id="project_id" name="project_id" class="form-control">
                        <option value="" {if $data["project_id"]=="" } selected{/if}>{t}Choisissez...{/t}</option>
                        {foreach $projects as $project}
                        <option value="{$project.project_id}" {if $project.project_id==$data.project_id} selected {/if}>
                            {$project.project_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="wgs84_x" class="control-label col-md-4">
                    {t}Longitude :{/t}
                </label>
                <div class="col-md-8">
                    <input id="wgs84_x" type="text" class="form-control taux position" name="wgs84_x"
                        value="{$data.wgs84_x}">
                </div>
            </div>
            <div class="form-group">
                <label for="wgs84_y" class="control-label col-md-4">
                    {t}Latitude :{/t}
                </label>
                <div class="col-md-8">
                    <input id="wgs84_y" type="text" class="form-control taux position" name="wgs84_y"
                        value="{$data.wgs84_y}">
                </div>
            </div>
            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
                {if $data.station_id > 0 }
                <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
                {/if}
            </div>
            {$csrf}
        </form>
    </div>
    <div class="col-md-6">
        {include file="sample/stationMap.tpl"}
    </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>