<script>
    $(document).ready(function () {
        var isForm = "{$sampleListIsForm}";
        var index = -1;
        $("#checkSample").change(function () {
            $(".checkSample").prop("checked", this.checked);
        });
    });
</script>

<table id="sampleList" class="table table-bordered table-hover datatable-export-paging display" data-order={if
    $sampleListIsForm==1}'[[1, "asc" ]]'{else}'[[0, "asc" ]]'{/if}>
    <thead>
        <tr>
            {if $sampleListIsForm == 1}
            <th class="center">
                <input type="checkbox" id="checkSample" class="checkSample">
            </th>
            {/if}
            <th>{t}Code{/t}</th>
            {if $showAllTypes == 1}<th>{t}Type{/t}</th>{/if}
            <th>{t}Volume réel{/t}</th>
            <th>{t}Volume initial{/t}</th>
            <th>{t}Masse{/t}</th>
            <th>{t}Parents{/t}</th>
            <th>{t}Condition{/t}</th>
            <th>{t}Méthode de transport{/t}</th>
            <th>{t}Type{/t}</th>
            <th>{t}Type de stockage{/t}</th>
            <th>{t}Lieu de stockage{/t}</th>
            {if $sample_type_id == 3}
            <th>{t}Date d'acquisition{/t}</th>
            <th>{t}Date de traitement{/t}</th>
            {/if}
            <th>{t}Commentaires{/t}</th>
            <th>{t}UUID{/t}</th>
        </tr>
    </thead>
    <tbody>
        {foreach $samples as $sample}
        <tr>
            {if $sampleListIsForm == 1}
            <td class="center">
                <input type="checkbox" class="checkSample" name="samples[]" value="{$sample.sample_id}">
            </td>
            {/if}
            <td class="center" nowrap>
                <a
                    href="sampleDisplay?project_id={$sample.project_id}&condition_id={$sample.condition_id}&sample_id={$sample.sample_id}">
                    {$sample.sample_name}
                </a>
            </td>
            {if $showAllTypes == 1}<td>{$sample.sample_type_name}</td>{/if}
            <td class="right">{if $sample.volume > 0}{$sample.volume} {$sample.unit_volume}{/if}</td>
            <td class="right">{if $sample.sampled_volume > 0}{$sample.sampled_volume} {$sample.unit_volume}{/if}</td>
            <td class="right">{if $sample.weight > 0}{$sample.weight} {$sample.unit_weight}{/if}</td>
            <td>
                {foreach $sample.parents as $parent}
                <a
                    href="sampleDisplay?project_id{$sample.project_id}&condition_id={$sample.condition_id}&sample_id={$parent.sample_id}">
                    <span class="nowrap">{$parent.sample_name}</span>
                </a>
                {/foreach}
            </td>
            <td nowrap>
                <a href="conditionDisplay?project_id{$sample.project_id}&condition_id={$sample.condition_id}">
                    {$sample.condition_code}
                </a>
            </td>
            <td>{$sample.transport_method_name}</td>
            <td>{$sample.sample_type_name}</td>
            <td>{$sample.storage_type_name}</td>
            <td>{$sample.storage_location_name}</td>
            {if $sample_type_id == 3}
            <td>
                {if $sample.acquisition_id > 0}
                <a
                    href="acquisitionDisplay?acquisition_id={$sample.acquisition_id}&project_id={$sample.project_id}">{$sample.acquisition_date}</a>
                {/if}
            </td>
            <td>
                {if $sample.treatment_id > 0}
                <a
                    href="treatmentDisplay?acquisition_id={$sample.treatment_id}&project_id={$sample.project_id}">{$sample.treatment_date}</a>
                {/if}
            </td>
            {/if}
            <td class="tableTextarea">{$sample.remarks}</td>
            <td nowrap>{$sample.uuid}</td>
        </tr>
        {/foreach}
    </tbody>
</table>