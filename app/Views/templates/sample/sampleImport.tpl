<script>
  $(document).ready(function() {
    var project_id = "{$project_id}";
    try {
      if (parseInt(project_id) > 0) {
        Cookies.set('project_id', project_id, { expires: 35, secure: true });
      }
    } catch (e) { }
    $("#project_id").change(function() {
      $("#projectSelect").submit();
    });
    $("#importForm").submit (function(event){
      var extraction_id = $("#extraction_id").val();
      var condition_id = $("#condition_id").val();
      if (extraction_id > 0 && condition_id > 0) {
        event.preventDefault();
        alert("{t}Il n'est pas possible de rattacher les échantillons à la fois à un échantillonnage et à une extraction{/t}");
      }
    });
  });
</script>
<h2>{t}Importer des échantillons{/t}</h2>
<div class="row">
  <div class="col-md-6 bg-info">
    {t}Le fichier d'import doit être au format CSV, et contenir les champs suivants :{/t}
    <ul>
      <li><b>sample_name</b>: {t}Nom de l'échantillon (obligatoire){/t}</li>
      <li><b>parent_name</b>: {t}Nom de l'échantillon parent, s'il s'agit d'un échantillon dérivé{/t}</li>
      <li><b>volume</b>: {t}Volume de l'échantillon{/t}</li>
      <li><b>weight</b>: {t}Poids de l'échantillon{/t}</li>
      <li><b>unit_volume</b>: {t}Unité de mesure du volume{/t}</li>
      <li><b>unit_weight</b>: {t}Unité de mesure du poids{/t}</li>
      <li><b>uuid</b>: {t}UUID de l'échantillon, si connu{/t}</li>
    </ul>
    {t}Voici les règles d'importation :{/t}
    <ul>
      <li>{t}Si le nom de l'échantillon existe déjà pour le projet considéré, l'échantillon n'est pas traité{/t}</li>
      <li>{t}Si le parent ne peut être retrouvé à partir de son nom, une erreur est déclenchée, et l'import est annulé{/t}</li>
    </ul>
  </div>
</div>
{if !empty($messages)}
<div class="row">
  <fieldset class="col-md-6">
    <legend>{t}Résultats de l'importation{/t}</legend>
    <table class="table datatable table-bordered table-hover display">
      <thead>
        <tr>
        <th>{t}N° de ligne{/t}</th>
        <th>{t}Message{/t}</th>
      </tr>
      </thead>
      <tbody>
        {foreach $messages as $mes}
        <tr>
          <td class="center">{$mes.line}</td>
          <td class="tableTextarea">{$mes.message}</td>
        </tr>
        {/foreach}
      </tbody>
    </table>
  </fieldset>
</div>
{/if}
<div class="row">
  <div class="col-md-6 form-horizontal">
    <form id="projectSelect" method="get" action="sampleImport">
      <div class="form-group">
        <label for="project_id" class="control-label col-md-4">
          {t}Projet :{/t}
        </label>
        <div class="col-md-6">
          <select name="project_id" id="project_id" class="form-control">
            {foreach $projects as $project}
              <option value="{$project.project_id}" {if $project.project_id == $project_id}selected{/if}>
                {$project.project_name}
              </option>
            {/foreach}
          </select>
        </div>
        <div class="col-md-2">
          <button type="submit" class="btn btn-primary">{t}Sélectionner{/t}</button>
        </div>
      </div>
    {$csrf}</form>

    <form id="importForm" method="post" action="sampleImportExec" enctype="multipart/form-data" >
      <input type="hidden" name="module" value="">
      <input type="hidden" name="project_id" value="{$project_id}">
      <div class="form-group">
        <label for="sample_type_id" class="control-label col-md-4">
          {t}Types d'échantillons :{/t}
        </label>
        <div class="col-md-8">
          <select class="form-control" id="sample_type_id" name="sample_type_id">
            {foreach $sampleTypes as $sampleType}
              <option value="{$sampleType.sample_type_id}" {if $sampleType.sample_type_id == $sample_type_id}selected{/if}>
                {$sampleType.sample_type_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="transport_method_id" class="control-label col-md-4"> 
          {t}Méthode de transport des échantillons :{/t}
        </label>
        <div class="col-md-8">
          <select class="form-control" id="transport_method_id" name="transport_method_id">
            <option value="" selected>{t}Choisissez...{/t}</option>
            {foreach $transport_methods as $transport_method}
              <option value="{$transport_method.transport_method_id}">
                {$transport_method.transport_method_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="condition_id" class="control-label col-md-4">
          {t}Rattachement à un échantillonnage :{/t}
        </label>
        <div class="col-md-8">
          <select class="form-control" id="condition_id" name="condition_id">
            <option value="0" {if $condition_id == 0} selected{/if}>{t}Choisissez...{/t}</option>
            {foreach $conditions as $condition}
              <option value="{$condition.condition_id}" {if $condition.condition_id == $condition_id}selected{/if}>
                {$condition.experimentation_name} - {$condition.sampling_method_name} - {t}du{/t} {$condition.sampling_date}
              </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="extraction_id" class="control-label col-md-4">
          {t}Rattachement à une extraction :{/t}
        </label>
        <div class="col-md-8">
          <select class="form-control" id="extraction_id" name="extraction_id">
            <option value="0" {if $extraction_id == 0} selected{/if}>{t}Choisissez...{/t}</option>
            {foreach $extractions as $extraction}
              <option value="{$extraction.extraction_id}" {if $extraction.extraction_id == $extraction_id}selected{/if}>
                {t}échantillonnage{/t} {$extraction.experimentation_name},{$extraction.sampling_method_name}, {t}du{/t} {$extraction.sampling_date} - {t}Extraction du{/t} {$extraction.extraction_date} ({$extraction.extraction_method_name})
              </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="filename" class="control-label col-md-4">
          {t}Fichier à importer :{/t}
        </label>
        <div class="col-md-8">
          <input id="filename" type="file" class="form-control"	name="filename" required>
        </div>
      </div>
      <div class="form-group">
        <label for="separator" class="control-label col-md-4">
          {t}Séparateur utilisé :{/t}
        </label>
        <div class="col-md-8">
          <select id="separator" name="separator">
            <option value="," {if $separator == ","}selected{/if}>{t}Virgule{/t}</option>
            <option value=";" {if $separator == ";"}selected{/if}>{t}Point-virgule{/t}</option>
            <option value="tab" {if $separator == "tab"}selected{/if}>{t}Tabulation{/t}</option>
          </select>
        </div>
      </div>
      <div class="form-group center">
        <button type="submit" class="btn btn-primary">{t}Déclencher l'import{/t}</button>
      </div>
    {$csrf}</form>
  </div>
</div>
