<h2>{t}Création - Modification d'un type d'échantillon{/t}</h2>
<a href="sampleTypeList"><img src="display/images/list.png" height="25">{t}Retour à la liste{/t}</a>
<div class="row">
    <div class="col-md-6">
        <form class="form-horizontal protoform" id="paramForm" method="post" action="sampleTypeWrite">
            <input type="hidden" name="moduleBase" value="sampleType">
            <input type="hidden" name="sample_type_id" value="{$data.sample_type_id}">
            <div class="form-group">
                <label for="paramName" class="control-label col-md-4">
                    <span class="red">*</span> {t}Libellé :{/t}
                </label>
                <div class="col-md-8">
                    <input id="paramName" type="text" class="form-control" name="sample_type_name"
                        value="{$data.sample_type_name}" autofocus required>
                </div>
            </div>
            <div class="form-group">
                <label for="collec_type_name" class="control-label col-md-4">
                    <span class="red">*</span> {t}Nom du type d'échantillon dans l'instance Collec-Science :{/t}
                </label>
                <div class="col-md-8">
                    <input id="collec_type_name" type="text" class="form-control" name="collec_type_name"
                        value="{$data.collec_type_name}">
                </div>
            </div>
            <div class="form-group">
                <label for="sortOrder" class="control-label col-md-4">
                    <span class="red">*</span> {t}Ordre de tri :{/t}
                </label>
                <div class="col-md-8">
                    <input id="sortOrder" type="number" class="form-control" name="sortorder" value="{$data.sortorder}">
                </div>
            </div>
            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
                {if $data.$fieldid > 0 }
                <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
                {/if}
            </div>
            {$csrf}
        </form>
    </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>