<h2>{t}Liste des types d'échantillons{/t}</h2>
<div class="row">
	<div class="col-md-6">
		<table id="sampletypeList" class="table table-bordered table-hover datatable display" data-order='[["1","asc"]]'>
			<thead>
				<tr>
					<th>{t}Id{/t}</th>
					<th>{t}Libellé{/t}</th>
          <th>{t}Nom du type d'échantillon dans l'instance Collec-Science{/t}</th>
					<th>{t}Ordre de tri{/t}</th>
				</tr>
			</thead>
			<tbody>
				{foreach $data as $row}
				<tr>
					<td class="center">{$row.sample_type_id}</td>
					<td>
						{if $rights.param == 1}
						<a href="sampleTypeChange?sample_type_id={$row.sample_type_id}">
							{$row.sample_type_name}
						</a>
						{else}
						{$row.sample_type_name}
						{/if}
					</td>
          <td>{$row.collec_type_name}</td>
					<td class="center">{$row["sortorder"]}</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
</div>
