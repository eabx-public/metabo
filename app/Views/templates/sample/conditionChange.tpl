<h2>{t}Création - Modification d'une condition{/t}</h2>
<div class="row">
    <div class="col-md-12">
        <a href="projectDisplay?project_id={$experimentation.project_id}"><img src="display/images/display.png"
                height="25">
            {t}Retour au projet{/t}&nbsp;{$experimentation.project_name}
        </a>
        &nbsp;
        <a
            href="experimentationDisplay?project_id={$experimentation.project_id}&experimentation_id={$data.experimentation_id}"><img
                src="display/images/display-blue.png" height="25">
            {t}Retour à l'expérimentation{/t}&nbsp;{$experimentation.experimentation_name}
        </a>
        {if $data.condition_id > 0}
        &nbsp;
        <a
            href="conditionDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}&condition_id={$data.condition_id}">
            <img src="display/images/display-green.png" height="25">
            {t}Retour au détail{/t}
        </a>
        {/if}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <form class="form-horizontal protoform" id="conditionForm" method="post" action="conditionWrite">
            <input type="hidden" name="moduleBase" value="condition">
            <input type="hidden" name="condition_id" value="{$data.condition_id}">
            <input type="hidden" name="project_id" value="{$experimentation.project_id}">
            <input type="hidden" name="experimentation_id" value="{$data.experimentation_id}">
            <div class="form-group">
                <label for="condition_code" class="control-label col-md-4">
                    <span class="red">*</span>{t}Code de la condition :{/t}
                </label>
                <div class="col-md-8">
                    <input id="condition_code" class="form-control col-md-8" name="condition_code"
                        value="{$data.condition_code}" required>
                </div>
            </div>
            <div class="form-group">
                <label for="matrice_type_id" class="control-label col-md-4">
                    {t}Type de matrice :{/t}
                </label>
                <div class="col-md-8">
                    <select class="form-control" id="matrice_type_id" name="matrice_type_id">
                        <option value="" {if $data.matrice_type_id=="" }selected{/if}>{t}Choisissez...{/t}</option>
                        {foreach $types as $type}
                        <option value="{$type.matrice_type_id}" {if
                            $type.matrice_type_id==$data.matrice_type_id}selected{/if}>
                            {$type.matrice_type_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="condition_description" class="control-label col-md-4">
                    {t}Description :{/t}
                </label>
                <div class="col-md-8">
                    <textarea id="condition_description" name="condition_description" class="form-control textarea-edit"
                        rows="10">{$data.condition_description}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="station_id" class="control-label col-md-4">
                    {t}Station :{/t}
                </label>
                <div class="col-md-8">
                    <select class="form-control" id="station_id" name="station_id">
                        <option value="" {if $data.station_id=="" }selected{/if}>{t}Choisissez...{/t}</option>
                        {foreach $stations as $station}
                        <option value="{$station.station_id}" {if $station.station_id==$data.station_id}selected{/if}>
                            {$station.station_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>

            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
                {if $data.condition_id > 0 }
                <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
                {/if}
            </div>
            {$csrf}
        </form>
    </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>