<table id="treatmentList" class="table table-bordered table-hover datatable display">
  <thead>
    <tr>
      <th>{t}Date de traitement{/t}</th>
      <th>{t}Logiciel utilisé{/t}</th>
      <th class="center"><img src="display/images/corbeille.png" height="25"></th>
    </tr>
  </thead>
  <tbody>
    {foreach $treatments as $treatment}
      <tr>
        <td class="center">
          <a href="treatmentDisplay?sample_id={$treatment.sample_id}&treatment_id={$treatment.treatment_id}">
            {$treatment.treatment_date}
          </a>
        </td>
        <td>{$treatment.software_name}</td>
        <td class="center">
          <a href="sampleTreatmentDelete?sample_id={$data.sample_id}&treatment_sample_id={$treatment.treatment_sample_id}" onclick="return confirm('{t}Confirmez-vous la suppression ?{/t}');" title="{t}Supprimer l'aliquot du traitement{/t}">
            <img src="display/images/corbeille.png" height="25">
        </a>
      </tr>
    {/foreach}
  </tbody>
</table>
