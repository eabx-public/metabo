  <h2>{t 1=$experimentation.experimentation_name}Importer des conditions dans l'expérimentation %1{/t}</h2>
  <div class="row">
      <div class="col-md-12">
    <a href="projectDisplay?project_id={$experimentation.project_id}"><img src="display/images/project.png" height="25">{t}Retour au projet{/t}&nbsp;{$experimentation.project_name}</a>
    &nbsp;
    <a href="experimentationDisplay?project_id={$experimentation.project_id}&experimentation_id={$experimentation.experimentation_id}"><img src="display/images/experiment.png" height="25">{t}Retour à l'expérimentation{/t}&nbsp;{$experimentation.experimentation_name}</a>
  </div>
  </div>

  <div class="row">
    <div class="col-md-6 bg-info">
      {t}Le fichier d'import doit être au format CSV et encodé en UTF-8 (sans BOM), et contenir les champs suivants :{/t}
      <ul>
        <li><b>condition_code</b>: {t}Code de la condition (obligatoire){/t}</li>
        <li><b>condition_description</b>: {t}Description de la condition{/t}</li>
        <li><b>matrice_type_id</b>: {t}Identifiant du type de matrice, ou :{/t}</li>
        <li><b>matrice_type_name</b>: {t}Nom de la matrice{/t}</li>
        <li><b>station_id</b>: {t}Identifiant informatique de la station, ou :{/t}</li>
        <li><b>station_name</b>: {t}Nom de la station{/t}</li>
      </ul>
      {t}Voici les règles d'importation :{/t}
      <ul>
        <li>{t}Si le nom de la condition pour l'expérimentation considérée existe, la condition est mise à jour{/t}</li>
        <li>{t}Si la matrice est renseignée (id ou name) mais n'existe pas dans la base de données, l'importation échouera{/t}</li>
        <li>{t}Si la station est renseignée (id ou name) mais n'existe pas dans la base de données, l'importation échouera{/t}</li>
      </ul>
    </div>
  </div>

  <div class="row">

  </div>
  {if !empty($messages)}
  <div class="row">
    <fieldset class="col-md-6">
      <legend>{t}Résultats de l'importation{/t}</legend>
      <table class="table datatable table-bordered table-hover display">
        <thead>
          <tr>
          <th>{t}N° de ligne{/t}</th>
          <th>{t}Message{/t}</th>
        </tr>
        </thead>
        <tbody>
          {foreach $messages as $mes}
          <tr>
            <td class="center">{$mes.line}</td>
            <td class="tableTextarea">{$mes.message}</td>
          </tr>
          {/foreach}
        </tbody>
      </table>
    </fieldset>
  </div>
  {/if}
  <div class="row">
    <div class="col-md-6 form-horizontal"> 
      <form id="importForm" method="post" action="conditionImportExec" enctype="multipart/form-data" >
        <input type="hidden" name="project_id" value="{$project_id}">
        <input type="hidden" name="experimentation_id" value="{$experimentation_id}">
        <div class="form-group">
          <label for="filename" class="control-label col-md-4">{t}Fichier à importer :{/t}</label>
          <div class="col-md-8">
            <input id="filename" type="file" class="form-control" name="filename" required>
          </div>
        </div>
        <div class="form-group">
          <label for="separator" class="control-label col-md-4">{t}Séparateur utilisé :{/t}</label>
          <div class="col-md-8">
            <select id="separator" name="separator" class="form-control">
              <option value="," {if $separator == ","}selected{/if}>{t}Virgule{/t}</option>
              <option value=";" {if $separator == ";"}selected{/if}>{t}Point-virgule{/t}</option>
              <option value="tab" {if $separator == "tab"}selected{/if}>{t}Tabulation{/t}</option>
            </select>
          </div>
        </div>
        <div class="form-group center">
          <button type="submit" class="btn btn-primary">{t}Déclencher l'import{/t}</button>
        </div>
      {$csrf}</form>
    </div>
  </div>
  