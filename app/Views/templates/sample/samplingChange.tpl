<h2>{t}Création - Modification d'un échantillonnage{/t}</h2>
<div class="row">
  <div class="col-md-12">
     <a href="projectDisplay?project_id={$condition.project_id}"><img src="display/images/display.png" height="25">{t}Retour au projet{/t}&nbsp;{$condition.project_name}</a>
    &nbsp;
    <a href="experimentationDisplay?project_id={$condition.project_id}&experimentation_id={$condition.experimentation_id}"><img src="display/images/display-blue.png" height="25">{t}Retour à l'expérimentation{/t}&nbsp;{$experimentation.experimentation_name}</a>
    &nbsp;
      <a href="conditionDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}&condition_id={$data.condition_id}">
        <img src="display/images/condition.png" height="25">{t}Retour à la condition{/t}
      </a>
    {if $data.sampling_id > 0}
      &nbsp;
      <a href="samplingDisplay?project_id={$condition.project_id}&experimentation_id={$condition.experimentation_id}&condition_id={$data.condition_id}&sampling_id={$data.sampling_id}">
        <img src="display/images/display-green.png" height="25">{t}Retour au détail{/t}
      </a>
    {/if}
  </div>
</div>
{if $data.sample_number > 0}
  <div class="row">
    <div class="col-md-12">
      {t}Nombre de réplicats d'échantillons rattachés à l'échantillonnage : {/t}{$data.sample_number}
    </div>
  </div>
{/if}
<div class="row">
  <div class="col-md-6">
    <form class="form-horizontal protoform" id="samplingForm" method="post" action="samplingWrite">
      <input type="hidden" name="moduleBase" value="sampling">
      <input type="hidden" name="condition_id" value="{$data.condition_id}">
      <input type="hidden" name="project_id" value="{$project_id}">
      <input type="hidden" name="sampling_id" value="{$data.sampling_id}">
      <div class="form-group">
        <label for="sampling_code" class="control-label col-md-4"> 
          <span class="red">*</span>{t}Code :{/t}
        </label>
        <div class="col-md-8">
          <input id="sampling_code" class="form-control" name="sampling_code" value="{$data.sampling_code}" required>
        </div>
      </div>
      <div class="form-group">
        <label for="sampling_description" class="control-label col-md-4">
          {t}Description :{/t}
        </label>
        <div class="col-md-8">
          <textarea id="sampling_description" name="sampling_description" class="form-control textarea-edit" rows="3">{$data.sampling_description}</textarea>
        </div>
      </div>
      <div class="form-group">
        <label for="sampling_method_id" class="control-label col-md-4">
          {t}Méthode utilisée :{/t}
        </label>
        <div class="col-md-8">
          <select class="form-control" id="sampling_method_id" name="sampling_method_id">
            <option value="" {if $data.sampling_method_id == ""}selected{/if}>{t}Choisissez{/t}</option>
            {foreach $methods as $method}
              <option value="{$method.sampling_method_id}" {if $method.sampling_method_id == $data.sampling_method_id}selected{/if}>
                {$method.sampling_method_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="sampling_date" class="control-label col-md-4"> 
          {t} date d'échantillonnage :{/t}
        </label>
        <div class="col-md-8">
          <input id="sampling_date" class="datepicker form-control" name="sampling_date" value="{$data.sampling_date}">
        </div>
      </div>
      <fieldset>
      <legend>{t}Paramètres de création des réplicats d'échantillons{/t}</legend>
      <div class="form-group">
        <label for="default_volume" class="control-label col-md-4"> {t}Volume par défaut :{/t}</label>
        <div class="col-md-8">
          <input id="default_volume" class="form-control" name="default_volume" value="{$data.default_volume}" >
        </div>
      </div>
      <div class="form-group">
        <label for="default_weight" class="control-label col-md-4"> {t}Poids par défaut :{/t}</label>
        <div class="col-md-8">
          <input id="default_weight" class="form-control" name="default_weight" value="{$data.default_weight}" >
        </div>
      </div>
      <div class="form-group">
        <label for="default_unit" class="control-label col-md-4"> {t}Unité (volume) par défaut :{/t}</label>
        <div class="col-md-8">
          <input id="default_unit" class="form-control" name="default_unit_volume" value="{$data.default_unit_volume}" >
        </div>
      </div>
      <div class="form-group">
        <label for="default_unit" class="control-label col-md-4"> {t}Unité (poids) par défaut :{/t}</label>
        <div class="col-md-8">
          <input id="default_unit" class="form-control" name="default_unit_weight" value="{$data.default_unit_weight}" >
        </div>
      </div>
      <div class="form-group">
        <label for="level1_code" class="control-label col-md-4"> {t}Code du niveau 1 :{/t}</label>
        <div class="col-md-8">
          <input id="level1_code" class="form-control" name="level1_code" value="{$data.level1_code}" >
        </div>
      </div>
      <div class="form-group">
        <label for="level1_number" class="control-label col-md-4"> 
          <span class="red">*</span>{t}Nombre d'occurrences à créer pour le niveau 1 :{/t}
        </label>
        <div class="col-md-8">
          <input id="level1_number" class="form-control number" name="level1_number" value="{$data.level1_number}" required>
        </div>
      </div>
      <div class="form-group">
        <label for="level2_code" class="control-label col-md-4"> {t}Code du niveau 2 :{/t}</label>
        <div class="col-md-8">
          <input id="level2_code" class="form-control" name="level2_code" value="{$data.level2_code}" >
        </div>
      </div>
      <div class="form-group">
        <label for="level2_number" class="control-label col-md-4"> 
          {t}Nombre d'occurrences à créer pour le niveau 2 :{/t}
        </label>
        <div class="col-md-8">
          <input id="level2_number" class="form-control number" name="level2_number" value="{$data.level2_number}" >
        </div>
      </div>
      <div class="form-group">
        <label for="separator" class="control-label col-md-4"> {t}Séparateur :{/t}</label>
        <div class="col-md-8">
          <input id="separator" class="form-control" name="separator" value="{$data.separator}" >
        </div>
      </div>
            <div class="form-group">
        <label for="default_storage_type" class="control-label col-md-4">
          {t}Type de stockage par défaut :{/t}
        </label>
        <div class="col-md-8">
          <select class="form-control" id="default_storage_type" name="default_storage_type">
            <option value="" {if $data.storage_type_id == ""}selected{/if}>{t}Choisissez...{/t}</option>
            {foreach $storage_types as $storage_type}
              <option value="{$storage_type.storage_type_id}" {if $storage_type.storage_type_id == $data.default_storage_type}selected{/if}>
                {$storage_type.storage_type_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="default_storage_location" class="control-label col-md-4">
          {t}Lieu de stockage par défaut :{/t}
        </label>
        <div class="col-md-8">
          <select class="form-control" id="default_storage_location" name="default_storage_location">
            <option value="" {if $data.storage_location_id == ""}selected{/if}>{t}Choisissez...{/t}</option>
            {foreach $storage_locations as $storage_location}
              <option value="{$storage_location.storage_location_id}" {if $storage_location.storage_location_id == $data.default_storage_location}selected{/if}>
                {$storage_location.storage_location_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>

      </fieldset>

      <div class="form-group center">
        <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
        {if $data.sampling_id > 0 }
        <button class="btn btn-danger button-delete">{t}Supprimer{/t} {if $data.sample_number > 0}
          {t 1=$data.sample_number} (y compris %1 réplicats d'échantillons){/t}
          {/if}</button>
        {/if}
      </div>
    {$csrf}</form>
  </div>
</div>
<div class="row">
  <div class="message col-md-10 col-lg-6">
  {t}Les réplicats d'échantillons seront nommés ainsi lors de la génération automatique :{/t}<br>
  {t}code de la condition + séparateur + code de l'échantillonnage + séparateur + code du niveau 1  + numéro de 1 au nombre d'occurrences du niveau 1 + code du niveau 2 + séparateur + numéro de 1 au nombre d'occurrences du niveau 2{/t}
  <br>
  {t}Exemple : COND1-S1-A1-B1, COND1-S1-A1-B2, COND1-S1-A2-B1, COND1-S1-A2-B2{/t}
  <br>
  {t}Si le nombre d'occurrences de niveau 2 est inférieur à 1, le niveau 2 ne sera pas utilisé.{/t}
  <br>
  {t}Si le nombre d'occurrences de niveau 1 est inférieur à 1, la génération automatique n'aboutira pas{/t}
  </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>
