<h2>{t}Stations{/t}</h2>
<div class="row">
    <div class="col-md-12">
        {if $rights.manage == 1}
        <a href="stationChange?station_id=0">
            <img src="display/images/new.png" height="25">
            {t}Nouveau...{/t}
        </a>
        {/if}
        <table id="stationList" class="table table-bordered table-hover datatable-export-paging display">
            <thead>
                <tr>
                    <th>{t}Id{/t}</th>
                    <th>{t}Nom{/t}</th>
                    <th>{t}Projet{/t}</th>
                    <th>{t}Longitude{/t}</th>
                    <th>{t}Latitude{/t}</th>
                </tr>
            </thead>
            <tbody>
                {section name=lst loop=$data}
                <tr>
                    <td>{$data[lst].station_id}</td>
                    <td>
                        {if $rights.manage == 1}
                        <a href="stationChange?station_id={$data[lst].station_id}">
                            {$data[lst].station_name}
                        </a>
                        {else}
                        {$data[lst].station_name}
                        {/if}
                    </td>
                    <td>{$data[lst].project_name}</td>
                    <td>{$data[lst].wgs84_x}</td>
                    <td>{$data[lst].wgs84_y}</td>
                </tr>
                {/section}
            </tbody>
        </table>
    </div>
</div>

{if $rights["manage"] == 1}
<div class="row col-md-6">
    <fieldset>
        <legend>{t}Importer des stations depuis un fichier CSV{/t}</legend>
        <form class="form-horizontal protoform" id="metadataImport" method="post" action="stationImport"
            enctype="multipart/form-data">
            <div class="form-group">
                <label for="upfile" class="control-label col-md-4">
                    <span class="red">*</span> {t}Nom du fichier à importer (CSV) :{/t}
                </label>
                <div class="col-md-8">
                    <input type="file" name="upfile" required>
                </div>
            </div>
            <div class="form-group">
                <label for="separator" class="control-label col-md-4">
                    <span class="red">*</span> {t}Séparateur :{/t}
                </label>
                <div class="col-md-8">
                    <select id="separator" class="form-control" name="separator">
                        <option value=";">{t}Point-virgule{/t}</option>
                        <option value=",">{t}Virgule{/t}</option>
                        <option value="t">{t}Tabulation{/t}</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="project_id" class="control-label col-md-4">
                    {t}Projet éventuel de rattachement :{/t}
                </label>
                <div class="col-md-8">
                    <select id="project_id" name="project_id" class="form-control">
                        <option value="" {if $data["project_id"]=="" } selected{/if}>{t}Choisissez...{/t}</option>
                        {foreach $projects as $project}
                        <option value="{$project.project_id}" {if $project.project_id==$data.project_id} selected {/if}>
                            {$project.project_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group center">
                <button type="submit" class="btn btn-primary">{t}Importer les stations{/t}</button>
            </div>
            <div class="bg-info">
                {t}Description du fichier :{/t}
                <ul>
                    <li>{t}name : nom de la station{/t}</li>
                    <li>{t}wgs84_x : longitude du point en projection WGS84, sous forme numérique (séparateur : point){/t}</li>
                    <li>{t}wgs84_y : latitude du point{/t}</li>
                </ul>
            </div>
            {$csrf}
        </form>
    </fieldset>
</div>
{/if}