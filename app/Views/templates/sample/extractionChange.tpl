<h2>{t}Création - Modification d'une extraction{/t}</h2>
<div class="row">
    <div class="col-md-12">
        <a href="projectDisplay?project_id={$sampling.project_id}"><img src="display/images/display.png" height="25">
            {t}Retour au projet{/t}&nbsp;{$sampling.project_name}
        </a>
        &nbsp;
        <a
            href="experimentationDisplay?project_id={$sampling.project_id}&experimentation_id={$sampling.experimentation_id}"><img
                src="display/images/experiment.png" height="25">
            {t}Retour à l'expérimentation{/t}&nbsp;{$sampling.experimentation_name}
        </a>
        &nbsp;
        <a
            href="conditionDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}&condition_id={$sampling.condition_id}">
            <img src="display/images/condition.png" height="25">
            {t}Retour à la condition{/t} {$sampling.condition_code}
        </a>
        &nbsp;
        <a href="samplingDisplay?project_id={$sampling.project_id}&sampling_id={$data.sampling_id}">
            <img src="display/images/sampling.png" height="25">
            {t}Retour à l'échantillonnage{/t} {$sampling.sampling_code}
        </a>
        {if $data.extraction_id > 0}
        &nbsp;
        <a
            href="extractionDisplay?project_id={$data.project_id}&sampling_id={$data.sampling_id}&extraction_id={$data.extraction_id}">
            <img src="display/images/extract.png" height="25">{t}Retour à l'extraction{/t}
        </a>
        {/if}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <form class="form-horizontal protoform" id="samplingForm" method="post" action="extractionWrite">
            <input type="hidden" name="moduleBase" value="extraction">
            <input type="hidden" name="sampling_id" value="{$data.sampling_id}">
            <input type="hidden" name="project_id" value="{$project_id}">
            <input type="hidden" name="extraction_id" value="{$data.extraction_id}">
            <div class="form-group">
                <label for="extraction_date" class="control-label col-md-4">
                    {t}Date d'extraction :{/t}
                </label>
                <div class="col-md-8">
                    <input id="extraction_date" class="datepicker form-control" name="extraction_date"
                        value="{$data.extraction_date}">
                </div>
            </div>
            <div class="form-group">
                <label for="extraction_method_id" class="control-label col-md-4">
                    {t}Méthode d'extraction :{/t}
                </label>
                <div class="col-md-8">
                    <select class="form-control" id="extraction_method_id" name="extraction_method_id">
                        <option value="" {if $data.extraction_method_id=="" }selected{/if}>{t}Choisissez{/t}</option>
                        {foreach $methods as $method}
                        <option value="{$method.extraction_method_id}" {if
                            $method.extraction_method_id==$data.extraction_method_id}selected{/if}>
                            {$method.extraction_method_name}
                        </option>
                        {/foreach}

                    </select>
                </div>
            </div>

            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
                {if $data.extraction_id > 0 }
                <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
                {/if}
            </div>
            {$csrf}
        </form>
    </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>