
<script>
  $(document).ready(function() {
    $("#checkSampling").change(function() {
      $(".checkSampling").prop("checked", this.checked);
    });
  });
</script>
<table id="samplingList" class="table table-bordered table-hover datatable display">
  <thead>
    <tr>
      <th class="center">
        <input type="checkbox" id="checkSampling" class="checkSampling" >
      </th>
      <th>{t}Code{/t}</th>
      <th>{t}Description{/t}</th>
      <th >{t}Date{/t}</th>
      <th>{t}Méthode d'échantillonnage{/t}</th>
      <th>{t}Volume par défaut{/t}</th>
      <th>{t}Masse par défaut{/t}</th>
      <th>{t}Unité par défaut{/t}</th>
      <th>{t}Code ou radical de niveau 1{/t}</th>
      <th>{t}Nombre d'occurrences de niveau 1 à créer{/t}</th>
      <th>{t}Code ou radical de niveau 2{/t}</th>
      <th>{t}Nombre d'occurrences de niveau 2 à créer{/t}</th>
      <th>{t}Séparateur{/t}</th>
      <th>{t}Nombre de réplicats créés{/t}</th>
      <th>{t}Type de stockage par défaut{/t}</th>
      <th>{t}Lieu de stockage par défaut{/t}</th>
    </tr>
  </thead>
  <tbody>
  {foreach $samplings as $sampling}
    <tr>
      <td class="center">
        <input type="checkbox" class="checkSampling" name="samplings[]" value="{$sampling.sampling_id}" >
      </td>
      <td class="nowrap"><a href="samplingDisplay?project_id={$sampling.project_id}&sampling_id={$sampling.sampling_id}&experimentation_id={$sampling.experimentation_id}&condition_id={$sampling.condition_id}">{$sampling.sampling_code}</a></td>
      <td class="tableTextarea">{$sampling.sampling_description}</td>
      <td class="center">{$sampling.sampling_date}</td>
      <td>{$sampling.sampling_method_name}</td>
      <td class="center">{$sampling.default_volume}</td>
      <td class="center">{$sampling.default_weight}</td>
      <td class="center">{$sampling.default_unit}</td>
      <td class="center">{$sampling.level1_code}</td>
      <td class="center">{$sampling.level1_number}</td>
      <td class="center">{$sampling.level2_code}</td>
      <td class="center">{$sampling.level2_number}</td>
      <td class="center">{$sampling.separator}</td>
      <td class="center">{$sampling.sample_number}</td>
      <td>{$sampling.storage_type_name}</td>
      <td>{$sampling.storage_location_name}</td>
    </tr>
  {/foreach}
  </tbody>
</table>
