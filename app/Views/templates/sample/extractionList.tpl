<table id="extractionList" class="table table-bordered table-hover datatable display">
    <thead>
        <tr>
            <th>{t}Détail{/t}</th>
            <th>{t}Échantillonnage concerné{/t}</th>
            <th class="center">{t}Date{/t}</th>
            <th>{t}Méthode{/t}</th>
        </tr>
    </thead>
    <tbody>
        {foreach $extractions as $extraction}
        <tr>
            <td class="center" title="{t}Afficher le détail{/t}">
                <a
                    href="extractionDisplay?project_id={$extraction.project_id}&extraction_id={$extraction.extraction_id}">
                    <img src="display/images/display.png" height="25">
                </a>
            </td>
            <td class="nowrap">
                <a
                    href="samplingDisplay?project_id={$data.project_id}&sampling_id={$extraction.sampling_id}&experimentation_id={$extraction.experimentation_id}&condition_id={$extraction.condition_id}">
                    {$extraction.condition_code} {$extraction.sampling_code}
                </a>
            </td>
            <td class="center">{$extraction.extraction_date}</td>
            <td>{$extraction.extraction_method_name}</td>
        </tr>
        {/foreach}
    </tbody>
</table>