<script>
    $(document).ready(function () {
        $("#checkCondition").change(function () {
            $(".checkCondition").prop("checked", this.checked);
        });
        $(".checkCondition").click(function () {
            $("#formHidden").show();
        });
        var visible = false;
        $("#formLabel").click(function () {
            if (!visible) {
                $("#formHidden").show();
                visible = true;
            } else {
                $("#formHidden").hide();
                visible = false;
            }

        });
        $("#createSamplingFromConditions").submit(function (event) {
            if (!confirm("{t}Confirmez-vous la création des échantillonnages à partir des conditions sélectionnées ?{/t}")) {
                event.preventDefault();
            }
        });
    });
</script>
{if $rights.manage == 1 && !empty($experimentation.experimentation_id)}
<a
    href="conditionChange?project_id={$experimentation.project_id}&condition_id=0&experimentation_id={$experimentation.experimentation_id}">
    <img src="display/images/new.png" height="25">
    {t}Nouvelle condition{/t}
</a>
<a
    href="conditionImport?project_id={$experimentation.project_id}&experimentation_id={$experimentation.experimentation_id}">
    <img src="display/images/download.png" height="25">
    {t}Importer les conditions à partir d'un fichier CSV{/t}
</a>
{/if}
<form id="createSamplingFromConditions" method="post" action="createSamplingsFromConditions">
    <input type="hidden" name="project_id" value="{$data.project_id}">
    <table id="conditionList" class="table table-bordered table-hover datatable display">
        <thead>
            <tr>
                {if $rights.manage == 1}
                <th class="center">
                    <input type="checkbox" class="checkCondition" id="checkCondition">
                </th>
                {/if}
                <th>{t}Code{/t}</th>
                <th>{t}Description{/t}</th>
                <th>{t}Type de matrice{/t}</th>
                <th>{t}Expérimentation{/t}</th>
                <th>{t}Station{/t}</th>
            </tr>
        </thead>
        <tbody>
            {foreach $conditions as $condition}
            <tr>
                {if $rights.manage == 1}
                <td class="center">
                    <input type="checkbox" class="checkCondition" name="conditions[]" value="{$condition.condition_id}">
                </td>
                {/if}
                <td><a
                        href="conditionDisplay?project_id={$condition.project_id}&condition_id={$condition.condition_id}&experimentation_id={$condition.experimentation_id}">{$condition.condition_code}</a>
                </td>
                <td>{$condition.condition_description}</td>
                <td>{$condition.matrice_type_name}</td>
                <td>{$condition.experimentation_name}</td>
                <td>{$condition.station_name}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    {if $rights.manage == 1}
    <fieldset>
        <label id="formLabel"><a href="#">
                {t}Créez les échantillonnages pour les conditions sélectionnées :{/t}
            </a>
        </label>
        <div class="form-horizontal" id="formHidden" hidden>
            <div class="form-group">
                <label for="sampling_suffix" class="control-label col-md-4">
                    {t}Suffixe optionnel rajouté au code de la condition (ajoutez également le séparateur désiré avant
                    le suffixe) :{/t}
                </label>
                <div class="col-md-8">
                    <input id="sampling_suffix" class="form-control" name="sampling_suffix" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="sampling_description" class="control-label col-md-4">{t}Description :{/t}</label>
                <div class="col-md-8">
                    <textarea id="sampling_description" name="sampling_description" class="form-control textarea-edit"
                        rows="3">{$sampling.sampling_description}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="sampling_method_id" class="control-label col-md-4">
                    {t}Méthode utilisée :{/t}
                </label>
                <div class="col-md-8">
                    <select class="form-control" id="sampling_method_id" name="sampling_method_id">
                        <option value="" {if $sampling.sampling_method_id=="" }selected{/if}>
                            {t}Choisissez{/t}
                        </option>
                        {foreach $sampling_methods as $method}
                        <option value="{$method.sampling_method_id}" {if
                            $method.sampling_method_id==$sampling.sampling_method_id}selected{/if}>
                            {$method.sampling_method_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="sampling_date" class="control-label col-md-4">
                    {t} date d'échantillonnage :{/t}
                </label>
                <div class="col-md-8">
                    <input id="sampling_date" class="datepicker form-control" name="sampling_date"
                        value="{$sampling.sampling_date}">
                </div>
            </div>
            <fieldset>
                <legend>{t}Paramètres de création des réplicats d'échantillons{/t}</legend>
                <div class="form-group">
                    <label for="default_volume" class="control-label col-md-4">
                        {t}Volume par défaut :{/t}
                    </label>
                    <div class="col-md-8">
                        <input id="default_volume" class="form-control" name="default_volume"
                            value="{$sampling.default_volume}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="default_weight" class="control-label col-md-4">
                        {t}Poids par défaut :{/t}
                    </label>
                    <div class="col-md-8">
                        <input id="default_weight" class="form-control" name="default_weight"
                            value="{$sampling.default_weight}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="default_unit" class="control-label col-md-4">
                        {t}Unité (volume) par défaut :{/t}
                    </label>
                    <div class="col-md-8">
                        <input id="default_unit" class="form-control" name="default_unit_volume"
                            value="{$sampling.default_unit_volume}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="default_unit" class="control-label col-md-4">
                        {t}Unité (poids) par défaut :{/t}
                    </label>
                    <div class="col-md-8">
                        <input id="default_unit" class="form-control" name="default_unit_weight"
                            value="{$sampling.default_unit_weight}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="level1_code" class="control-label col-md-4">
                        {t}Code du niveau 1 :{/t}
                    </label>
                    <div class="col-md-8">
                        <input id="level1_code" class="form-control" name="level1_code" value="{$sampling.level1_code}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="level1_number" class="control-label col-md-4"> <span class="red">*</span>
                        {t}Nombre d'occurrences à créer pour le niveau 1 :{/t}
                    </label>
                    <div class="col-md-8">
                        <input id="level1_number" class="form-control number" name="level1_number"
                            value="{$sampling.level1_number}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="level2_code" class="control-label col-md-4">
                        {t}Code du niveau 2 :{/t}
                    </label>
                    <div class="col-md-8">
                        <input id="level2_code" class="form-control" name="level2_code" value="{$sampling.level2_code}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="level2_number" class="control-label col-md-4">
                        {t}Nombre d'occurrences à créer pour le niveau 2 :{/t}
                    </label>
                    <div class="col-md-8">
                        <input id="level2_number" class="form-control number" name="level2_number"
                            value="{$sampling.level2_number}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="separator" class="control-label col-md-4">
                        {t}Séparateur :{/t}
                    </label>
                    <div class="col-md-8">
                        <input id="separator" class="form-control" name="separator" value="{$sampling.separator}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="default_transport_method" class="control-label col-md-4">
                        {t}Méthode de transport :{/t}
                    </label>
                    <div class="col-md-8">
                        <select class="form-control" id="default_transport_method" name="default_transport_method">
                            <option value="">{t}Choisissez...{/t}</option>
                            {foreach $transport_methods as $transport_method}
                            <option value="{$transport_method.transport_method_id}">
                                {$transport_method.transport_method_name}
                            </option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="default_storage_type" class="control-label col-md-4">
                        {t}Type de stockage par défaut :{/t}
                    </label>
                    <div class="col-md-8">
                        <select class="form-control" id="default_storage_type" name="default_storage_type">
                            <option value="" {if $sampling.storage_type_id=="" }selected{/if}>
                                {t}Choisissez...{/t}
                            </option>
                            {foreach $storage_types as $storage_type}
                            <option value="{$storage_type.storage_type_id}">
                                {$storage_type.storage_type_name}
                            </option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="default_storage_location" class="control-label col-md-4">
                        {t}Lieu de stockage par défaut :{/t}
                    </label>
                    <div class="col-md-8">
                        <select class="form-control" id="default_storage_location" name="default_storage_location">
                            <option value="" {if $sampling.storage_location_id=="" }selected{/if}>
                                {t}Choisissez...{/t}
                            </option>
                            {foreach $storage_locations as $storage_location}
                            <option value="{$storage_location.storage_location_id}" {if
                                $storage_location.storage_location_id==$sampling.default_storage_location}selected{/if}>
                                {$storage_location.storage_location_name}
                            </option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group center">
                    <button id="createSamplingsButton" type="submit" class="btn btn-primary button-valid">
                        {t}Créer les échantillonnages à partir des conditions{/t}
                    </button>
                </div>
            </fieldset>
        </div>
    </fieldset>
    {/if}
    {$csrf}
</form>