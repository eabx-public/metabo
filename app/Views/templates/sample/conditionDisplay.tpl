<script>
$(document).ready( function () {
  var condition_id = "{$data.condition_id}";
  try {
    if (parseInt(condition_id) > 0) {
      Cookies.set('condition_id', condition_id, { expires: 35, secure: true });
    }
  } catch (e) { }
  /**
   * Management of tabs
   */
   var moduleName = "conditionDisplay";
   var localStorage = window.localStorage;
  try {
    activeTab = localStorage.getItem(moduleName + "Tab");
  } catch (Exception) {
    activeTab = "";
  }
  try {
    if (activeTab.length > 0) {
      $("#"+activeTab).tab('show');
    }
  } catch (Exception) { }
  $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
			localStorage.setItem(moduleName + "Tab", $(this).attr("id"));
		});
  $("#extraction_id").change(function() {
    var extractionId = $(this).val();
    if (extractionId == 0) {
      $(".newExtraction").show();
    } else {
      $(".newExtraction").hide();
    }
  });
  $("#sampleSelect").submit( function(event) {
    if ( ! operationConfirm()) {
      event.preventDefault();
    }
		});
  $("#form-create-replicate").submit(function(event) {
    if ( ! operationConfirm() ) {
      event.preventDefault();
    }
  });
});
</script>
<div class="row">
  <h2>{t 1=$data.condition_code}Condition %1{/t} ({$data.matrice_type_name})</h2>
  <div class="col-md-12">
    <a href="projectDisplay?project_id={$data.project_id}"><img src="display/images/project.png" height="25">{t}Retour au projet{/t}&nbsp;{$data.project_name}</a>
    &nbsp;
    <a href="experimentationDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}"><img src="display/images/experiment.png" height="25">{t}Retour à l'expérimentation{/t}&nbsp;{$data.experimentation_name}</a>
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <ul class="nav nav-tabs" id="myTab" role="tablist" >
      <li class="nav-item active">
        <a class="nav-link" id="tab-detail" data-toggle="tab"  role="tab" aria-controls="nav-detail" aria-selected="true" href="#nav-detail">
          <img src="display/images/zoom.png" height="25">
          {t}Détails{/t}
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tab-sampling" href="#nav-sampling"  data-toggle="tab" role="tab" aria-controls="nav-sampling" aria-selected="false">
          <img src="display/images/sampling.png" height="25">
          {t}Échantillonnages{/t}
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tab-file" href="#nav-file"  data-toggle="tab" role="tab" aria-controls="nav-file" aria-selected="false">
          <img src="display/images/files.png" height="25">
          {t}Fichiers{/t}
        </a>
      </li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane active in" id="nav-detail" role="tabpanel" aria-labelledby="tab-detail">
        <div class="row">
          <a href="conditionChange?experimentation_id={$data.experimentation_id}&project_id={$data.project_id}&condition_id={$data.condition_id}">
            <img src="display/images/edit.gif" height="25">&nbsp;
            {t}Modifier{/t}
          </a>
        </div>
        <div class="col-md-6 form-display">
        <dl class="dl-horizontal">
            <dt>{t}Code :{/t}</dt>
            <dd>{$data.condition_code}</dd>
          </dl>
          <dl class="dl-horizontal">
            <dt>{t}Description :{/t}</dt>
            <dd class="textareaDisplay">{$data.condition_description}</dd>
          </dl>
          <dl class="dl-horizontal">
            <dt>{t}Type de matrice :{/t}</dt>
            <dd>{$data.matrice_type_name}</dd>
          </dl>
          <dl class="dl-horizontal">
            <dt>{t}Station :{/t}</dt>
            <dd>{$data.station_name}</dd>
        </div>

      </div>
      <div class="tab-pane fade in" id="nav-sampling" role="tabpanel" aria-labelledby="tab-sampling">
        <div class="row">
          <div class="col-md-8">
            {if $rights.manage == 1}
              <a href="samplingChange?project_id={$data.project_id}&sampling_id=0&condition_id={$data.condition_id}">
                <img src="display/images/new.png" height="25">
                {t}Nouvel échantillonnage{/t}
              </a>
            {/if}
            {include file="sample/samplingList.tpl"}
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-file" role="tabpanel" aria-labelledby="tab-file">
        <div class="row">
          <div class="col-md-12">
            {include file="file/fileLoad.tpl"}
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            {include file="file/fileList.tpl"}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
