<table id="acquisitionList" class="table table-bordered table-hover datatable display">
  <thead>
    <tr>
      <th>{t}Date d'acquisition{/t}</th>
      <th>{t}Utilisé pour la calibration ?{/t}</th>
      <th>{t}Machine utilisée{/t}</th>
      <th class="center"><img src="display/images/corbeille.png" height="25"></th>
    </tr>
  </thead>
  <tbody>
    {foreach $acquisition as $ana}
      <tr>
        <td class="center">
          <a href="acquisitionDisplay?sample_id={$ana.sample_id}&acquisition_id={$ana.acquisition_id}">
            {$ana.acquisition_date}
          </a>
        </td>
        <td class="center">{if $ana.is_calibration == 1}{t}oui{/t}{/if}</td>
        <td>{$ana.machine_name}</td>
        <td class="center">
          <a href="sampleAcquisitionDelete?sample_id={$data.sample_id}&acquisition_sample_id={$ana.acquisition_sample_id}" onclick="return confirm('{t}Confirmez-vous la suppression ?{/t}');" title="{t}Supprimer l'aliquot de l'acquisition{/t}">
            <img src="display/images/corbeille.png" height="25">
        </a>
      </tr>
    {/foreach}
  </tbody>
</table>
