<script>
    $(document).ready(function () {
        var extraction_id = "{$data.extraction_id}";
        try {
            if (parseInt(extraction_id) > 0) {
                Cookies.set('extraction_id', extraction_id, { expires: 35, secure: true });
            }
        } catch (e) { }
        /**
         * Management of tabs
         */
        var moduleName = "extractionDisplay";
        var localStorage = window.localStorage;
        try {
            activeTab = localStorage.getItem(moduleName + "Tab");
        } catch (Exception) {
            activeTab = "";
        }
        try {
            if (activeTab.length > 0) {
                $("#" + activeTab).tab('show');
            }
        } catch (Exception) { }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
            localStorage.setItem(moduleName + "Tab", $(this).attr("id"));
        });
        $("#createAliquot").submit(function (event) {
            if (!operationConfirm()) {
                event.preventDefault();
            }
        });
        $("#acquisition_id").change(function () {
            var id = $(this).val();
            if (id == 0) {
                $("#acquisitionCreate").show();
                $("#acquisition_date").attr("required",true);
            } else {
                $("#acquisitionCreate").hide();
                $("#acquisition_date").attr("required",false);
            }
        });
    });
</script>
<div class="row">
    <h2>{t}Extraction du{/t} {$data.extraction_date} ({$data.extraction_method_name})</h2>
    <div class="col-md-12">
        <a href="projectDisplay?project_id={$sampling.project_id}"><img src="display/images/display.png" height="25">
            {t}Retour au projet{/t}&nbsp;{$data.project_name}
        </a>
        {if $sampling.sampling_id > 0}
        &nbsp;
        <a
            href="experimentationDisplay?project_id={$sampling.project_id}&experimentation_id={$sampling.experimentation_id}"><img
                src="display/images/experiment.png" height="25">
            {t}Retour à 'expérimentation{/t}&nbsp;{$sampling.experimentation_name}
        </a>
        &nbsp;
        <a
            href="conditionDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}&condition_id={$sampling.condition_id}">
            <img src="display/images/condition.png" height="25">
            {t}Retour à la condition{/t} {$sampling.condition_code}
        </a>
        &nbsp;
        <a href="samplingDisplay?project_id={$sampling.project_id}&sampling_id={$data.sampling_id}">
            <img src="display/images/sampling.png" height="25">
            {t}Retour à l'échantillonnage{/t} {$sampling.sampling_code}
        </a>
        {/if}
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item active">
                <a class="nav-link" id="tab-detail" data-toggle="tab" role="tab" aria-controls="nav-detail"
                    aria-selected="true" href="#nav-detail">
                    <img src="display/images/zoom.png" height="25">
                    {t}Détails{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-sample" href="#nav-sample" data-toggle="tab" role="tab"
                    aria-controls="nav-sample" aria-selected="false">
                    <img src="display/images/sample.png" height="25">
                    {t}Extraits{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-file" href="#nav-file" data-toggle="tab" role="tab" aria-controls="nav-file"
                    aria-selected="false">
                    <img src="display/images/files.png" height="25">
                    {t}Fichiers{/t}
                </a>
            </li>
        </ul>

        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane active in" id="nav-detail" role="tabpanel" aria-labelledby="tab-detail">
                <div class="row">
                    <a
                        href="extractionChange?sampling_id={$data.sampling_id}&project_id={$data.project_id}&extraction_id={$data.extraction_id}">
                        <img src="display/images/edit.gif" height="25">&nbsp;
                        {t}Modifier{/t}
                    </a>
                </div>
                <div class="col-md-6 form-display">
                    <dl class="dl-horizontal">
                        <dt>{t}Date :{/t}</dt>
                        <dd>{$data.extraction_date}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>{t}Méthode utilisée :{/t}</dt>
                        <dd>{$data.extraction_method_name}</dd>
                    </dl>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-sample" role="tabpanel" aria-labelledby="tab-sample">
                <div class="row">
                    <div class="col-lg-12">
                        {if $rights.manage == 1}
                        <div class="row">
                            <a href="sampleImport?extraction_id={$data.extraction_id}&sample_type_id=2">
                                <img src="display/images/input.png" height="25">
                                {t}Importer des extraits{/t}
                            </a>
                        </div>
                        {/if}
                        {if $rights.manage == 1}
                        <form id="createAliquot" method="post" action="sampleCreateAliquot">
                            <input name="extraction_id" value="{$data.extraction_id}" type="hidden">
                            <input name="project_id" value="{$data.project_id}" type="hidden">
                            {/if}
                            <div class="row">
                                {include file="sample/sampleList.tpl"}
                            </div>
                            {if $rights.manage == 1}
                            <fieldset class="form-horizontal col-md-6">
                                <legend>{t}Créer des aliquots pour les extraits sélectionnés{/t}</legend>
                                <div class="form-group">
                                    <label for="number_aliquots" class="control-label col-md-4">
                                        <span class="red">*</span> {t}Nombre d'aliquots à créer par extrait :{/t}
                                    </label>
                                    <div class="col-md-8">
                                        <input class="form-control nombre" name="number_aliquots" id="number_aliquots"
                                            value="1" autofocus required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sampled_volume" class="control-label col-md-4">
                                        {t}Volume prélevé depuis l'extrait (µL) :{/t}
                                    </label>
                                    <div class="col-md-8">
                                        <input class="form-control taux" name="sampled_volume" id="sampled_volume">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="volume" class="control-label col-md-4">
                                        {t}Volume de reprise (après dilution ou concentration) (µL) :{/t}
                                    </label>
                                    <div class="col-md-8">
                                        <input class="form-control taux" name="volume" id="volume">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="acquisition_id" class="control-label col-md-4">
                                        {t}Acquisition de rattachement des aliquots :{/t}
                                    </label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="acquisition_id" name="acquisition_id">
                                            <option value="-1" selected>
                                                {t}Pas de rattachement à une acquisition{/t}
                                            </option>
                                            <option value="0">
                                                {t}Créer une nouvelle acquisition{/t}
                                            </option>
                                            {foreach $acquisitions as $acquisition}
                                            <option value="{$acquisition.acquisition_id}">
                                                {$acquisition.acquisition_date} - {$acquisition.acquisition_type_name}
                                            </option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div id="acquisitionCreate" hidden>
                                    <div class="form-group">
                                        <label for="acquisition_date" class="control-label col-md-4"><span
                                                class="red">*</span> {t}Date d'acquisition :{/t}</label>
                                        <div class="col-md-8">
                                            <input id="acquisition_date"  class="form-control datepicker"
                                                name="acquisition_date">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="acquisition_type_id" class="control-label col-md-4"><span
                                                class="red">*</span> {t}Type d'acquisition :{/t}</label>
                                        <div class="col-md-8">
                                            <select id="acquisition_type_id" name="acquisition_type_id"
                                                class="form-control">
                                                {foreach $acquisitionTypes as $at}
                                                <option value="{$at.acquisition_type_id}">
                                                    {$at.acquisition_type_name}
                                                </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="machine_id" class="control-label col-md-4">
                                            {t}Machine utilisée :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <select id="machine_id" name="machine_id" class="form-control">
                                                <option value="" selected>
                                                    {t}Choisissez{/t}</option>
                                                {foreach $machines as $machine}
                                                <option value="{$machine.machine_id}" {if
                                                    $data.machine_id==$machine.machine_id}selected{/if}>
                                                    {$machine.machine_name}
                                                </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="chromatography_method_id" class="control-label col-md-4">
                                            {t}Méthode de chromatographie :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <select id="chromatography_method_id" name="chromatography_method_id"
                                                class="form-control">
                                                <option value="" selected>
                                                    {t}Choisissez{/t}</option>
                                                {foreach $chromatographys as $chromatography}
                                                <option value="{$chromatography.chromatography_method_id}">
                                                    {$chromatography.chromatography_method_name}
                                                </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="acquisition_method_id" class="control-label col-md-4">
                                            {t}Méthode d'acquisition :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <select id="acquisition_method_id" name="acquisition_method_id"
                                                class="form-control">
                                                <option value="" selected>
                                                    {t}Choisissez{/t}</option>
                                                {foreach $acquisition_methods as $method}
                                                <option value="{$method.acquisition_method_id}">
                                                    {$method.acquisition_method_name}
                                                </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="is_calibration" class="control-label col-md-4">
                                        {t}Les aliquots sont-ils utilisés pour la calibration ?{/t}
                                    </label>
                                    <div class="col-md-8" id="is_calibration">
                                        <label class="radio-inline">
                                            <input type="radio" name="is_calibration" id="is_calibration1"
                                                value="1">{t}oui{/t}
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="is_calibration" id="is_calibration0" value="0" checked="checked">
                                                {t}non{/t}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group center">
                                    <button type="submit" class="btn btn-primary button-valid">
                                        {t}Créer les aliquots{/t}
                                    </button>
                                </div>
                            </fieldset>
                            {$csrf}
                        </form>
                        {/if}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-file" role="tabpanel" aria-labelledby="tab-file">
                <div class="row">
                    <div class="col-md-12">
                        {include file="file/fileLoad.tpl"}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {include file="file/fileList.tpl"}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>