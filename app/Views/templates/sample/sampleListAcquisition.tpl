<script>
$(document).ready(function() {
    /*
    var index = 0;
    $('#sample-list thead th').each( function () {
        var title = $(this).text();
				var size = title.trim().length;
				if ( size > 0) {
        	$(this).html( '<input type="text" placeholder="'+title+'" size="'+size+'" class="searchInput">' );
				}
		});
		var table = $("#sample-list").DataTable();
		table.columns().every( function () {
			var that = this;
			if (that.index() > index) {
				$( 'input', this.header() ).on( 'keyup change clear', function () {
					if ( that.search() !== this.value ) {
						that.search( this.value ).draw();
					}
				});
			}
		});
    $(".searchInput").hover(function() {
			$(this).focus();
		});
    */
    $("#checkSample").change(function() {
      $(".checkSample").prop("checked", this.checked);
    });

    var actions = {
			"treatmentCreateWithSamples": "treatmentForm"
    };
		$( "#sampleAction" ).change( function () {
			var action = $( this ).val();
			var actionClass = actions[ action ];
			var value;
			for ( const key in actions ) {
				if ( actions.hasOwnProperty( key ) ) {
					value = actions[ key ];
					if ( value == actionClass ) {
						$( "#" + value ).show();
					} else {
						$( "#" + value ).hide();
					}
				}
			};
      $("#selectModule").val(action);
		} );
    $("#sampleAcquisitionList").submit( function(event) {
      if ($("#sampleAction").val().length == 0) {
        event.preventDefault();
      } else {
        if(! operationConfirm()) {
          event.preventDefault();
        }
      }
    });
    var moduleName = "acquisitionDisplaySample";
    try {
      activeTab = Cookies.get(moduleName + "Tab");
    } catch (Exception) {
      activeTab = "";
    }
    try {
      if (activeTab.length > 0) {
        $("#"+activeTab).tab('show');
      }
    } catch (Exception) { }
    $('a[data-second="tab"]').on('shown.bs.tab', function () {
			Cookies.set(moduleName + "Tab", $(this).attr("id"), { secure: true});
		});
  });
</script>
<div class="row">
  <div class="col-xs-12">
    <ul class="nav nav-tabs" id="myTab2" role="tablist" >
      <li class="nav-item active">
        <a class="nav-link" id="tab-samplelist" href="#nav-samplelist" data-second="tab" data-toggle="tab" role="tab" aria-controls="nav-samplelist" aria-selected="false">
          <img src="display/images/sample.png" height="25">
          {t}Aliquots attachés{/t}
        </a>
      </li>
      {if $acquisition_sample_id > 0}
      <li class="nav-item">
        <a class="nav-link" id="tab-samplefile" href="#nav-samplefile"  data-second="tab" data-toggle="tab" role="tab" aria-controls="nav-samplefile" aria-selected="false">
          <img src="display/images/files.png" height="25">
          {t}Fichiers associés à l'aliquot {/t} {$acquisitionSample.sample_name}
        </a>
      </li>
      {/if}
    </ul>
    <div class="tab-content" id="nav-tabContentsample">
      <div class="tab-pane active in" id="nav-samplelist" role="tabpanel" aria-labelledby="tab-samplelist">
        <form id="sampleAcquisitionList" method="POST" action="">
          <input type="hidden" id="selectModule" name="module">
          <input type="hidden" name="acquisition_id" value="{$data.acquisition_id}">
          <input type="hidden" name="project_id" value="{$data.project_id}">
          <div class="row">
            <div class="col-lg-8">
              <table id="sampleListAcquisition" class="table table-bordered table-hover datatable-searching display">
                <thead>
                  <tr>
                    <th class="center">
                      <input type="checkbox" id="checkSample" class="checkSample" >
                    </th>
                    <th>{t}Code{/t}</th>
                    <th>{t}Type{/t}</th>
                    <th>{t}Volume de reprise{/t}</th>
                    <th>{t}Volume extrait{/t}</th>
                    <th>{t}Utilisé pour la calibration ?{/t}</th>
                    <th>{t}Parents{/t}</th>
                    <th>{t}UUID{/t}</th>
                    <th>{t}Fichiers associés{/t}</th>
                    <th>{t}Détacher l'aliquot{/t}</th>
                  </tr>
                </thead>
                <tbody>
                  {foreach $samples as $sample}
                    <tr>
                      <td class="center">
                        <input type="checkbox" class="checkSample" name="samples[]" value="{$sample.sample_id}" >
                      </td>
                      <td class="center" nowrap>
                        <a href="sampleDisplay?project_id={$sample.project_id}&acquisition_id={$sample.acquisition_id}&sample_id={$sample.sample_id}">
                          {$sample.sample_name}
                        </a>
                      </td>
                      <td>{$sample.sample_type_name}</td>
                      <td class="right">{$sample.volume}</td>
                      <td class="right">{$sample.sampled_volume}</td>
                      <td class="center">{if $sample.is_calibration == 1}{t}oui{/t}{/if}</td>
                      <td >
                        {foreach $sample.parents as $parent}
                          <a href="sampleDisplay?project_id={$sample.project_id}&acquisition_id={$acquisition.acquisition_id}&sample_id={$parent.sample_id}">
                          <span class="nowrap">{$parent.sample_name}</span>
                          </a>
                        {/foreach}
                      </td>
                      <td nowrap>{$sample.uuid}</td>
                      <td class="center {if $acquisition_sample_id == $sample.acquisition_sample_id}itemselected{/if}"
                    title="{t}Visualisez ou ajoutez les fichiers de résultat de l'acquisition{/t}">
                      <a href="acquisitionDisplay?project_id={$sample.project_id}&acquisition_sample_id={$sample.acquisition_sample_id}&acquisition_id={$data.acquisition_id}">
                        <img src="display/images/files.png" height="25">
                        </a>
                      </td>
                      <td class="center">
                        <a href="acquisition_sampleDelete?acquisition_sample_id={$sample.acquisition_sample_id}&project_id={$sample.project_id}&acquisition_id={$data.acquisition_id}" onclick="return confirm('{t}Confirmez-vous la suppression ?{/t}');">
                          <img src="display/images/corbeille.png" height="25">
                        </a>
                      </td>
                    </tr>
                  {/foreach}
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8">
              <div class="form-horizontal">
                <div class="form-group">
                  <label for="sampleAction" class="control-label col-md-4">{t}Pour les aliquots sélectionnés :{/t}</label>
                  <div class="col-md-8">
                    <select id="sampleAction" class="form-control">
                      <option value="" selected>{t}Choisissez :{/t}</option>
                      <option value="treatmentCreateWithSamples">{t}Attachez les aliquots à un traitement{/t}</option>
                    </select>
                  </div>
                </div>
                <div id="treatmentForm" hidden>
                  <div class="form-group">
                    <label for="treatment_id" class="control-label col-md-4">{t}Traitement{/t}</label>
                    <div class="col-md-8">
                      <select id="treatment_id" name="treatment_id" class="form-control">
                        <option value="0" selected>{t}Nouveau traitement{/t}</option>
                        {foreach $treatments as $treatment}
                          <option value="{$treatment.treatment_id}">{$treatment.treatment_date} ({$treatment.software_name})</option>
                        {/foreach}
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="treatment_date" class="control-label col-md-4">{t}Date du traitement :{/t}</label>
                    <div class="col-md-8">
                      <input id="treatment_date" name="treatment_date" class="col-md-8 form-control datepicker" value="{$dateDefault}">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="software_id" class="control-label col-md-4">{t}Logiciel utilisé :{/t}</label>
                    <div class="col-md-8">
                      <select id="software_id" name="software_id" class="form-control">
                        <option value="" selected>{t}Choisissez...{/t}</option>
                        {foreach $softwares as $software}
                          <option value="{$software.software_id}">{$software.software_name}</option>
                        {/foreach}
                      </select>
                    </div>
                  </div>
                  <div class="form-group center ">
                    <button  type="submit" class="btn btn-danger button-valid">{t}Associez les aliquots au traitement{/t}</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        {$csrf}</form>
      </div>
      {if $acquisition_sample_id > 0}
        <div class="tab-pane fade" id="nav-samplefile" role="tabpanel" aria-labelledby="tab-samplefile">
          <div class="row">
            <div class="col-lg-12">
              {include file="file/fileLoad2.tpl"}
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              {include file="file/fileList2.tpl"}
            </div>
          </div>
        </div>
      {/if}
    </div>
  </div>
</div>
