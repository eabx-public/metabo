<h2>
    {t}Création - Modification d'un{/t}&nbsp;{if $data.sample_type_id == 1}{t}réplicat{/t}
    {elseif $data.sample_type_id == 2}{t}extrait{/t}{elseif $data.sample_type_id == 3}{t}aliquot{/t}{else}{t}réplicat{/t}{/if}
</h2>
<div class="row">
    <div class="col-md-12">
        <a href="projectDisplay?project_id={$data.project_id}"><img src="display/images/project.png" height="25">
            {t}Retour au projet{/t}&nbsp;{$data.project_name}</a>
        {if !empty($data["experimentation_id"])}
        &nbsp;
        <a href="experimentationDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}"><img
                src="display/images/experiment.png" height="25">
            {t}Retour à l'expérimentation{/t}&nbsp;{$data.experimentation_name}
        </a>
        {/if}
        {if !empty($data[condition_id])}
        &nbsp;
        <a href="conditionDisplay?project_id={$data.project_id}&condition_id={$data.condition_id}">
            <img src="display/images/condition.png" height="25">
            {t}Retour au prélèvement{/t}
        </a>
        {/if}
        {if $data.sample_id > 0}
        &nbsp;
        <a
            href="sampleDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}&sample_id={$data.sample_id}">
            <img src="display/images/sample.png" height="25">
            {t}Retour au détail{/t}
        </a>
        {/if}
        {if $parent["sample_id"] > 0 }
        &nbsp;
        <a
            href="sampleDisplay?project_id={$parent.project_id}&experimentation_id={$parent.experimentation_id}&sample_id={$parent.sample_id}">
            <img src="display/images/sample.png" height="25">
            {t}Retour au parent{/t}&nbsp;{$parent.sample_name}
        </a>
        {/if}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <form class="form-horizontal protoform" id="conditionForm" method="post" action="sampleWrite">
            <input type="hidden" name="moduleBase" value="sample">
            <input type="hidden" name="action" value="Write">
            <input type="hidden" name="project_id" value="{$project_id}">
            <input type="hidden" name="sample_id" value="{$data.sample_id}">
            <input type="hidden" name="sampling_id" value="{$data.sampling_id}">
            <input type="hidden" name="sample_parent_id" value="{$data.sample_parent_id}">
            <div class="form-group">
                <label for="sample_id" class="control-label col-md-4">
                    <span class="red">*</span> {t}Code :{/t}
                </label>
                <div class="col-md-8">
                    <input class="form-control" name="sample_name" id="sample_name" value="{$data.sample_name}"
                        autofocus required>
                </div>
            </div>
            <div class="form-group">
                <label for="sample_type_id" class="control-label col-md-4">
                    <span class="red">*</span> {t}Type :{/t}
                </label>
                <div class="col-md-8">
                    <select class="form-control" id="sample_type_id" name="sample_type_id" readonly>
                        {foreach $types as $type}
                        <option value="{$type.sample_type_id}" {if
                            $type.sample_type_id==$data.sample_type_id}selected{/if}>
                            {$type.sample_type_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="sample_parent_name" class="control-label col-md-4">
                    {t}Code du parent :{/t}
                </label>
                <div class="col-md-8">
                    <input id="sample_parent_name" class="form-control" name="sample_parent_name"
                        value="{$data.sample_parent_name}" readonly>
                </div>
            </div>
            <div class="form-group">
                <label for="transport_method_id" class="control-label col-md-4">
                    {t}Méthode de transport :{/t}
                </label>
                <div class="col-md-8">
                    <select class="form-control" id="transport_method_id" name="transport_method_id">
                        <option value="" {if $data.transport_method_id=="" }selected{/if}>{t}Choisissez...{/t}</option>
                        {foreach $transport_methods as $transport_method}
                        <option value="{$transport_method.transport_method_id}" {if
                            $transport_method.transport_method_id==$data.transport_method_id}selected{/if}>
                            {$transport_method.transport_method_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="sampled_volume" class="control-label col-md-4"> {t}Volume initial (avant concentration ou dilution) :{/t}</label>
                <div class="col-md-8">
                    <input id="sampled_volume" class="form-control taux" name="sampled_volume" value="{$data.sampled_volume}">
                </div>
            </div>
            <div class="form-group">
                <label for="volume" class="control-label col-md-4"> {t}Volume de reprise (après concentration ou dilution) :{/t}</label>
                <div class="col-md-8">
                    <input id="volume" class="form-control taux" name="volume" value="{$data.volume}">
                </div>
            </div>
            <div class="form-group">
                <label for="weight" class="control-label col-md-4"> {t}Masse :{/t}</label>
                <div class="col-md-8">
                    <input id="weight" class="form-control taux" name="weight" value="{$data.weight}">
                </div>
            </div>

            <div class="form-group">
                <label for="sample_nature_id" class="control-label col-md-4">{t}Nature :{/t}</label>
                <div class="col-md-8">
                    <select class="form-control" id="sample_nature_id" name="sample_nature_id">
                        {foreach $sample_natures as $sample_nature}
                        <option value="{$sample_nature.sample_nature_id}" {if
                            $sample_nature.sample_nature_id==$data.sample_nature_id}selected{/if}>
                            {$sample_nature.sample_nature_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="storage_type_id" class="control-label col-md-4">
                    {t}Type de stockage :{/t}
                </label>
                <div class="col-md-8">
                    <select class="form-control" id="storage_type_id" name="storage_type_id">
                        <option value="" {if $data.storage_type_id=="" }selected{/if}>{t}Choisissez...{/t}</option>
                        {foreach $storage_types as $storage_type}
                        <option value="{$storage_type.storage_type_id}" {if
                            $storage_type.storage_type_id==$data.storage_type_id}selected{/if}>
                            {$storage_type.storage_type_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="storage_location_id" class="control-label col-md-4">
                    {t}Lieu de stockage :{/t}
                </label>
                <div class="col-md-8">
                    <select class="form-control" id="storage_location_id" name="storage_location_id">
                        <option value="" {if $data.storage_location_id=="" }selected{/if}>{t}Choisissez...{/t}</option>
                        {foreach $storage_locations as $storage_location}
                        <option value="{$storage_location.storage_location_id}" {if
                            $storage_location.storage_location_id==$data.storage_location_id}selected{/if}>
                            {$storage_location.storage_location_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="remarks" class="control-label col-md-4">
                    {t}Commentaires :{/t}
                </label>
                <div class="col-md-8">
                    <textarea class="form-control" name="remarks" id="remarks" rows="5">{$data.remarks}</textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="uuid" class="control-label col-md-4"> {t}UUID :{/t}</label>
                <div class="col-md-8">
                    <input id="uuid" class="form-control" name="uuid" value="{$data.uuid}">
                </div>
            </div>

            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
                {if $data.sample_id > 0 }
                <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
                {/if}
            </div>
            {$csrf}
        </form>
    </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>