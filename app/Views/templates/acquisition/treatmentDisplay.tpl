<script>
$(document).ready( function () {
  var treatment_id = "{$data.treatment_id}";
  try {
    if (parseInt(treatment_id) > 0) {
      Cookies.set('treatment_id', treatment_id, { expires: 35, secure: true });
    }
  } catch (e) { }
  /**
   * Management of tabs
   */
  var moduleName = "treatmentDisplay";
  var localStorage = window.localStorage;
  try {
    activeTab = localStorage.getItem(moduleName + "Tab");
  } catch (Exception) {
    activeTab = "";
  }
  try {
    if (activeTab.length > 0) {
      $("#"+activeTab).tab('show');
    }
  } catch (Exception) { }
  $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
			localStorage.setItem(moduleName + "Tab", $(this).attr("id"));
		});
});
</script>
<div class="row">
  <h2>{t}Traitement du {/t}{$data.treatment_date} {t}Logiciel {/t} {$data.software_name}</h2>
  <div class="col-md-12">
    <a href="projectDisplay?project_id={$data.project_id}"><img src="display/images/project.png" height="25">{t}Retour au projet{/t}&nbsp;{$data.project_name}</a>
    {if $rights.manage == 1}
      &nbsp;<a href="treatmentChange?project_id={$data.project_id}&treatment_id={$data.treatment_id}">
        <img src="display/images/edit.gif" height="25">
        {t}Modifier{/t}
      </a>
    {/if}
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <ul class="nav nav-tabs" id="myTab" role="tablist" >
      <li class="nav-item active">
        <a class="nav-link" id="tab-sample" href="#nav-sample"  data-toggle="tab" role="tab" aria-controls="nav-sample" aria-selected="false">
          <img src="display/images/sample.png" height="25">
          {t}Acquisitions d'aliquots rattachées{/t}
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tab-file" href="#nav-file"  data-toggle="tab" role="tab" aria-controls="nav-file" aria-selected="false">
          <img src="display/images/files.png" height="25">
          {t}Fichiers{/t}
        </a>
      </li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane active in" id="nav-sample" role="tabpanel" aria-labelledby="tab-sample">
        <div class="row">
          <div class="col-md-12">
          {include file="acquisition/treatmentListAcquisitionSample.tpl"}
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-file" role="tabpanel" aria-labelledby="tab-file">
        <div class="row">
          <div class="col-lg-12 ">
            {include file="file/fileLoad.tpl"}
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 ">
            {include file="file/fileList.tpl"}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
