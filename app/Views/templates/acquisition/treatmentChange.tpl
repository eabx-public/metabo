<h2>{t}Création - Modification d'un traitement{/t}</h2>
<div class="row">
    <div class="col-md-6">
        <a href="projectDisplay"><img src="display/images/project.png" height="25">
            {t}Retour au projet{/t}
        </a>
        {if $data.treatment_id > 0}
        &nbsp;
        <a href="treatmentDisplay?treatment_id={$data.treatment_id}">
            <img src="display/images/exec.png" height="25">
            {t}Retour au détail du traitement{/t}
        </a>
        {/if}
        <form class="form-horizontal protoform" id="treatmentForm" method="post" action="treatmentWrite">
            <input type="hidden" name="moduleBase" value="treatment">
            <input type="hidden" name="treatment_id" value="{$data.treatment_id}">
            <input type="hidden" name="project_id" value="{$data.project_id}">
            <div class="form-group">
                <label for="treatment_date" class="control-label col-md-4"><span class="red">*</span>
                    {t}Date du traitement :{/t}
                </label>
                <div class="col-md-8">
                    <input id="treatment_date" type="text" class="form-control" name="treatment_date"
                        value="{$data.treatment_date}" autofocus required>
                </div>
            </div>
            <div class="form-group">
                <label for="software_id" class="control-label col-md-4"><span class="red">*</span>
                    {t}Logiciel utilisé :{/t}
                </label>
                <div class="col-md-8">
                    <select id="software_id" name="software_id" class="form-control">
                        {foreach $softwares as $software}
                        <option value="{$software.software_id}" {if
                            $software.software_id==$data.software_id}selected{/if}>
                            {$software.software_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
                {if $data.treatment_id > 0 }
                <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
                {/if}
            </div>
            {$csrf}
        </form>
    </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>