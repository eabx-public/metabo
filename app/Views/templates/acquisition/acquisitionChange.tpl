<h2>{t}Création - Modification d'une acquisition{/t}</h2>
<div class="row">
  <div class="col-md-6">
    <a href="projectDisplay"><img src="display/images/project.png" height="25">{t}Retour au projet{/t}</a>
    {if $data.acquisition_id > 0}
      &nbsp;
      <a href="acquisitionDisplay?acquisition_id={$data.acquisition_id}">
        <img src="display/images/acquisition.png" height="25">
        {t}Retour au détail de l'acquisition{/t}
      </a>
    {/if}
    <form class="form-horizontal protoform" id="acquisitionForm" method="post" action="acquisitionWrite">
      <input type="hidden" name="moduleBase" value="acquisition">
      <input type="hidden" name="acquisition_id" value="{$data.acquisition_id}">
      <input type="hidden" name="project_id" value="{$data.project_id}">
      <div class="form-group">
        <label for="acquisition_date" class="control-label col-md-4"><span class="red">*</span> {t}Date d'acquisition :{/t}</label>
        <div class="col-md-8">
          <input id="acquisition_date" type="text" class="form-control" name="acquisition_date" value="{$data.acquisition_date}"
            autofocus required>
        </div>
      </div>
      <div class="form-group">
        <label for="acquisition_type_id" class="control-label col-md-4"><span class="red">*</span> {t}Type d'acquisition :{/t}</label>
        <div class="col-md-8">
          <select id="acquisition_type_id" name="acquisition_type_id" class="form-control">
            {foreach $acquisitionTypes as $at}
              <option value="{$at.acquisition_type_id}" {if $at.acquisition_type_id == $data.acquisition_type_id}selected{/if}>
              {$at.acquisition_type_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="machine_id" class="control-label col-md-4">{t}Machine utilisée :{/t}</label>
        <div class="col-md-8">
          <select id="machine_id" name="machine_id" class="form-control">
            <option value="" {if $data.machine_id == ""}selected{/if}>{t}Choisissez{/t}</option>
            {foreach $machines as $machine}
              <option value="{$machine.machine_id}" {if $data.machine_id == $machine.machine_id}selected{/if}>
                {$machine.machine_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>

      <div class="form-group">
        <label for="chromatography_method_id" class="control-label col-md-4">{t}Méthode de chromatographie :{/t}</label>
        <div class="col-md-8">
          <select id="chromatography_method_id" name="chromatography_method_id" class="form-control">
            <option value="" {if $data.chromatography_method_id == ""}selected{/if}>{t}Choisissez{/t}</option>
            {foreach $chromatographys as $chromatography}
              <option value="{$chromatography.chromatography_method_id}" {if $data.chromatography_method_id == $chromatography.chromatography_method_id}selected{/if}>
                {$chromatography.chromatography_method_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="acquisition_method_id" class="control-label col-md-4">{t}Méthode d'acquisition :{/t}</label>
        <div class="col-md-8">
          <select id="acquisition_method_id" name="acquisition_method_id" class="form-control">
            <option value="" {if $data.acquisition_method_id == ""}selected{/if}>{t}Choisissez{/t}</option>
            {foreach $acquisition_methods as $method}
              <option value="{$method.acquisition_method_id}" {if $data.acquisition_method_id == $method.acquisition_method_id}selected{/if}>
                {$method.acquisition_method_name}
              </option>
            {/foreach}
          </select>
        </div>
      </div>

      <div class="form-group center">
        <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
        {if $data.acquisition_id > 0 }
        <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
        {/if}
      </div>
    {$csrf}</form>
  </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>
