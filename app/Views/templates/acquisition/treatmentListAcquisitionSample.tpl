<script>
  $(document).ready(function () {
    /**
   * Management of tabs
   */
  var moduleName = "treatmentDisplaySample";
  try {
    activeTab = Cookies.get(moduleName + "Tab");
  } catch (Exception) {
    activeTab = "";
  }
  try {
    if (activeTab.length > 0) {
      $("#"+activeTab).tab('show');
    }
  } catch (Exception) { }
  $('a[data-second="tab"]').on('shown.bs.tab', function () {
			Cookies.set(moduleName + "Tab", $(this).attr("id"), { secure: true});
		});
    $("#checkSample").change(function() {
    $(".checkSample").prop("checked", this.checked);
    });
  });
</script>
<div class="row">
  <div class="col-xs-12">
    <ul class="nav nav-tabs" id="myTab2" role="tablist" >
      <li class="nav-item active">
        <a class="nav-link" id="tab-samplelist" href="#nav-samplelist" data-second="tab" data-toggle="tab" role="tab" aria-controls="nav-samplelist" aria-selected="false">
          <img src="display/images/sample.png" height="25">
          {t}Résultats des acquisitions d'aliquots retraités{/t}
        </a>
      </li>
      {if $treatment_as_id > 0}
      <li class="nav-item">
        <a class="nav-link" id="tab-samplefile" href="#nav-samplefile"  data-second="tab" data-toggle="tab" role="tab" aria-controls="nav-samplefile" aria-selected="false">
          <img src="display/images/files.png" height="25">
          {t}Fichiers associés à l'aliquot {/t} {$treatmentAs.sample_name}
        </a>
      </li>
      {/if}
    </ul>
    <div class="tab-content" id="nav-tabContentsample">
      <div class="tab-pane active in" id="nav-samplelist" role="tabpanel" aria-labelledby="tab-samplelist">
        <div class="row">
          <div class="col-md-8">
            <table id="sample-list" class="table table-bordered table-hover datatable-searching display">
              <thead>
                <tr>
                  <th>{t}Code{/t}</th>
                  <th>{t}Type{/t}</th>
                  <th>{t}Parents{/t}</th>
                  <th>{t}Date d'acquisition{/t}</th>
                  <th>{t}Type d'acquisition{/t}</th>
                  <th>{t}Volume{/t}</th>
                  <th>{t}Utilisé pour la calibration ?{/t}</th>
                  <th>{t}UUID{/t}</th>
                  <th>{t}Fichiers associés{/t}</th>
                  <th>{t}Détacher l'aliquot{/t}</th>
                </tr>
              </thead>
              <tbody>
                {foreach $treatmentsAs as $sample}
                  <tr>
                    <td class="center" nowrap>
                      <a href="sampleDisplay?project_id={$sample.project_id}&acquisition_id={$sample.acquisition_id}&treatment_id={$sample.treatment_id}&sample_id={$sample.sample_id}&treatment_as_id={$sample.treatment_as_id}">
                        {$sample.sample_name}
                      </a>
                    </td>
                    <td>{$sample.sample_type_name}</td>
                    <td >
                      {foreach $sample.parents as $parent}
                        <a href="sampleDisplay?project_id{$sample.project_id}&condition_id={$condition.condition_id}&sample_id={$parent.sample_id}">
                        <span class="nowrap">{$parent.sample_name}</span>
                        </a>
                      {/foreach}
                    </td>
                    <td>
                      <a href="acquisitionDisplay?acquisition_id={$sample.acquisition_id}&project_id={$sample.project_id}">
                        {$sample.acquisition_date}
                      </a>
                    </td>
                    <td>{$sample.acquisition_type_name}</td>
                    <td class="right">{$sample.volume} {$sample.unit}</td>
                    <td class="center">{if $sample.is_calibration == 1}{t}oui{/t}{/if}</td>
                    <td nowrap>{$sample.uuid}</td>
                    <td class="center {if $treatment_as_id == $sample.treatment_as_id}itemselected{/if}"
                    title="{t}Visualisez ou ajoutez les fichiers de résultat du traitement{/t}">
                      <a href="treatmentDisplay?project_id={$sample.project_id}&acquisition_sample_id={$sample.acquisition_sample_id}&treatment_id={$sample.treatment_id}&treatment_as_id={$sample.treatment_as_id}">
                        <img src="display/images/files.png" height="25">
                      </a>
                    </td>
                    <td class="center">
                      <a href="treatment_asDelete?treatment_as_id={$sample.treatment_as_id}&project_id={$sample.project_id}&treatment_id={$data.treatment_id}" onclick="return confirm('{t}Confirmez-vous la suppression ?{/t}');">
                        <img src="display/images/corbeille.png" height="25">
                      </a>
                    </td>
                  </tr>
                {/foreach}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {if $treatment_as_id > 0}
        <div class="tab-pane fade" id="nav-samplefile" role="tabpanel" aria-labelledby="tab-samplefile">
          <div class="row">
            <div class="col-lg-12">
              {include file="file/fileLoad2.tpl"}
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              {include file="file/fileList2.tpl"}
            </div>
          </div>
        </div>
      {/if}
    </div>
  </div>
</div>
