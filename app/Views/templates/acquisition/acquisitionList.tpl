{if $rights.manage == 1}
  <a href="acquisitionChange?project_id={$data.project_id}&acquisition_id=0">
    <img src="display/images/new.png" height="25">
    {t}Nouvelle acquisition{/t}
  </a>
{/if}
<table id="acquisitionList" class="table table-bordered table-hover datatable display">
  <thead>
    <tr>
      <th>{t}Détail{/t}</th>
      <th>{t}Type d'acquisition{/t}</th>
      <th >{t}Date d'acquisition{/t}</th>
      <th >{t}Machine utilisée{/t}</th>
      <th>{t}Méthode de chromatographie{/t}</th>
      <th>{t}Méthode d'acquisition{/t}</th>
  </tr>
  </thead>
  <tbody>
  {foreach $acquisitions as $acquisition}
    <tr>
      <td class="center">
        <a href="acquisitionDisplay?project_id={$data.project_id}&acquisition_id={$acquisition.acquisition_id}">
          <img src="display/images/acquisition.png" height="25">
        </a>
      </td>
      <td>{$acquisition.acquisition_type_name}</td>
      <td class="center">{$acquisition.acquisition_date}</td>
      <td>{$acquisition.machine_name}</td>
      <td>{$acquisition.chromatography_method_name}</td>
      <td>{$acquisition.acquisition_method_name}</td>
    </tr>
  {/foreach}
  </tbody>
</table>
