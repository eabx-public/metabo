<script>
$(document).ready( function () {
  var acquisition_id = "{$data.acquisition_id}";
  try {
    if (parseInt(acquisition_id) > 0) {
      Cookies.set('acquisition_id', acquisition_id, { expires: 35, secure: true });
    }
  } catch (e) { }
  /**
   * Management of tabs
   */
   var moduleName = "acquisitionDisplay";
   var localStorage = window.localStorage;
  try {
    activeTab = localStorage.getItem(moduleName + "Tab");
  } catch (Exception) {
    activeTab = "";
  }
  try {
    if (activeTab.length > 0) {
      $("#"+activeTab).tab('show');
    }
  } catch (Exception) { }
  $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
			localStorage.setItem(moduleName + "Tab", $(this).attr("id"));
		});
});
</script>
<div class="row">
  <h2>{t}Acquisition du {/t}{$data.acquisition_date}</h2>
  <div class="col-md-12">
    <a href="projectDisplay?project_id={$data.project_id}"><img src="display/images/project.png" height="25">{t}Retour au projet{/t}&nbsp;{$data.project_name}</a>
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <ul class="nav nav-tabs" id="myTab" role="tablist" >
      <li class="nav-item active">
        <a class="nav-link" id="tab-detail" data-toggle="tab"  role="tab" aria-controls="nav-detail" aria-selected="true" href="#nav-detail">
          <img src="display/images/zoom.png" height="25">
          {t}Détails{/t}
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tab-sample" href="#nav-sample"  data-toggle="tab" role="tab" aria-controls="nav-sample" aria-selected="false">
          <img src="display/images/sample.png" height="25">
          {t}Aliquots{/t}
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tab-file" href="#nav-file"  data-toggle="tab" role="tab" aria-controls="nav-file" aria-selected="false">
          <img src="display/images/files.png" height="25">
          {t}Fichiers{/t}
        </a>
      </li>
    </ul>
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane active in" id="nav-detail" role="tabpanel" aria-labelledby="tab-detail">
        <div class="row">
          <a href="acquisitionChange?project_id={$data.project_id}&acquisition_id={$data.acquisition_id}">
            <img src="display/images/edit.gif" height="25">&nbsp;
            {t}Modifier{/t}
          </a>
        </div>
        <div class="col-md-6 form-display">
          <dl class="dl-horizontal">
            <dt>{t}Date d'acquisition :{/t}</dt>
            <dd>{$data.acquisition_date}</dd>
          </dl>
          <dl class="dl-horizontal">
            <dt>{t}Type d'acquisition :{/t}   </dt>
            <dd>{$data.acquisition_type_name}</dd>
          </dl>
          <dl class="dl-horizontal">
            <dt>{t}Machine utilisée :{/t}</dt>
            <dd>{$data.machine_name}</dd>
          </dl>

          <dl class="dl-horizontal">
            <dt>{t}Méthode de chromatographie :{/t}</dt>
            <dd>{$data.chromatography_method_name}</dd>
          </dl>
          <dl class="dl-horizontal">
            <dt>{t}Méthode d'acquisition :{/t}</dt>
            <dd>{$data.acquisition_method_name}</dd>
          </dl>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-sample" role="tabpanel" aria-labelledby="tab-sample">
        <div class="row">
          <div class="col-md-12">
            {include file="sample/sampleListAcquisition.tpl"}
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="nav-file" role="tabpanel" aria-labelledby="tab-file">
        <div class="row">
          <div class="col-md-12">
            {include file="file/fileLoad.tpl"}
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            {include file="file/fileList.tpl"}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
