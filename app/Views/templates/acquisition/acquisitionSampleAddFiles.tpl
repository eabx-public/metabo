<script>
    $(document).ready(function () {
        $(".source").draggable({
            revert: "invalid",
            cursor: "move",
            helper: "clone"
        });
        $(".dest").droppable({
            hoverClass: "ui-state-active",
            drop: function (event, ui) {
                var content = $("input", ui.draggable).val();
                $("input", ui.draggable).val("");
                if (content === undefined) {
                    content = ui.draggable.text();
                    $(ui.draggable).addClass("blue");
                }
                $("input", this).val(content);
            }
        });
        /*
        $("destinput").draggable({
            revert: "invalid",
            cursor: "move",
            helper: "clone"
        });*/
        $("#dateChange").change(function () {
            $(".dateChange").val($(this).val());
        });
        $("#fileType").change(function () {
            $(".fileType").val($(this).val());
        });
    });
</script>
<h2>{t}Associer les fichiers téléchargés avec les aliquots{/t}</h2>
<div class="row">
    <div class="col-md-12">
        <a href="projectList"><img src="display/images/list.png" height="25">
            {t}Retour à la liste des projets{/t}
        </a>
        &nbsp;
        <a href="projectDisplay?project_id={$acquisition.project_id}"><img src="display/images/project.png" height="25">
            {t}Retour au projet{/t}&nbsp;{$acquisition.project_name}
        </a>
        &nbsp;
        <a href="acquisitionDisplay?acquisition_id={$acquisition.acquisition_id}">
            <img src="display/images/acquisition.png" height="25">{t}Retour à l'acquisition{/t}
        </a>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <fieldset>
            <legend>{t}aliquots{/t}</legend>
            <form id="formSample" method="POST" action="acquisitionSampleAddDownloadedFileExec">
                <input type="hidden" name="acquisition_id" value="{$acquisition.acquisition_id}">
                <table class="table table-bordered table-hover datatable-nopaging display">
                    <thead>
                        <tr>
                            <th>{t}Code{/t}</th>
                            <th>
                                {t}Date de création{/t}
                                <br>
                                <input id="dateChange" class="tableinput datetimepicker" value="{$defaultDateTime}"
                                    title="{t}Changer l'ensemble des dates-heures{/t}">
                            </th>
                            <th>
                                {t}Type de fichier{/t}
                                <br>
                                <select class="tableinput" id="fileType">
                                    {foreach $filetypes as $filetype}
                                    <option value="{$filetype.filetype_id}">{$filetype.filetype_name}</option>
                                    {/foreach}
                                </select>
                            </th>
                            <th>{t}Nom du fichier{/t}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $samples as $sample}
                        <tr>
                            <td>
                                <a href="sampleDisplay?sample_id={$sample.sample_id}">
                                    {$sample.sample_name}
                                </a>
                            </td>
                            <td>
                                <input class="tableinput datetimepicker dateChange"
                                    name="date{$sample.acquisition_sample_id}" value="{$defaultDateTime}">
                            </td>
                            <td>
                                <select class="fileType tableinput" name="type{$sample.acquisition_sample_id}">
                                    {foreach $filetypes as $filetype}
                                    <option value="{$filetype.filetype_id}">{$filetype.filetype_name}</option>
                                    {/foreach}
                                </select>
                            </td>
                            <td class="dest">
                                <input class="tableinput" name="sample{$sample.acquisition_sample_id}"
                                    value="{$sample.filename}">
                            </td>
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
                <div class="center">
                    <button type="submit" class="bg-primary">{t}Charger et apparier les fichiers{/t}</button>
                </div>
                {$csrf}
            </form>
        </fieldset>
        <div class="row center">
            <span class="bg-info">
                {t}Vous pouvez déplacer les noms de fichiers avec la souris !{/t}
            </span>
        </div>
    </div>
    <div class="col-md-4">
        <fieldset>
            <legend>{t}Fichiers téléchargés{/t}</legend>
            {include file="file/downloadedList.tpl"}
        </fieldset>
    </div>
</div>