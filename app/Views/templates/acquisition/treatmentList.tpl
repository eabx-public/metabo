{if $rights.manage == 1}
  <a href="treatmentChange?project_id={$data.project_id}&treatment_id=0">
    <img src="display/images/new.png" height="25">
    {t}Nouveau traitement{/t}
  </a>
{/if}

<table id="treatmentList" class="table table-bordered table-hover datatable display">
  <thead>
    <tr>
      <th>{t}Date{/t}</th>
      <th>{t}Logiciel utilisé{/t}</th>
      <th>{t}Nombre d'acquisitions rattachées{/t}</th>
    </tr>
  </thead>
  <tbody>
    {foreach $treatments as $treatment}
      <tr>
        <td>
          <a href="treatmentDisplay?treatment_id={$treatment.treatment_id}&project_id={$treatment.project_id}">
            {$treatment.treatment_date}
          </a>
        </td>
        <td>{$treatment.software_name}</td>
        <td class="center">{$treatment.acquisition_nb}</td>
      </tr>
    {/foreach}
  </tbody>
</table>
