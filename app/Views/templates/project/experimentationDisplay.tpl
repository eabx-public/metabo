<script>
    $(document).ready(function () {
        var experimentation_id = "{$experimentation.experimentation_id}";
        try {
            if (parseInt(experimentation_id) > 0) {
                Cookies.set('experimentation_id', experimentation_id, { expires: 35, secure: true });
            }
        } catch (e) { }
        /**
         * Management of tabs
         */
        var moduleName = "experimentationDisplay";
        var localStorage = window.localStorage;
        try {
            activeTab = localStorage.getItem(moduleName + "Tab");
        } catch (Exception) {
            activeTab = "";
        }
        try {
            if (activeTab.length > 0) {
                $("#" + activeTab).tab('show');
            }
        } catch (Exception) { }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
            localStorage.setItem(moduleName + "Tab", $(this).attr("id"));
        });
    });
</script>

<h2>{t}Expérimentation{/t}&nbsp;{$experimentation.experimentation_name}
    ({t}projet{/t}&nbsp;{$experimentation.project_name})</h2>
<div class="row">
    <div class="col-md-12">
        <a href="projectDisplay?project_id={$experimentation.project_id}"><img src="display/images/project.png"
                height="25">{t}Retour au projet{/t}&nbsp;{$project.project_name}</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item active">
                <a class="nav-link" id="tab-detail" data-toggle="tab" role="tab" aria-controls="nav-detail"
                    aria-selected="true" href="#nav-detail">
                    <img src="display/images/zoom.png" height="25">
                    {t}Détails{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-condition" href="#nav-condition" data-toggle="tab" role="tab"
                    aria-controls="nav-sample" aria-selected="false">
                    <img src="display/images/condition.png" height="25">
                    {t}Conditions{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-file" href="#nav-file" data-toggle="tab" role="tab" aria-controls="nav-file"
                    aria-selected="false">
                    <img src="display/images/files.png" height="25">
                    {t}Fichiers{/t}
                </a>
            </li>
        </ul>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane active in" id="nav-detail" role="tabpanel" aria-labelledby="tab-detail">
                <div class="row">
                    <a
                        href="experimentationChange?experimentation_id={$experimentation.experimentation_id}&project_id={$experimentation.project_id}">
                        <img src="display/images/edit.gif" height="25">
                        {t}Modifier{/t}
                    </a>
                </div>

                <div class="col-md-6 form-display">
                    <dl class="dl-horizontal">
                        <dt>{t}Type :{/t}</dt>
                        <dd>{$experimentation.experimentation_type_name}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>{t}du :{/t}</dt>
                        <dd>{$experimentation.experimentation_from}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>{t}au :{/t}</dt>
                        <dd>{$experimentation.experimentation_to}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>{t}Description :{/t}</dt>
                        <dd class="textareaDisplay">{$experimentation.experimentation_comment}</dd>
                    </dl>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-condition" role="tabpanel" aria-labelledby="tab-condition">
                <div class="row">
                    <div class="col-md-8">
                        {include file="sample/conditionList.tpl"}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-file" role="tabpanel" aria-labelledby="tab-file">
                <div class="row">
                    <div class="col-lg-12">
                        {include file="file/fileLoad.tpl"}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {include file="file/fileList.tpl"}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>