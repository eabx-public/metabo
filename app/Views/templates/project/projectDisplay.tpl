<script>
    $(document).ready(function () {
        var project_id = "{$data.project_id}";
        var sampleTypeId = "{$sample_type_id}";
        if (sampleTypeId == 2) {

        } else if (sampleTypeId == 3) {
            $(".newAliquote").hide();
        } else {
            $("#acquisitionForm").hide();
        }
        try {
            if (parseInt(project_id) > 0) {
                Cookies.set('project_id', project_id, { expires: 35, secure: true });
            }
        } catch (e) { }
        $("#project_id").change(function () {
            $("#projectSelect").submit();
        });
        $("#sample_type_id").change(function () {
            Cookies.set("sample_type_id", $(this).val(), { secure: true });
            $("#projectSampleSelectType").submit();
        });
        $("#filetype_id").change(function () {
            Cookies.set("filetype_id", $(this).val(), { secure: true });
            $("#projectFileSelectType").submit();
        });
        /**
        * Management of tabs
        */
        var moduleName = "projectDisplay";
        var localStorage = window.localStorage;
        try {
            activeTab = localStorage.getItem(moduleName + "Tab");
        } catch (Exception) {
            activeTab = "";
        }
        try {
            if (activeTab.length > 0) {
                $("#" + activeTab).tab('show');
            }
        } catch (Exception) { }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
            localStorage.setItem(moduleName + "Tab", $(this).attr("id"));
        });
        $("#acquisition_id").change(function () {
            var extractionId = $(this).val();
            if (extractionId == 0) {
                $(".newAcquisition").show();
            } else {
                $(".newAcquisition").hide();
            }
        });

        $("#sampleAction").change(function () {
            var action = $("#sampleAction").val();
        });
        var actions = {
            "acquisitionCreateWithSamples": "acquisitionForm",
            "samplesSendToCollec": "samplesSend",
            "sampleDeleteMulti": "sampleDeleteMulti",
            "sampleAttachToAcquisition": "sampleAttachToAcquisition",
            "createExtractsFromReplicates": "extractsFromReplicates"
        };
        $("#sampleAction").change(function () {
            var action = $(this).val();
            var actionClass = actions[action];
            var value;
            for (const key in actions) {
                if (actions.hasOwnProperty(key)) {
                    value = actions[key];
                    if (value == actionClass) {
                        $("#" + value).show();
                    } else {
                        $("#" + value).hide();
                    }
                }
            };
            $("#sampleSelectModule").val(action);
        });
        $("#sampleSelect").submit(function (event) {
            if ($("#sampleSelectModule").val().length == 0) {
                event.preventDefault();
            } else {
                if (!operationConfirm()) {
                    event.preventDefault();
                } else {
                    $("#sampleSelect").attr("action", $("#sampleSelectModule").val());
                }
            }
        });

        $("#fileListAction").change(function () {
            var action = $("#fileListAction").val();
            $(".fileListItem").hide();
            $("#" + action).show();
        });
        $("#projectFileList").submit(function (event) {
            var action = $("#fileListAction").val();
            if (action == "nothing") {
                event.preventDefault();
            } else {
                if (!confirm("{t}Confirmez-vous cette opération ?{/t}")) {
                    event.preventDefault();
                } else {
                    $(this).attr("action", action);
                }
            }
        });
    });
</script>


{if $rights.param == 1}
<div class="row">
    <div class="col-md-6">
        <a href="projectChange?project_id=0">
            <img src="display/images/new.png" height="25">
            {t}Nouveau projet{/t}
        </a>
    </div>
</div>
{/if}
<div class="row">
    <div class="col-md-6">
        <form id="projectSelect" class="form-horizontal" method="get" action="projectDisplay">
            <div class="form-group">
                <label for="project_id" class="control-label col-md-4">{t}Nom :{/t}</label>
                <div class="col-md-6">
                    <select name="project_id" id="project_id" class="form-control">
                        {foreach $projects as $project}
                        <option value="{$project.project_id}" {if $project.project_id==$data.project_id}selected{/if}>
                            {$project.project_year} {$project.project_name} {if !empty($project.project_code) ||
                            !empty($project.mama_code)}({$project.project_code} {$project.mama_code}){/if}
                        </option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary">{t}Ouvrir{/t}</button>
                </div>
            </div>
            {$csrf}
        </form>
    </div>
    {if $data.project_id > 0}
    <div class="col-md-6 form-display">
        <dl class="dl-horizontal">
            <dt>{t}Description :{/t}</dt>
            <dd class="textareaDisplay">{$data.project_description}</dd>
        </dl>
    </div>
    {/if}
</div>
{if $data.project_id > 0}
<div class="row">
    <div class="col-xs-12">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item active">
                <a class="nav-link" id="tab-experimentation" data-toggle="tab" role="tab"
                    aria-controls="nav-experimentation" aria-selected="true" href="#nav-experimentation">
                    <img src="display/images/experiment.png" height="25">
                    {t}Expérimentations{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-condition" href="#nav-condition" data-toggle="tab" role="tab"
                    aria-controls="nav-condition" aria-selected="false">
                    <img src="display/images/condition.png" height="25">
                    {t}Conditions{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-sampling" href="#nav-sampling" data-toggle="tab" role="tab"
                    aria-controls="nav-sampling" aria-selected="false">
                    <img src="display/images/sampling.png" height="25">
                    {t}Échantillonnages{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-extraction" href="#nav-extraction" data-toggle="tab" role="tab"
                    aria-controls="nav-extraction" aria-selected="false">
                    <img src="display/images/extract.png" height="25">
                    {t}Extractions{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-acquisition" href="#nav-acquisition" data-toggle="tab" role="tab"
                    aria-controls="nav-acquisition" aria-selected="false">
                    <img src="display/images/acquisition.png" height="25">
                    {t}Acquisitions{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-treatment" href="#nav-treatment" data-toggle="tab" role="tab"
                    aria-controls="nav-treatment" aria-selected="false">
                    <img src="display/images/exec.png" height="25">
                    {t}Traitements{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-sample" href="#nav-sample" data-toggle="tab" role="tab"
                    aria-controls="nav-sample" aria-selected="false">
                    <img src="display/images/sample.png" height="25">
                    {t}Tous types d'échantillons{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-file" href="#nav-file" data-toggle="tab" role="tab" aria-controls="nav-file"
                    aria-selected="false">
                    <img src="display/images/files.png" height="25">
                    {t}Tous fichiers associés{/t}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="tab-fileproject" href="#nav-fileproject" data-toggle="tab" role="tab"
                    aria-controls="nav-fileproject" aria-selected="false">
                    <img src="display/images/files.png" height="25">
                    {t}Fichiers descriptifs du projet{/t}
                </a>
            </li>
        </ul>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane active in" id="nav-experimentation" role="tabpanel"
                aria-labelledby="tab-experimentation">
                <div class="row">
                    <div class="col-md-8">
                        {include file="project/experimentationList.tpl"}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-condition" role="tabpanel" aria-labelledby="tab-condition">
                <div class="row">
                    <div class="col-md-8">
                        {include file="sample/conditionList.tpl"}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-sampling" role="tabpanel" aria-labelledby="tab-sampling">
                <div class="row">
                    <div class="col-md-8">
                        <form id="samplingSelect" method="post" action="extractionCreateFromSamplings">
                            <input type="hidden" name="project_id" value="{$data.project_id}">
                            <input type="hidden" name="extraction_id" value="0">
                            {include file="sample/samplingList.tpl"}

                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-md-4">
                                        {t}Pour les échantillonnages sélectionnés :{/t}
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="sampling_extraction_date" class="control-label col-md-4">
                                        {t}Date d'extraction :{/t}
                                    </label>
                                    <div class="col-md-8">
                                        <input id="sampling_extraction_date" name="extraction_date"
                                            value="{$extraction_date}" class="datepicker form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sampling_extraction_method_id" class="control-label col-md-4">
                                        {t}Méthode d'extraction :{/t}
                                    </label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="sampling_extraction_method_id"
                                            name="extraction_method_id">
                                            <option value="" selected>{t}Choisissez{/t}</option>
                                            {foreach $extraction_methods as $method}
                                            <option value="{$method.extraction_method_id}">
                                                {$method.extraction_method_name}
                                            </option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group center">
                                    <button id="newExtractionFromSamplingButton" type="submit"
                                        class="btn btn-danger button-valid confirm">
                                        {t}Créez les extractions{/t}
                                    </button>
                                </div>
                            </div>
                            {$csrf}
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-extraction" role="tabpanel" aria-labelledby="tab-extraction">
                <div class="row">
                    <div class="col-md-8">
                        {include file="sample/extractionList.tpl"}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-acquisition" role="tabpanel" aria-labelledby="tab-acquisition">
                <div class="row">
                    <div class="col-md-8">
                        {include file="acquisition/acquisitionList.tpl"}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-treatment" role="tabpanel" aria-labelledby="tab-treatment">
                <div class="row">
                    <div class="col-md-8">
                        {include file="acquisition/treatmentList.tpl"}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-sample" role="tabpanel" aria-labelledby="tab-sample">
                <div class="row">
                    <form id="projectSampleSelectType" action="projectDisplay" method="post">
                        <input type="hidden" name="project_id" value="{$data.project_id}">
                        <label class="control-label col-md-2 right">{t}Type d'échantillon :{/t}</label>
                        <div class="col-md-2">
                            <select class="form-control" id="sample_type_id" name="sample_type_name">
                                <option value="0" {if $sample_type_id==0}selected{/if}>{t}Tous types{/t}</option>
                                {foreach $sampleTypes as $sampleType}
                                <option value="{$sampleType.sample_type_id}" {if
                                    $sampleType.sample_type_id==$sample_type_id}selected{/if}>
                                    {$sampleType.sample_type_name}
                                </option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">{t}Rechercher{/t}</button>
                        </div>
                        {$csrf}
                    </form>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form id="sampleSelect" method="post" action="">
                            <input type="hidden" id="sampleSelectModule" name="module" value="">
                            <input type="hidden" name="project_id" value="{$data.project_id}">
                            <input type="hidden" name="sample_type_id" value="{$sample_type_id}">
                            {include file="sample/sampleList.tpl"}
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="sampleAction" class="control-label col-md-4">
                                        {t}Pour les échantillons sélectionnés :{/t}
                                    </label>
                                    <div class="col-md-8">
                                        <select id="sampleAction" class="form-control">
                                            <option value="">{t}Choisissez :{/t}</option>
                                            {if $sample_type_id == 1}
                                            <option value="createExtractsFromReplicates">
                                                {t}Créez les extraits à partir des réplicats{/t}
                                            </option>
                                            {/if}
                                            {if $sample_type_id == 2}
                                            <option value="acquisitionCreateWithSamples">
                                                {t}Créez les aliquots et associez-les à une acquisition{/t}
                                            </option>
                                            {/if}
                                            {if $sample_type_id == 3}
                                            <option value="sampleAttachToAcquisition">
                                                {t}Attachez les aliquots à une acquisition{/t}
                                            </option>
                                            {/if}
                                            <option value="samplesSendToCollec">
                                                {t}Envoyez les échantillons vers l'instance de Collec-Science{/t}
                                            </option>
                                            <option value="sampleDeleteMulti">
                                                {t}Supprimez les échantillons{/t}
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div id="extractsFromReplicates" hidden>
                                    <input type="hidden" name="sampling_id" value="0">
                                    <div class="form-group">
                                        <label for="extraction_id"
                                            class="control-label col-md-4">{t}Extraction{/t}</label>
                                        <div class="col-md-8">
                                            <select id="extraction_id" name="extraction_id" class="form-control">
                                                <option value="0" selected>{t}Nouvelle extraction{/t}</option>
                                                {foreach $extractions as $extraction}
                                                <option value="{$extraction.extraction_id}">
                                                    {$extraction.extraction_date} - {$extraction.extraction_method_name}
                                                </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group newExtraction">
                                        <label for="extraction_date" class="control-label col-md-4">
                                            {t}Date d'extraction :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <input id="extraction_date" name="extraction_date"
                                                value="{$extraction_date}" class="datepicker form-control">
                                        </div>
                                    </div>
                                    <div class="form-group newExtraction">
                                        <label for="extraction_method_id" class="control-label col-md-4">
                                            {t}Méthode d'extraction :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="extraction_method_id"
                                                name="extraction_method_id">
                                                <option value="" {if $data.extraction_method_id=="" }selected{/if}>
                                                    {t}Choisissez{/t}</option>
                                                {foreach $extraction_methods as $method}
                                                <option value="{$method.extraction_method_id}" {if
                                                    $method.extraction_method_id==$data.extraction_method_id}selected{/if}>
                                                    {$method.extraction_method_name}
                                                </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="sample_nature_id" class="control-label col-md-4">
                                            {t}Nature des extraits :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <select class="form-control" id="sample_nature_id" name="sample_nature_id">
                                                {foreach $sample_natures as $sample_nature}
                                                <option value="{$sample_nature.sample_nature_id}" {if
                                                    $sample_nature.sample_nature_id==$data.sample_nature_id}selected{/if}>
                                                    {$sample_nature.sample_nature_name}
                                                </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nbextrait" class="control-label col-md-4"> 
                                            {t}Nombre d'extraits à créer (numérotés à partir de A,B si > 1) :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <input type="number" id="nbextrait" class="form-control nombre"
                                                name="nbextrait" value="1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="volumeExtract" class="control-label col-md-4"> 
                                            {t}Volume extrait (µl) :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <input id="volumeExtract" class="form-control taux" name="volumeExtract"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="weight" class="control-label col-md-4"> 
                                            {t}ou masse extraite (mg) :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <input id="weight" class="form-control taux" name="weight" value="">
                                        </div>
                                    </div>
                                    <div class="form-group center">
                                        <button id="sampleButton" type="submit"
                                            class="btn btn-primary button-valid">
                                            {t}Créer les extraits{/t}
                                        </button>
                                    </div>
                                </div>
                                <div id="acquisitionForm" hidden>
                                    <div class="form-group">
                                        <label for="acquisition_id"
                                            class="control-label col-md-4">{t}Acquisition{/t}</label>
                                        <div class="col-md-8">
                                            <select id="acquisition_id" name="acquisition_id" class="form-control">
                                                <option value="0" selected>{t}Nouvelle acquisition{/t}</option>
                                                {foreach $acquisitions as $acquisition}
                                                <option value="{$acquisition.acquisition_id}">
                                                    {$acquisition.acquisition_date} ({$acquisition.machine_name})
                                                </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="newAcquisition">
                                        <div class="form-group">
                                            <label for="acquisition_date" class="control-label col-md-4">
                                                <span
                                                    class="red">*</span> {t}Date d'acquisition :{/t}
                                                </label>
                                            <div class="col-md-8">
                                                <input id="acquisition_date" type="text" class="form-control datepicker"
                                                    name="acquisition_date" value="{$acquisition_date}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="acquisition_type_id" class="control-label col-md-4">
                                                <span
                                                    class="red">*</span> {t}Type d'acquisition :{/t}
                                                </label>
                                            <div class="col-md-8">
                                                <select id="acquisition_type_id" name="acquisition_type_id"
                                                    class="form-control">
                                                    {foreach $acquisitionTypes as $at}
                                                    <option value="{$at.acquisition_type_id}">
                                                        {$at.acquisition_type_name}
                                                    </option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="machine_id" class="control-label col-md-4">
                                                {t}Machine utilisée :{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <select id="machine_id" name="machine_id" class="form-control">
                                                    <option value="" selected>{t}Choisissez{/t}</option>
                                                    {foreach $machines as $machine}
                                                    <option value="{$machine.machine_id}">
                                                        {$machine.machine_name}
                                                    </option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="chromatography_method_id"
                                                class="control-label col-md-4">
                                                {t}Méthode de chromatographie :{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <select id="chromatography_method_id" name="chromatography_method_id"
                                                    class="form-control">
                                                    <option value="" selected>{t}Choisissez{/t}</option>
                                                    {foreach $chromatographys as $chromatography}
                                                    <option value="{$chromatography.chromatography_method_id}">
                                                        {$chromatography.chromatography_method_name}
                                                    </option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="acquisition_method_id" class="control-label col-md-4">
                                                {t}Méthode d'acquisition :{/t}
                                            </label>
                                            <div class="col-md-8">
                                                <select id="acquisition_method_id" name="acquisition_method_id"
                                                    class="form-control">
                                                    <option value="" selected>{t}Choisissez{/t}</option>
                                                    {foreach $acquisition_methods as $method}
                                                    <option value="{$method.acquisition_method_id}">
                                                        {$method.acquisition_method_name}
                                                    </option>
                                                    {/foreach}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group newAliquote">
                                        <label for="sampled_volume" class="control-label col-md-4"> 
                                            {t}Volume extrait (µL) :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <input id="sampled_volume" class="form-control taux" name="sampled_volume">
                                        </div>
                                    </div>
                                    <div class="form-group newAliquote">
                                        <label for="volume" class="control-label col-md-4"> 
                                            {t}Volume de reprise (après dilution ou concentration) (µL) :{/t}
                                        </label>
                                        <div class="col-md-8">
                                            <input id="volume" class="form-control taux" name="volume">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="is_calibrationfirst" class="control-label col-md-4">
                                            {t}Utilisé pour la calibration ?{/t}
                                        </label>
                                        <div class="col-md-8" id="is_calibrationfirst">
                                            <label class="radio-inline">
                                                <input type="radio" name="is_calibration" id="is_calibrationfirst1"
                                                    value="1">{t}oui{/t}
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="is_calibration" id="is_calibrationfirst0" value="0"
                                                    checked="checked">{t}non{/t}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group center">
                                        <button id="sampleAcquisitionButton" type="submit"
                                            class="btn btn-primary button-valid">
                                            {t}Créer les aliquots/associer à l'acquisition{/t}
                                        </button>
                                    </div>
                                </div>
                                <div id="samplesSend" hidden>
                                    <div class="form-group center">
                                        <button id="sampleSendButton" type="submit"
                                            class="btn btn-primary button-valid">
                                            {t}Envoyez vers Collec-Science{/t}
                                        </button>
                                    </div>
                                </div>
                                <div id="sampleDeleteMulti" hidden>
                                    <div class="form-group center">
                                        <button id="sampleDeleteMultiButton" type="submit"
                                            class="btn btn-danger button-valid">
                                            {t}Supprimez les échantillons{/t}
                                        </button>
                                    </div>
                                </div>
                                <div id="sampleAttachToAcquisition" hidden>
                                    <div class="form-group">
                                        <label for="acquisition_id"
                                            class="control-label col-md-4">{t}Acquisition{/t}</label>
                                        <div class="col-md-8">
                                            <select id="acquisition_id" name="acquisition_id" class="form-control">
                                                {foreach $acquisitions as $acquisition}
                                                <option value="{$acquisition.acquisition_id}">
                                                    {$acquisition.acquisition_date} ({$acquisition.machine_name})
                                                </option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="is_calibration" class="control-label col-md-4">
                                            {t}Utilisé pour la calibration ?{/t}
                                        </label>
                                        <div class="col-md-8" id="is_calibration">
                                            <label class="radio-inline">
                                                <input type="radio" name="is_calibration" id="is_calibration1"
                                                    value="1">{t}oui{/t}
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="is_calibration" id="is_calibration0" value="0"
                                                    checked="checked">{t}non{/t}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group center">
                                        <button id="sampleAttachToAcquisitionButton" type="submit"
                                            class="btn btn-danger button-valid">
                                            {t}Attachez les aliquots{/t}
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {$csrf}
                        </form>
                    </div>
                </div>
            </div>
            <!-- files -->
            <div class="tab-pane fade" id="nav-file" role="tabpanel" aria-labelledby="tab-file">
                <div class="row">
                    <form id="projectFileSelectType" action="projectDisplay" method="post">
                        <input type="hidden" name="project_id" value="{$data.project_id}">
                        <label class="control-label col-md-2 right">{t}Type de fichier :{/t}</label>
                        <div class="col-md-2">
                            <select class="form-control" id="filetype_id" name="filetype_name">
                                <option value="0" {if $filetype_id==0}selected{/if}>{t}Tous types{/t}</option>
                                {foreach $fileTypes as $fileType}
                                <option value="{$fileType.filetype_id}" {if
                                    $fileType.filetype_id==$filetype_id}selected{/if}>
                                    {$fileType.filetype_name}
                                </option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">{t}Rechercher{/t}</button>
                        </div>
                        {$csrf}
                    </form>
                </div>
                <form id="projectFileList" action="projectDisplay" method="post">
                    <div class="row">
                        <div class="col-md-10 col-lg-8 col-sm-12">
                            <input type="hidden" name="project_id" value="{$data.project_id}">
                            <input id="projectFileListModule" type="hidden" name="module" value="">
                            {include file="file/fileList.tpl"}

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-lg-8 col-sm-12 form-horizontal">
                            <fieldset>
                                <legend>{t}Pour les fichiers sélectionnés :{/t}</legend>
                                <div class="form-group">
                                    <select class="form-control" id="fileListAction">
                                        <option value="nothing" selected>{t}Choisissez...{/t}</option>
                                        <option value="filesGenerateSimpleLink">
                                            {t}Générez des liens de téléchargement "simples"{/t}
                                        </option>
                                        <option value="filesGenerateLink">
                                            {t}Générez les liens de téléchargement associés avec le code de contrôle et le nom du fichier{/t}
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="center">
                                        <button type="submit" class="btn btn-danger"
                                            id="buttonExec">{t}Exécuter{/t}</button>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    {$csrf}
                </form>
            </div>
            <div class="tab-pane fade" id="nav-fileproject" role="tabpanel" aria-labelledby="tab-fileproject">
                <div class="row">
                    <div class="col-md-12">
                        {include file="file/fileLoad2.tpl"}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        {include file="file/fileList2.tpl"}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{/if}