<h2>{t}Projets{/t}</h2>

{if $rights.param == 1}
<div class="row">
  <div class="col-md-6">
    <a href="projectChange?project_id=0">
      <img src="display/images/new.png" height="25">
      {t}Nouveau projet{/t}
    </a>
  </div>
</div>
{/if}
<div class="row">
  <div class="col-lg-8">
    <table id="projectList" class="table table-bordered table-hover datatable display" data-order='[[0, "desc"],[1,"asc"]]'>
      <thead>
        <tr>
          <th>{t}Année{/t}</th>
          <th>{t}Nom du projet{/t}</th>
          <th>{t}Id{/t}</th>
          <th>{t}Description{/t}</th>
          <th>{t}Code interne{/t}</th>
          <th>{t}Code MAMA{/t}</th>
          <th>{t}Groupes autorisés{/t}</th>
          <th>{t}Chemin de stockage des fichiers externes{/t}</th>
        </tr>
      </thead>
      <tbody>
        {section name=lst loop=$data}
        <tr>
          <td class="center">{$data[lst].project_year}</td>
          <td>
            {if $rights.param == 1}
            <a href="projectChange?project_id={$data[lst].project_id}">
              {$data[lst].project_name}
            </a>
            {else}
            {$data[lst].project_name}
            {/if}
          </td>
          <td class="center">{$data[lst].project_id}</td>
          <td><span class="textareaDisplay">{$data[lst].project_description}</span></td>
          <td>{$data[lst].project_code}</td>
          <td>{$data[lst].mama_code}</td>
          <td >{$data[lst].groupe}</td>
          <td>{$data[lst].external_path}</td>
        </tr>
        {/section}
      </tbody>
    </table>
  </div>
</div>
