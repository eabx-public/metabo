{if $rights.manage == 1}
  <a href="experimentationChange?project_id={$data.project_id}&experimentation_id=0">
    <img src="display/images/new.png" height="25">
    {t}Nouvelle expérimentation{/t}
  </a>
{/if}
<table id="experimentationList" class="table table-bordered table-hover datatable display">
  <thead>
    <tr>
      <th>{t}Nom{/t}</th>
      <th class="center">{t}Du{/t}</th>
      <th class="center">{t}Au{/t}</th>
      <th>{t}Type{/t}</th>
      <th>{t}Description{/t}</th>
  </tr>
  </thead>
  <tbody>
  {foreach $experimentations as $exp}
    <tr>
      <td>
        <a href="experimentationDisplay?project_id={$data.project_id}&experimentation_id={$exp.experimentation_id}">
          {$exp.experimentation_name}
        </a>
      </td>
      <td class="center">{$exp.experimentation_from}</td>
      <td class="center">{$exp.experimentation_to}</td>
      <td>{$exp.experimentation_type_name}</td>
      <td class="textareaDisplay">{$exp.experimentation_comment}</td>
    </tr>
  {/foreach}
  </tbody>
</table>
