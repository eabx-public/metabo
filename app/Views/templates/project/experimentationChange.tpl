<h2>{t}Création - Modification d'une expérimentation{/t}</h2>
<div class="row">
    <div class="col-md-6">
        <a href="projectDisplay?project_id={$data.project_id}"><img src="display/images/project.png"
                height="25">{t}Retour au projet{/t}&nbsp;{$project.project_name}</a>
        {if $data.experimentation_id > 0}
        &nbsp;
        <a href="experimentationDisplay?project_id={$data.project_id}&experimentation_id={$data.experimentation_id}">
            <img src="display/images/experiment.png" height="25">
            {t}Retour au détail{/t}
        </a>
        {/if}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <form class="form-horizontal protoform" id="experimentationForm" method="post" action="experimentationWrite">
            <input type="hidden" name="moduleBase" value="experimentation">
            <input type="hidden" name="experimentation_id" value="{$data.experimentation_id}">
            <input type="hidden" name="project_id" value="{$data.project_id}">
            <div class="form-group">
                <label for="experimentationName" class="control-label col-md-4">
                    <span class="red">*</span> {t}Nom :{/t}
                </label>
                <div class="col-md-8">
                    <input id="experimentationName" type="text" class="form-control" name="experimentation_name"
                        value="{$data.experimentation_name}" autofocus required>
                </div>
            </div>
            <div class="form-group">
                <label for="experimentation_type_id" class="control-label col-md-4">
                    <span class="red">*</span> {t}Type :{/t}
                </label>
                <div class="col-md-8">
                    <select class="form-control" id="experimentation_type_id" name="experimentation_type_id">
                        {foreach $types as $type}
                        <option value="{$type.experimentation_type_id}" {if
                            $type.experimentation_type_id==$data.experimentation_type_id}selected{/if}>
                            {$type.experimentation_type_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="experimentation_from" class="control-label col-md-4">
                    {t} début :{/t}
                </label>
                <div class="col-md-8">
                    <input id="experimentation_from" class="datepicker" name="experimentation_from"
                        value="{$data.experimentation_from}">
                </div>
            </div>
            <div class="form-group">
                <label for="experimentation_to" class="control-label col-md-4">
                    {t} fin :{/t}

                </label>
                <div class="col-md-8">
                    <input id="experimentation_to" class="datepicker" name="experimentation_to"
                        value="{$data.experimentation_to}">
                </div>
            </div>
            <div class="form-group">
                <label for="experimentation_comment" class="control-label col-md-4">
                    {t}Description :{/t}
                </label>
                <div class="col-md-8">
                    <textarea id="experimentation_comment" name="experimentation_comment"
                        class="form-control textarea-edit">{$data.experimentation_comment}</textarea>
                </div>
            </div>

            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
                {if $data.experimentation_id > 0 }
                <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
                {/if}
            </div>
            {$csrf}
        </form>
    </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>