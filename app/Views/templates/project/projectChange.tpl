<h2>{t}Création - Modification d'un projet{/t}</h2>
<div class="row">
    <div class="col-md-6">
        <a href="projectList"><img src="display/images/list.png" height="25">{t}Retour à la liste{/t}</a>

        <form class="form-horizontal protoform" id="projectForm" method="post" action="projectWrite">
            <input type="hidden" name="moduleBase" value="project">
            <input type="hidden" name="project_id" value="{$data.project_id}">
            <div class="form-group">
                <label for="projectName" class="control-label col-md-4">
                    <span class="red">*</span> {t}Nom :{/t}
                </label>
                <div class="col-md-8">
                    <input id="projectName" type="text" class="form-control" name="project_name"
                        value="{$data.project_name}" autofocus required>
                </div>
            </div>
            <div class="form-group">
                <label for="projectYear" class="control-label col-md-4">
                    <span class="red">*</span> {t}Année :{/t}
                </label>
                <div class="col-md-8">
                    <input id="projectYear" type="number" class="form-control" name="project_year"
                        value="{$data.project_year}" required>
                </div>
            </div>
            <div class="form-group">
                <label for="project_description" class="control-label col-md-4">
                    {t}Description :{/t}
                </label>
                <div class="col-md-8">
                    <textarea id="project_description" type="text" class="form-control textarea-edit"
                        name="project_description" rows="5">{$data.project_description}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="project_code" class="control-label col-md-4">
                    {t}Code interne :{/t}
                </label>
                <div class="col-md-8">
                    <input id="project_code" type="text" class="form-control" name="project_code"
                        value="{$data.project_code}">
                </div>
            </div>
            <div class="form-group">
                <label for="mama_code" class="control-label col-md-4">
                    {t}Code MAMA :{/t}
                </label>
                <div class="col-md-8">
                    <input id="mama_code" type="text" class="form-control" name="mama_code" value="{$data.mama_code}"
                        placeholder="2208011234">
                </div>
            </div>
            <div class="form-group">
                <label for="external_path" class="control-label col-md-4">
                    {t}Chemin de stockage des fichiers externes (le chemin est celui vu depuis le serveur, complétez ou modifiez le chemin par défaut) :{/t}</label>
                <div class="col-md-8">
                    <input id="external_path" type="text" class="form-control" name="external_path"
                        value="{$data.external_path}">
                </div>
            </div>
            <div class="form-group">
                <label for="groupes" class="control-label col-md-4">{t}Groupes :{/t}</label>
                <div class="col-md-7">
                    {section name=lst loop=$groupes}
                    <div class="col-md-2 col-sm-offset-3">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="groupes[]" value="{$groupes[lst].aclgroup_id}" {if
                                    $groupes[lst].checked==1}checked{/if}>
                                {$groupes[lst].groupe}
                            </label>
                        </div>
                    </div>
                    {/section}
                </div>
            </div>

            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
                {if $data.project_id > 0 }
                <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
                {/if}
            </div>
            {$csrf}
        </form>
    </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>