<h2>{t}Liste des paramètres : {/t}{$tabledescription}</h2>
<div class="row">
	<div class="col-md-6">
		{if $rights.param == 1}
		<a href="{$tablename}Change?{$tablename}_id=0">
			<img src="display/images/new.png" height="25">
			{t}Nouveau...{/t}
		</a>
		{/if}
		<table id="paramList" class="table table-bordered table-hover datatable display" data-order='[["2","asc"]]'>
			<thead>
				<tr>
					<th>{t}Id{/t}</th>
					<th>{t}Libellé{/t}</th>
					<th>{t}Type d'acquisition concerné{/t}</th>
					<th>{t}Ordre de tri{/t}</th>
				</tr>
			</thead>
			<tbody>
				{foreach $data as $row}
				<tr>
					<td class="center">{$row.method_id}</td>
					<td>
						{if $rights.param == 1}
						<a href="methodChange?method_id={$row.method_id}">
							{$row.method_name}
						</a>
						{else}
						{$row.method_name}
						{/if}
					</td>
					<td>{$row.acquisition_type_name}</td>
					<td class="center">{$row.sortorder}</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
</div>
