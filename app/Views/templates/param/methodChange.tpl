<h2>{t}Création - Modification d'une méthode d'acquisition {/t}</h2>
<a href="methodList"><img src="display/images/list.png" height="25">
    {t}Retour à la liste{/t}
</a>
<div class="row">
    <div class="col-md-6">
        <form class="form-horizontal protoform" id="paramForm" method="post" action="methodWrite">
            <input type="hidden" name="moduleBase" value="method">
            <input type="hidden" name="method_id" value="{$data.method_id}">
            <div class="form-group">
                <label for="paramName" class="control-label col-md-4">
                    <span class="red">*</span> {t}Libellé :{/t}
                </label>
                <div class="col-md-8">
                    <input id="paramName" type="text" class="form-control" name="method_name"
                        value="{$data.method_name}" autofocus required>
                </div>
            </div>
            <div class="form-group">
                <label for="acquisition_type_id" class="control-label col-md-4">
                    <span class="red">*</span> {t}Type d'acquisition :{/t}
                </label>
                <div class="col-md-8">
                    <select id="acquisition_type_id" name="acquisition_type_id" class="form-control">
                        {foreach $acquisitionTypes as $at}
                        <option value="{$at.acquisition_type_id}" {if
                            $at.acquisition_type_id==$data.acquisition_type_id}selected{/if}>
                            {$at.acquisition_type_name}
                        </option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="sortOrder" class="control-label col-md-4">
                    <span class="red">*</span> {t}Ordre de tri :{/t}
                </label>
                <div class="col-md-8">
                    <input id="sortOrder" type="number" class="form-control" name="sortorder" value="{$data.sortorder}">
                </div>
            </div>
            <div class="form-group center">
                <button type="submit" class="btn btn-primary button-valid">{t}Valider{/t}</button>
                {if $data.$fieldid > 0 }
                <button class="btn btn-danger button-delete">{t}Supprimer{/t}</button>
                {/if}
            </div>
            {$csrf}
        </form>
    </div>
</div>
<span class="red">*</span><span class="messagebas">{t}Donnée obligatoire{/t}</span>