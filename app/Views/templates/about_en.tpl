<h2>About <b>METABO</b></h2>
<p>
Version {$version} of {$versiondate}.
</p>

<p>
    METABO is an application for tracking metabolomics experiments, from the description of projects and experiments to the analyses carried out. It allows the files produced at each stage to be associated with the different 'materials' handled.
</p>

    <h3>Copyright © 2020-2024 - Eric Quinton for INRAE - EABX - CESTAS (France)</h3>
<p>

<br>
    This program is free software: you can redistribute it and/or modify<br>
    it under the terms of the GNU Affero General Public License as<br>
    published by the Free Software Foundation, either version 3 of the<br>
    License, or (at your option) any later version.<br>
<br>
    This program is distributed in the hope that it will be useful,<br>
    but WITHOUT ANY WARRANTY; without even the implied warranty of<br>
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the<br>
    GNU Affero General Public License for more details.<br>
<br>
    You should have received a copy of the GNU Affero General Public License<br>
    along with this program.  If not, see <a href=http://www.gnu.org/licenses>http://www.gnu.org/licenses</a>
<br>
<h3>{t}Credits photos :{/t}</h3>
<ul>
    <li>Picture of default page: Image by <a href="https://pixabay.com/users/MasterTux-470906/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1818492">MasterTux</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1818492">Pixabay</a></li>
</ul>