Version 25.0.0 du 20/02/2025
----------------------------
Corrections de bugs :
- La création des extractions ne fonctionnait pas correctement
- en création des extraits depuis l'échantillonnage, les lettres n'étaient pas correctement utilisées quand il y avait plusieurs échantillons
- le rattachement des aliquots à une acquisition, depuis le détail d'un projet, ne fonctionnait pas
- les entêtes de tableaux étaient parfois dupliquées dans l'écran

Nouveautés :
- ajout d'une colonne "sampled_volume", pour distinguer le volume réel (colonne "volume") de l'échantillon concerné (après dilution ou concentration) du volume prélevé de l'échantillon parent
- depuis le détail d'une extraction, il est maintenant possible de créer les aliquots depuis la liste des extraits

Version 24.0.0 du 6/11/2024
----------------------------
Basculement vers le framework CodeIgniter

Version 23.0.0 du 11/12/2023
----------------------------
Correction de bugs :
- en création automatique des réplicats, la numérotation commençait à 0 au lieu de 1

Nouveautés :
- la création automatique des échantillonnages est modifiée : leur nom est maintenant composé du code de la condition et d'un suffixe optionnel
- les extraits créés depuis les échantillonnages sont maintenant numérotés avec des lettres définies manuellement par les utilisateurs
- il est maintenant possible d'importer la liste des conditions d'une expérimentation à partir d'un fichier CSV

Version 1.5.0 du 28 février 2023
-----------------------------
Correction de bugs :
- l'accès à la suppression d'un échantillonnage n'était pas possible. La procédure de suppression a été revue, pour supprimer les échantillons associés et bloquer l'opération si les échantillons ont été utilisés
- diverses corrections d'affichage

Nouveautés :
- il est maintenant possible de créer des extractions depuis la liste des échantillonnages
- des fichiers peuvent maintenant être associés aux projets, selon le même mécanisme qu'ailleurs dans l'application

Version 1.4.0 du 26 janvier 2023
-----------------------------
Corrections de bugs :
- l'accès au dernier échantillonnage ne fonctionnait pas
- l'importation de pièces jointes a été amélioré, pour signaler si le dossier de stockage n'est pas accessible

Nouveautés :
- en création d'un échantillonnage, le code de niveau 2 n'est plus renseigné par défaut
- les poids et les volumes des échantillons sont maintenant gérés totalement indépendamment
- il est maintenant possible de créer rapidement des extraits à partir du détail d'un projet, en sélectionnant les réplicats concernés
- il est maintenant possible de créer les échantillonnages et les réplicats correspondants depuis le détail d'un projet, en sélectionnant les conditions concernées

Version 1.3.0 du 12 août 2022
-----------------------------
Corrections de bugs:
- la suppression des comptes dans le module d'administration (gacllogin) ne fonctionnait pas
- diverses opérations de suppression ne fonctionnaient pas (projets, échantillons, etc.)

Nouveautés :
- il est maintenant possible d'indiquer un code MAMA à un projet
- depuis la liste des réplicats, il est possible de créer plusieurs extraits pour un même réplicat, numérotés avec une lettre
- il est maintenant possible d'indiquer le type de stockage d'un échantillon (lame, etc.) et de rajouter un commentaire
- dans les listes d'échantillons, la condition est maintenant systématiquement affichée
- dans certains cas, le retour à l'échantillonnage n'était pas possible
- l'ensemble des libellés de l'interface ont été traduits en anglais

Version 1.2.1 du 12/04/2022
---------------------------
Correction de bugs :
- la création des traitements échouait avec un message d'erreur
- la génération des aliquots ne fonctionnait pas et renvoyait vers la page d'accueil
- affichage des échantillons : les UUID sont maintenant affichés systématiquement sur une seule ligne

Évolutions :
- la liste de l'ensemble des parents est maintenant affichée pour chaque échantillon

Version 1.2.0 du 12/01/2022
---------------------------
Version majeure, entraînant des modifications profondes dans la base de données. Il n'est pas possible de migrer facilement de la version 1.1 à la version 1.2 : il faut recréer une base de données et migrer les informations manuellement.

Évolutions :
- ajout de la gestion des traitements
- renommage des types d'échantillons dans les écrans, pour correspondre à la vision "métier"
- les échantillons sont quantifiés en volume ou en masse, l'unité peut être changée
- les stations peuvent être rattachées à un projet
- l'analyse devient une acquisition
- les traitements (retraitement des fichiers issus des acquisitions) sont rajoutés
- la table des conditions d'expérimentation a été rajoutée
- les conditions d'expérimentation, définies par échantillon, sont rattachées à la fois aux stations et aux expérimentations


Version 1.1.0 du 02/09/2021
---------------------------
Évolutions :
- il est maintenant possible de créer un nouveau projet depuis le détail d'un projet, si les droits sont suffisants
- l'année est associée au projet, ainsi qu'un code interne
- les libellés des échantillons et des prélèvements ont été modifiés pour mieux coller au processus de traitement
- depuis le détail d'un échantillonnage, il est possible d'accéder à l'importation de masse, avec pré-positionnement des informations
- les échantillons se voient dotés d'une nature
- depuis le détail d'un aliquot, il est possible de le rattacher directement à une analyse
- de nouveaux onglets sont affichés dans le détail du projet
- il est maintenant possible d'associer des fichiers stockés dans une arborescence bureautique à un échantillon, analyse, etc. L'arborescence doit être visible depuis le serveur
- il est maintenant possible de générer des liens de téléchargement sans la somme de contrôle, pour utilisation rapide dans W4M

Corrections de bugs :
- dans l'onglet "échantillons rattachés", la création d'un nouvel échantillon n'aboutissait pas
