-- Diff code generated with pgModeler (PostgreSQL Database Modeler)
-- pgModeler version: 1.2.0-beta
-- Diff date: 2025-02-18 15:25:00
-- Source model: metabo
-- Database: metabo
-- PostgreSQL version: 16.0

-- [ Diff summary ]
-- Dropped objects: 50
-- Created objects: 47
-- Changed objects: 11

SET check_function_bodies = false;
-- ddl-end --

SET search_path=public,pg_catalog,gacl,metabo;
-- ddl-end --


-- [ Created objects ] --
-- object: sampled_volume | type: COLUMN --
ALTER TABLE metabo.sample ADD COLUMN sampled_volume float;
-- ddl-end --

COMMENT ON COLUMN metabo.sample.sampled_volume IS E'Volume initial, before concentration or dilution';
-- ddl-end --


-- ddl-end --
ALTER TABLE metabo.project ALTER COLUMN project_year SET DEFAULT extract(year from now());
-- ddl-end --
ALTER TABLE metabo.sampling ALTER COLUMN default_volume TYPE float;
-- ddl-end --
ALTER TABLE metabo.sampling ALTER COLUMN default_weight TYPE float;
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.default_storage_type IS E'Default storage type for the creation of aliquots';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.default_storage_location IS E'Default storage location of aliquots';
-- ddl-end --
ALTER TABLE metabo.station ALTER COLUMN geom_point TYPE geometry(POINT, 4326);
-- ddl-end --
ALTER TABLE metabo.sample ALTER COLUMN uuid SET DEFAULT gen_random_uuid();
-- ddl-end --
COMMENT ON COLUMN metabo.sample.volume IS E'Real volume, in µL, after concentration or dilution';
-- ddl-end --
ALTER TABLE metabo.sample ALTER COLUMN volume TYPE float;
-- ddl-end --
ALTER TABLE metabo.sample ALTER COLUMN weight TYPE float;
-- ddl-end --
ALTER TABLE metabo.extraction ALTER COLUMN project_id SET NOT NULL;
-- ddl-end --

insert into metabo.dbversion (dbversion_number, dbversion_date) values ('25.0','2025-02-20');