-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler version: 1.2.0-alpha1
-- PostgreSQL version: 17.0
-- Project Site: pgmodeler.io
-- Model Author: ---
-- -- object: metabo | type: ROLE --
-- -- DROP ROLE IF EXISTS metabo;
-- CREATE ROLE metabo WITH 
-- 	LOGIN
-- 	 PASSWORD 'metaboPassword';
-- -- ddl-end --
-- 


SET check_function_bodies = false;
-- ddl-end --

-- object: metabo | type: SCHEMA --
-- DROP SCHEMA IF EXISTS metabo CASCADE;
CREATE SCHEMA metabo;
-- ddl-end --
ALTER SCHEMA metabo OWNER TO metabo;
-- ddl-end --

-- object: gacl | type: SCHEMA --
-- DROP SCHEMA IF EXISTS gacl CASCADE;
CREATE SCHEMA gacl;
-- ddl-end --
ALTER SCHEMA gacl OWNER TO metabo;
-- ddl-end --
COMMENT ON SCHEMA gacl IS E'Rights management';
-- ddl-end --

SET search_path TO pg_catalog,public,metabo,gacl;
-- ddl-end --

-- object: metabo.dbversion_dbversion_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS metabo.dbversion_dbversion_id_seq CASCADE;
CREATE SEQUENCE metabo.dbversion_dbversion_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE metabo.dbversion_dbversion_id_seq OWNER TO metabo;
-- ddl-end --

-- object: metabo.dbversion | type: TABLE --
-- DROP TABLE IF EXISTS metabo.dbversion CASCADE;
CREATE TABLE metabo.dbversion (
	dbversion_id integer NOT NULL DEFAULT nextval('metabo.dbversion_dbversion_id_seq'::regclass),
	dbversion_number character varying NOT NULL,
	dbversion_date timestamp NOT NULL,
	CONSTRAINT dbversion_pk PRIMARY KEY (dbversion_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.dbversion IS E'Table des versions de la base de donnees';
-- ddl-end --
COMMENT ON COLUMN metabo.dbversion.dbversion_number IS E'Numero de la version';
-- ddl-end --
COMMENT ON COLUMN metabo.dbversion.dbversion_date IS E'Date de la version';
-- ddl-end --
ALTER TABLE metabo.dbversion OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.dbversion (dbversion_date, dbversion_number) VALUES (E'2023-01-26', E'1.4');
-- ddl-end --

-- object: metabo.dbparam | type: TABLE --
-- DROP TABLE IF EXISTS metabo.dbparam CASCADE;
CREATE TABLE metabo.dbparam (
	dbparam_id serial NOT NULL,
	dbparam_name character varying NOT NULL,
	dbparam_value character varying,
	CONSTRAINT dbparam_pk PRIMARY KEY (dbparam_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.dbparam IS E'Table des parametres associes de maniere intrinseque a l''instance';
-- ddl-end --
COMMENT ON COLUMN metabo.dbparam.dbparam_name IS E'Nom du parametre';
-- ddl-end --
COMMENT ON COLUMN metabo.dbparam.dbparam_value IS E'Valeur du paramètre';
-- ddl-end --
ALTER TABLE metabo.dbparam OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'1', E'APPLI_title', E'Metabo - gestion des projets de métabolomique');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'2', E'real_storage_path', E'/mnt/metabo/projects');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'3', E'download_path', E'/mnt/metabo/download');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'4', E'last_folder_used', E'default');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'5', E'authorized_keys', E'/home/metabo/.ssh/authorized_keys');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'6', E'otp_issuer', E'metabo');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'7', E'batch_folder', E'/tmp');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'8', E'collec_token', DEFAULT);
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'9', E'collec_sample_address', E'https://instance.collec-science.net/index.php?module=apiv1sampleWrite');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'10', E'collec_login', E'metabo');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'11', E'collec_certificate', E'param/collec.pem');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'12', E'external_path_default', E'/mnt/metabo/external_files');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'13', E'mapDefaultLat', E'45');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'14', E'mapDefaultLong', E'0');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'15', E'mapDefaultZoom', E'12');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'16', E'mapMaxZoom', E'15');
-- ddl-end --
INSERT INTO metabo.dbparam (dbparam_id, dbparam_name, dbparam_value) VALUES (E'17', E'mapMinZoom', E'5');
-- ddl-end --

-- object: gacl.aclgroup_aclgroup_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS gacl.aclgroup_aclgroup_id_seq CASCADE;
CREATE SEQUENCE gacl.aclgroup_aclgroup_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START WITH 7
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE gacl.aclgroup_aclgroup_id_seq OWNER TO metabo;
-- ddl-end --

-- object: gacl.aclacl | type: TABLE --
-- DROP TABLE IF EXISTS gacl.aclacl CASCADE;
CREATE TABLE gacl.aclacl (
	aclaco_id integer NOT NULL,
	aclgroup_id integer NOT NULL,
	CONSTRAINT aclacl_pk PRIMARY KEY (aclaco_id,aclgroup_id)
);
-- ddl-end --
COMMENT ON TABLE gacl.aclacl IS E'Table des droits attribués';
-- ddl-end --
ALTER TABLE gacl.aclacl OWNER TO metabo;
-- ddl-end --

INSERT INTO gacl.aclacl (aclaco_id, aclgroup_id) VALUES (E'1', E'1');
-- ddl-end --
INSERT INTO gacl.aclacl (aclaco_id, aclgroup_id) VALUES (E'2', E'2');
-- ddl-end --
INSERT INTO gacl.aclacl (aclaco_id, aclgroup_id) VALUES (E'3', E'3');
-- ddl-end --
INSERT INTO gacl.aclacl (aclaco_id, aclgroup_id) VALUES (E'4', E'4');
-- ddl-end --
INSERT INTO gacl.aclacl (aclaco_id, aclgroup_id) VALUES (E'5', E'5');
-- ddl-end --

-- object: gacl.aclaco_aclaco_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS gacl.aclaco_aclaco_id_seq CASCADE;
CREATE SEQUENCE gacl.aclaco_aclaco_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START WITH 7
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE gacl.aclaco_aclaco_id_seq OWNER TO metabo;
-- ddl-end --

-- object: gacl.aclaco | type: TABLE --
-- DROP TABLE IF EXISTS gacl.aclaco CASCADE;
CREATE TABLE gacl.aclaco (
	aclaco_id integer NOT NULL DEFAULT nextval('gacl.aclaco_aclaco_id_seq'::regclass),
	aclappli_id integer NOT NULL,
	aco character varying NOT NULL,
	CONSTRAINT aclaco_pk PRIMARY KEY (aclaco_id)
);
-- ddl-end --
COMMENT ON TABLE gacl.aclaco IS E'Table des droits gérés';
-- ddl-end --
ALTER TABLE gacl.aclaco OWNER TO metabo;
-- ddl-end --

INSERT INTO gacl.aclaco (aclaco_id, aclappli_id, aco) VALUES (E'1', E'1', E'admin');
-- ddl-end --
INSERT INTO gacl.aclaco (aclaco_id, aclappli_id, aco) VALUES (E'2', E'1', E'param');
-- ddl-end --
INSERT INTO gacl.aclaco (aclaco_id, aclappli_id, aco) VALUES (E'3', E'1', E'gestion');
-- ddl-end --
INSERT INTO gacl.aclaco (aclaco_id, aclappli_id, aco) VALUES (E'4', E'1', E'consult');
-- ddl-end --
INSERT INTO gacl.aclaco (aclaco_id, aclappli_id, aco) VALUES (E'5', E'1', E'project');
-- ddl-end --

-- object: gacl.aclappli_aclappli_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS gacl.aclappli_aclappli_id_seq CASCADE;
CREATE SEQUENCE gacl.aclappli_aclappli_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START WITH 2
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE gacl.aclappli_aclappli_id_seq OWNER TO metabo;
-- ddl-end --

-- object: gacl.aclappli | type: TABLE --
-- DROP TABLE IF EXISTS gacl.aclappli CASCADE;
CREATE TABLE gacl.aclappli (
	aclappli_id integer NOT NULL DEFAULT nextval('gacl.aclappli_aclappli_id_seq'::regclass),
	appli character varying NOT NULL,
	applidetail character varying,
	CONSTRAINT aclappli_pk PRIMARY KEY (aclappli_id)
);
-- ddl-end --
COMMENT ON TABLE gacl.aclappli IS E'Table des applications gérées';
-- ddl-end --
COMMENT ON COLUMN gacl.aclappli.appli IS E'Nom de l''application pour la gestion des droits';
-- ddl-end --
COMMENT ON COLUMN gacl.aclappli.applidetail IS E'Description de l''application';
-- ddl-end --
ALTER TABLE gacl.aclappli OWNER TO metabo;
-- ddl-end --

INSERT INTO gacl.aclappli (aclappli_id, appli, applidetail) VALUES (E'1', E'metabo', DEFAULT);
-- ddl-end --

-- object: gacl.aclgroup | type: TABLE --
-- DROP TABLE IF EXISTS gacl.aclgroup CASCADE;
CREATE TABLE gacl.aclgroup (
	aclgroup_id integer NOT NULL DEFAULT nextval('gacl.aclgroup_aclgroup_id_seq'::regclass),
	groupe character varying NOT NULL,
	aclgroup_id_parent integer,
	CONSTRAINT aclgroup_pk PRIMARY KEY (aclgroup_id)
);
-- ddl-end --
COMMENT ON TABLE gacl.aclgroup IS E'Groupes des logins';
-- ddl-end --
ALTER TABLE gacl.aclgroup OWNER TO metabo;
-- ddl-end --

INSERT INTO gacl.aclgroup (aclgroup_id, groupe, aclgroup_id_parent) VALUES (E'1', E'admin', DEFAULT);
-- ddl-end --
INSERT INTO gacl.aclgroup (aclgroup_id, groupe, aclgroup_id_parent) VALUES (E'2', E'consult', DEFAULT);
-- ddl-end --
INSERT INTO gacl.aclgroup (aclgroup_id, groupe, aclgroup_id_parent) VALUES (E'3', E'gestion', E'2');
-- ddl-end --
INSERT INTO gacl.aclgroup (aclgroup_id, groupe, aclgroup_id_parent) VALUES (E'4', E'project', E'3');
-- ddl-end --
INSERT INTO gacl.aclgroup (aclgroup_id, groupe, aclgroup_id_parent) VALUES (E'5', E'param', E'4');
-- ddl-end --

-- object: gacl.acllogin_acllogin_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS gacl.acllogin_acllogin_id_seq CASCADE;
CREATE SEQUENCE gacl.acllogin_acllogin_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START WITH 2
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE gacl.acllogin_acllogin_id_seq OWNER TO metabo;
-- ddl-end --

-- object: gacl.acllogin | type: TABLE --
-- DROP TABLE IF EXISTS gacl.acllogin CASCADE;
CREATE TABLE gacl.acllogin (
	acllogin_id integer NOT NULL DEFAULT nextval('gacl.acllogin_acllogin_id_seq'::regclass),
	login character varying NOT NULL,
	logindetail character varying NOT NULL,
	totp_key varchar,
	CONSTRAINT acllogin_pk PRIMARY KEY (acllogin_id)
);
-- ddl-end --
COMMENT ON TABLE gacl.acllogin IS E'Table des logins des utilisateurs autorisés';
-- ddl-end --
COMMENT ON COLUMN gacl.acllogin.logindetail IS E'Nom affiché';
-- ddl-end --
COMMENT ON COLUMN gacl.acllogin.totp_key IS E'TOTP secret key for the user';
-- ddl-end --
ALTER TABLE gacl.acllogin OWNER TO metabo;
-- ddl-end --

INSERT INTO gacl.acllogin (acllogin_id, login, logindetail) VALUES (E'1', E'admin', E'Administrator account - must be deleted before production');
-- ddl-end --

-- object: gacl.acllogingroup | type: TABLE --
-- DROP TABLE IF EXISTS gacl.acllogingroup CASCADE;
CREATE TABLE gacl.acllogingroup (
	acllogin_id integer NOT NULL,
	aclgroup_id integer NOT NULL,
	CONSTRAINT acllogingroup_pk PRIMARY KEY (acllogin_id,aclgroup_id)
);
-- ddl-end --
COMMENT ON TABLE gacl.acllogingroup IS E'Table des relations entre les logins et les groupes';
-- ddl-end --
ALTER TABLE gacl.acllogingroup OWNER TO metabo;
-- ddl-end --

INSERT INTO gacl.acllogingroup (acllogin_id, aclgroup_id) VALUES (E'1', E'1');
-- ddl-end --
INSERT INTO gacl.acllogingroup (acllogin_id, aclgroup_id) VALUES (E'1', E'5');
-- ddl-end --

-- object: gacl.log_log_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS gacl.log_log_id_seq CASCADE;
CREATE SEQUENCE gacl.log_log_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE gacl.log_log_id_seq OWNER TO metabo;
-- ddl-end --

-- object: gacl.log | type: TABLE --
-- DROP TABLE IF EXISTS gacl.log CASCADE;
CREATE TABLE gacl.log (
	log_id integer NOT NULL DEFAULT nextval('gacl.log_log_id_seq'::regclass),
	login character varying NOT NULL,
	nom_module character varying,
	log_date timestamp NOT NULL,
	commentaire character varying,
	ipaddress character varying,
	CONSTRAINT log_pk PRIMARY KEY (log_id)
);
-- ddl-end --
COMMENT ON TABLE gacl.log IS E'Liste des connexions ou des actions enregistrées';
-- ddl-end --
COMMENT ON COLUMN gacl.log.log_date IS E'Heure de connexion';
-- ddl-end --
COMMENT ON COLUMN gacl.log.commentaire IS E'Donnees complementaires enregistrees';
-- ddl-end --
COMMENT ON COLUMN gacl.log.ipaddress IS E'Adresse IP du client';
-- ddl-end --
ALTER TABLE gacl.log OWNER TO metabo;
-- ddl-end --

-- object: gacl.seq_logingestion_id | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS gacl.seq_logingestion_id CASCADE;
CREATE SEQUENCE gacl.seq_logingestion_id
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 999999
	START WITH 2
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE gacl.seq_logingestion_id OWNER TO metabo;
-- ddl-end --

-- object: gacl.logingestion | type: TABLE --
-- DROP TABLE IF EXISTS gacl.logingestion CASCADE;
CREATE TABLE gacl.logingestion (
	id integer NOT NULL DEFAULT nextval('gacl.seq_logingestion_id'::regclass),
	login character varying NOT NULL,
	password character varying,
	nom character varying,
	prenom character varying,
	mail character varying,
	datemodif timestamp,
	actif smallint DEFAULT 1,
	is_clientws boolean DEFAULT false,
	tokenws character varying,
	is_expired boolean DEFAULT false,
	CONSTRAINT pk_logingestion PRIMARY KEY (id)
);
-- ddl-end --
ALTER TABLE gacl.logingestion OWNER TO metabo;
-- ddl-end --

INSERT INTO gacl.logingestion (id, login, password, nom, prenom, mail, datemodif, actif, is_clientws, tokenws, is_expired) VALUES (E'1', E'admin', E'$2y$13$evOzF08OtKIzqZlIlTQ.i.8JeVT9940H28VdR7ZWOkB.BlPn.4D6u', E'Admin account - must be deleted in production', DEFAULT, DEFAULT, DEFAULT, E'1', E'false', DEFAULT, E'false');
-- ddl-end --

-- object: gacl.passwordlost_passwordlost_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS gacl.passwordlost_passwordlost_id_seq CASCADE;
CREATE SEQUENCE gacl.passwordlost_passwordlost_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE gacl.passwordlost_passwordlost_id_seq OWNER TO metabo;
-- ddl-end --

-- object: gacl.passwordlost | type: TABLE --
-- DROP TABLE IF EXISTS gacl.passwordlost CASCADE;
CREATE TABLE gacl.passwordlost (
	passwordlost_id integer NOT NULL DEFAULT nextval('gacl.passwordlost_passwordlost_id_seq'::regclass),
	id integer NOT NULL,
	token character varying NOT NULL,
	expiration timestamp NOT NULL,
	usedate timestamp,
	CONSTRAINT passwordlost_pk PRIMARY KEY (passwordlost_id)
);
-- ddl-end --
COMMENT ON TABLE gacl.passwordlost IS E'Table de suivi des pertes de mots de passe';
-- ddl-end --
COMMENT ON COLUMN gacl.passwordlost.token IS E'Jeton utilise pour le renouvellement';
-- ddl-end --
COMMENT ON COLUMN gacl.passwordlost.expiration IS E'Date d''expiration du jeton';
-- ddl-end --
ALTER TABLE gacl.passwordlost OWNER TO metabo;
-- ddl-end --

-- object: log_date_idx | type: INDEX --
-- DROP INDEX IF EXISTS gacl.log_date_idx CASCADE;
CREATE INDEX log_date_idx ON gacl.log
USING btree
(
	log_date
)
WITH (FILLFACTOR = 90);
-- ddl-end --

-- object: log_login_idx | type: INDEX --
-- DROP INDEX IF EXISTS gacl.log_login_idx CASCADE;
CREATE INDEX log_login_idx ON gacl.log
USING btree
(
	login
)
WITH (FILLFACTOR = 90);
-- ddl-end --

-- object: log_commentaire_idx | type: INDEX --
-- DROP INDEX IF EXISTS gacl.log_commentaire_idx CASCADE;
CREATE INDEX log_commentaire_idx ON gacl.log
USING btree
(
	commentaire
)
WITH (FILLFACTOR = 90);
-- ddl-end --

-- object: log_ip_idx | type: INDEX --
-- DROP INDEX IF EXISTS gacl.log_ip_idx CASCADE;
CREATE INDEX log_ip_idx ON gacl.log
USING btree
(
	ipaddress
)
WITH (FILLFACTOR = 90);
-- ddl-end --

-- object: logingestion_login_idx | type: INDEX --
-- DROP INDEX IF EXISTS gacl.logingestion_login_idx CASCADE;
CREATE UNIQUE INDEX logingestion_login_idx ON gacl.logingestion
USING btree
(
	login
)
WITH (FILLFACTOR = 90);
-- ddl-end --

-- object: metabo.file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.file CASCADE;
CREATE TABLE metabo.file (
	file_id serial NOT NULL,
	filename varchar NOT NULL,
	mimetype_id integer,
	storage_path varchar,
	comment varchar,
	filedateimport timestamp NOT NULL DEFAULT now(),
	filesize integer,
	filedate timestamp,
	storage_type varchar NOT NULL DEFAULT 'I',
	filetype_id integer,
	project_id integer NOT NULL,
	CONSTRAINT file_pk PRIMARY KEY (file_id)
);
-- ddl-end --
COMMENT ON COLUMN metabo.file.filename IS E'Public file name';
-- ddl-end --
COMMENT ON COLUMN metabo.file.storage_path IS E'Relative storage path, for large files';
-- ddl-end --
COMMENT ON COLUMN metabo.file.comment IS E'Description of the file';
-- ddl-end --
COMMENT ON COLUMN metabo.file.filedateimport IS E'Date of import of the file into the database';
-- ddl-end --
COMMENT ON COLUMN metabo.file.filesize IS E'Size of the file, in bytes';
-- ddl-end --
COMMENT ON COLUMN metabo.file.filedate IS E'Date of the creation of the file';
-- ddl-end --
COMMENT ON COLUMN metabo.file.storage_type IS E'I: internal - E: external\nIf internal, the storage of the file is realized by the application. If external, the link points to a real storage';
-- ddl-end --
ALTER TABLE metabo.file OWNER TO metabo;
-- ddl-end --

-- object: metabo.mimetype_mimetype_id_seq | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS metabo.mimetype_mimetype_id_seq CASCADE;
CREATE SEQUENCE metabo.mimetype_mimetype_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START WITH 20
	CACHE 1
	NO CYCLE
	OWNED BY NONE;

-- ddl-end --
ALTER SEQUENCE metabo.mimetype_mimetype_id_seq OWNER TO metabo;
-- ddl-end --

-- object: metabo.mimetype | type: TABLE --
-- DROP TABLE IF EXISTS metabo.mimetype CASCADE;
CREATE TABLE metabo.mimetype (
	mimetype_id integer NOT NULL DEFAULT nextval('metabo.mimetype_mimetype_id_seq'::regclass),
	extension character varying NOT NULL,
	content_type character varying NOT NULL,
	CONSTRAINT mime_type_pk PRIMARY KEY (mimetype_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.mimetype IS E'Mime types of imported files';
-- ddl-end --
COMMENT ON COLUMN metabo.mimetype.extension IS E'File extension';
-- ddl-end --
COMMENT ON COLUMN metabo.mimetype.content_type IS E'Official mime type';
-- ddl-end --
ALTER TABLE metabo.mimetype OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'pdf', E'application/pdf');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'zip', E'application/zip');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'mp3', E'audio/mpeg');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'jpg', E'image/jpeg');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'jpeg', E'image/jpeg');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'png', E'image/png');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'tiff', E'image/tiff');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'csv', E'text/csv');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'odt', E'application/vnd.oasis.opendocument.text');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'ods', E'application/vnd.oasis.opendocument.spreadsheet');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'xls', E'application/vnd.ms-excel');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'xlsx', E'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'doc', E'application/msword');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'docx', E'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'ab1', E'application/octet-stream');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'seq', E'application/octet-stream');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'fa', E'application/octet-stream');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'fsa', E'application/octet-stream');
-- ddl-end --
INSERT INTO metabo.mimetype (extension, content_type) VALUES (E'xml', E'application/xml');
-- ddl-end --

-- object: file_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.file DROP CONSTRAINT IF EXISTS file_pk1 CASCADE;
ALTER TABLE metabo.file ADD CONSTRAINT file_pk1 FOREIGN KEY (mimetype_id)
REFERENCES metabo.mimetype (mimetype_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.filelink | type: TABLE --
-- DROP TABLE IF EXISTS metabo.filelink CASCADE;
CREATE TABLE metabo.filelink (
	filelink_id serial NOT NULL,
	file_id integer NOT NULL,
	linkname varchar NOT NULL,
	expiration_date timestamp NOT NULL,
	last_use timestamp

);
-- ddl-end --
COMMENT ON TABLE metabo.filelink IS E'Temporary links used to download files directly';
-- ddl-end --
COMMENT ON COLUMN metabo.filelink.last_use IS E'Last datetime of usage of the file link';
-- ddl-end --
ALTER TABLE metabo.filelink OWNER TO metabo;
-- ddl-end --

-- object: filelink_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.filelink DROP CONSTRAINT IF EXISTS filelink_pk CASCADE;
ALTER TABLE metabo.filelink ADD CONSTRAINT filelink_pk FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.filetype | type: TABLE --
-- DROP TABLE IF EXISTS metabo.filetype CASCADE;
CREATE TABLE metabo.filetype (
	filetype_id serial NOT NULL,
	filetype_name varchar NOT NULL,
	is_sampling boolean NOT NULL DEFAULT false,
	is_experimentation boolean NOT NULL DEFAULT false,
	is_sample boolean NOT NULL DEFAULT false,
	is_acquisition_sample boolean NOT NULL DEFAULT false,
	is_acquisition boolean NOT NULL DEFAULT false,
	is_extraction boolean NOT NULL DEFAULT false,
	sortorder smallint DEFAULT 1,
	is_treatment boolean NOT NULL DEFAULT false,
	is_condition boolean NOT NULL DEFAULT false,
	is_treatment_as boolean NOT NULL DEFAULT false,
	is_project boolean NOT NULL DEFAULT false,
	CONSTRAINT filetype_pk PRIMARY KEY (filetype_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.filetype IS E'Types of files';
-- ddl-end --
ALTER TABLE metabo.filetype OWNER TO metabo;
-- ddl-end --

-- object: file_pk2 | type: CONSTRAINT --
-- ALTER TABLE metabo.file DROP CONSTRAINT IF EXISTS file_pk2 CASCADE;
ALTER TABLE metabo.file ADD CONSTRAINT file_pk2 FOREIGN KEY (filetype_id)
REFERENCES metabo.filetype (filetype_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.project | type: TABLE --
-- DROP TABLE IF EXISTS metabo.project CASCADE;
CREATE TABLE metabo.project (
	project_id serial NOT NULL,
	project_name varchar NOT NULL,
	project_description varchar,
	project_code varchar,
	project_year smallint NOT NULL DEFAULT extract(year from now()),
	external_path varchar,
	mama_code varchar,
	CONSTRAINT project_pk PRIMARY KEY (project_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.project IS E'List of projects';
-- ddl-end --
COMMENT ON COLUMN metabo.project.project_name IS E'Short name of the project';
-- ddl-end --
COMMENT ON COLUMN metabo.project.project_description IS E'Long name of the project, or description';
-- ddl-end --
COMMENT ON COLUMN metabo.project.project_code IS E'Internal code of the project';
-- ddl-end --
COMMENT ON COLUMN metabo.project.project_year IS E'Main year of the project';
-- ddl-end --
COMMENT ON COLUMN metabo.project.external_path IS E'Path of storage of external files';
-- ddl-end --
COMMENT ON COLUMN metabo.project.mama_code IS E'MAMA code';
-- ddl-end --
ALTER TABLE metabo.project OWNER TO metabo;
-- ddl-end --

-- object: metabo.project_group | type: TABLE --
-- DROP TABLE IF EXISTS metabo.project_group CASCADE;
CREATE TABLE metabo.project_group (
	project_id integer NOT NULL,
	aclgroup_id integer NOT NULL,
	CONSTRAINT project_fk PRIMARY KEY (project_id,aclgroup_id) DEFERRABLE INITIALLY IMMEDIATE
);
-- ddl-end --
COMMENT ON TABLE metabo.project_group IS E'List of groups granted to a project';
-- ddl-end --
ALTER TABLE metabo.project_group OWNER TO metabo;
-- ddl-end --

-- object: project_group_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.project_group DROP CONSTRAINT IF EXISTS project_group_pk CASCADE;
ALTER TABLE metabo.project_group ADD CONSTRAINT project_group_pk FOREIGN KEY (project_id)
REFERENCES metabo.project (project_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: project_group_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.project_group DROP CONSTRAINT IF EXISTS project_group_pk1 CASCADE;
ALTER TABLE metabo.project_group ADD CONSTRAINT project_group_pk1 FOREIGN KEY (aclgroup_id)
REFERENCES gacl.aclgroup (aclgroup_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.experimentation | type: TABLE --
-- DROP TABLE IF EXISTS metabo.experimentation CASCADE;
CREATE TABLE metabo.experimentation (
	experimentation_id serial NOT NULL,
	project_id integer NOT NULL,
	experimentation_name varchar NOT NULL,
	experimentation_from timestamp,
	experimentation_to timestamp,
	experimentation_comment varchar,
	experimentation_type_id integer NOT NULL,
	CONSTRAINT experimentation_pk PRIMARY KEY (experimentation_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.experimentation IS E'List of experimentations';
-- ddl-end --
COMMENT ON COLUMN metabo.experimentation.experimentation_name IS E'Name or description of the experimentation';
-- ddl-end --
COMMENT ON COLUMN metabo.experimentation.experimentation_from IS E'Start date of the experimentation';
-- ddl-end --
COMMENT ON COLUMN metabo.experimentation.experimentation_to IS E'End date of the experimentation';
-- ddl-end --
COMMENT ON COLUMN metabo.experimentation.experimentation_comment IS E'Description of the experimentation';
-- ddl-end --
ALTER TABLE metabo.experimentation OWNER TO metabo;
-- ddl-end --

-- object: experimentation_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.experimentation DROP CONSTRAINT IF EXISTS experimentation_pk1 CASCADE;
ALTER TABLE metabo.experimentation ADD CONSTRAINT experimentation_pk1 FOREIGN KEY (project_id)
REFERENCES metabo.project (project_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.sampling | type: TABLE --
-- DROP TABLE IF EXISTS metabo.sampling CASCADE;
CREATE TABLE metabo.sampling (
	sampling_id serial NOT NULL,
	sampling_code varchar NOT NULL,
	sampling_description varchar,
	sampling_date timestamp,
	default_volume float,
	default_weight float,
	default_unit_volume varchar,
	separator varchar,
	level1_code varchar,
	level1_number smallint NOT NULL DEFAULT 1,
	level2_code varchar,
	level2_number smallint,
	default_storage_type integer,
	default_storage_location integer,
	sampling_method_id integer,
	condition_id integer NOT NULL,
	default_unit_weight varchar DEFAULT 'mg',
	CONSTRAINT condition_pk PRIMARY KEY (sampling_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.sampling IS E'List of samplings';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.sampling_code IS E'Code of the sampling';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.sampling_description IS E'Description of the sampling';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.sampling_date IS E'Date of sampling';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.default_volume IS E'Default volume of the repliquats';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.default_weight IS E'Default weight of the repliquat';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.default_unit_volume IS E'Unit of the volume/weight';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.separator IS E'Separator used to generate the code of replicates';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.level1_code IS E'Code of the level 1 to generate the identifier of the replicates. Allways used to generate the identifier, but can be empty';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.level1_number IS E'Number of replicats to create (level 1)';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.level2_code IS E'Code of the level 2 used to generate the identifier of the replicates. Only used if it''s not empty';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.level2_number IS E'Number of replicates to create for each level 1 occurrence. Must be not null if level2_code is not empty';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.default_storage_type IS E'Default storage type for the creation of aliquots';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling.default_storage_location IS E'Default storage location of aliquots';
-- ddl-end --
ALTER TABLE metabo.sampling OWNER TO metabo;
-- ddl-end --

-- -- object: postgis | type: EXTENSION --
-- -- DROP EXTENSION IF EXISTS postgis CASCADE;
-- CREATE EXTENSION postgis
-- WITH SCHEMA public;
-- -- ddl-end --
-- 
-- object: metabo.station | type: TABLE --
-- DROP TABLE IF EXISTS metabo.station CASCADE;
CREATE TABLE metabo.station (
	station_id serial NOT NULL,
	station_name varchar NOT NULL,
	wgs84_x double precision,
	wgs84_y double precision,
	geom_point geometry(POINT, 4326),
	project_id integer,
	CONSTRAINT station_pk PRIMARY KEY (station_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.station IS E'List of stations of sampling or origin of sampling';
-- ddl-end --
COMMENT ON COLUMN metabo.station.station_name IS E'Description of the station';
-- ddl-end --
COMMENT ON COLUMN metabo.station.wgs84_x IS E'Longitude of the station, in WGS84';
-- ddl-end --
COMMENT ON COLUMN metabo.station.wgs84_y IS E'Latitude of the station, in WGS84';
-- ddl-end --
ALTER TABLE metabo.station OWNER TO metabo;
-- ddl-end --

-- object: metabo.sampling_file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.sampling_file CASCADE;
CREATE TABLE metabo.sampling_file (
	sampling_id integer NOT NULL,
	file_id integer NOT NULL,
	CONSTRAINT sampling_fk PRIMARY KEY (sampling_id,file_id) DEFERRABLE INITIALLY IMMEDIATE
);
-- ddl-end --
ALTER TABLE metabo.sampling_file OWNER TO metabo;
-- ddl-end --

-- object: sampling_file_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.sampling_file DROP CONSTRAINT IF EXISTS sampling_file_pk CASCADE;
ALTER TABLE metabo.sampling_file ADD CONSTRAINT sampling_file_pk FOREIGN KEY (sampling_id)
REFERENCES metabo.sampling (sampling_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: sampling_file_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.sampling_file DROP CONSTRAINT IF EXISTS sampling_file_pk1 CASCADE;
ALTER TABLE metabo.sampling_file ADD CONSTRAINT sampling_file_pk1 FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.experimentation_file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.experimentation_file CASCADE;
CREATE TABLE metabo.experimentation_file (
	experimentation_id integer NOT NULL,
	file_id integer NOT NULL,
	CONSTRAINT experimentation_fk PRIMARY KEY (experimentation_id,file_id) DEFERRABLE INITIALLY IMMEDIATE
);
-- ddl-end --
ALTER TABLE metabo.experimentation_file OWNER TO metabo;
-- ddl-end --

-- object: experimentation_file_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.experimentation_file DROP CONSTRAINT IF EXISTS experimentation_file_pk CASCADE;
ALTER TABLE metabo.experimentation_file ADD CONSTRAINT experimentation_file_pk FOREIGN KEY (experimentation_id)
REFERENCES metabo.experimentation (experimentation_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: experimentation_file_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.experimentation_file DROP CONSTRAINT IF EXISTS experimentation_file_pk1 CASCADE;
ALTER TABLE metabo.experimentation_file ADD CONSTRAINT experimentation_file_pk1 FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- -- object: pgcrypto | type: EXTENSION --
-- -- DROP EXTENSION IF EXISTS pgcrypto CASCADE;
-- CREATE EXTENSION pgcrypto
-- WITH SCHEMA metabo;
-- -- ddl-end --
-- 
-- object: metabo.sample_type | type: TABLE --
-- DROP TABLE IF EXISTS metabo.sample_type CASCADE;
CREATE TABLE metabo.sample_type (
	sample_type_id serial NOT NULL,
	sample_type_name varchar NOT NULL,
	sortorder smallint,
	collec_type_name varchar,
	CONSTRAINT sampletype_pk PRIMARY KEY (sample_type_id)
);
-- ddl-end --
COMMENT ON COLUMN metabo.sample_type.sortorder IS E'Sort order';
-- ddl-end --
COMMENT ON COLUMN metabo.sample_type.collec_type_name IS E'Name of the type of sample in a collec-science instance';
-- ddl-end --
ALTER TABLE metabo.sample_type OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.sample_type (sample_type_name, sortorder) VALUES (E'replicate', E'1');
-- ddl-end --
INSERT INTO metabo.sample_type (sample_type_name, sortorder) VALUES (E'extract', E'2');
-- ddl-end --
INSERT INTO metabo.sample_type (sample_type_name, sortorder) VALUES (E'aliquot', E'3');
-- ddl-end --

-- object: metabo.sample | type: TABLE --
-- DROP TABLE IF EXISTS metabo.sample CASCADE;
CREATE TABLE metabo.sample (
	sample_id serial NOT NULL,
	sample_name varchar NOT NULL,
	sample_parent_id integer,
	uuid uuid NOT NULL DEFAULT gen_random_uuid(),
	volume float,
	sample_nature_id integer NOT NULL DEFAULT 1,
	weight float,
	unit_volume varchar,
	remarks varchar,
	sampling_id integer,
	sample_type_id integer NOT NULL,
	extraction_id integer,
	project_id integer NOT NULL,
	transport_method_id integer,
	storage_location_id integer,
	storage_type_id integer,
	unit_weight varchar DEFAULT 'mg',
	CONSTRAINT sample_pk PRIMARY KEY (sample_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.sample IS E'List of all samples used';
-- ddl-end --
COMMENT ON COLUMN metabo.sample.sample_name IS E'Code of the sample';
-- ddl-end --
COMMENT ON COLUMN metabo.sample.sample_parent_id IS E'Id of the parent of the sample';
-- ddl-end --
COMMENT ON COLUMN metabo.sample.uuid IS E'Unique identifier, for exchange with others pieces of software';
-- ddl-end --
COMMENT ON COLUMN metabo.sample.volume IS E'Volume, in µL';
-- ddl-end --
COMMENT ON COLUMN metabo.sample.weight IS E'Weight of the sample. The unit is defined by the unit field';
-- ddl-end --
COMMENT ON COLUMN metabo.sample.unit_volume IS E'Unit of the volume or the weight';
-- ddl-end --
ALTER TABLE metabo.sample OWNER TO metabo;
-- ddl-end --

-- object: sample_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.sample DROP CONSTRAINT IF EXISTS sample_pk1 CASCADE;
ALTER TABLE metabo.sample ADD CONSTRAINT sample_pk1 FOREIGN KEY (sampling_id)
REFERENCES metabo.sampling (sampling_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.sample_file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.sample_file CASCADE;
CREATE TABLE metabo.sample_file (
	sample_id integer NOT NULL,
	file_id integer NOT NULL,
	CONSTRAINT sample_fk PRIMARY KEY (sample_id,file_id) DEFERRABLE INITIALLY IMMEDIATE
);
-- ddl-end --
ALTER TABLE metabo.sample_file OWNER TO metabo;
-- ddl-end --

-- object: sample_file_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.sample_file DROP CONSTRAINT IF EXISTS sample_file_pk CASCADE;
ALTER TABLE metabo.sample_file ADD CONSTRAINT sample_file_pk FOREIGN KEY (sample_id)
REFERENCES metabo.sample (sample_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: sample_file_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.sample_file DROP CONSTRAINT IF EXISTS sample_file_pk1 CASCADE;
ALTER TABLE metabo.sample_file ADD CONSTRAINT sample_file_pk1 FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.acquisition | type: TABLE --
-- DROP TABLE IF EXISTS metabo.acquisition CASCADE;
CREATE TABLE metabo.acquisition (
	acquisition_id serial NOT NULL,
	acquisition_date timestamp NOT NULL DEFAULT now(),
	project_id integer NOT NULL,
	machine_id integer,
	acquisition_type_id integer NOT NULL,
	chromatography_method_id integer,
	acquisition_method_id integer,
	CONSTRAINT analysis_pk PRIMARY KEY (acquisition_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.acquisition IS E'List of acquisitions';
-- ddl-end --
COMMENT ON COLUMN metabo.acquisition.acquisition_date IS E'Date of the acquisition';
-- ddl-end --
ALTER TABLE metabo.acquisition OWNER TO metabo;
-- ddl-end --

-- object: metabo.acquisition_sample | type: TABLE --
-- DROP TABLE IF EXISTS metabo.acquisition_sample CASCADE;
CREATE TABLE metabo.acquisition_sample (
	acquisition_sample_id serial NOT NULL,
	is_calibration boolean NOT NULL DEFAULT false,
	acquisition_id integer NOT NULL,
	sample_id integer NOT NULL,
	CONSTRAINT lot_pk PRIMARY KEY (acquisition_sample_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.acquisition_sample IS E'Lot of samples into an acquisition';
-- ddl-end --
COMMENT ON COLUMN metabo.acquisition_sample.is_calibration IS E'Specify if the sample is used to calibrate';
-- ddl-end --
ALTER TABLE metabo.acquisition_sample OWNER TO metabo;
-- ddl-end --

-- object: acquisition_sample_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition_sample DROP CONSTRAINT IF EXISTS acquisition_sample_pk CASCADE;
ALTER TABLE metabo.acquisition_sample ADD CONSTRAINT acquisition_sample_pk FOREIGN KEY (acquisition_id)
REFERENCES metabo.acquisition (acquisition_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: acquisition_sample_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition_sample DROP CONSTRAINT IF EXISTS acquisition_sample_pk1 CASCADE;
ALTER TABLE metabo.acquisition_sample ADD CONSTRAINT acquisition_sample_pk1 FOREIGN KEY (sample_id)
REFERENCES metabo.sample (sample_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.acquisition_sample_file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.acquisition_sample_file CASCADE;
CREATE TABLE metabo.acquisition_sample_file (
	acquisition_sample_id integer NOT NULL,
	file_id integer NOT NULL,
	CONSTRAINT acquisition_sample_fk PRIMARY KEY (acquisition_sample_id,file_id) DEFERRABLE INITIALLY IMMEDIATE
);
-- ddl-end --
COMMENT ON TABLE metabo.acquisition_sample_file IS E'Results of acquisition';
-- ddl-end --
ALTER TABLE metabo.acquisition_sample_file OWNER TO metabo;
-- ddl-end --

-- object: acquisition_sample_file_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition_sample_file DROP CONSTRAINT IF EXISTS acquisition_sample_file_pk CASCADE;
ALTER TABLE metabo.acquisition_sample_file ADD CONSTRAINT acquisition_sample_file_pk FOREIGN KEY (acquisition_sample_id)
REFERENCES metabo.acquisition_sample (acquisition_sample_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: acquisition_sample_file_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition_sample_file DROP CONSTRAINT IF EXISTS acquisition_sample_file_pk1 CASCADE;
ALTER TABLE metabo.acquisition_sample_file ADD CONSTRAINT acquisition_sample_file_pk1 FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.acquisition_file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.acquisition_file CASCADE;
CREATE TABLE metabo.acquisition_file (
	acquisition_id integer NOT NULL,
	file_id integer NOT NULL,
	CONSTRAINT acquisition_fk PRIMARY KEY (acquisition_id,file_id) DEFERRABLE INITIALLY IMMEDIATE
);
-- ddl-end --
COMMENT ON TABLE metabo.acquisition_file IS E'Analysis conditions';
-- ddl-end --
ALTER TABLE metabo.acquisition_file OWNER TO metabo;
-- ddl-end --

-- object: acquisition_file_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition_file DROP CONSTRAINT IF EXISTS acquisition_file_pk CASCADE;
ALTER TABLE metabo.acquisition_file ADD CONSTRAINT acquisition_file_pk FOREIGN KEY (acquisition_id)
REFERENCES metabo.acquisition (acquisition_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: acquisition_file_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition_file DROP CONSTRAINT IF EXISTS acquisition_file_pk1 CASCADE;
ALTER TABLE metabo.acquisition_file ADD CONSTRAINT acquisition_file_pk1 FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: acquisition_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition DROP CONSTRAINT IF EXISTS acquisition_pk CASCADE;
ALTER TABLE metabo.acquisition ADD CONSTRAINT acquisition_pk FOREIGN KEY (project_id)
REFERENCES metabo.project (project_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: file_pk3 | type: CONSTRAINT --
-- ALTER TABLE metabo.file DROP CONSTRAINT IF EXISTS file_pk3 CASCADE;
ALTER TABLE metabo.file ADD CONSTRAINT file_pk3 FOREIGN KEY (project_id)
REFERENCES metabo.project (project_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: sample_pk2 | type: CONSTRAINT --
-- ALTER TABLE metabo.sample DROP CONSTRAINT IF EXISTS sample_pk2 CASCADE;
ALTER TABLE metabo.sample ADD CONSTRAINT sample_pk2 FOREIGN KEY (sample_type_id)
REFERENCES metabo.sample_type (sample_type_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: sample_sample_parent_id_idx | type: INDEX --
-- DROP INDEX IF EXISTS metabo.sample_sample_parent_id_idx CASCADE;
CREATE INDEX sample_sample_parent_id_idx ON metabo.sample
USING btree
(
	sample_parent_id
);
-- ddl-end --

-- object: metabo.experimentation_type | type: TABLE --
-- DROP TABLE IF EXISTS metabo.experimentation_type CASCADE;
CREATE TABLE metabo.experimentation_type (
	experimentation_type_id serial NOT NULL,
	experimentation_type_name varchar NOT NULL,
	sortorder smallint,
	CONSTRAINT experimentation_type_pk PRIMARY KEY (experimentation_type_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.experimentation_type IS E'Types of experimentations';
-- ddl-end --
COMMENT ON COLUMN metabo.experimentation_type.sortorder IS E'Sort order';
-- ddl-end --
ALTER TABLE metabo.experimentation_type OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.experimentation_type (experimentation_type_name, sortorder) VALUES (E'In situ', E'1');
-- ddl-end --
INSERT INTO metabo.experimentation_type (experimentation_type_name, sortorder) VALUES (E'In vivo', E'2');
-- ddl-end --

-- object: experimentation_pk2 | type: CONSTRAINT --
-- ALTER TABLE metabo.experimentation DROP CONSTRAINT IF EXISTS experimentation_pk2 CASCADE;
ALTER TABLE metabo.experimentation ADD CONSTRAINT experimentation_pk2 FOREIGN KEY (experimentation_type_id)
REFERENCES metabo.experimentation_type (experimentation_type_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.matrice_type | type: TABLE --
-- DROP TABLE IF EXISTS metabo.matrice_type CASCADE;
CREATE TABLE metabo.matrice_type (
	matrice_type_id serial NOT NULL,
	matrice_type_name varchar NOT NULL,
	sortorder smallint,
	CONSTRAINT matrice_type_pk PRIMARY KEY (matrice_type_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.matrice_type IS E'Types of matrice';
-- ddl-end --
COMMENT ON COLUMN metabo.matrice_type.sortorder IS E'Sort order';
-- ddl-end --
ALTER TABLE metabo.matrice_type OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.matrice_type (sortorder) VALUES (E'2');
-- ddl-end --
INSERT INTO metabo.matrice_type (sortorder) VALUES (E'1');
-- ddl-end --

-- object: metabo.sampling_method | type: TABLE --
-- DROP TABLE IF EXISTS metabo.sampling_method CASCADE;
CREATE TABLE metabo.sampling_method (
	sampling_method_id serial NOT NULL,
	sampling_method_name varchar NOT NULL,
	sortorder smallint,
	CONSTRAINT sampling_method_pk PRIMARY KEY (sampling_method_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.sampling_method IS E'List of methods of exposition';
-- ddl-end --
COMMENT ON COLUMN metabo.sampling_method.sortorder IS E'Sort order';
-- ddl-end --
ALTER TABLE metabo.sampling_method OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.sampling_method (sampling_method_name, sortorder) VALUES (E'Encagement', E'1');
-- ddl-end --
INSERT INTO metabo.sampling_method (sampling_method_name, sortorder) VALUES (E'Petits canaux', E'2');
-- ddl-end --

-- object: sampling_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.sampling DROP CONSTRAINT IF EXISTS sampling_pk CASCADE;
ALTER TABLE metabo.sampling ADD CONSTRAINT sampling_pk FOREIGN KEY (sampling_method_id)
REFERENCES metabo.sampling_method (sampling_method_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.machine | type: TABLE --
-- DROP TABLE IF EXISTS metabo.machine CASCADE;
CREATE TABLE metabo.machine (
	machine_id serial NOT NULL,
	machine_name varchar NOT NULL,
	sortorder smallint,
	CONSTRAINT machine_pk PRIMARY KEY (machine_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.machine IS E'List of analysis machines';
-- ddl-end --
ALTER TABLE metabo.machine OWNER TO metabo;
-- ddl-end --

-- object: acquisition_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition DROP CONSTRAINT IF EXISTS acquisition_pk1 CASCADE;
ALTER TABLE metabo.acquisition ADD CONSTRAINT acquisition_pk1 FOREIGN KEY (machine_id)
REFERENCES metabo.machine (machine_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.software | type: TABLE --
-- DROP TABLE IF EXISTS metabo.software CASCADE;
CREATE TABLE metabo.software (
	software_id serial NOT NULL,
	software_name varchar NOT NULL,
	sortorder smallint,
	CONSTRAINT software_pk PRIMARY KEY (software_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.software IS E'Used piece of software for analysis';
-- ddl-end --
ALTER TABLE metabo.software OWNER TO metabo;
-- ddl-end --

-- object: metabo.extraction | type: TABLE --
-- DROP TABLE IF EXISTS metabo.extraction CASCADE;
CREATE TABLE metabo.extraction (
	extraction_id serial NOT NULL,
	extraction_date timestamp NOT NULL DEFAULT now(),
	sampling_id integer,
	extraction_method_id integer,
	project_id integer NOT NULL,
	CONSTRAINT extraction_pk PRIMARY KEY (extraction_id)
);
-- ddl-end --
COMMENT ON COLUMN metabo.extraction.extraction_date IS E'Date of extraction';
-- ddl-end --
ALTER TABLE metabo.extraction OWNER TO metabo;
-- ddl-end --

-- object: extraction_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.extraction DROP CONSTRAINT IF EXISTS extraction_pk1 CASCADE;
ALTER TABLE metabo.extraction ADD CONSTRAINT extraction_pk1 FOREIGN KEY (sampling_id)
REFERENCES metabo.sampling (sampling_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: sample_pk3 | type: CONSTRAINT --
-- ALTER TABLE metabo.sample DROP CONSTRAINT IF EXISTS sample_pk3 CASCADE;
ALTER TABLE metabo.sample ADD CONSTRAINT sample_pk3 FOREIGN KEY (extraction_id)
REFERENCES metabo.extraction (extraction_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.extraction_method | type: TABLE --
-- DROP TABLE IF EXISTS metabo.extraction_method CASCADE;
CREATE TABLE metabo.extraction_method (
	extraction_method_id serial NOT NULL,
	extraction_method_name varchar NOT NULL,
	sortorder smallint,
	CONSTRAINT extraction_method_pk PRIMARY KEY (extraction_method_id)
);
-- ddl-end --
ALTER TABLE metabo.extraction_method OWNER TO metabo;
-- ddl-end --

-- object: extraction_pk2 | type: CONSTRAINT --
-- ALTER TABLE metabo.extraction DROP CONSTRAINT IF EXISTS extraction_pk2 CASCADE;
ALTER TABLE metabo.extraction ADD CONSTRAINT extraction_pk2 FOREIGN KEY (extraction_method_id)
REFERENCES metabo.extraction_method (extraction_method_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.extraction_file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.extraction_file CASCADE;
CREATE TABLE metabo.extraction_file (
	extraction_id integer NOT NULL,
	file_id integer NOT NULL,
	CONSTRAINT extraction_fk PRIMARY KEY (extraction_id,file_id) DEFERRABLE INITIALLY IMMEDIATE
);
-- ddl-end --
ALTER TABLE metabo.extraction_file OWNER TO metabo;
-- ddl-end --

-- object: extraction_file_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.extraction_file DROP CONSTRAINT IF EXISTS extraction_file_pk CASCADE;
ALTER TABLE metabo.extraction_file ADD CONSTRAINT extraction_file_pk FOREIGN KEY (extraction_id)
REFERENCES metabo.extraction (extraction_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: extraction_file_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.extraction_file DROP CONSTRAINT IF EXISTS extraction_file_pk1 CASCADE;
ALTER TABLE metabo.extraction_file ADD CONSTRAINT extraction_file_pk1 FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: sample_pk4 | type: CONSTRAINT --
-- ALTER TABLE metabo.sample DROP CONSTRAINT IF EXISTS sample_pk4 CASCADE;
ALTER TABLE metabo.sample ADD CONSTRAINT sample_pk4 FOREIGN KEY (project_id)
REFERENCES metabo.project (project_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: sample_name_idx | type: INDEX --
-- DROP INDEX IF EXISTS metabo.sample_name_idx CASCADE;
CREATE INDEX sample_name_idx ON metabo.sample
USING btree
(
	sample_name
);
-- ddl-end --

-- object: gacl.sshkey | type: TABLE --
-- DROP TABLE IF EXISTS gacl.sshkey CASCADE;
CREATE TABLE gacl.sshkey (
	sshkey_id serial NOT NULL,
	sshkey_name varchar NOT NULL,
	sshkey_value varchar NOT NULL,
	acllogin_id integer,
	CONSTRAINT sshkey_pk PRIMARY KEY (sshkey_id)
);
-- ddl-end --
COMMENT ON TABLE gacl.sshkey IS E'Table of ssh keys';
-- ddl-end --
COMMENT ON COLUMN gacl.sshkey.sshkey_name IS E'Name of the sshkey';
-- ddl-end --
COMMENT ON COLUMN gacl.sshkey.sshkey_value IS E'String contents the ssh_key';
-- ddl-end --
ALTER TABLE gacl.sshkey OWNER TO metabo;
-- ddl-end --

-- object: sshkey_pk1 | type: CONSTRAINT --
-- ALTER TABLE gacl.sshkey DROP CONSTRAINT IF EXISTS sshkey_pk1 CASCADE;
ALTER TABLE gacl.sshkey ADD CONSTRAINT sshkey_pk1 FOREIGN KEY (acllogin_id)
REFERENCES gacl.acllogin (acllogin_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.transport_method | type: TABLE --
-- DROP TABLE IF EXISTS metabo.transport_method CASCADE;
CREATE TABLE metabo.transport_method (
	transport_method_id serial NOT NULL,
	transport_method_name varchar NOT NULL,
	sortorder smallint DEFAULT 1,
	CONSTRAINT transport_method_pk PRIMARY KEY (transport_method_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.transport_method IS E'List of methods of transport of the samples';
-- ddl-end --
ALTER TABLE metabo.transport_method OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.transport_method (transport_method_id, transport_method_name, sortorder) VALUES (DEFAULT, E'Carboglace', E'1');
-- ddl-end --
INSERT INTO metabo.transport_method (transport_method_id, transport_method_name, sortorder) VALUES (DEFAULT, E'Azote liquide', E'1');
-- ddl-end --
INSERT INTO metabo.transport_method (transport_method_id, transport_method_name, sortorder) VALUES (DEFAULT, E'Congélateur -80°', E'1');
-- ddl-end --

-- object: sample_pk5 | type: CONSTRAINT --
-- ALTER TABLE metabo.sample DROP CONSTRAINT IF EXISTS sample_pk5 CASCADE;
ALTER TABLE metabo.sample ADD CONSTRAINT sample_pk5 FOREIGN KEY (transport_method_id)
REFERENCES metabo.transport_method (transport_method_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.acquisition_type | type: TABLE --
-- DROP TABLE IF EXISTS metabo.acquisition_type CASCADE;
CREATE TABLE metabo.acquisition_type (
	acquisition_type_id serial NOT NULL,
	acquisition_type_name varchar NOT NULL,
	sortorder smallint DEFAULT 1,
	CONSTRAINT analysis_type_pk PRIMARY KEY (acquisition_type_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.acquisition_type IS E'Types of acquisitions';
-- ddl-end --
ALTER TABLE metabo.acquisition_type OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.acquisition_type (sortorder) VALUES (E'1');
-- ddl-end --

-- object: acquisition_pk2 | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition DROP CONSTRAINT IF EXISTS acquisition_pk2 CASCADE;
ALTER TABLE metabo.acquisition ADD CONSTRAINT acquisition_pk2 FOREIGN KEY (acquisition_type_id)
REFERENCES metabo.acquisition_type (acquisition_type_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: acllogin_login_idx | type: INDEX --
-- DROP INDEX IF EXISTS gacl.acllogin_login_idx CASCADE;
CREATE UNIQUE INDEX acllogin_login_idx ON gacl.acllogin
USING btree
(
	login
);
-- ddl-end --

-- object: metabo.request | type: TABLE --
-- DROP TABLE IF EXISTS metabo.request CASCADE;
CREATE TABLE metabo.request (
	request_id serial NOT NULL,
	create_date timestamp NOT NULL,
	last_exec timestamp,
	title character varying NOT NULL,
	body character varying NOT NULL,
	login character varying NOT NULL,
	datefields character varying,
	CONSTRAINT request_pk PRIMARY KEY (request_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.request IS E'Request table in database';
-- ddl-end --
COMMENT ON COLUMN metabo.request.create_date IS E'Date of create of the request';
-- ddl-end --
COMMENT ON COLUMN metabo.request.last_exec IS E'Date of the last execution';
-- ddl-end --
COMMENT ON COLUMN metabo.request.title IS E'Title of the request';
-- ddl-end --
COMMENT ON COLUMN metabo.request.body IS E'Body of the request. Don''t begin it by SELECT, which will be added automatically';
-- ddl-end --
COMMENT ON COLUMN metabo.request.login IS E'Login of the creator of the request';
-- ddl-end --
COMMENT ON COLUMN metabo.request.datefields IS E'List of the date fields used in the request, separated by a comma, for format it';
-- ddl-end --
ALTER TABLE metabo.request OWNER TO metabo;
-- ddl-end --

-- object: metabo.chromatography_method | type: TABLE --
-- DROP TABLE IF EXISTS metabo.chromatography_method CASCADE;
CREATE TABLE metabo.chromatography_method (
	chromatography_method_id serial NOT NULL,
	chromatography_method_name varchar NOT NULL,
	sortorder smallint DEFAULT 1,
	CONSTRAINT chromatography_method_pk PRIMARY KEY (chromatography_method_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.chromatography_method IS E'List of methods of chromatography';
-- ddl-end --
ALTER TABLE metabo.chromatography_method OWNER TO metabo;
-- ddl-end --

-- object: metabo.acquisition_method | type: TABLE --
-- DROP TABLE IF EXISTS metabo.acquisition_method CASCADE;
CREATE TABLE metabo.acquisition_method (
	acquisition_method_id serial NOT NULL,
	acquisition_method_name varchar NOT NULL,
	sortorder smallint DEFAULT 1,
	CONSTRAINT acquisition_method_pk PRIMARY KEY (acquisition_method_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.acquisition_method IS E'List of methods of acquisition';
-- ddl-end --
ALTER TABLE metabo.acquisition_method OWNER TO metabo;
-- ddl-end --

-- object: acquisition_pk3 | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition DROP CONSTRAINT IF EXISTS acquisition_pk3 CASCADE;
ALTER TABLE metabo.acquisition ADD CONSTRAINT acquisition_pk3 FOREIGN KEY (chromatography_method_id)
REFERENCES metabo.chromatography_method (chromatography_method_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: acquisition_pk4 | type: CONSTRAINT --
-- ALTER TABLE metabo.acquisition DROP CONSTRAINT IF EXISTS acquisition_pk4 CASCADE;
ALTER TABLE metabo.acquisition ADD CONSTRAINT acquisition_pk4 FOREIGN KEY (acquisition_method_id)
REFERENCES metabo.acquisition_method (acquisition_method_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.sftpserver | type: TABLE --
-- DROP TABLE IF EXISTS metabo.sftpserver CASCADE;
CREATE TABLE metabo.sftpserver (
	sftpserver_id serial NOT NULL,
	acllogin_id integer NOT NULL,
	sftpserver_name varchar,
	address varchar NOT NULL,
	port smallint DEFAULT 22,
	login varchar NOT NULL,
	password varchar NOT NULL,
	CONSTRAINT sftpserver_pk PRIMARY KEY (sftpserver_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.sftpserver IS E'List of servers sftp attached to a login';
-- ddl-end --
COMMENT ON COLUMN metabo.sftpserver.sftpserver_name IS E'Name or description of the server';
-- ddl-end --
COMMENT ON COLUMN metabo.sftpserver.address IS E'Address of the server (ex: server.society.com)';
-- ddl-end --
COMMENT ON COLUMN metabo.sftpserver.port IS E'Number of the connection port';
-- ddl-end --
COMMENT ON COLUMN metabo.sftpserver.login IS E'Login used to connect to the server';
-- ddl-end --
COMMENT ON COLUMN metabo.sftpserver.password IS E'Password used to connect to the server';
-- ddl-end --
ALTER TABLE metabo.sftpserver OWNER TO metabo;
-- ddl-end --

-- object: sftpserver_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.sftpserver DROP CONSTRAINT IF EXISTS sftpserver_pk1 CASCADE;
ALTER TABLE metabo.sftpserver ADD CONSTRAINT sftpserver_pk1 FOREIGN KEY (acllogin_id)
REFERENCES gacl.acllogin (acllogin_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.sample_nature | type: TABLE --
-- DROP TABLE IF EXISTS metabo.sample_nature CASCADE;
CREATE TABLE metabo.sample_nature (
	sample_nature_id integer NOT NULL,
	sample_nature_name varchar NOT NULL,
	sortorder smallint,
	CONSTRAINT sample_nature_pk PRIMARY KEY (sample_nature_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.sample_nature IS E'List of natures of samples';
-- ddl-end --
ALTER TABLE metabo.sample_nature OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.sample_nature (sample_nature_id, sample_nature_name, sortorder) VALUES (E'1', E'Sample', E'1');
-- ddl-end --
INSERT INTO metabo.sample_nature (sample_nature_id, sample_nature_name, sortorder) VALUES (E'2', E'Quality control', E'2');
-- ddl-end --
INSERT INTO metabo.sample_nature (sample_nature_id, sample_nature_name, sortorder) VALUES (E'3', E'Blank process', E'3');
-- ddl-end --
INSERT INTO metabo.sample_nature (sample_nature_id, sample_nature_name, sortorder) VALUES (E'4', E'Blank injection', E'4');
-- ddl-end --

-- object: metabo.storage_location | type: TABLE --
-- DROP TABLE IF EXISTS metabo.storage_location CASCADE;
CREATE TABLE metabo.storage_location (
	storage_location_id serial NOT NULL,
	storage_location_name varchar NOT NULL,
	sortorder smallint DEFAULT 1,
	CONSTRAINT storage_location_pk PRIMARY KEY (storage_location_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.storage_location IS E'List of locations of storage of samples';
-- ddl-end --
ALTER TABLE metabo.storage_location OWNER TO metabo;
-- ddl-end --

-- object: sample_pk6 | type: CONSTRAINT --
-- ALTER TABLE metabo.sample DROP CONSTRAINT IF EXISTS sample_pk6 CASCADE;
ALTER TABLE metabo.sample ADD CONSTRAINT sample_pk6 FOREIGN KEY (storage_location_id)
REFERENCES metabo.storage_location (storage_location_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.treatment | type: TABLE --
-- DROP TABLE IF EXISTS metabo.treatment CASCADE;
CREATE TABLE metabo.treatment (
	treatment_id serial NOT NULL,
	treatment_date timestamp NOT NULL DEFAULT now(),
	software_id integer,
	project_id integer NOT NULL,
	CONSTRAINT treatment_pk PRIMARY KEY (treatment_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.treatment IS E'Treatments of results with software';
-- ddl-end --
ALTER TABLE metabo.treatment OWNER TO metabo;
-- ddl-end --

-- object: treatment_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.treatment DROP CONSTRAINT IF EXISTS treatment_pk1 CASCADE;
ALTER TABLE metabo.treatment ADD CONSTRAINT treatment_pk1 FOREIGN KEY (software_id)
REFERENCES metabo.software (software_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.treatment_file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.treatment_file CASCADE;
CREATE TABLE metabo.treatment_file (
	treatment_id integer NOT NULL,
	file_id integer NOT NULL,
	CONSTRAINT treatment_fk PRIMARY KEY (treatment_id,file_id) DEFERRABLE INITIALLY IMMEDIATE
);
-- ddl-end --
ALTER TABLE metabo.treatment_file OWNER TO metabo;
-- ddl-end --

-- object: treatment_file_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.treatment_file DROP CONSTRAINT IF EXISTS treatment_file_pk CASCADE;
ALTER TABLE metabo.treatment_file ADD CONSTRAINT treatment_file_pk FOREIGN KEY (treatment_id)
REFERENCES metabo.treatment (treatment_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: treatment_file_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.treatment_file DROP CONSTRAINT IF EXISTS treatment_file_pk1 CASCADE;
ALTER TABLE metabo.treatment_file ADD CONSTRAINT treatment_file_pk1 FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.condition | type: TABLE --
-- DROP TABLE IF EXISTS metabo.condition CASCADE;
CREATE TABLE metabo.condition (
	condition_id serial NOT NULL,
	condition_code varchar NOT NULL,
	condition_description varchar,
	experimentation_id integer NOT NULL,
	matrice_type_id integer,
	station_id integer,
	CONSTRAINT condition_pk_1 PRIMARY KEY (condition_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.condition IS E'Table of experimental conditions';
-- ddl-end --
COMMENT ON COLUMN metabo.condition.condition_code IS E'Code of the condition';
-- ddl-end --
COMMENT ON COLUMN metabo.condition.condition_description IS E'Description of the condition';
-- ddl-end --
ALTER TABLE metabo.condition OWNER TO metabo;
-- ddl-end --

-- object: condition_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.condition DROP CONSTRAINT IF EXISTS condition_pk CASCADE;
ALTER TABLE metabo.condition ADD CONSTRAINT condition_pk FOREIGN KEY (experimentation_id)
REFERENCES metabo.experimentation (experimentation_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: condition_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.condition DROP CONSTRAINT IF EXISTS condition_pk1 CASCADE;
ALTER TABLE metabo.condition ADD CONSTRAINT condition_pk1 FOREIGN KEY (matrice_type_id)
REFERENCES metabo.matrice_type (matrice_type_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: condition_pk2 | type: CONSTRAINT --
-- ALTER TABLE metabo.condition DROP CONSTRAINT IF EXISTS condition_pk2 CASCADE;
ALTER TABLE metabo.condition ADD CONSTRAINT condition_pk2 FOREIGN KEY (station_id)
REFERENCES metabo.station (station_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: sampling_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.sampling DROP CONSTRAINT IF EXISTS sampling_pk1 CASCADE;
ALTER TABLE metabo.sampling ADD CONSTRAINT sampling_pk1 FOREIGN KEY (condition_id)
REFERENCES metabo.condition (condition_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.condition_file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.condition_file CASCADE;
CREATE TABLE metabo.condition_file (
	condition_id integer NOT NULL,
	file_id integer NOT NULL,
	CONSTRAINT condition_fk PRIMARY KEY (condition_id,file_id) DEFERRABLE INITIALLY IMMEDIATE
);
-- ddl-end --
ALTER TABLE metabo.condition_file OWNER TO metabo;
-- ddl-end --

-- object: condition_file_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.condition_file DROP CONSTRAINT IF EXISTS condition_file_pk CASCADE;
ALTER TABLE metabo.condition_file ADD CONSTRAINT condition_file_pk FOREIGN KEY (condition_id)
REFERENCES metabo.condition (condition_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: condition_file_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.condition_file DROP CONSTRAINT IF EXISTS condition_file_pk1 CASCADE;
ALTER TABLE metabo.condition_file ADD CONSTRAINT condition_file_pk1 FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: station_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.station DROP CONSTRAINT IF EXISTS station_pk1 CASCADE;
ALTER TABLE metabo.station ADD CONSTRAINT station_pk1 FOREIGN KEY (project_id)
REFERENCES metabo.project (project_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: treatment_pk2 | type: CONSTRAINT --
-- ALTER TABLE metabo.treatment DROP CONSTRAINT IF EXISTS treatment_pk2 CASCADE;
ALTER TABLE metabo.treatment ADD CONSTRAINT treatment_pk2 FOREIGN KEY (project_id)
REFERENCES metabo.project (project_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.treatment_as | type: TABLE --
-- DROP TABLE IF EXISTS metabo.treatment_as CASCADE;
CREATE TABLE metabo.treatment_as (
	treatment_as_id serial NOT NULL,
	treatment_id integer NOT NULL,
	acquisition_sample_id integer NOT NULL,
	CONSTRAINT treatment_as_pk PRIMARY KEY (treatment_as_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.treatment_as IS E'List of acquisition_samples attached to a treatment';
-- ddl-end --
ALTER TABLE metabo.treatment_as OWNER TO metabo;
-- ddl-end --

-- object: treatment_as_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.treatment_as DROP CONSTRAINT IF EXISTS treatment_as_pk1 CASCADE;
ALTER TABLE metabo.treatment_as ADD CONSTRAINT treatment_as_pk1 FOREIGN KEY (treatment_id)
REFERENCES metabo.treatment (treatment_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: treatment_as_pk2 | type: CONSTRAINT --
-- ALTER TABLE metabo.treatment_as DROP CONSTRAINT IF EXISTS treatment_as_pk2 CASCADE;
ALTER TABLE metabo.treatment_as ADD CONSTRAINT treatment_as_pk2 FOREIGN KEY (acquisition_sample_id)
REFERENCES metabo.acquisition_sample (acquisition_sample_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.treatment_as_file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.treatment_as_file CASCADE;
CREATE TABLE metabo.treatment_as_file (
	treatment_as_id integer NOT NULL,
	file_id integer NOT NULL,
	CONSTRAINT treatment_as_fk PRIMARY KEY (treatment_as_id,file_id) DEFERRABLE INITIALLY IMMEDIATE
);
-- ddl-end --
ALTER TABLE metabo.treatment_as_file OWNER TO metabo;
-- ddl-end --

-- object: treatment_as_file_pk | type: CONSTRAINT --
-- ALTER TABLE metabo.treatment_as_file DROP CONSTRAINT IF EXISTS treatment_as_file_pk CASCADE;
ALTER TABLE metabo.treatment_as_file ADD CONSTRAINT treatment_as_file_pk FOREIGN KEY (treatment_as_id)
REFERENCES metabo.treatment_as (treatment_as_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: treatment_as_file_pk1 | type: CONSTRAINT --
-- ALTER TABLE metabo.treatment_as_file DROP CONSTRAINT IF EXISTS treatment_as_file_pk1 CASCADE;
ALTER TABLE metabo.treatment_as_file ADD CONSTRAINT treatment_as_file_pk1 FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.storage_type | type: TABLE --
-- DROP TABLE IF EXISTS metabo.storage_type CASCADE;
CREATE TABLE metabo.storage_type (
	storage_type_id serial NOT NULL,
	storage_type_name varchar NOT NULL,
	sortorder smallint,
	CONSTRAINT storage_type_pk PRIMARY KEY (storage_type_id)
);
-- ddl-end --
COMMENT ON TABLE metabo.storage_type IS E'Type of storage of the sample';
-- ddl-end --
ALTER TABLE metabo.storage_type OWNER TO metabo;
-- ddl-end --

INSERT INTO metabo.storage_type (storage_type_name, sortorder) VALUES (E'Lame de verre lyophilisée', E'1');
-- ddl-end --
INSERT INTO metabo.storage_type (storage_type_name, sortorder) VALUES (E'Lame de verre non lyophilisée', E'2');
-- ddl-end --
INSERT INTO metabo.storage_type (storage_type_name, sortorder) VALUES (E'Matrice brute lyophilisée', E'3');
-- ddl-end --
INSERT INTO metabo.storage_type (storage_type_name, sortorder) VALUES (E'Matrice brute non lyophilisée', E'4');
-- ddl-end --
INSERT INTO metabo.storage_type (storage_type_name, sortorder) VALUES (E'Liquide', E'5');
-- ddl-end --

-- object: sample | type: CONSTRAINT --
-- ALTER TABLE metabo.sample DROP CONSTRAINT IF EXISTS sample CASCADE;
ALTER TABLE metabo.sample ADD CONSTRAINT sample FOREIGN KEY (storage_type_id)
REFERENCES metabo.storage_type (storage_type_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE DEFERRABLE INITIALLY IMMEDIATE;
-- ddl-end --

-- object: metabo.get_condition_id | type: FUNCTION --
-- DROP FUNCTION IF EXISTS metabo.get_condition_id(integer) CASCADE;
CREATE OR REPLACE FUNCTION metabo.get_condition_id (sample_id_from integer)
	RETURNS integer
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	PARALLEL UNSAFE
	COST 1
	AS $$
declare resultat integer;
begin
with recursive sampleparent as
(
select sample_id, sample_parent_id, sampling_id from metabo.sample where sample_id = sample_id_from
union 
select s.sample_id, s.sample_parent_id, s.sampling_id from metabo.sample s join sampleparent sp on (s.sample_id = sp.sample_parent_id)
)
select condition_id into resultat
from sampleparent sp
join metabo.sampling s on (sp.sampling_id = s.sampling_id)
join metabo.condition using (condition_id)
;
return resultat;
end;
$$;
-- ddl-end --
ALTER FUNCTION metabo.get_condition_id(integer) OWNER TO metabo;
-- ddl-end --
COMMENT ON FUNCTION metabo.get_condition_id(integer) IS E'Get the condition_id with recursive function from sample_id';
-- ddl-end --

-- object: metabo.get_sampling_id | type: FUNCTION --
-- DROP FUNCTION IF EXISTS metabo.get_sampling_id(integer) CASCADE;
CREATE OR REPLACE FUNCTION metabo.get_sampling_id (sample_id_from integer)
	RETURNS integer
	LANGUAGE plpgsql
	VOLATILE 
	CALLED ON NULL INPUT
	SECURITY INVOKER
	PARALLEL UNSAFE
	COST 1
	AS $$
declare resultat integer;
begin
with recursive sampleparent as
(
select sample_id, sample_parent_id, sampling_id from metabo.sample where sample_id = sample_id_from
union 
select s.sample_id, s.sample_parent_id, s.sampling_id from metabo.sample s join sampleparent sp on (s.sample_id = sp.sample_parent_id)
)
select sampling_id into resultat
from sampleparent sp
where sampling_id is not null
;
return resultat;
end;
$$;
-- ddl-end --
ALTER FUNCTION metabo.get_sampling_id(integer) OWNER TO metabo;
-- ddl-end --
COMMENT ON FUNCTION metabo.get_sampling_id(integer) IS E'Get the sampling_id from recursive';
-- ddl-end --

-- object: extraction_pk3 | type: CONSTRAINT --
-- ALTER TABLE metabo.extraction DROP CONSTRAINT IF EXISTS extraction_pk3 CASCADE;
ALTER TABLE metabo.extraction ADD CONSTRAINT extraction_pk3 FOREIGN KEY (project_id)
REFERENCES metabo.project (project_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: metabo.project_file | type: TABLE --
-- DROP TABLE IF EXISTS metabo.project_file CASCADE;
CREATE TABLE metabo.project_file (
	project_id integer NOT NULL,
	file_id integer NOT NULL,
	CONSTRAINT project_file_pk PRIMARY KEY (project_id,file_id)
);
-- ddl-end --
ALTER TABLE metabo.project_file OWNER TO metabo;
-- ddl-end --

-- object: project_fk | type: CONSTRAINT --
-- ALTER TABLE metabo.project_file DROP CONSTRAINT IF EXISTS project_fk CASCADE;
ALTER TABLE metabo.project_file ADD CONSTRAINT project_fk FOREIGN KEY (project_id)
REFERENCES metabo.project (project_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: file_fk | type: CONSTRAINT --
-- ALTER TABLE metabo.project_file DROP CONSTRAINT IF EXISTS file_fk CASCADE;
ALTER TABLE metabo.project_file ADD CONSTRAINT file_fk FOREIGN KEY (file_id)
REFERENCES metabo.file (file_id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: aclaco_aclacl_fk | type: CONSTRAINT --
-- ALTER TABLE gacl.aclacl DROP CONSTRAINT IF EXISTS aclaco_aclacl_fk CASCADE;
ALTER TABLE gacl.aclacl ADD CONSTRAINT aclaco_aclacl_fk FOREIGN KEY (aclaco_id)
REFERENCES gacl.aclaco (aclaco_id) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: aclgroup_aclacl_fk | type: CONSTRAINT --
-- ALTER TABLE gacl.aclacl DROP CONSTRAINT IF EXISTS aclgroup_aclacl_fk CASCADE;
ALTER TABLE gacl.aclacl ADD CONSTRAINT aclgroup_aclacl_fk FOREIGN KEY (aclgroup_id)
REFERENCES gacl.aclgroup (aclgroup_id) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: aclappli_aclaco_fk | type: CONSTRAINT --
-- ALTER TABLE gacl.aclaco DROP CONSTRAINT IF EXISTS aclappli_aclaco_fk CASCADE;
ALTER TABLE gacl.aclaco ADD CONSTRAINT aclappli_aclaco_fk FOREIGN KEY (aclappli_id)
REFERENCES gacl.aclappli (aclappli_id) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: aclgroup_aclgroup_fk | type: CONSTRAINT --
-- ALTER TABLE gacl.aclgroup DROP CONSTRAINT IF EXISTS aclgroup_aclgroup_fk CASCADE;
ALTER TABLE gacl.aclgroup ADD CONSTRAINT aclgroup_aclgroup_fk FOREIGN KEY (aclgroup_id_parent)
REFERENCES gacl.aclgroup (aclgroup_id) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: aclgroup_acllogingroup_fk | type: CONSTRAINT --
-- ALTER TABLE gacl.acllogingroup DROP CONSTRAINT IF EXISTS aclgroup_acllogingroup_fk CASCADE;
ALTER TABLE gacl.acllogingroup ADD CONSTRAINT aclgroup_acllogingroup_fk FOREIGN KEY (aclgroup_id)
REFERENCES gacl.aclgroup (aclgroup_id) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: acllogin_acllogingroup_fk | type: CONSTRAINT --
-- ALTER TABLE gacl.acllogingroup DROP CONSTRAINT IF EXISTS acllogin_acllogingroup_fk CASCADE;
ALTER TABLE gacl.acllogingroup ADD CONSTRAINT acllogin_acllogingroup_fk FOREIGN KEY (acllogin_id)
REFERENCES gacl.acllogin (acllogin_id) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: logingestion_passwordlost_fk | type: CONSTRAINT --
-- ALTER TABLE gacl.passwordlost DROP CONSTRAINT IF EXISTS logingestion_passwordlost_fk CASCADE;
ALTER TABLE gacl.passwordlost ADD CONSTRAINT logingestion_passwordlost_fk FOREIGN KEY (id)
REFERENCES gacl.logingestion (id) MATCH SIMPLE
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_sample_sample_parent_id | type: CONSTRAINT --
-- ALTER TABLE metabo.sample DROP CONSTRAINT IF EXISTS fk_sample_sample_parent_id CASCADE;
ALTER TABLE metabo.sample ADD CONSTRAINT fk_sample_sample_parent_id FOREIGN KEY (sample_parent_id)
REFERENCES metabo.sample (sample_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: sample_nature_fk | type: CONSTRAINT --
-- ALTER TABLE metabo.sample DROP CONSTRAINT IF EXISTS sample_nature_fk CASCADE;
ALTER TABLE metabo.sample ADD CONSTRAINT sample_nature_fk FOREIGN KEY (sample_nature_id)
REFERENCES metabo.sample_nature (sample_nature_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


