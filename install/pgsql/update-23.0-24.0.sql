set search_path = metabo,gacl,public;
create unique index if not exists dbparamname_idx on dbparam (dbparam_name);
insert into dbparam (dbparam_name, dbparam_value, dbparam_description, dbparam_description_en)
values (
'APPLI_code', 
'code_of_instance',
'Code de l''instance, pour les exportations',
'Code of the instance, to export data'
)
on conflict do nothing;
insert into dbparam (dbparam_name, dbparam_value, dbparam_description, dbparam_description_en)
values (
'fileLinkDuration', 
'7',
'Durée de vie des liens de téléchargement des fichiers, en jours',
'Lifetime of file download links, in days'
);
alter table gacl.acllogin add column email varchar;
alter table gacl.logingestion add column if not exists is_expired boolean;
alter table gacl.logingestion add column if not exists nbattempts integer;
alter table gacl.logingestion add column if not exists lastattempt datetime;

update gacl.aclgroup set groupe = 'manage' where groupe = 'gestion';
update gacl.aclaco set aco = 'manage' where aco = 'gestion';
insert into dbversion(dbversion_number, dbversion_date )
values ('24.0','2024-11-01');
