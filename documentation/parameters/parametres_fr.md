# Configuration du logiciel

## Paramètres d'administration

Ces paramètres doivent être renseignés par l'administrateur système qui aura installé l'application. Ils contiennent notamment les chemins utiisés pour déposer ou stocker les fichiers.

*Administration > Paramètres de l'application*

{: .datatable}

| Paramètre | Contenu |
|--|--|
| APPLI_title | Titre de l'application, affiché à côté de l'icône |
| authorized_keys | Fichier utilisé pour stocker les clés publiques des utilisateurs qui déposent des fichiers. En principe, /home/metabo/.ssh/authorized_keys |
| batch_folder | dossier utilisé pour le téléchargement de fichiers par l'intermédiaire du protocole sftp (obsolète) |
| download_path | Dossier où sont déposés les fichiers lors de leur dépôt via SFTP (obsolète) |
| collec_certificate | chemin et nom du fichier contenant le certificat de l'application Collec-Science, utilisée pour envoyer les échantillons |
| collec_login | login de connexion à l'application Collec-Science |
| collec_sample_address | URL permettant d'envoyer les demandes d'écriture d'échantillons vers Collec-Science. URL par défaut : https://instance.collec-science.net/index.php?module=apiv1sampleWrite |
| collec_token | jeton d'identification vers Collec-Science |
| download_path | Dossier où sont déposés les fichiers lors de leur dépôt via SFTP (obsolète) |
| external_path_default | Chemin par défaut où sont déposés les fichiers associés avec les échantillons |
| last_folder_used | Dossier courant utilisé pour stocker les fichiers. Quand le nombre de fichiers dans ce dossier dépasse une certaine valeur, un nouveau dossier est créé |
| fileLinkDuration | Durée de vie des liens de téléchargement des fichiers |
| mapDefaultLat | Latitude par défaut de centrage des cartes |
| mapDefaultLong | Longitude par défaut de centrage des cartes |
| mapDefaultZoom | Facteur de zoom par défaut dans les cartes (OpenStreetMap) |
| mapMaxZoom | Grossissement maximum de la carte |
| mapMinZoom | Grossissement minimum de la carte |
| real_storage_path | Chemin d'accès au stockage des fichiers chargés directement dans l'application |

{.table .table-bordered .table-hover}

## Paramètres applicatifs

### Paramètres pré-renseignés

Ces paramètres sont pré-renseignés à l'initialisation de l'application :

- types d'expérimentation
- types d'échantillonnage
- méthodes d'échantillonnage
- types d'échantillons. Pour ce paramètre, il est conseillé de ne pas modifier les types déjà positionnés, les codes étant utilisés par l'application (création des aliquots, par exemple).

### Paramètres techniques à renseigner

Ces tables doivent être renseignées avec les valeurs utilisées pendant les expérimentations ou les analyses :

- méthodes d'extraction
- machines d'analyse
- méthodes d'analyse
- logiciels utilisés lors des analyses
- lieux de stockage des échantillons (frigos, etc.)
- moyens de transport des échantillons
- types de matrices (liées aux conditions d'expérimentation)
- types d'acquisistion
- méthodes de chromatographie
- méthodes d'acquisition
- logiciels de retraitement utilisés

### Types de fichiers

Les fichiers doivent être classés selon leur type. Ces types sont configurables, mais il faut indiquer à quelle nature d'objets ils se rapportent :

- expérimentation
- condition
- échantillonnage
- extraction
- échantillon
- opération d'acquisition
- résultat par aliquot après acquisition
- opération de retraitement des résultats
- résultat par aliquot des retraitements

À noter qu'un même type de fichier peut être utilisé pour plusieurs objets.

Si aucun type de fichiers n'est déterminé pour un objet, il ne sera pas possible d'ajouter des fichiers à celui-ci.

### Extensions - types Mime

Les types MIME permettent d'indiquer aux navigateurs ce qu'ils doivent faire des fichiers qu'ils reçoivent. Ils sont associés aux extensions des fichiers : par exemple, un fichier *csv* aura un type mime *text/csv*.

Si certains types manquaient, il est toujours possible de les rajouter.
