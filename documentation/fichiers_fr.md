# Traitement des fichiers associés

L'application Metabo permet d'associer des fichiers à la plupart des objets manipulés : _projet_, _expérimentation_, _échantillonnage_, _acquisition_, _traitement_, _condition_ principalement.

Les fichiers ne sont pas stockés directement dans la base de données pour des questions de volumétrie. L'application permet soit de :

*   charger un fichier directement depuis son ordinateur : le fichier va être stocké dans une arborescence accessible depuis le serveur web ;
*   associer un fichier déjà présent dans une arborescence : c'est la solution à privilégier pour les fichiers volumineux (dépassant quelques Mo) issus des machines d'analyse.

Pour que les fichiers puissent être manipulés, il faut paramétrer les emplacements de stockage utilisés.

D'autre part, il est possible de générer une liste de liens qui permettra à un correspondant de télécharger directement des fichiers, sans qu'il lui soit nécessaire de se connecter dans l'application.

## Associer des fichiers à un objet de la base de données

Dans le détail des objets manipulés (expérimentation, etc.), vous retrouverez un onglet _Fichiers_. En cliquant sur le lien _Importer un ou plusieurs fichiers_, vous affichez deux formulaires de saisie.  
Celui de gauche vous permet de télécharger des fichiers depuis votre ordinateur : les fichiers seront stockés dans une arborescence dédiée dans le serveur. Il suffit alors de sélectionner les fichiers à importer pour que ceux-ci soient téléchargés.

Attention : seuls certains types de fichiers sont autorisés au téléchargement. Si vous devez ajouter des fichiers dont le type n'existe pas, vous devrez auparavant le décrire depuis cette [liste](mimetypeList).

Celui de droite va afficher la liste des fichiers qui existent déjà dans l'arborescence bureautique. Vous pouvez naviguer dans les sous-dossiers (s'ils existent). En cliquant sur un fichier, celui-ci sera alors automatiquement associé à l'objet que vous manipulez.

## Générer des liens de téléchargement

Depuis le détail du projet, dans l'onglet *Tous fichiers associés*, sélectionnez les fichiers pour lesquels vous souhaitez générer un lien. Sélectionnez ensuite, dans le formulaire situé sous la liste, soit :

- la génération de liens de téléchargement "simples"
- la génération des liens associés avec un code de contrôle et comprenant le nom du fichier

La liste est fournie sous la forme d'un fichier CSV.

le second choix est plus destiné à des systèmes de traitement automatiques, qui vont vérifier l'intégrité du fichier chargé.

Attention : les liens de téléchargement ne sont valables que 7 jours par défaut. Si vous souhaitez modifier leur durée de vie, vous devrez modifier les paramètres de l'application (menu *Administration > Paramètres de l'application*) et mettre à jour la variable *fileLinkDuration*.

## Configuration

### Configuration du stockage de fichiers téléchargés depuis l'ordinateur de l'utilisateur

Depuis le menu _Administration > Paramètres de l'application_, renseignez la variable **real\_storage\_path**. Il s'agit du chemin d'accès à l'espace de stockage, _tel qu'il est vu par le serveur web_ : le dossier indiqué doit être accessible en lecture/écriture au serveur web Apache. Ce paramétrage doit être réalisé en collaboration avec l'administrateur du serveur Linux.

À noter que le chemin indiqué peut pointer vers un autre serveur (montage externe).

### Configuration de l'accès aux fichiers déjà existants

Dans ce cas de figure, l'administrateur du serveur Linux doit impérativement mettre en place un montage vers l'arborescence bureautique où sont entreposés les fichiers.

Le paramétrage dans l'application va être réalisé de deux manières différentes.

D'abord, il est nécessaire d'indiquer la _racine_ qui pointe vers l'arborescence bureautique (le point de montage). Cette information est à indiquer dans les paramètres de l'application (menu _Administration > Paramètres de l'application_), en renseignant la variable **external\_path\_default**.

Ensuite, il est possible d'indiquer un chemin particulier pour chaque projet (un sous-dossier du point de montage précédent) : cela permet de visualiser directement tous les fichiers générés dans le cadre d'un projet. Pour cela, depuis le menu _Paramètres > Liste des projets_, sélectionnez le projet à configurer, puis renseignez le **Chemin de stockage des fichiers externes**. Ainsi, seuls les fichiers présents dans le sous-dossier considéré pourront être rattachés aux objets manipulés dans le projet.

## Fonctionnement interne de l'application

Tous les fichiers rattachés sont décrits dans la table _file_.  
S'il s'agit d'un fichier téléchargé depuis l'ordinateur de l'utilisateur, les informations suivantes sont renseignées :

*   _storage\_type_ : **I** (pour interne)
*   _storage\_path_ : nom du fichier tel qu'il est enregistré dans le serveur. Le nom est généré automatiquement par l'application, et est précédé par le nom du sous-dossier dans lequel le fichier est déposé, ce sous-dossier étant également généré automatiquement. Le stockage est conçu pour supporter plusieurs centaines de milliers de fichiers, le programme créant automatiquement des dossiers quand les précédents sont "pleins". Techniquement, le nom du dernier dossier utilisé est stocké dans les paramètres de l'application, dans la variable _last\_folder\_used_.

Pour les fichiers déjà présents dans l'arborescence bureautique, les informations suivantes sont enregistrées :

*   _storage\_type_ : **E** (pour externe)
*   _storage\_path_ : le chemin relatif par rapport soit au _chemin de stockage des fichiers externes_ défini pour le projet, soit par rapport à la racine du stockage (_external\_path\_default_).