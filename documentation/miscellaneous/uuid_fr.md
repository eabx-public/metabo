# Qu'est-ce que l'UUID ?

L'UUID (*Universal Unique Identifier*) est un identifiant généré avec divers algorithmes, très souvent cryptographiques. Le procédé de génération garantit que cet identifiant est unique au niveau mondial.

## Pourquoi l'utilise-t-on ?

Si, dans la base de données, chaque "objet" est identifié par un numéro unique basé sur un compteur interne, l'échange d'informations avec d'autres bases de données nécessite d'être sûr que l'on parle bien de la même chose.

Pour cela, divers mécanismes peuvent être employés (nom de l'objet, sa nature, etc.), mais pour éviter les risques de confusion, on essaie de s'appuyer sur des identifiants uniques. 

Il en existe de plusieurs sortes. Les plus courants sont des identifiants fournis par des organismes internationaux, souvent très lisibles, mais qui impliquent de s'abonner ou de gérer des objets compatibles.

Le second type d'identifiants sont ceux générés par chaque producteur, mais qui impliquent l'utilisation d'algorithmes connus pour ne fournir que des numéros uniques. Ceux-ci sont souvent peu lisibles, très longs et basés principalement sur des algorithmes de chiffrement. L'UUID est un de ces types d'identifiants.

## Quel usage dans Metabo ?

Tous les objets manipulés par l'application disposent d'un UUID qui est soit généré automatiquement, soit peut être fourni par une autre application qui le manipule. Les échanges d'informations sont alors facilités, et il est beaucoup plus facile d'être sûr de savoir de quoi l'on parle.

L'application Collec-Science, qui s'interface avec Metabo, gère ce type d'identifiants.

## Peut-on importer des UUID dans Metabo ?

Il est possible de modifier manuellement les UUID dans Metabo, pour les faire correspondre aux valeurs fournies par d'autres applications. 

Dans les procédures d'importation, le champ UUID peut également être renseigné.
