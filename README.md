# METABO

Metabo est une application destinée à gérer les différents échantillons manipulés lors des expérimentations en métabolomique.

Le logiciel a été développé en PHP avec CodeIgniter, et s'appuie sur une base de données Postgresql.

Copyright © Éric Quinton, pour INRAE - EABX